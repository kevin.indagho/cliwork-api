<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Api_cita extends REST_Controller {
	function __construct(){
        parent::__construct();
		$headers = $this->input->request_headers();
		$dbConfig = json_decode($this->encryption->decrypt($headers['dbconfig']));
		$this->load->database(db_config($dbConfig));
		
		$this->load->model('Citas_model','citas');
		$this->load->model('Paciente_model','paciente');
		$this->load->model('Treatplan_model','tp');
	}
	
	public function calendario_get(){
		$dateStart = $this->get('dateStart');
		$dateEnd = $this->get('dateEnd');
		$status = $this->get('status');
		$inBox = $this->get('inbox');
		$provider = $this->get('provider');
		$data['data'] = $this->citas->calendario_lista($dateStart,$dateEnd,$status,$inBox,$provider);
		print_r(json_encode($data));
	}
	
	public function calendariobox_get(){
		$data['data'] = $this->citas->calendario_lista_box();
		print_r(json_encode($data));
	}
		
	#region CRUD
		public function citas_get(){
			$id = $this->get('id');
			$data['data'] = $this->citas->read($id);
			$this->response($data);
		}
		
		public function citas_put(){
			$procupd = $this->put('procupd');
			$AptId = $this->put('id');
			$providerId = $this->put('doctor');
			$confirmed = $this->put('confirmed');
			$allday = $this->put('alldaytime');
			$reason = $this->put('reason');
			if($reason != ""){ $reason = implode(",",$reason); }
			
			$editAppt = array(
				'ProvId' => $providerId,
				'IsNewPatient' => $this->put('new'),
				'inBox' => $this->put('box'),
				'Needs' => $this->put('needs'),
				'inClinic' => $this->put('inclinic'),
				'reason' => $reason,
				'Confirmed' => $confirmed,
				'all_Day' => $allday,
				'Sedation' => $this->put('sedation'),
				'SedationNote' => $this->put('sedationnote'),
				'ProvHyg' => $this->put('hygienis'),
				'Note' => $this->put('notes'),
				'AptStatus' => $this->put('status'),
				'IsNewPatient' => $this->put('new'));
			$error = $this->citas->edit($AptId,$editAppt);
			if($confirmed == 1){ }
			if($allday == 1){
				$date = $this->put('date');
				$time = $this->put('timeAllDay');
				$time = explode('-',$time);
				$dateI  = new DateTime($date." ".$time[0]);
				$dateF  = new DateTime($date." ".$time[1]);
				$editapptDate['AptDateTime'] = $dateI->format('Y-m-d H:i');
				$editapptDate['AptDateTimeEnd'] = $dateF->format('Y-m-d H:i');
				$this->citas->edit($AptId,$editapptDate);
			}else{
				//$this->citas->update_duration($AptId);
			}
			$this->response($error);
		}
		
		public function citasproce_put(){
			$AptId = $this->put('id');
			
			$procupd = $this->put('procupd');
			$services = $this->put('procedures');
			$error = $this->tp->procedure_add_appt($services,$AptId,$procupd);
			$this->citas->update_duration($AptId);
			$this->response($error);
		}
	
		public function citas_post(){
			$status = $this->post('status');			
			$providerId = $this->post('doctor');
			$date = $this->post('date');
			$time = $this->post('time');
			$times = explode("-",$time);
			$apptdate = new DateTime($date." ".$times[0]);
			$apptdateEnd = new DateTime($date." ".$times[1]);
			$diferencia = date_diff($apptdate,$apptdateEnd);
			$pattern = $this->calculate_pattern($diferencia);
			$patData = $this->post('patient');
			$new = $this->post('new');
			
			if(isset($_POST['procedures'])){
				$services = $this->post('procedures');
			}else{
				$services = array();
			}
			if($new == 1){
				$nuevoPaciente = array(
				'FName' => $patData['nombre'],
				'LName' => $patData['apellidos'],
				'Email' => $patData['correo'],
				'HmPhone' => $patData['casatelefono'],
				'WkPhone' => $patData['trabajotelefono'],
				'WirelessPhone' => $patData['moviltelefono']);
				$this->paciente->agregar($nuevoPaciente);
				$patDataid = $this->db->insert_id();
			}else{
				$patDataid = $patData['id'];
			}
			
			$reason = $this->put('reason');
			if($reason != ""){ $reason = implode(",",$reason); }
			$newAppt = array(
				'PatId' => $patDataid,
				'ProvId' => $providerId,
				'AptDateTime' => $apptdate->format('Y-m-d H:i:s'),
				'AptDateTimeEnd' => $apptdateEnd->format('Y-m-d H:i:s'),
				'ProvHyg' => $this->post('hygienis'),
				'IsNewPatient' => $this->post('new'),
				'reason' => $reason,
				'Needs' => $this->post('needs'),
				'Sedation' => $this->post('sedation'),
				'SedationNote' => $this->post('sedationnote'),
				'inBox' => $this->post('inbox'),
				'Note' => $this->post('notes'),
				'AptStatus' => $status,
				'IsNewPatient' => $this->post('new'),
				'active' => 1);
			$error = $this->citas->create($newAppt,$services);
			
			//$this->citas->upt_provider($patDataid,$providerId);
			
			$this->notificaciones->cita_nueva($error['id']);
			$this->response($error);
			
		}
		
		public function citas_patch($apptid){
			$editappt = array();
			if($this->patch('apptdateEnd') != null){
				$editappt['AptDateTimeEnd'] = $this->patch('apptdateEnd');
			}
			if($this->patch('pattern') != null){ $editappt['Pattern'] = $this->patch('pattern'); }
			if($this->patch('resourceId') != null){ $editappt['ProvId'] = $this->patch('resourceId'); }
			if($this->patch('date') != null){ $editappt['AptDateTime'] = $this->patch('date'); }
			if($this->patch('status') != null){ $editappt['AptStatus'] = $this->patch('status'); }
			if($this->patch('planapt') != null){ $editappt['AptStatus'] =  ($this->patch('planapt') == 'true' ? 7 : 2); }
			if($this->patch('active') != null){ $editappt['active'] = $this->patch('active');
				$this->notificaciones->cita_cancelada($apptid);
			}
			

			$error = $this->citas->edit($apptid,$editappt);
			if($this->patch('date') != null){ $this->citas->update_duration($apptid);}
			
			$this->response($error);
		}
	
		public function citas_delete(){
			$id = $this->delete('id');
			$data['data'] = $this->citas->deleted($id);
			$this->response($data);
		}
	
		public function aptflight_post(){
			$AptId = $this->post('id');
			$pacienteid = $this->post('patient');
			$archivo = "";
			$config['encrypt_name'] = true;
			$config['upload_path']  = './files/patients/'.$pacienteid;
			$config['allowed_types'] = ['jpg','png','pdf'];
			/*$config['max_size']             = 100;
			$config['max_width']            = 1024;
			$config['max_height']           = 768;*/
			
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload('file')){
				$fileError = array(
				'code'=> '1',
				'type'=>'error',
				'title'=>'<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Error',
				'error' => $this->upload->display_errors());
			}else{
				$fileData = $this->upload->data();
				$archivo = $fileData['file_name'];
			}				
			$fecha = new DateTime($this->post('date'));
			$data = array(
				'Vuelo' => $this->post('flight'),
				'VueloFecha' => $fecha->format('Y-m-d'),
				'VueloSalida' => $this->post('horas'),
				'VueloDocu' => $archivo,
				'VueloLlegada' => $this->post('horal'),
				'VueloNotas' => $this->post('note')
			);
			if($archivo == ''){ unset($data['VueloDocu']); }
			$error = $this->citas->edit($AptId,$data);
			$this->response($error);
		}
	#endregion
	
	public function aptback_post(){
		$aptid = $this->post('aptid');
		$error = $this->citas->apt_back_deal($aptid);
		$this->response($error);
	}
	
	private function calculate_pattern($diffData){
		$patternFormat = '';
		$horas = $diffData->h;
		$minutos = $diffData->i;
		$total = ($horas*12);
		for($i = 1; $i <= $total; $i++){
			$patternFormat .='/';
		}
		
		$total = ($minutos/10);
		for($i = 1; $i <= $total; $i++){
			$patternFormat .='//';
		}		
		return $patternFormat;
	}
}