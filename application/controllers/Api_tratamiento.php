<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Api_tratamiento extends REST_Controller {
	function __construct(){
        parent::__construct();
		$headers = $this->input->request_headers();
		$dbConfig = json_decode($this->encryption->decrypt($headers['dbconfig']));
		$this->load->database(db_config($dbConfig));
		
		$this->load->model('Tratamiento_model','paciente');
	}
	
	
	public function lista_get(){
		$activo = $this->get('activo');
		$data['data'] = $this->paciente->lista($activo);
        $this->response($data);
	}
	
	public function totalcosto_post(){
		$tratamientos = $this->post('tratamientos');
		$data['data'] = $this->paciente->totalcosto($tratamientos);
        $this->response($data);
	}
	
}