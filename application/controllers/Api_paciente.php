<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Api_paciente extends REST_Controller {
	function __construct(){
        parent::__construct();
		$headers = $this->input->request_headers();
		$dbConfig = json_decode($this->encryption->decrypt($headers['dbconfig']));
		$this->load->database(db_config($dbConfig));
		
		$this->load->model('Paciente_model','paciente');
		$this->load->model('Usuario_model','usuario');
		$this->load->model('Treatplan_model','tp');
	}
	
	public function agregar_post(){
		$birthdate = new DateTime($this->post('birthdate'));
		$nuevoPaciente = array(
			'FName' => $this->post('nombre'),
			'LName' => $this->post('apellidos'),
			'Email' => $this->post('correo'),
			'Gender' => $this->post('genero'),
			'Birthdate' => $birthdate->format('Y-m-d'),
			'HmPhone' => $this->post('casatelefono'),
			'WkPhone' => $this->post('trabajotelefono'),
			'WirelessPhone' => $this->post('moviltelefono'),
			'EtapaId' => $this->post('etapa'),
			'Country' => $this->post('country'),
			'State' => $this->post('state'),
			'City' => $this->post('city'),
			'Zip' => $this->post('zip'),
			'Origen' => $this->post('origin'),
			'SecDateEntry' => fechaActual('Y-m-d'),
			'Address' => $this->post('address'));
		$error = $this->paciente->agregar($nuevoPaciente);
		
		if($error['code'] == 0){
			$id = $this->db->insert_id();
			$newTP = array(
				'DateTP' => fechaactual('Y-m-d'),
				'PatNum' => $id,
				'Heading' => "Default Treat Plan",
				'Note' => "This treatment plan was created by default when the patient was created.",
				'TPStatus' => '1');
			$this->tp->create($newTP);
			
			$config['upload_path'] = config_system('url_files').$id.'/';
			$config['allowed_types'] = 'jpg|png';
			$config['encrypt_name'] = true;
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload('foto')){
				//$error = array('msg' => $this->upload->display_errors());
				$foto = array('foto'=> 'defaultuser.png');
				$this->db->where('id',$id);
				$this->db->update('patient',$foto);
			}else{
				$archivo = $this->upload->data();
				$foto = array('foto'=> $archivo['file_name']);			
				$this->db->where('id',$id);
				$this->db->update('patient',$foto);
			}
		}
        $this->response($error);
	}
	
	public function editar_post(){
		$pacienteid = $this->post('id');
		
		if (!is_dir('files/patients/'.$pacienteid)){
			mkdir('files/patients/'.$pacienteid, 0777, TRUE);
		}
		
		$config['upload_path'] = './files/patients/'.$pacienteid.'/';
		$config['allowed_types'] = 'jpg|png';
		$config['encrypt_name'] = true;
		/*$config['max_size'] = 100;
		$config['max_width'] = 1024;
		$config['max_height'] = 768;*/
		
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload('foto')){
			$error = array('msg' => $this->upload->display_errors());
		}else{
			$archivo = $this->upload->data();
			$foto = array('foto'=> $archivo['file_name']);			
			$this->db->where('id',$pacienteid);
			$this->db->update('patient',$foto);
			$error = $this->db->error();
			//oldpicture
			
			$oldpicture = $this->input->post('oldpicture');
			unlink('./files/patients/'.$pacienteid.'/'.$oldpicture);
		}
		/* Editar un usuario */
		$birthdate = new DateTime($this->post('birthdate'));
		$paciente = array(
			'FName' => $this->post('nombre'),
			'LName' => $this->post('apellidos'),
			'Email' => $this->post('correo'),
			'Gender' => $this->post('genero'),
			'EtapaId' => $this->post('etapa'),
			'Birthdate' => $birthdate->format('Y-m-d'),
			'HmPhone' => $this->post('casatelefono'),
			'WkPhone' => $this->post('trabajotelefono'),
			'Origen' => $this->post('origin'),
			'Country' => $this->post('country'),
			'State' => $this->post('state'),
			'City' => $this->post('city'),
			'Zip' => $this->post('zip'),
			'Address' => $this->post('address'),
			'WirelessPhone' => $this->post('moviltelefono'),
			);
		$error = $this->paciente->editar($paciente,$pacienteid);
        $this->response($error);
	}
	
	public function pacienteEstado_get(){
		/* Lista de paciente; activo define el tipo de paciente que se mostrara*/
		$estado = $this->get('estado');
		$data['data'] = $this->paciente->pacientes_estado($estado);
		$this->response($data);
    }
	
	public function lista_get(){
		/* Lista de paciente; activo define el tipo de paciente que se mostrara*/
		$activo = $this->get('activo');
		$data['data'] = $this->paciente->lista($activo);
		$this->response($data);
    }

	public function listaestados_get(){
		$activo = $this->get('activo');
		$data['data'] = $this->paciente->listaestados($activo);
		$this->response($data);
	}
	
	public function detallestatus_get(){
		$id = $this->get('id');
		$data['data'] = $this->paciente->detallestatus($id);
		$this->response($data);
	}
	
	public function detalle_get(){
		$id = $this->get('id');
		$data['data'] = $this->paciente->detalle($id);
		$this->response($data);
	}
	
	public function buscador_get(){
		$nombre = $this->get('nombre');
		$data['data'] = $this->paciente->buscador($nombre);
		$this->response($data);
	}

	public function editaretapa_post(){
		$estado = array('EtapaId' => $this->post('etapa'));
		$pacienteid = $this->post('id');
		$error = $this->paciente->editarestado($estado,$pacienteid);
        $this->response($error);
	}
		
	public function notes_get(){
		$pacienteid = $this->get('id');
		$data['data'] = $this->paciente->notas_paciente($pacienteid);
		$this->response($data);
	}	
	
	public function procedimientos_get(){
		$pacienteid = $this->get('id');
		$data['data'] = $this->paciente->procedimientos($pacienteid);
		$this->response($data);
	}
	
	public function archivos_get(){
		$pacienteid = $this->get('id');
		$data['data'] = $this->paciente->archivos_lista($pacienteid);
		$this->response($data);
	}

	#region pagos
		public function payment_get(){
			$id = $this->get('patient');
			$data['data'] = $this->paciente->patient_payment($id);
			$this->response($data);
		}
	#endregion
	
	#region Ofertas
		public function editaretapaoferta_post(){
			$vendida = $this->post('vendida');
			$etapaid = $this->post('etapa');
			$oldetapa = $this->post('oldetapa');
			$etapa = array(
				'etapaid' => $etapaid,
				'vendida' => $vendida);
			$ofertaid = $this->post('idoferta');
			$error = $this->paciente->editar_oferta_etapa($ofertaid,$etapa,$oldetapa);
			if($error['code'] == 0){
				$fecha = new DateTime();
				
				$this->db->select('etapa.name');
				$this->db->from('etapa');
				$this->db->where('etapa.id',$etapaid);
				$etapaData = $this->db->get()->result();
				
				$this->db->select('etapa.name');
				$this->db->from('etapa');
				$this->db->where('etapa.id',$oldetapa);
				$oldetapaData = $this->db->get()->result();
				
				$nuevaActividad = array(
				'usuario' => 1,
				'fecha' => $fecha->format('Y-m-d'),
				'hora' => $fecha->format('H:i:00'),
				'tipoid' => 1,
				'ofertaid' => $ofertaid,
				'actividadTipo' => 0,
				'etapaid' => $oldetapa,
				'inicioEtapa' => 0,
				'finEtapa' => 1,
				'estadoid' => 1,
				'pacienteid' => $this->post('patientid'),
				'nota' => "End stage: <b>".$oldetapaData[0]->name."</b>");
				$fileError = $this->paciente->agregar_actividad($nuevaActividad);
				
				$nuevaActividad = array(
				'usuario' => 1,
				'fecha' => $fecha->format('Y-m-d'),
				'hora' => $fecha->format('H:i:00'),
				'tipoid' => 1,
				'ofertaid' => $ofertaid,
				'actividadTipo' => 0,
				'etapaid' => $etapaid,
				'inicioEtapa' => 1,
				'estadoid' => 1,
				'pacienteid' => $this->post('patientid'),
				'nota' => "Start stage: <b>".$etapaData[0]->name."</b>");
				$fileError = $this->paciente->agregar_actividad($nuevaActividad);
				if($vendida == 1){
					$this->db->select('GROUP_CONCAT(ofertas_tratamientos.tratamientoid) as id');
					$this->db->from('ofertas_tratamientos');
					$this->db->where('ofertas_tratamientos.ofertaid',$ofertaid);
					$tratamientos = $this->db->get()->result();
					
					$this->notificaciones->crear(4,'Deal vendida','success',null,$ofertaid,3);
					
					$this->db->select('ofertas.doctorid,ofertas.valor');
					$this->db->from('ofertas');
					$this->db->where('ofertas.id',$ofertaid);
					$dataDoctor = $this->db->get()->result();
					
					//Crear cita sin agendar
					$newAppt = array(
						'PatId' => $this->post('patientid'),
						'ProvId' => $dataDoctor[0]->doctorid,
						'AmountApp' => $dataDoctor[0]->valor,
						'reason' => $tratamientos[0]->id,
						'AptStatus' => 4,
						'active' => 1,
						'ofertaid' => $ofertaid);
					$this->db->insert('appointment',$newAppt);
				}
			}
			$this->response($error);
		}	
		
		public function agregaroferta_post(){
			$headers = $this->input->request_headers();
			$fecha =  new DateTime();
			$valor = $this->post('value');
			$valor = str_ireplace(',','',$valor);
			$fechaCierre = new DateTime($this->post('closedate'));	
			$usuario = $this->post('usuario');
			/*
			* Si el usuario que inicio sesion es de tipo Administrador(ID: 3)
			* tomara el usuario que seleccione del input en lugar del usuario que inicio sesion.
			*/
			if($headers['usertype'] == 1){
				$usuario = $this->post('seller');
			}else{
				$usuario = $this->post('usuario');
			}
			
			/***********/
			
			
			$nuevaOferta = array(
				'pacienteid' => $this->post('patient'),
				'usuarioid' => $usuario,
				'fecha' => $fecha->format('Y-m-d'),
				'valor' => $valor,
				'doctorid' => $this->post('doctor'),
				'estadoid' => 6,
				'activo' => 1,
				'etapaid' => 1,
				'fechaCierre' => $fechaCierre->format('Y-m-d')
			);
			$tratamientos = $this->post('treatment');
			$error = $this->paciente->agregar_oferta($nuevaOferta,$tratamientos);
			if($error['code'] == 0){
				$this->send_email();
				$control = array(
					'ofertaid' => $error['id'],
					'etapaid' => 1,
					'fechaInicio' => $fecha->format('Y-m-d H:i'),
					'terminado' => 0);
				$this->db->insert('etapa_control',$control);
				$this->notificaciones->oferta_nueva($error['id']);
			}
			$this->response($error);
		}
		
		public function editaroferta_post(){
			$fecha =  new DateTime();
			$ofertaid = $this->post('id');
			$valor = $this->post('value');
			$valor = str_ireplace(',','',$valor);
			
			$fecha = new DateTime($this->post('closedate'));
			$editarOferta = array(
				'valor' => $valor,
				'doctorid' => $this->post('doctor'),
				'fechaCierre' => $fecha->format('Y-m-d')
			);
			$proceadd = $this->post('proceadd');
			$procedel = $this->post('procedel');
			$error = $this->paciente->editar_oferta($ofertaid,$editarOferta,$procedel,$proceadd);
			$this->response($error);
		}
		
		public function detalleoferta_get(){
			$ofertaid = $this->get('id');
			$data = $this->paciente->detalle_oferta($ofertaid);
			$this->response($data);
		}
		
		public function lost_post(){
			$fecha = new DateTime();
			$ofertaid = $this->post('ofertaid');
			$tratamientos = $this->post('treatment');
			$editarOferta = array('activo' => 0,'estadoid' => 2);
			
			$error = $this->paciente->editar_oferta($ofertaid,$editarOferta,$tratamientos);
			if($error['code'] == 0){
				$oferta = array(
				'perdidaFecha' => $fecha->format('Y-m-d'),
				'perdidaRazon' => $this->post('reason'));
				$error = $this->paciente->oferta_estado($ofertaid,$oferta);
				
				if($error['code'] == 0){
					$tipo = 1;
					$pacienteid = $this->post('id');
					$paciente = $this->paciente->detalle($pacienteid);
					$usuario = $this->usuario->detalle($this->post('userid'));
					$color = "danger";
					$mensaje =
					'<div class="title margin-bottom-0"><a href="#">'.$usuario[0]->nombre.' '.$usuario[0]->apellidos.'</a></div>
					<p>Change the patient <b>'.$paciente[0]->FName.' '.$paciente[0]->LName.'</b> to lost</p>';
					$this->notificaciones->crear(1,$mensaje,$color,$pacienteid,$ofertaid,$tipo);
				}
			}
			$this->response($error);
		}
		
		public function ofertas_get(){
			$estadoid = $this->get('estado');
			$pacienteid = $this->get('paciente');
			$data['data'] = $this->paciente->ofertas($estadoid,$pacienteid);
			$this->response($data);
		}
		
		public function ofertasvendidas_get(){
			$data['data'] = $this->paciente->ofertas_vendidas();
			$this->response($data);
		}
		
		public function ofertaslost_get(){
			$data['data'] =  $this->paciente->ofertas_perdidas();
			$this->response($data);
		}
		
		public function lostinformer_get(){
			$data['data'] =  $this->paciente->ofertas_perdidas_informes();
			$this->response($data);
		}
	
		public function provcomp_get(){
			$origen = $this->get('origen');
			$data['data'] = $this->paciente->prov_com_ingresos($origen);
			$this->response($data);
		}
		
		public function provcompdeals_get(){
			$data = array();
			$anio = new DateTime();
			$meses = array('January','February','March','April','May','June','July','August','September','October','November','December');
			foreach($meses  as $i => $mes){
				$this->db->select('provider.id,CONCAT(provider.LName," ",provider.FName) as provi');
				$this->db->from('provider');
				$providers = $this->db->get()->result();
				foreach($providers as $provider){
					$this->db->select('Sum(appointment.AmountApp) as mount');
					$this->db->from('appointment');
					$this->db->where('appointment.ProvId',$provider->id);
					$this->db->where('DATE_FORMAT(appointment.AptDateTime,"%Y-%m")',$anio->format('Y')."-".($i+1));
					$mount = $this->db->get()->result();
					$provider->mount = number_format($mount[0]->mount,2);
				}
				$data[] = array('mes'=>$mes,'providers' => $providers);
			}
			$this->response($data);
		}
	#endregion 
	
	#region Actividades
		public function actividaddetalle_get(){
			$id = $this->get('id');
			$data = $this->paciente->detalle_actividad($id);
			$this->response($data);
		}
		
		public function editarnota_post(){
			$id = $this->post('id');
			$nota = $this->post('note');
			$data = $this->paciente->editar_nota($id,$nota);
			$this->response($data);
		}
		
		public function actividadestado_post(){
			$id = $this->post('id');
			$estadoid = $this->post('estado');
			$data = $this->paciente->actividad_estado($id,$estadoid);
			$this->response($data);
		}
		
		public function editaracti_post(){
			$id = $this->post('id');
			$tipo = $this->post('type');
			$nota = $this->post('note');
			$data = $this->paciente->editar_actividad($id,$nota,$tipo);
			$this->response($data);
		}
		
		public function prueba_get(){
			$msg['body'] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed ante quam. Proin a sem turpis. Ut convallis neque et scelerisque placerat. Nunc molestie dolor sed enim luctus, in sollicitudin felis varius. Quisque et fringilla ligula, id aliquet neque. Nulla leo urna, finibus at suscipit a, aliquam sed dolor. Nunc lorem lacus, auctor ac tortor a, consectetur dapibus orci. Morbi in lobortis tellus. Maecenas et ultrices augue. Suspendisse risus sem, maximus sit amet iaculis nec, tempus accumsan purus.
			Phasellus ut nisi et lectus congue ultrices. Donec viverra nisi quis egestas iaculis. Curabitur pharetra, ipsum et efficitur tristique, mi orci pulvinar urna, non blandit arcu ante in tortor. Vivamus ut eros massa. Quisque aliquam est sit amet urna lobortis tempus. Phasellus eget molestie massa, quis tincidunt sem. Nullam sagittis dui et nibh maximus, et faucibus enim vestibulum. Sed nunc ante, lacinia quis ullamcorper et, dignissim sit amet est. Fusce sodales fermentum accumsan. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus sodales sapien vel urna congue ultrices. Mauris id tortor vitae diam suscipit lobortis. Sed ullamcorper est vitae ex gravida placerat. Nulla velit lacus, condimentum nec metus at, dignissim lobortis libero.";
			$this->load->view('correo_plantillas/email_simple',$msg);
		}
		
		public function sendemail_post(){
			$config['upload_path']          = './uploads/';
			$config['allowed_types']        = 'gif|jpg|png';
			/*$config['max_size']             = 100;
			$config['max_width']            = 1024;
			$config['max_height']           = 768;*/
			
			$this->load->library('upload', $config);
			$fileError = array();
			$fileUploads = array();
			
			$templates = $this->post('templates');
			$filespatient = $this->post('filespatient');
			
			$files = $_FILES;
			$cpt = count($_FILES['files']['name']);
			for($i=0; $i < $cpt; $i++){           
				$_FILES['file']['name'] = $files['files']['name'][$i];
				$_FILES['file']['type'] = $files['files']['type'][$i];
				$_FILES['file']['tmp_name'] = $files['files']['tmp_name'][$i];
				$_FILES['file']['error'] = $files['files']['error'][$i];
				$_FILES['file']['size'] = $files['files']['size'][$i];
				if(!$this->upload->do_upload('file')){
					$error = array('msg' => $this->upload->display_errors());
					$archivos = false;
				}else{
					$archivos = true;
					//$fileUploads = );
					array_push($fileUploads,array('data' => $this->upload->data()));
				}
			}
			$msg['body'] = $this->post('body');			
			$this->db->select('notificaciones_reportes.instrucciones');
			$this->db->from('notificaciones_reportes');
			$this->db->where('notificaciones_reportes.id',2);
			$bcc = $this->db->get()->result();
			
			$config = $this->configcode->emailconfig();
			$this->load->library('email', $config);
			//$this->email->initialize($config);
			$this->email->clear();
			$this->email->set_crlf( "\r\n" );
			$this->email->set_newline("\r\n");
			foreach($fileUploads as $archivo){ 
				$this->email->attach($archivo['data']['full_path']);
			}
			if(isset($_POST['templates']) == true){
				foreach($templates as $template){ $this->email->attach('./files/'.$template); }
			}
			if(isset($_POST['filespatient']) == true){
				foreach($filespatient as $files){ $this->email->attach('./files/'.$file); }
			}
			$this->email->set_header('Header1', 'Value1');
			$this->email->from('system@smile4ever.com');
			$this->email->to($this->post('to'));
			$this->email->cc($this->post('cc'));
			if(count($bcc) > 0){
				$this->email->bcc($bcc[0]->instrucciones);
			}
			$this->email->subject($this->post('subject'));
			$this->email->message($this->load->view('correo_plantillas/email_template',$msg,TRUE));
			if($this->email->send()){
				$nota = '<p><b>To:</b> '.$this->post('to').'</p>
				<p><b>CC:</b> '.$this->post('cc').'</p> 
				<p><b>Subject: </b>'.$this->post('subject').'</p>'.$this->post('body');
				$fecha =  new DateTime();
				$nuevaActividad = array(
					'usuario' => $this->post('userid'),
					'ofertaid' => $this->post('ofertaid'),
					'fecha' => $fecha->format('Y-m-d'),
					'hora' => $fecha->format('h:i:00'),
					'tipoid' => 4,
					'actividadTipo' => 5,
					'etapaid' => 1,
					'estadoid' => 2,
					'pacienteid' => $this->post('id'),
					'nota' => $nota);
					$error = $this->paciente->agregar_actividad($nuevaActividad);
			}else{
				$error = array('code' => 100, "msg" => "Error email failed to send");
				$error = $this->email->print_debugger();
				
			}
			$this->response($error);
		}
		
		public function agregararchivos_post(){
			$foldername = $this->input->post('foldername');
			$fecha = new DateTime();
			$pacienteid = $this->post('patient');
			
			if(!is_dir(config_system('url_files').'/'.$foldername.'/patients/'.$pacienteid)){
				mkdir(config_system('url_files').'/'.$foldername.'/patients/'.$pacienteid, 0777, TRUE);
			}
			
			$config['encrypt_name'] = true;
			//                                       files\766317b7bea97273e5f8165466d766fa\patients\89
			$config['upload_path'] = config_system('url_files').'/'.$foldername.'/patients/'.$pacienteid;
			$config['allowed_types'] = ['jpg','jpeg','png','pdf'];
			/*$config['max_size'] = 100;
			$config['max_width'] = 1024;
			$config['max_height'] = 768;*/
			
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload('file')){
				$fileError = array(
				'code'=> '1',
				'type'=>'error',
				'title'=>'<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Error',
				'error' => $this->upload->display_errors());
			}else{
				$fileData = $this->upload->data();
				
					$archivo = array(
						'usuarioid' => $this->post('user'),
						'pacienteid' => $pacienteid,
						'nombre' => $fileData['client_name'],
						'nombreFisico' => $fileData['file_name'],
						'fecha' => $fecha->format('Y-m-d H:i'),
						'activo' => 1);
					$this->db->insert('paciente_archivos',$archivo);
					$archivoid = $this->db->insert_id();
					
					$nuevaActividad = array(
					'usuario' => $this->post('user'),
					'fecha' => $fecha->format('Y-m-d'),
					'hora' => $fecha->format('H:i:00'),
					'archivoid' => $archivoid,
					'tipoid' => 3,
					'etapaid' => $this->post('etapaId'),
					'estadoid' => 2,
					'pacienteid' => $pacienteid,
					'ofertaid' => $this->post('ofertaid'),
					'nota' => '<dl class="dl-horizontal"> 
								<dt>File</dt><dd>'.$fileData['client_name'].'</dd> 
								<dt>Size</dt><dd>'.($fileData['file_size'] / 1000).' MB.</dd> 
								<dt>Type</dt><dd>'.$fileData['file_type'].'</dd> 
							</dl>');
					$fileError = $this->paciente->agregar_actividad($nuevaActividad);
					
					$fileError = array(
					'code'=> '0',
					'type'=>'success',
					'title'=>'<i class="fa fa-fa-check-circle"></i> Complete',
					"error"=>"Data saved in the system.");
			}
			
			$this->response($fileError);
		}
		
		public function agregarnota_post(){
			$fecha =  new DateTime();
			$nuevaActividad = array(
				'usuario' => $this->post('userid'),
				'fecha' => $fecha->format('Y-m-d'),
				'hora' => $fecha->format('h:i:00'),
				'tipoid' => $this->post('tipo'),
				'actividadTipo' => 0,
				'ofertaid' => $this->post('ofertaid'),
				'etapaid' => 1,
				'estadoid' => 1,
				'pacienteid' => $this->post('id'),
				'nota' => $this->post('note'));
			$error = $this->paciente->agregar_actividad($nuevaActividad);
			$this->response($error);
		}
		
		public function agregaractividad_post(){
			$activityTime = $this->input->post('activityTime');
			$fechaActividad = new DateTime($this->post('startdate')." ".$this->post('starthour'));
			$HoraActividadFin = $fechaActividad->modify("+".$activityTime." minute");
			
			$fechaInicio =  new DateTime($this->post('startdate'));
			$fecha =  new DateTime();
			$nuevaActividad = array(
				'fecha' => $fecha->format('Y-m-d'),
				'hora' => $fecha->format('h:i:00'),
				'fechaInicio' => $fechaInicio->format('Y-m-d'),
				'horaInicio' => $this->post('starthour'),
				'horaTerminado' => $HoraActividadFin->format("H:i A"),
				'tipoid' => $this->post('tipo'),
				'actividadTipo' => $this->post('type'),
				'usuario' => $this->post('user'),
				'etapaid' => $this->post('etapaid'),
				'ofertaid' => $this->post('ofertaid'),
				'estadoid' => 1,
				'pacienteid' => $this->post('id'),
				'nota' => $this->post('note'));
			$error = $this->paciente->agregar_actividad($nuevaActividad);
			$this->response($error);
		}
		
		public function listaactividades_get(){
			$pacienteid = $this->get('id');
			$ofertaid = $this->get('ofertaid');
			$etapaid = $this->get('etapaid');
			$error['data'] = $this->paciente->lista_actividades($pacienteid,$ofertaid,$etapaid);
			$this->response($error);
		}
		
		public function listaactividadespendientes_get(){
			$pacienteid = $this->get('id');
			$ofertaid = $this->get('ofertaid');
			$etapaid = $this->get('etapaid');
			$error['data'] = $this->paciente->lista_actividades_pendientes($pacienteid,$ofertaid,$etapaid);
			$this->response($error);
		}
	
		public function sms_post(){
			$number = $this->post('number');
			$text = $this->post('message');
			
			$nota ='<dl class="dl-horizontal"> 
						<dt>Number</dt><dd>'.$number.'</dd> 
						<dt>Message</dt><dd>'.$text.'</dd></dl>';
			$fecha =  new DateTime();
			$nuevaActividad = array(
				'usuario' => $this->post('userid'),
				'fecha' => $fecha->format('Y-m-d'),
				'hora' => $fecha->format('h:i:00'),
				'tipoid' => $this->post('tipo'),
				'actividadTipo' => 7,
				'ofertaid' => $this->post('ofertaid'),
				'etapaid' => 1,
				'estadoid' => 1,
				'pacienteid' => $this->post('patid'),
				'nota' => $nota);
			$error = $this->paciente->agregar_actividad($nuevaActividad);
			
			/*
			
				'to' => '16194092080',
    'from' => '18582861970',
    'text' => 'Hello from Vonage SMS API'
			*/
			$this->load->library('nexmo');
			$this->nexmo->set_format('json');
			$from = 18582861970;
			$to = 16194092080;
			$message = array( 'text' => $text);
			$response = $this->nexmo->send_message($from, $to, $message);
			$this->response($response);
			$this->response($error);
		}
	#endregion

	#region Perfil
		public function nextapptlimit_get(){
			$this->load->helper('html');
			$patid = $this->get('pat');
			$limit = $this->get('limit');
			$data = $this->paciente->nextapptlimit($patid,$limit);
			$this->response($data);
		}
		
		public function pdetails_get(){
			$patid = $this->get('id');
			$data = $this->paciente->details_perfil($patid);
			$this->response($data);
		}
	#endregion

	#region plan de pagos
		public function paymentplan_get(){
			$patid = $this->get('patient');
			$data['data'] = $this->paciente->paymentplan_list($patid);
			$this->response($data);
		}
	#endregion


	private function send_email(){
		$data["body"] = "Hola mensaje de texto";
		$config = $this->configcode->emailconfig();
		$this->load->library('email', $config);
		$this->email->clear();
		$this->email->set_crlf( "\r\n" );
		$this->email->set_newline("\r\n");
		$this->email->set_header('Header1', 'Value1');
		$this->email->from('system@smile4ever.com');
		$this->email->to("kevin.edua@live.com.mx");
		$this->email->subject("Mensaje de bienvenida");
		$this->email->message($this->load->view('correo_plantillas/email_template',$data,TRUE));
		if($this->email->send()){
			/*$nota = '<p><b>To:</b> '.$this->post('to').'</p>
			<p><b>CC:</b> '.$this->post('cc').'</p> 
			<p><b>Subject: </b>'.$this->post('subject').'</p>'.$this->post('body');
			$fecha =  new DateTime();
			$nuevaActividad = array(
				'usuario' => $this->post('userid'),
				'ofertaid' => $this->post('ofertaid'),
				'fecha' => $fecha->format('Y-m-d'),
				'hora' => $fecha->format('h:i:00'),
				'tipoid' => 4,
				'actividadTipo' => 5,
				'etapaid' => 1,
				'estadoid' => 2,
				'pacienteid' => $this->post('id'),
				'nota' => $nota);
				$error = $this->paciente->agregar_actividad($nuevaActividad);*/
				$error = array('code' => 0, "msg" => "");
		}else{
			$error = array('code' => 100, "msg" => "Error email failed to send");
			$error = $this->email->print_debugger();
		}
	}
}