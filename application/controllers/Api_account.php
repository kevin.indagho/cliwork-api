<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Api_account extends REST_Controller {
	function __construct(){
        parent::__construct();
		$headers = $this->input->request_headers();
		$dbConfig = json_decode($this->encryption->decrypt($headers['dbconfig']));
		$this->load->database(db_config($dbConfig));
		$this->load->model('Account_model','account');
	}
	
	public function account_get(){
		$patient = $this->get('patient');
		$tpid = $this->get('treatplan');
		$status = $this->get('status');
		$data['data'] = $this->account->account_list($tpid,$patient,$status);
		$this->response($data);
	}

	public function accounttp_get(){
		$patient = $this->get('patient');
		$tpid = $this->get('treatplan');
		$status = $this->get('status');
		$data['data'] = $this->account->tp_details($tpid,$patient,$status);
		$this->response($data);
	}
	
	public function balance_get(){
		$patid = $this->get('patient');
		$data['data'] = $this->account->account_balance($patid);
		$this->response($data);
	}
	
	public function accounttotal_get(){
		$patient = $this->get('patient');
		$tpid = $this->get('treatplan');
		$data['data'] = $this->account->account_total($tpid,$patient);
		$this->response($data);	
	}

	public function payment_post(){
		$fecha = new Datetime();
		$pacienteid = $this->post('patient');
		$monto = $this->post('amount');
		$archivo = "";
		
		$config['upload_path'] = './files/patients/'.$pacienteid;
		$config['allowed_types'] = 'jpg|png';
		$config['encrypt_name'] = true;
		/*$config['max_size'] = 100;
		$config['max_width'] = 1024;
		$config['max_height'] = 768;*/
		
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload('document')){
			$error = array('msg' => $this->upload->display_errors());
			$archivo = "";
		}else{
			$archivo = $this->upload->data();
			$archivo = $archivo['file_name'];
		}
		
		$newPayment = array(
		'fecha' => $fecha->format('Y-m-d H:i'),
		'monto' => $monto,
		'archivo' => $archivo,
		'doctor' => $this->post('provider'),
		'notas' => $this->post('notes'),
		'tipo' => $this->post('type'),
		'usuarioid' => $this->post('userid'),
		'patientid' => $this->post('patient'));
		
		$error = $this->account->payment_add($newPayment,$pacienteid);		
		//$error = $this->account->validate_pay_planpagos($newPayment,55,57);
		$this->response($error);
	}
	
	public function payment_get(){
		$id = $this->get('payment');
		$data['data'] = $this->account->payment_read($id);
		$this->response($data);
	}
	
	public function paymentedit_post(){
		$id = $this->post('id');
		$pacienteid = $this->post('patient');
		$monto = $this->post('amount');
		$archivo = "";
		
		$config['upload_path'] = './files/patients/'.$pacienteid;
		$config['allowed_types'] = 'jpg|png';
		$config['encrypt_name'] = true;
		/*$config['max_size'] = 100;
		$config['max_width'] = 1024;
		$config['max_height'] = 768;*/
		
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload('document')){
			$error = array('msg' => $this->upload->display_errors());
			$archivo = "";
		}else{
			$archivo = $this->upload->data();
			$archivo = $archivo['file_name'];
		}
		
		$payment = array(
		'monto' => $monto,
		//'archivo' => $archivo,
		'notas' => $this->post('notes'),
		'doctor' => $this->post('provider'),
		'tipo' => $this->post('type'),
		'usuarioid' => $this->post('userid'),
		'patientid' => $this->post('patient'));	
		$error = $this->account->payment_upd($id,$payment);
		$this->response($error);
	}
	
	public function adjustment_post(){
		$fecha = new Datetime();
		$newAdjusment = array(
			'fecha' => $fecha->format('Y-m-d H:i'),
			'pacienteid' => $this->post('patient'),
			'treatplanid' => $this->post('treatplan'),
			'usuarioid' => $this->post('userid'),
			'monto' => $this->post('amount'),
			'tipo' => $this->post('type'),
			'notas' => $this->post('notes'));
		$error = $this->account->adjusment_add($newAdjusment);
		$this->response($error);
	}	
	
	public function valid_get(){
		$patid = $this->get('patient');
		$data = $this->account->valid_tp_incomple($patid);
		$this->response($data);
	}

	#region PaymentPLan
		public function paymentplanpdf_get(){
			$planid = $this->get('planid');
			$data = $this->account->playmentplan_details($planid);
			$this->load->view('pdf/payment_plan',$data);
		}
	
		public function paycalculate_post(){
			$fechas = array();
			
			$dateStart = $this->post('datestart');
			$quantity = $this->post('quantity');
			$type = $this->post('type');
			
			$dateStart = date($dateStart);
			for($i = 0; $i < $quantity; $i++){
				if($type == 1){
					$nuevafecha = strtotime('+'.$i.' month',strtotime($dateStart));
				}else{
					$nuevafecha = strtotime('+'.$i.' week',strtotime($dateStart));	
				}				
				$fechas[] = date('Y-m-d',$nuevafecha);
			}
			$this->response($fechas);
		}
	
		public function paymentplanactive_get(){
			$patientid = $this->get('patient');
			$data['data'] = $this->account->playmenta_active($patientid);
			$this->response($data);
		}
		
		public function paymentplan_get(){
			$planid = $this->get('planid');
			$data['data'] = $this->account->paymentplan($planid);
			$this->response($data);
		}
		
		public function paymentplan_post(){
			$planid = $this->post('planid');
			$fecha = new DateTime();
			$newPaymentPlan = array(
				'fecha' => $fecha->format('Y-m-d H:i'),
				'planAnterior' => $this->post('planid'),
				'usuario' => $this->post('userid'),
				'pacienteid' => $this->post('patientid'),
				'comision' => $this->post('profit'),
				'deuda' => $this->post('value'),
				'duracion' => $this->post('quantity'),
				'pago' => $this->post('pay'),
				'tipo' => $this->post('type'),
				'activo' => 1);
			$fechas = $this->post('fechas');
			$error = $this->account->playmentplan_add($newPaymentPlan,$fechas,$planid);
			$this->response($error);
		}
		
		public function paymentplan_put(){
			$planid = $this->put('planid');
			$error = $this->account->playmentplan_complete($planid);
			$this->response($error);
		}
	#endregion
	
	#region PDF
		public function pdf_tp_account_get(){
			$accountData['databusiness'] = data_bussines();
			$patient = $this->get('patient');
			$this->db->select('patient.Id,
			CONCAT(patient.LName," ",patient.FName) as cliente,
			patient.Address,
			patient.City,
			patient.Email,
			patient.State as state');
			$this->db->from('patient');
			$this->db->where('patient.Id',$patient);
			$accountData['encabezado'] = $this->db->get()->result();
			$accountData['account'] = $this->account->account_pdf($patient);
			$this->load->view('pdf/treatplan_account',$accountData);
			//$this->load->view('pdf/treatplan_account',$accountData);
			
			// Cargamos la librería
			//$this->load->library('pdfgenerator');
			//$filename = 'comprobante_pago';
			//$this->pdfgenerator->generate($template, $filename, true, 'Letter', 'portrait');
		}
	#endregion
}