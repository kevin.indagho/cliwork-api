<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Api_usuariotipo extends REST_Controller {
	
	function __construct(){
		parent::__construct();
		/*
		* Leer el encabezado de las peticiones HTTP
		* y obtener los datos de configuracion para la conexion a la base de datos
		*/
		$headers = $this->input->request_headers();
		$dbConfig = json_decode($this->encryption->decrypt($headers['dbconfig']));
		$this->load->database(db_config($dbConfig));
		
		$this->load->model('Usuariotipo_model','tipo');
	}
	
	public function tipos_get(){
		$activo = $this->get('activo');
		$data['data'] = $this->tipo->lista($activo);
		$this->response($data);
	}
	
}