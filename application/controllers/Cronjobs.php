<?php
class Cronjobs extends CI_Controller{
	public function __construct(){
		parent::__construct();
		/*
		* Leer el encabezado de las peticiones HTTP
		* y obtener los datos de configuracion para la conexion a la base de datos
		*/
		$headers = $this->input->request_headers();
		$dbConfig = json_decode($this->encryption->decrypt($headers['dbconfig']));
		$this->load->database(db_config($dbConfig));
		
		$this->load->model('Paciente_model','paciente');	
		$this->load->model('Cronjobs_model','cronjobs');	
	}
	
	public function test(){
		$this->load->view('correo_plantillas/email_template');
	}
	
	public function proce_cuidados(){
		$this->db->select('procedurelog.DateComplete,
		procedurelog.ProcStatus,
		procedurecode.EmailCuidados,
		CONCAT(patient.LName," ",patient.FName) as cliente,
		procedurecode.ProcCode,
		procedurecode.Descript,
		patient.Email');
		$this->db->from('procedurelog');
		$this->db->join('procedurecode','procedurelog.CodeNum = procedurecode.id','inner');
		$this->db->join('patient','procedurelog.PatNum = patient.Id','inner');
		$this->db->where('procedurecode.EmailCuidados',1);
		$procedures = $this->db->get()->result();
		foreach($procedures as $proce){
			$data['body'] = "
			<h1>Hi ".$proce->cliente."</h1>
			<p>Complete procedure<br>".$proce->ProcCode." - ".$proce->Descript."</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>";
		#region Enviar correo
			$config = $this->configcode->emailconfig();
			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from('system@cliwork.com','Cliwork');
			$this->email->to($proce->Email);
			//$this->email->attach($pdfString, 'attachment', 'statement.pdf', 'application/pdf');
			$this->email->subject('SMILE4EVER | Dental Indications!');
			$this->email->message($this->load->view('correo_plantillas/email_template',$data,true));
			$error = $this->email->send();
		#endregion
		}		
	}
		
	public function notification_pago(){
		$pdfroot  = "files/";		
		$this->db->select('pagos.id,
		pagos.fecha,
		pagos.monto,
		pagos.notas,
		pagos.archivo,
		pagos.usuarioid,
		pagos.patientid,
		pagos.tipo,
		pagos.treatplanid,
		CONCAT(patient.LName," ",patient.FName) as patient,
		CONCAT(usuario.nombre," ",usuario.apellidos) as user');
		$this->db->from('pagos');
		$this->db->join('patient','pagos.patientid = patient.Id','inner');
		$this->db->join('usuario','pagos.usuarioid = usuario.id','inner');
		$this->db->where('pagos.patientid',89);
		$data = $this->db->get()->result();
		
		
		$data['body'] = "<h1>Dear ".$data[0]->patient."</h1>
		<p>Thank you for the recent payment you have made to us for the sum of <b>$".number_format($data[0]->monto,2)."</b>.</p>
		<p>I hereby acknowledge receipt of payment which has been set against the following invoices.</p>
		<p>@PaidInvoiceList@</p>
		<p>If I can be of any further assistance, please do not hesitate to contact me.</p>
		<p>".$data[0]->user."</p>";
		
		//$template = $this->load->view('pdf/treatplan_details',$data,true);
		
		#region PATIENT ACCOUNT
			$this->db->select('patient.Id,
			CONCAT(patient.LName,patient.FName) as cliente,
			patient.Address,
			patient.City,
			patient.Email,
			patient.State as state');
			$this->db->from('patient');
			$this->db->where('patient.Id',$data[0]->id);
			$accountData['encabezado'] = $this->db->get()->result();
			$accountData['account'] = $this->cronjobs->account_list($data[0]->patientid);
			$template = $this->load->view('pdf/treatplan_account',$accountData,true);
			//$this->load->view('pdf/treatplan_account',$accountData);
			//print_r(json_encode($accountData));
		#endregion
		
		#region Generar PDF		
			//file_put_contents($pdfroot."/patients/".$data[0]->patientid."/".MD5($data[0]->id).".pdf",$pdfString); 
			$this->load->library('pdfgenerator');
			$filename = 'comprobante_pago';
			$pdfString = $this->pdfgenerator->generate($template, $filename, false, 'Letter', 'portrait');
		#endregion
		
		#region Enviar correo
			$config = $this->configcode->emailconfig();
			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from('system@cliwork.com','Cliwork');
			$this->email->to('kevin.edua@live.com.mx');
			$this->email->attach($pdfString, 'attachment', 'statement.pdf', 'application/pdf');
			$this->email->subject('SMILE4EVER | Payment!');
			$this->email->message($this->load->view('correo_plantillas/email_template',$data,true));
			$error = $this->email->send();
		#endregion
		print_r($error);
	}
	
	public function recordatorio_citas_agendadas(){
		$databusiness = data_bussines();
		
		$this->db->select();
		$fecha = new DateTime();
		$this->db->select('notificaciones_reportes.instrucciones');
		$this->db->from('notificaciones_reportes');
		$this->db->where('notificaciones_reportes.id',8);
		$diasData = $this->db->get()->result();
		$dias = explode(",",$diasData[0]->instrucciones);
		foreach($dias as $dia){
			$nuevafecha = strtotime('+'.$dia.' day',strtotime($fecha->format('Y-m-d')));
			$nuevafecha = date ('Y-m-d',$nuevafecha);
			$this->db->select('appointment.id,
			CONCAT(patient.FName," ",patient.LName) AS patient,
			concat(provider.FName," ",provider.LName) AS provider,
			appointment.AptStatus,
			appointment.Note,
			DATE_FORMAT(appointment.AptDateTime,"%m-%d-%Y@%H:%i%p") as AptDateTimeMSG,
			DATE_FORMAT(appointment.AptDateTime,"%m-%d-%Y") as AptDateTimeFormatShort,
			DATE_FORMAT(appointment.AptDateTime,"%a %b %d %Y %r") as AptDateTimeFormatLong,
			appointment.AptDateTime,
			appointment.AptDateTimeEnd');
			$this->db->from('appointment');
			$this->db->join('patient','appointment.PatId = patient.Id','inner');
			$this->db->join('provider','appointment.ProvHyg = provider.id','inner');
			$this->db->where('appointment.AptStatus',2);
			$this->db->where('DATE_FORMAT(appointment.AptDateTime,"%Y-%m-%d")',$nuevafecha);
			$data = $this->db->get()->result();
			foreach($data as $cita){
				$this->db->select('procedurecode.ProcCode,
				procedurecode.Descript,
				procedurelog.ProcFee');
				$this->db->from('procedurelog');
				$this->db->join('procedurecode','procedurelog.CodeNum = procedurecode.id','inner');
				$this->db->where('procedurelog.AptNum',$cita->id);
				$procedures = $this->db->get()->result();
				$li = '';
				foreach($procedures as $proce){
					$li = '<li>'.$proce->Descript.'</b></li>';
				}
				$fechaI = date_create($fecha->format('Y-m-d'));
				$fechaF = date_create($cita->AptDateTime);
				$dif = date_diff($fechaI,$fechaF);
				
				$fechaI = date_create($cita->AptDateTime);
				$fechaF = date_create($cita->AptDateTimeEnd);
				$duration = date_diff($fechaI,$fechaF);
				
				$dataPatient['doctor']['name'] = $cita->provider;
				/*$dataPatient['body'] = '<h1>You have a appointment!</h1>
				<p>Hey there, '.$cita->patient.'! Just a quick reminder that you are scheduled for a visit with <b>'.$cita->provider.'</b> on <b>'.$cita->AptDateTimeFormatLong.'</b>.</p>
				<p>If you have any questions or you need to reschedule, don’t hesitate to call us at <b>'.$databusiness->Phone.'</b> or visit our website <a href="'.$databusiness->Weblink.'">'.$databusiness->Weblink.'</a>.</p>
				<p>We’re here '.$databusiness->BusHours.' on Monday - Friday. See you soon!</p>
				<p>Don’t forget:
					<ul>
						<li>Bring your [IMPORTANT-DOCUMENT].</li>
						<li>Try to get here 15 minutes early.</li>
					</ul>
				</p>
				<p>Best regards, SMILE4EVER</p>';*/
				
				$dataPatient['body'] = '<h1>Hi '.$cita->patient.'.</h1>
				<p>Please review the details below for your upcoming appointment.</p>
					<p><b>When:</b><br>'.$cita->AptDateTimeFormatLong.'</p>
					<p><b>Service:</b><br> Crown Zr. Impression</p>
					<p><b>Provider:</b><br> '.$cita->provider.'</p>
					<p><b>Notes:</b><br> '.$cita->Note.'</p>
					<p>If you have any questions or you need to reschedule, don’t hesitate to call us at <b>'.$databusiness->Phone.'</b> or visit our website <a href="'.$databusiness->Weblink.'">'.$databusiness->Weblink.'</a>.</p>
					<p>We’re here '.$databusiness->BusHours.' on Monday - Friday. See you soon!</p>';
				
				
				
				$dataDoctor['body'] = '<h1>You have a appointment!</h1>
				<p>Hey there, '.$cita->provider.'! Just a quick reminder that you are scheduled for a visit with <b>'.$cita->patient.'</b> on <b>'.$cita->AptDateTime.'</b>.</p>
				<p>Best regards, SMILE4EVER</p>';
				
					$config = $this->configcode->emailconfig();
					$this->load->library('email', $config);
					$this->email->set_newline("\r\n");
					$this->email->from('system@cliwork.com','Cliwork');
					$this->email->to('kevin.edua@live.com.mx',);
					$this->email->subject('SMILE4EVER | We’ll see you on '.$cita->AptDateTimeFormatShort.'!');
					$this->email->message($this->load->view('correo_plantillas/recordatorio_citas_patient',$dataPatient,TRUE));
					//$this->email->send();
					
					$config = $this->configcode->emailconfig();
					$this->load->library('email', $config);
					$this->email->set_newline("\r\n");
					$this->email->from('system@cliwork.com','Cliwork');
					$this->email->to('kevin.edua@live.com.mx',);
					$this->email->subject('SMILE4EVER | Reminder-Your appointment on '.$cita->AptDateTimeFormatShort.'!');
					$this->email->message($this->load->view('correo_plantillas/email_template',$dataDoctor,TRUE));
					//$this->email->send();
					//$error = $this->email->print_debugger(array('headers'));
					$this->load->view('correo_plantillas/recordatorio_citas_patient',$dataPatient);
					
					
					$this->load->library('nexmo');
					$this->nexmo->set_format('json');
					$from = '14139981993';
					$to = '16198449107';
					$message = array( 'text' => 'Smile4Ever Appt reminder '.$cita->AptDateTimeMSG);
					$response = $this->nexmo->send_message($from, $to, $message);
				}
				//$this->load->view('correo_plantillas/email_template',$dataDoctor);
		}
	}
	
	public function recordatorio_citas_planeadas(){
		$databusiness = data_bussines();
		
		$fecha = new DateTime();
		$this->db->select('notificaciones_reportes.instrucciones');
		$this->db->from('notificaciones_reportes');
		$this->db->where('notificaciones_reportes.id',9);
		$diasData = $this->db->get()->result();
		$dias = explode(",",$diasData[0]->instrucciones);
		foreach($dias as $dia){
			$nuevafecha = strtotime('+'.$dia.' day',strtotime($fecha->format('Y-m-d')));
			$nuevafecha = date ('Y-m-d',$nuevafecha);
			$this->db->select('appointment.id,
			CONCAT(patient.FName," ",patient.LName) AS patient,
			concat(provider.FName," ",provider.LName) AS provider,
			appointment.AptStatus,
			appointment.Note,
			DATE_FORMAT(appointment.AptDateTime,"%m-%d-%Y") as AptDateTimeFormatShort,
			DATE_FORMAT(appointment.AptDateTime,"%a %b %d %Y %r") as AptDateTimeFormatLong,
			appointment.AptDateTime,
			appointment.AptDateTimeEnd');
			$this->db->from('appointment');
			$this->db->join('patient','appointment.PatId = patient.Id','inner');
			$this->db->join('provider','appointment.ProvHyg = provider.id','inner');
			$this->db->where('appointment.AptStatus',7);
			$this->db->where('DATE_FORMAT(appointment.AptDateTime,"%Y-%m-%d")',$nuevafecha);
			$data = $this->db->get()->result();
			foreach($data as $cita){
				$this->notificaciones->cita_planeada($cita->id);
				$this->db->select('procedurecode.ProcCode,
				procedurecode.Descript,
				procedurelog.ProcFee');
				$this->db->from('procedurelog');
				$this->db->join('procedurecode','procedurelog.CodeNum = procedurecode.id','inner');
				$this->db->where('procedurelog.AptNum',$cita->id);
				$procedures = $this->db->get()->result();
				$li = '';
				foreach($procedures as $proce){
					$li = '<li>'.$proce->Descript.'</b></li>';
				}
				$fechaI = date_create($fecha->format('Y-m-d'));
				$fechaF = date_create($cita->AptDateTime);
				$dif = date_diff($fechaI,$fechaF);
				
				$fechaI = date_create($cita->AptDateTime);
				$fechaF = date_create($cita->AptDateTimeEnd);
				$duration = date_diff($fechaI,$fechaF);
				
				$dataPatient['doctor']['name'] = $cita->provider;				
				
				$dataDoctor['body'] = '<h1>You have appointment planned !</h1>
				<p>Hey there, '.$cita->provider.'!<br>
				The appointment planned with <b>'.$cita->patient.'</b> on <b>'.$cita->AptDateTime.'</b>, has not been confirmed or scheduled.</p>
				<p>Best regards, SMILE4EVER</p>';
				
				$config = $this->configcode->emailconfig();
				$this->load->library('email', $config);
				$this->email->set_newline("\r\n");
				$this->email->from('system@cliwork.com','Cliwork');
				$this->email->to('kevin.edua@live.com.mx',);
				$this->email->subject('SMILE4EVER | Reminder planned appointment on '.$cita->AptDateTimeFormatShort.'!');
				$this->email->message($this->load->view('correo_plantillas/email_template',$dataDoctor,TRUE));
				//$this->email->send();
				//$error = $this->email->print_debugger(array('headers'));
				$this->load->view('correo_plantillas/email_template',$dataDoctor);
					
			}
		}
	}
	
	public function revisar_etapa_control(){
		$this->db->select('etapa.name as etapaNombre,
		patient.LName,
		patient.FName,
		etapa_control.fechaInicio,
		etapa_control.fechaFin,
		ofertas.id,
		ofertas.valor,
		etapa.dias');
		$this->db->from('etapa_control');
		$this->db->join('etapa','etapa_control.etapaid = etapa.id','inner');
		$this->db->join('ofertas','etapa_control.ofertaid = ofertas.id','inner');
		$this->db->join('patient','ofertas.pacienteid = patient.Id','inner');
		$data = $this->db->get()->result();
		
		foreach($data as $etapa){
			$fechaI = date_create($etapa->fechaInicio);
			$fechaF = date_create($etapa->fechaFin);
			$duration = date_diff($fechaI,$fechaF);
			if($duration->days > $etapa->dias){
				
				$this->notificaciones->etapa_expirada($etapa->id,$etapa->etapaNombre,$duration->days);
			}
		}
	}
	
	public function revisar_ofertas(){
		$fecha = new Datetime();
		/*Revisa las actividades de las ofertas y actualiza el estado */
		$this->db->select('ofertas.id as ofertaid,
		ofertas.etapaid as EtapaId,
		ofertas.fecha,
		ofertas.pacienteid,
		ofertas.usuarioid,
		usuario.nombre,
		usuario.apellidos,
		patient.LName,
		patient.FName,
		ofertas.activo');
		$this->db->from('ofertas');
		$this->db->join('patient','ofertas.pacienteid = patient.Id','inner');
		$this->db->join('usuario','ofertas.usuarioid = usuario.id','inner');
		$ofertas = $this->db->get()->result();
		foreach($ofertas as $oferta){
			$fechaOferta = new DateTime($oferta->fecha);
			$dif = date_diff($fecha,$fechaOferta);
			
			$this->db->select('COUNT(historial_actividades.id ) as cantidad');
			$this->db->from('historial_actividades');
			$this->db->where('historial_actividades.ofertaid',$oferta->ofertaid);
			$this->db->where('historial_actividades.etapaid',$oferta->EtapaId);
			$this->db->where('historial_actividades.tipoid',2);
			//$this->db->where('historial_actividades.usuario',$usuarioid);
			$this->db->where('historial_actividades.estadoid',1);
			$pendientes = $this->db->get()->result();
			
			$this->db->select('COUNT(historial_actividades.id ) as cantidad');
			$this->db->from('historial_actividades');
			$this->db->where('historial_actividades.ofertaid',$oferta->ofertaid);
			$this->db->where('historial_actividades.etapaid',$oferta->EtapaId);
			$this->db->where('historial_actividades.tipoid',2);
			//$this->db->where('historial_actividades.usuario',$usuarioid);
			$this->db->where('historial_actividades.estadoid',6);
			$porvencer = $this->db->get()->result();
			
			$this->db->select('COUNT(historial_actividades.id ) as cantidad');
			$this->db->from('historial_actividades');
			$this->db->where('historial_actividades.ofertaid',$oferta->ofertaid);
			$this->db->where('historial_actividades.etapaid',$oferta->EtapaId);
			$this->db->where('historial_actividades.tipoid',2);
			//$this->db->where('historial_actividades.usuario',$usuarioid);
			$this->db->where('historial_actividades.estadoid',3);
			$vencidas = $this->db->get()->result();
			if($vencidas[0]->cantidad  > $porvencer[0]->cantidad){
				$estado = 5; // EXPIRED
			}else{
				if($porvencer[0]->cantidad  > $pendientes[0]->cantidad){
					$estado = 4;//Soon to expire
				}else{
					if($pendientes[0]->cantidad > 0){
						$estado = 1;//Pending
					}else{
						$estado = 6;//Without Activities
						$color = "info";
						$mensaje =
						'<div class="title margin-bottom-0"><a href="#">'.$oferta->nombre.' '.$oferta->apellidos.'</a></div>
						<p>Han pasado <b>'.$dif->days.'</b> dias y la oferta del paciente: <b>'.$oferta->FName.' '.$oferta->LName.'</b> no tiene actividades</p>';
						$this->notificaciones->crear(2,$mensaje,$color,$oferta->pacienteid,$oferta->ofertaid,2);
					}
				}
			}
			$this->db->where('id',$oferta->ofertaid);
			$this->db->update('ofertas',array('estadoid' => $estado));
		}
	}
	
	public function revisar_actividades(){
		$this->db->select('notificaciones_reportes.instrucciones');
		$this->db->from('notificaciones_reportes');
		$this->db->where('notificaciones_reportes.id',3);
		$reporte = $this->db->get()->result();
		
		$this->db->select('historial_actividades.id,
		historial_actividades.fechaInicio,
		historial_actividades.estadoid');
		$this->db->from('historial_actividades');
		$this->db->where('historial_actividades.tipoid','2');
		$this->db->where_in('historial_actividades.estadoid',array(1,6));
		$data = $this->db->get()->result();
		foreach($data as $actividad){
			$fechai = new DateTime($actividad->fechaInicio);
			$fechaActual = new DateTime();
			if($fechaActual >= $fechai){
				/*
					SI LA FECHA ACTUAL ES MAYOR A LA FECHA DE LA ACTIVIDAD
					LA ACTIVIDAD ESTA VENCIDA
				*/
				$this->db->where('id',$actividad->id);
				$this->db->update('historial_actividades',array('estadoid' => 3));
			}else{
				/*
					SI LA FECHA ACTUAL ES MENOR A LA FECHA DE LA ACTIVIDAD
					PUEDER ESTAR POR VENCERSE O NO
				*/
				$diferencia = date_diff($fechaActual,$fechai);
				if($diferencia->days >= $reporte[0]->instrucciones){
					$this->db->where('id',$actividad->id);
					//$this->db->update('historial_actividades',array('estadoid' => 6));
				}
			}
			
		}
		print_r(json_encode($data));
	}

	public function revisar_citas(){
		$this->db->select('configuracion_sistema.instrucciones');
		$this->db->from('configuracion_sistema');
		$this->db->where('configuracion_sistema.id',4);
		$dias = $this->db->get()->result();
		
		$fecha = new DateTime();
		$nuevafecha = strtotime('+'.$dias[0]->instrucciones.' day',strtotime($fecha->format('Y-m-d')));
		$nuevafecha = date('Y-m-d',$nuevafecha);
		echo $nuevafecha;
		
		
		$this->db->select('appointment.id,
		appointment_status.nombre,
		appointment.AptDateTime,
		appointment.AptDateTimeEnd,
		appointment.PatId,
		CONCAT(patient.LName," ",patient.FName) as Patient,
		CONCAT(provider.LName," ",provider.FName) as Doctor,
		appointment.Note');
		$this->db->from('appointment');
		$this->db->join('appointment_status','appointment.AptStatus = appointment_status.id','inner');
		$this->db->join('patient','appointment.PatId = patient.Id','inner');
		$this->db->join('provider','appointment.ProvId = provider.id','inner');
		//$this->db->where('appointment.AptDateTime');
		$citas = $this->db->get()->result();
		foreach($citas as $cita){
			$this->db->select('GROUP_CONCAT(procedurecode.Descript) as services,
			SUM(procedurelog.ProcFee) as Fee');
			$this->db->from('procedurecode');
			$this->db->join('procedurelog','procedurelog.CodeNum = procedurecode.id','inner');
			$this->db->where('procedurelog.AptNum',$cita->id);
			$procedures = $this->db->get()->result();
		
			$datetime1 = date_create($cita->AptDateTime);
			$datetime2 = date_create($cita->AptDateTimeEnd);
			$duration = date_diff($datetime1, $datetime2);
			
			$msg['body'] = "
			<h1>Hello <b>".$cita->Patient."</b></h1>
			<p><b>Date: </b>".$cita->AptDateTime
			."<br><b>Duration: </b>".($duration->h > 0? $duration->h.' Hr': '')." ".($duration->i > 0? $duration->i.' Mn': '')
			."<br><b>Doctor: </b>".$cita->Doctor
			."<br><b>Pay: </b>$ ".number_format($procedures[0]->Fee,2)."</p>
			<p>Lorem ipsum dolor sit amet, venenatis tellus rutrum aliquam lectus. Commodo ut, etiam praesent. Integer dolor non id tempus, interdum et ipsum nibh, sollicitudin cum, vulputate metus sit felis at, lectus volutpat a. Nibh amet ornare orci platea nec, fermentum tortor quis facilisis vel. Dignissim dolor vestibulum etiam vel mi, amet litora lacus orci suscipit. Nunc lobortis vel lectus praesent leo, etiam faucibus non ut urna nunc auctor. Vestibulum venenatis in nulla id mauris, interdum enim aliquam eros. Neque ac ac est viverra sapien in, elit malesuada pretium in aliquam et. Quis dictum, donec pellentesque velit tellus vel, sed morbi ipsum, accumsan aut mauris ut mus, ipsum et. Suspendisse aenean sit, vitae lacus et nullam dictum massa, lorem tellus ac, commodo nullam urna quam. Ornare at suspendisse pellentesque magna, eu et quam, amet leo viverra sed mauris fringilla sagittis, elementum mauris. Vehicula nisl nec posuere, nunc at, aliquam sem aliquam vitae amet, arcu hac eget.</p>";
			
			$config = $this->configcode->emailconfig();
			$this->load->library('email', $config);
			$this->email->clear();
			$this->email->set_crlf( "\r\n" );
			$this->email->set_newline("\r\n");
			$this->email->from('sistema@indagho.com');
			$this->email->to('kevin.edua@live.com.mx');
			/*$this->email->cc($this->post('cc'));
			if(count($bcc) > 0){
				$this->email->bcc($bcc[0]->instrucciones);
			}*/
			$this->email->subject('SMILE4EVER | You have a Appointment');
			$this->email->message($this->load->view('correo_plantillas/email_template',$msg,TRUE));
			if($this->email->send(FALSE)){
				$error = $this->email->print_debugger();
				$nota = '<p><b>To:</b> </p>
				<p><b>CC:</b> </p> 
				<p><b>Subject: </b>You have a Appointment</p>'.$msg['body'];
				$fecha =  new DateTime();
				$nuevaActividad = array(
					'usuario' => 1,
					'fecha' => $fecha->format('Y-m-d'),
					'hora' => $fecha->format('h:i:00'),
					'tipoid' => 4,
					'actividadTipo' => 5,
					'etapaid' => 1,
					'estadoid' => 2,
					'pacienteid' => $cita->PatId,
					'nota' => $nota);
					$error = $this->paciente->agregar_actividad($nuevaActividad);
			}else{
				$error = array('code' => 100, "msg" => "Error email failed to send");
				$error = $this->email->print_debugger();				
			}
		}
		
		/*
			$fecha = new DateTime();
			$notificacion = array(
			'usuarioid'=>3,
			'leida' => 0,
			'tipoid' => 4,
			'color' => 'info',
			'fecha' => $fecha->format('Y-m-d'),
			'pacienteid' => $appt[0]->PatId,
			'cuerpo'=> "You have a new appointment confirmed for <b>".$appt[0]->FName." ".$appt[0]->LName.
			"</b><br>Date: ".$appt[0]->AptDateTime.
			"<br>Duration: ".($duration->h > 0? $duration->h.' Hr': '')
			.($duration->i > 0? $duration->i.' Mn': ''));
			$this->db->insert('notificaciones',$notificacion);*/
	}


	public function pacientes_deuda(){
		$this->db->select('treatplan.id,
		CONCAT(patient.LName," ",patient.FName) as patient,
		treatplan.Heading,
		treatplan.PatNum,
		treatplan.Complete,
		treatplan.TPStatus');
		$this->db->from('treatplan');
		$this->db->join('patient','treatplan.PatNum = patient.Id','inner');
		$this->db->where('treatplan.Complete',0);
		$this->db->where('treatplan.TPStatus',1);
		$treatplan = $this->db->get()->result();
		
		foreach($treatplan as $tpitem){
			$data = array(
			'allSubTotal' => 0,
			'allTotal' => 0,
			'allPayments' => 0,
			'alladitions' => 0,
			'alldiscounts' => 0,
			'allDue' => 0,);
			
			$this->db->select('pagos_plan_encabezado.id,
			pagos_plan_encabezado.comision,
			pagos_plan_encabezado.deuda');
			$this->db->from('pagos_plan_encabezado');
			$this->db->where('pagos_plan_encabezado.activo',1);
			$this->db->where('pagos_plan_encabezado.pacienteid',$tpitem->PatNum);
			$planPagos = $this->db->get()->result();
			
			$this->db->select('ajustes.monto,definition.ItemValue');
			$this->db->from('ajustes');
			$this->db->join('definition','ajustes.tipo = definition.DefNum','inner');
			$this->db->join('treatplan','ajustes.treatplanid = treatplan.id','inner');
			$this->db->where('treatplan.TPStatus',1);
			$this->db->where('treatplan.Complete',0);
			$this->db->where('treatplan.PatNum',$tpitem->PatNum);
			$ajustes = $this->db->get()->result();
			
			foreach($ajustes as $item){
				if($item->ItemValue == "+"){
					$data['alladitions'] = (isset($item->monto) == true ? $item->monto : 0);
				}
				if($item->ItemValue == "-"){
					$data['alldiscounts'] = (isset($item->monto) == true ? $item->monto : 0);
				}
			}
			
			$this->db->select('SUM(procedurelog.ProcFee) as amount');
			$this->db->from('procedurelog');
			$this->db->join('treatplanattach','treatplanattach.ProcNum = procedurelog.Id','inner');
			$this->db->join('treatplan','treatplanattach.TreatPlanNum = treatplan.id','inner');
			$this->db->where('treatplan.TPStatus',1);
			$this->db->where('treatplan.Complete',0);
			$this->db->where('procedurelog.ProcStatus',2);
			$this->db->where('procedurelog.PatNum',$tpitem->PatNum);
			$allSubTotal = $this->db->get()->result();
			$allSubTotal = $allSubTotal[0]->amount;
			$data['allSubTotal'] = number_format($allSubTotal,2);
			
			$this->db->select('pagos.monto');
			$this->db->from('treatplan_pagos');
			$this->db->join('pagos','treatplan_pagos.pagoid = pagos.id','inner');
			$this->db->join('treatplan','treatplan_pagos.treatplanid = treatplan.id','inner');
			$this->db->where('treatplan.TPStatus',1);
			$this->db->where('treatplan.Complete',0);
			$this->db->where('treatplan.PatNum',$tpitem->PatNum);
			$this->db->group_by('pagos.id');
			$pays = $this->db->get()->result();
			$totalPayment = 0;
			foreach($pays as $pay){
				$totalPayment += $pay->monto;
			}
			
			
			$comision = (count($planPagos) > 0 ? ($planPagos[0]->comision * $planPagos[0]->deuda)/100 : 0);
			$allTotal = $allSubTotal + $data['alladitions'] - $data['alldiscounts'] + $comision;
			$allDue = $allTotal - $totalPayment;
			$tpitem->due = $allDue;
		}
		
		$data['treatplan'] = $treatplan;
		$data['databusiness'] = data_bussines();
		$this->load->view('pdf/reporte_pacientes_deuda',$data);
	}

	public function reporte_produccion(){
		$data['databusiness'] = data_bussines();
		$date = $this->input->get('date');
		$date = explode('-',$date);
		
		
		$fechaInicio = new DateTime($date[0]);
		$fechaFin = new DateTime($date[1]);
		$fechaInicio = $fechaInicio->format('Y-m-d');
		$fechaFin = $fechaFin->format('Y-m-d');
		
		$this->db->select('provider.id,
		CONCAT(provider.LName," ",provider.FName) as name');
		$this->db->from('provider');
		$providers = $this->db->get()->result();
		foreach($providers  as $provider){
			$this->db->select('Sum(procedurelog.ProcFee) as total');
			$this->db->from('procedurelog');
			$this->db->where('procedurelog.ProvNum',$provider->id);
			$this->db->where('procedurelog.ProcStatus',2);
			$this->db->where('procedurelog.DateComplete >=',$fechaInicio);
			$this->db->where('procedurelog.DateComplete <=',$fechaFin);
			$provider->production = $this->db->get()->result();
			
			$this->db->select('SUM(pagos.monto) as total');
			$this->db->from('pagos');
			$this->db->where('pagos.doctor',$provider->id);
			$this->db->where('pagos.fecha >=',$fechaInicio);
			$this->db->where('pagos.fecha <=',$fechaFin);
			$provider->income = $this->db->get()->result();
		}		
		$data['fechai'] = $fechaInicio;
		$data['fechaf'] = $fechaFin;
		$data['providers'] = $providers;
		$this->load->view('pdf/reporte_produccion',$data);
	}

	public function reporte_anual(){
		$data['databusiness'] = data_bussines();
		$anio = $this->input->get('date');
		
		$meses = array('January','February','March','April','May','June','July','August','September','October','November','December');
		
		$datos = array();
		foreach($meses  as $i => $mes){
			$item['name'] = $mes;
			$this->db->select('Sum(procedurelog.ProcFee) as total');
			$this->db->from('procedurelog');
			$this->db->where('procedurelog.ProcStatus',2);
			$this->db->where('DATE_FORMAT(procedurelog.DateComplete,"%Y-%m")',$anio.'-'.($i+1));
			$item['production'] = $this->db->get()->result();
			
			
			$this->db->select('SUM(pagos.monto) as total');
			$this->db->from('pagos');
			$this->db->where('DATE_FORMAT(pagos.fecha ,"%Y-%m") =',$anio.'-'.($i+1));
			$item['income'] = $this->db->get()->result();			
			
			array_push($datos,$item);
		}		
		$data['anio'] = $anio;
		$data['meses'] = $datos;
		$this->load->view('pdf/reporte_anual',$data);
	}
	
	public function reporte_pagos(){
		$data['databusiness'] = data_bussines();
		$date = $this->input->get('date');
		$date = explode('-',$date);
		$fechaInicio = new DateTime($date[0]);
		$fechaFin = new DateTime($date[1]);
		$fechaInicio = $fechaInicio->format('Y-m-d');
		$fechaFin = $fechaFin->format('Y-m-d');
		
		$this->db->select('definition.DefNum,
		definition.ItemName');
		$this->db->from('definition');
		$this->db->where('definition.Category',10);
		$this->db->where('definition.IsHidden',0);
		$types = $this->db->get()->result();
		foreach($types as  $type){
			$this->db->select('pagos.fecha,
			CONCAT(provider.FName," ",provider.LName) AS provider,
			CONCAT(patient.FName," ",patient.LName) AS patient,
			pagos.monto,
			pagos.tipo,
			definition.ItemName');
			$this->db->from('pagos');
			$this->db->join('provider','pagos.doctor = provider.id','inner');
			$this->db->join('patient','pagos.patientid = patient.Id','inner');
			$this->db->join('definition','pagos.tipo = definition.DefNum','inner');
			$this->db->where('pagos.tipo',$type->DefNum);
			$this->db->where('pagos.fecha >=',$fechaInicio);
			$this->db->where('pagos.fecha <=',$fechaFin);
			$type->pagos = $this->db->get()->result();
		}
		$data['fechai'] = $fechaInicio;
		$data['fechaf'] = $fechaFin;
		$data['types'] = $types;
		$this->load->view('pdf/report_daily_payments',$data);
	}
	
	public function reporte_procedures(){
		$data['databusiness'] = data_bussines();
		$date = $this->input->get('date');
		$date = explode('-',$date);
		$fechaInicio = new DateTime($date[0]);
		$fechaFin = new DateTime($date[1]);
		$fechaInicio = $fechaInicio->format('Y-m-d');
		$fechaFin = $fechaFin->format('Y-m-d');
		
		$this->db->select('CONCAT(patient.LName," ",patient.FName) as patient,
		CONCAT(provider.LName," ",provider.FName) as provider,
		procedurecode.ProcCode,
		procedurecode.Descript,
		procedurelog.ProcFee,
		procedurelog.ToothNum,
		procedurelog.DateComplete');
		$this->db->from('procedurecode');
		$this->db->join('procedurelog','procedurelog.CodeNum = procedurecode.id','inner');
		$this->db->join('patient','patient.Id = procedurelog.PatNum','inner');
		$this->db->join('provider','procedurelog.ProvNum = provider.id','inner');
		$this->db->where('procedurelog.DateComplete >=',$fechaInicio);
		$this->db->where('procedurelog.DateComplete <=',$fechaFin);
		$data['procedures'] = $this->db->get()->result();
		$data['fechai'] = $fechaInicio;
		$data['fechaf'] = $fechaFin;
		$this->load->view('pdf/report_daily_procedures',$data);
	}

	public function reporte_procedures_production(){
		$data['databusiness'] = data_bussines();
		$limit = $this->input->get('limit');
		$date = $this->input->get('date');
		$date = explode('-',$date);
		$fechaInicio = new DateTime($date[0]);
		$fechaFin = new DateTime($date[1]);
		$fechaInicio = $fechaInicio->format('Y-m-d');
		$fechaFin = $fechaFin->format('Y-m-d');
		
		
		$this->db->select('procedurecode.ProcCode,
		procedurecode.Descript,
		Count(procedurelog.Id) as qty,
		Sum(procedurelog.ProcFee) as total,
		procedurelog.ProcStatus,
		procedurecode.id,
		procedurelog.DateComplete');
		$this->db->from('procedurecode');
		$this->db->join('procedurelog','procedurelog.CodeNum = procedurecode.id','inner');
		$this->db->where('procedurelog.DateComplete >=',$fechaInicio);
		$this->db->where('procedurelog.DateComplete <=',$fechaFin);
		$this->db->order_by('total','DESC');
		$this->db->limit($limit);
		$procedures = $this->db->get()->result();
		foreach($procedures as $procedure){
			$procedure->totalFormato = number_format($procedure->total,2);
			$procedure->qtyFormato = number_format($procedure->qty,0);
		}
		
		$this->db->select('procedurecode.ProcCode,
		procedurecode.Descript,
		Count(procedurelog.Id) as qty,
		Sum(procedurelog.ProcFee) as total,
		procedurelog.ProcStatus,
		procedurecode.id,
		procedurelog.DateComplete');
		$this->db->from('procedurecode');
		$this->db->join('procedurelog','procedurelog.CodeNum = procedurecode.id','LEFT');
		$this->db->where('procedurelog.DateComplete >=',$fechaInicio);
		$this->db->where('procedurelog.DateComplete <=',$fechaFin);
		$this->db->order_by('total','ASC');
		$this->db->limit($limit);
		$leastProcedures = $this->db->get()->result();
		foreach($leastProcedures as $leastProcedure){
			$leastProcedure->totalFormato = number_format($leastProcedure->total,2);
			$leastProcedure->qtyFormato = number_format($leastProcedure->qty,0);
		}
		
		$data['procedures'] = $procedures;
		$data['leastProcedures'] = $leastProcedures;
		$data['fechai'] = $fechaInicio;
		$data['fechaf'] = $fechaFin;
		$this->load->view('pdf/report_production_procedures',$data);
	}
	
	public function envelope_template(){
		$data['databusiness'] = data_bussines();
		$data['logo'] = $this->input->get('logo');
		$patients = $this->input->get('patients');
		
		$this->db->select('patient.LName,
		patient.FName,
		patient.Address,
		patient.City,
		patient.Zip');
		$this->db->from('patient');
		$this->db->where_in('patient.Id',$patients);
		$patients = $this->db->get()->result();
		
		$data['patients'] = $patients;
		$template = $this->load->view('pdf/envelope_template',$data,true);
		$this->load->view('pdf/envelope_template');
		$this->load->library('pdfgenerator');
		$filename = 'report production procedures';
		$this->pdfgenerator->generate($template, $filename, true, 'commercial #10 envelope', 'portrait');
	}

	public function recordatorio_deals(){
		$tipo = 1;
		
		$this->db->select('ofertas.id,
		ofertas.fecha,
		ofertas_tratamientos.tratamientoid,
		ofertas.vendida');
		$this->db->from('ofertas');
		$this->db->join('ofertas_tratamientos','ofertas_tratamientos.ofertaid = ofertas.id','inner');
		$ofertas = $this->db->get()->result();
		foreach($ofertas as $oferta){
			
			$this->db->select('ofertas_recordatorios_procedure.id,
			ofertas_recordatorios_procedure.procedureid,
			ofertas_recordatorios_procedure.mensaje,
			ofertas_recordatorios_procedure.archivo,
			ofertas_recordatorios_procedure.tipo,
			ofertas_recordatorios_procedure.dias');
			$this->db->from('ofertas_recordatorios_procedure');
			$this->db->where('ofertas_recordatorios_procedure.procedureid',$oferta->tratamientoid);
			$recordatorios = $this->db->get()->result();
			foreach($recordatorios as $recordatorio){
				if($tipo == 1){
					$data['body'] = $recordatorio->mensaje;
					$config = $this->configcode->emailconfig();
					$this->load->library('email', $config);
					$this->email->set_newline("\r\n");
					$this->email->from('system@cliwork.com','Cliwork');
					$this->email->to('kevin.edua@live.com.mx',);
					$this->email->subject('SMILE4EVER | Title');
					//$this->email->message($this->load->view('correo_plantillas/email_simple',null,TRUE));
					$this->load->view('correo_plantillas/email_template',$data);
				}else{
					$this->load->library('nexmo');
					$this->nexmo->set_format('json');
					$from = '14139981993';
					$to = '16198449107';
					$message = array( 'text' => 'Smile4Ever allon4 $400');
					$response = $this->nexmo->send_message($from, $to, $message);
					print_r($response);
				}
			}
		}
	}
}
?>