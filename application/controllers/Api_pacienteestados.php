<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Api_pacienteestados extends REST_Controller {
	function __construct(){
        parent::__construct();
		$headers = $this->input->request_headers();
		$dbConfig = json_decode($this->encryption->decrypt($headers['dbconfig']));
		$this->load->database(db_config($dbConfig));
		
		$this->load->model('PacienteEstado_model','estado');
	}
	
	public function listapacientes_get(){
		$activo = $this->get('activo');
		$vendido = $this->get('vendido');
		$type = $this->get('usertype');
		$user = $this->get('user');
		$data['data'] = $this->estado->listapacientes_ofertas($activo,$vendido,$type,$user);
		$this->response($data);
	}
	
	public function lista_get(){
		$activo = $this->get('activo');
		$data['data'] = $this->estado->lista($activo);
		$this->response($data);
	}
}