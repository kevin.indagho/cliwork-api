<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Api_reportes extends REST_Controller {
	function __construct(){
        parent::__construct();
		$headers = $this->input->request_headers();
		$dbConfig = json_decode($this->encryption->decrypt($headers['dbconfig']));
		$this->load->database(db_config($dbConfig));
		
		$this->load->model('Reportes_model','reporte');
	}
	
	public function actvencidas_get(){
		$data['data'] = $this->reporte->actividades_vencidas();
		$this->response($data);
	}

	#region Ventas
		public function ventas_get(){
			$data['data'] = $this->reporte->ventas_activas();
			$this->response($data);
		}
		
		public function ventasinformes_get(){
			$data['data'] = $this->reporte->ventas_informes();
			$this->response($data);
		}
	#endregion
}