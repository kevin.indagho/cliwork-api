<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Api_usuario extends REST_Controller {
	
	function __construct(){
		parent::__construct();
		/*
		* Leer el encabezado de las peticiones HTTP
		* y obtener los datos de configuracion para la conexion a la base de datos
		*/
		$headers = $this->input->request_headers();
		$dbConfig = json_decode($this->encryption->decrypt($headers['dbconfig']));
		$this->load->database(db_config($dbConfig));
		
		$this->load->model('Usuario_model','usuario');
	}
	
	public function agregar_post(){
		/* Agregar un nuevo usuario */
		$nuevoUsuario = array(
			'nombre' => $this->post('nombre'),
			'apellidos' => $this->post('apellidos'),
			'activo' => $this->post('activo'),
			'tipoid' => $this->post('tipo'),
			'correo' => $this->post('correo'),
			'contrasena' => MD5($this->post('contrasena')));
		$error = $this->usuario->agregar($nuevoUsuario);
        $this->response($error);
	}
	
	public function editar_post(){
		/* Editar un usuario */
		$usuario = array(
			'nombre' => $this->post('nombre'),
			'apellidos' => $this->post('apellidos'),
			'tipoid' => $this->post('tipo'),
			'correo' => $this->post('correo'),
			'activo' => $this->post('activo'));
			
		$usuarioid = $this->post('id');
		$error = $this->usuario->editar($usuario,$usuarioid);
        $this->response($error);
	}
	
	public function lista_get(){
		/* Lista de usuaros; activo define el tipo de usuario que se mostrara*/
		$activo = $this->get('activo');
		$tipo = $this->get('tipo');
		$data['data'] = $this->usuario->lista($activo,$tipo);
		$this->response($data);
    }
	
	public function agenda_get(){
		$userid = $this->get('id');
		$data = $this->usuario->agenda($userid);
		$this->response($data);
	}
	
	public function notificaciones_get(){
		$usuarioid = $this->get('id');
		$new = $this->get('nueva');
		$data = $this->usuario->notificaciones($usuarioid,$new);
		$this->response($data);
	}
	
	public function buscador_get(){
		$nombre = $this->get('name');
		$tipo = $this->get('tipo');
		$data['data'] = $this->usuario->buscador($nombre,$tipo);
		$this->response($data);
	}
	
	#region Doctores
		public function users_get(){
			$type = $this->get('type');
			$data['data'] = $this->data->usuarios($type);
			$this->response($data);
		}
	#endregion
	
	/* Inicio de sesion */
	public function login_post(){
		$correo = $this->post('correo');
		$contrasena = MD5($this->post('contrasena'));
		$data = $this->usuario->autenticar($correo,$contrasena);
		$this->response($data);
	}
	
	/*permisos*/
	public function permisos_get(){
		$tipo = $this->get('tipo');
		$data['data'] = $this->usuario->permisos($tipo);
		$this->response($data);
	}
	
	public function permisoedit_post(){
		$permisoid = $this->post('id');
		$permitido = $this->post('permitido');
		$tipo = $this->post('tipo');
		$error = $this->usuario->permiso_editar($permisoid,$tipo,$permitido);
		$this->response($error);
	}
}