<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Api_contact extends REST_Controller {
	function __construct(){
        parent::__construct();
		$headers = $this->input->request_headers();
		$dbConfig = json_decode($this->encryption->decrypt($headers['dbconfig']));
        $this->load->database(db_config($dbConfig));
        $this->load->model("Contact_model","contact");
    }

    public function contact_post(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('lastname', 'LastName', 'required');
        $this->form_validation->set_rules('phone', 'Phone', 'max_length[10]|number');
        $this->form_validation->set_rules('email', 'Mail', 'valid_email');
        $this->form_validation->set_rules('comment', 'Comment', '');

        if ($this->form_validation->run() == FALSE) {
                $error =  array("msg" =>"No se pudo completar la peticion.","error"=>validation_errors());
        } else {
            $fecha = new DateTime();
            $contact = array(
                "name" => $this->post("name"),
                "lastname" => $this->post("lastname"),
                "email" => $this->post("email"),
                "phone" => $this->post("phone"),
                "comment"  => $this->post("comment"),
                "date" => $fecha->format("Y-m-d H:i:s")
            );
            $error = $this->contact->create($contact);
        }
        
        $this->response($error);
    }

    public function contacts_get(){
        $response['data'] = $this->contact->list_table();  
        $this->response($response);
    }

    public function contactdel_post(){
        $error = $this->contact->delete($this->post("id"));  
        $this->response($error);
    }
}