<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Api_noti extends REST_Controller {
	function __construct(){
        parent::__construct();
		$headers = $this->input->request_headers();
		$dbConfig = json_decode($this->encryption->decrypt($headers['dbconfig']));
		$this->load->database(db_config($dbConfig));
	}
	
	public function leida_post(){
		$notiid = $this->post('id');
		$error = $this->notificaciones->leida($notiid);
		$this->response($error);
	}
	
	public function prueba_get(){
		$this->load->view('correo_plantillas/email_simple');
	}
}
?>