<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Api_data extends REST_Controller {
	function __construct(){
        parent::__construct();
		$headers = $this->input->request_headers();
		$dbConfig = json_decode($this->encryption->decrypt($headers['dbconfig']));
		$this->load->database(db_config($dbConfig));
		
		$this->load->model('Paciente_model','paciente');
		$this->load->model('Data_model','data');
	}
	
	#region Pais
		public function pais_get(){
			$activo = $this->get('activo');
			$data['data'] = $this->data->pais_lista($activo);
			$this->response($data);
		}
		
		public function paisestado_get(){
			$pais = $this->get('pais');
			$data['data'] = $this->data->pais_estados_lista($pais);
			$this->response($data);
		}
	#endregion
	
	#region Assisten
		public function assistant_get(){
			$data['data'] = $this->data->asistentes();
			$this->response($data);
		}
	#endregion
	
	#region patient
		public function patient_get(){
			$text = $this->get('id');
			$data['data'] = $this->data->patient_read($text);
			$this->response($data);
		}
		
		public function patsearch_get(){
			$text = $this->get('name');
			$data['data'] = $this->data->patsearch($text);
			$this->response($data);
		}		
	#endregion
	
	#region Configuracion
		public function notireporte_put(){
			$reporteid = $this->put('pk');
			$instrucciones = $this->put('value');
			$data['error'] = $this->data->reporte_editar($reporteid,$instrucciones);
			$this->response($data);
		}
		
		public function notireporte_get(){
			$reporteid = $this->get('id');
			$data['data'] = $this->data->reporte_usuarios($reporteid);
			$this->response($data);
		}
		
		public function confignoti_get(){
			$data['data'] =  $this->data->noti_config_list();
			$this->response($data);
		}
		
		public function confignoti_put(){
			$id = $this->put('configid');
			$usertype = $this->put('type');
			$config = array('instrucciones' => $this->put('configuration'));
			$error = $this->data->confignoti_edit($id,$config,$usertype);
			$this->response($error);
		}
		
		public function config_get(){
			$configid = $this->get('id');
			$data['data'] = $this->data->configuracion($configid);
			$this->response($data);
		}
		
		public function config_patch(){
			$configid = $this->patch('configid');
			if($this->patch('starthour') != null && $this->patch('finishhour') != null){ 
				$config['instrucciones'] = $this->patch('starthour')."-".$this->patch('finishhour');
			}
			if($this->patch('instrucciones') != null){ 
				$config['instrucciones'] = $this->patch('instrucciones');
			}
			
			$data['data'] = $this->data->configuracion_edit($configid,$config);
			$this->response($data);
		}
	#endregion
	
	#region Procedures
		public function procedures_get(){
			$data['data'] = $this->data->procedures_lista();
			$this->response($data);
		}
		
		public function procedure_post(){
			$procedure = array(
				'ProcCode' => $this->post('code'),
				'AbbrDesc' => $this->post('abbr'),
				'Descript' => $this->post('descript'),
				'Duration' => $this->post('duration'),
				'isSurf' => $this->put('issurf'),
				'isCrown' => $this->put('iscrown'),
				'isImplant' => $this->put('isimplant'),
				'Active' => $this->post('status'));
			$error = $this->data->procedure_add($procedure);
			$this->response($error);
		}
		
		public function procedure_put(){
			$id =  $this->put('id');
			$procedure = array(
				'ProcCode' => $this->put('code'),
				'AbbrDesc' => $this->put('abbr'),
				'Descript' => $this->put('descript'),
				'Duration' => $this->put('duration'),
				'isQuarter' => $this->put('isquarter'),
				'toothRange' => $this->put('toothrange'),
				'isSurf' => $this->put('issurf'),
				'isCrown' => $this->put('iscrown'),
				'isImplant' => $this->put('isimplant'),
				'Active' => $this->put('status'));
			$error = $this->data->procedure_edit($id,$procedure);
			$this->response($error);
		}
		
		public function procedure_delete(){
			$id =  $this->delete('id');
			$error = $this->data->procedure_delete($id);
			$this->response($error);
		}			
		
		public function proceduressearch_get(){
			$by = $this->get('by');
			$text = $this->get('name');
			$limit = $this->get('limit');
			$data['data'] = $this->data->procedures_search($limit,$by,$text);
			$this->response($data);
		}
		
		public function procereasonvisit_get(){
			$data['data'] = $this->data->procedures_reasonvisit();
			$this->response($data);
		}
	
		public function procereminders_get(){
			$data['data'] = $this->data->procedure_reminders();
			$this->response($data);
		}
		
		public function procereminders_post(){
			$data = array(
				'procedureid' => $this->post('procedureid'),
				'tipo' => $this->post('type'),
				'dias' => $this->post('days'),
				'mensaje' => $this->post('message'));
			$error = $this->data->procedure_reminders_add($data);
			$this->response($error);
		}
		
		public function procereminders_put(){
			$id = $this->put('id');
			$procedureid = $this->put('procedureid');
			$data = array(
				'procedureid' => $procedureid,
				'tipo' => $this->put('type'),
				'dias' => $this->put('days'),
				'mensaje' => $this->put('message'));
			if($procedureid == ''){
				unset($data['procedureid']);
			}
			$error = $this->data->procedure_reminders_edit($id,$data);
			$this->response($error);
		}
		
		public function procereminders_delete(){
			$id = $this->delete('id');
			$error = $this->data->procedure_reminders_delete($id);
			$this->response($error);
		}
	#endregion
	
	#region Providers
		public function providers_get(){
			$IsHidden = $this->get('IsHidden');
			$specialty = $this->get('specialty');
			$data['data'] = $this->data->providers_lista($IsHidden,$specialty);
			$this->response($data);
		}
		
		public function providers_post(){
			$color = $this->post('color');
			$start = date("H:i", strtotime($this->post('hourstart')));
			$end = date("H:i", strtotime($this->post('hourend')));
			
			$horario = $start.'-'.$end;
			$provider = array(
				'Abbr' => $this->post('Abbr'),
				'IsHidden' => 0,
				'FName' => $this->post('fname'),
				'LName' => $this->post('lname'),
				'Specialty' => $this->post('specialty'),
				'horario' => $horario,
				'Color' => $color);
			$data['data'] = $this->data->provider_add($provider);
			$this->response($data);
		}
		
		public function providers_put(){
			$id = $this->put('id');
			$color = $this->put('color');
			$start = date("H:i", strtotime($this->put('hourstart')));
			$end = date("H:i", strtotime($this->put('hourend')));
			
			$horario = $start.'-'.$end;
			$provider = array(
				'Abbr' => $this->put('Abbr'),
				'FName' => $this->put('fname'),
				'LName' => $this->put('lname'),
				'Specialty' => $this->put('specialty'),
				'horario' => $horario,
				'Color' => $color);
			$data['data'] = $this->data->provider_edit($id,$provider);
			$this->response($data);
		}
		
		public function providers_delete(){
			$id = $this->delete('id');
			$provider = array('IsHidden' => 1);
			$data['data'] = $this->data->provider_delete($id,$provider);
			$this->response($data);
		}
	#endregion
	
	#region Citas(Appointment) Status
		public function appstatus_get(){
			$activo = $this->get('activo');
			$data['data'] = $this->data->status_cita_lista($activo);
			$this->response($data);
		}
	#endregion
	
	#region Etapas de las ofertas
		public function etapas_get(){
			$activo = $this->get('activo');
			$data['data'] = $this->data->etapas($activo);
			$this->response($data);
		}
		
		public function etapas_post(){

			$etapa = array('name' => $this->post('name'),'dias' => $this->post('days'),'activo' => $this->post('status'));		
			$data['data'] = $this->data->etapas_add($etapa);
			$this->response($data);
		}
		
		public function etapas_put(){
			$id = $this->put('id');
			$etapa = array('name' => $this->put('name'),'dias' => $this->put('days'),'activo' => $this->put('status'));
			
			$data['data'] = $this->data->etapas_editar($id,$etapa);
			$this->response($data);
		}

	#endregion

	#region Archivos de Templates
		public function archivostipo_get(){ //Tipos de archivos
			$data['data'] = $this->data->archivostipo_list();
			$this->response($data);
		}
		
		public function archivos_get(){
			$tipo = $this->get("tipo");
			$activo = $this->get("activo");
			$delete = $this->get("delete");
			$data['data'] = $this->data->archivos("none",$activo,$delete);
			$this->response($data);
		}
		
		public function archivos_post(){
			$type = $this->post("type");
			$typeData = $this->data->archivostipo_detalle($type);
			
			$config['upload_path'] = './files/'.$typeData[0]->carpeta.'/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['encrypt_name'] = true;
			
			/*$config['max_size']             = 100;
			$config['max_width']            = 1024;
			$config['max_height']           = 768;*/
			
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload('file')){
				$error = array('msg' => $this->upload->display_errors());
			}else{
				$archivo = $this->upload->data();
				$nuevoArchivo = array(
					'tipo'=> $type,
					'activo'=> $this->post('status'),
					'descripcion'=> $this->post('description'),
					'fisico'=> $archivo['file_name'],
					'nombre'=> $archivo['client_name']);
					$error = $this->data->archivos_add($nuevoArchivo);
			}
			$this->response($error);
		}
	
		public function archivos_put(){
			$id = $this->put('id');
			$archivo = array(
			'tipo'=> $this->put('type'),
			'activo'=> $this->put('status'),
			'descripcion'=> $this->put('description'));
			$error = $this->data->archivos_update($id,$archivo);
			$this->response($error);
			
		}

		public function archivos_delete(){
			$id = $this->delete('id');
			$archivo = array( 'delete' => 1 );
			$error = $this->data->archivos_delete($id,$archivo);
			$this->response($error);
			
		}
	#endregion

	#region ProButtonsQuick
		public function procbtn_get(){
			$data['data'] = $this->data->proce_botones();
			$this->response($data);
		}
		
		public function procbtncate_get(){
			$data['data'] = $this->data->proce_botones_category();
			$this->response($data);
		}
		
		public function procbtnquick_get(){
			$data['data'] = $this->data->proce_botones_list();
			$this->response($data);
		}
		
		public function procbtnquick_put(){
			$id = $this->put('id');
			$procedure = $this->put('procedurenew');
			$newBtn = array(
			'Description' => $this->put('descript'),
			'CodeValue' => $procedure,
			'Surf' => $this->put('surf'),
			'active' => $this->put('status'),
			'IsLabel' => 0);
			if($procedure == ''){ unset($newBtn['CodeValue']); }
			$error = $this->data->proce_botones_edit($id,$newBtn);
			$this->response($error);
		}
		
		public function procbtnquick_post(){
			$procedure = $this->post('procedurenew');
			$newBtn = array(
			'categoryid' => $this->post('category'),
			'Description' => $this->post('descript'),
			'CodeValue' => $procedure,
			'Surf' => $this->post('surf'),
			'active' => $this->post('status'),
			'IsLabel' => 0);
			$error = $this->data->proce_botones_add($newBtn);
			$this->response($error);
		}
	#endregion
	
	#region Documentos
		public function documentslist_get(){
			$patid = $this->get('patient');
			$data['data'] = $this->data->document_list($patid);
			$this->response($data);
		}
	#endregion

	#region Definiciones
	public function definitions_get(){
		$type = $this->get('type');
		$data['data'] = $this->data->definition_by_type($type);
		$this->response($data);
	}
	#endregion

	#region Cuestionarios
		public function cuesinfo_get(){
			$data = $this->data->cuestionario_informes();
			$this->response($data);
		}
		
		public function precedure_preguntas_get(){
			$data['data'] = $this->data->proce_preguntas_lista();
			$this->response($data);
		}
		
		public function precedure_preguntas_delete(){
			$id = $this->delete('id');
			$data['data'] = $this->data->proce_pregunta_delete($id);
			$this->response($data);
		}
		
		public function precedure_preguntas_post(){
			$newQuestion = array(
				'procedureid' => $this->post('procedure'),
				'nombre' => $this->post('name'),
				'descripcion' => $this->post('description'),
				'tipo' =>  $this->post('type'),
				'activo' =>  $this->post('active'),
			);
			$error = $this->data->proce_pregunta_agregar($newQuestion);
			$this->response($error);
		}
		
		public function precedure_preguntas_put(){
			$id = $this->put('id');
			$Question = array(
				'nombre' => $this->put('name'),
				'descripcion' => $this->put('description'),
				'tipo' =>  $this->put('type'),
				'activo' =>  $this->put('active'),
			);
			$error = $this->data->proce_pregunta_editar($id,$Question);
			$this->response($error);
		}
		
		public function propre_all_get(){
			$this->load->helper('form');
			$id = $this->get('id');
			$data['data'] = $this->data->proce_preguntas($id);
			$this->response($data);
		}
		
		public function cuestionario_post(){
			$paciente = $this->post('fname')." ".$this->post('lname');
			$nuevoPaciente = array(
			'FName' => $this->post('fname'),
			'LName' => $this->post('lname'),
			'Email' => $this->post('email'),
			'HmPhone' => $this->post('phone'),
			'WirelessPhone' => $this->post('cell'),
			'EtapaId' => 1);
			$error = $this->paciente->agregar($nuevoPaciente);
			$pacienteid = $error['id'];
			if($error['code'] == 0){
				$fecha = new DateTime();
				$preguntas = $this->post('preguntas');
				$procedureid = $this->post('procedure');
				$encabezado = array(
				'patientid' => $error['id'],
				'fecha' => $fecha->format('Y-m-d'),
				'estado' => 0,
				'procedureid' => $procedureid);
				$error = $this->data->cuestionario_add($encabezado,$preguntas);
				if($error['code'] == 0){
					$this->notificaciones->cuestionario_nuevo($error['id'],$pacienteid,$paciente,$procedureid);
				}
				
			}
			//$this->data->correo_cuestionario_complete();
			$this->response($error);
		}
				
		public function cuestionario_get(){
			$this->load->helper('form');
			$id = $this->get('id');
			$data['data'] = $this->data->cuestionario_detalle($id);
			$this->response($data);
		}
		
		public function cuestionarionotas_get(){
			$id = $this->get('id');
			$data['data'] = $this->data->cuestionario_notas($id);
			$this->response($data);
		}
		
		public function cuestionarios_get($filter,$user,$tipo){
			$data['data'] = $this->data->cuestionario_list($filter,$user,$tipo);
			$this->response($data);
		}
		
		public function cuestionario_patch(){
			$editcuest = array();
			$id = $this->patch('id');
			if($this->patch('doctor') != null){ $editcuest['doctor'] = $this->patch('doctor'); }
			if($this->patch('usuario') != null){ $editcuest['usuario'] = $this->patch('usuario'); }
			$data = $this->data->cuestionario_edit($id,$editcuest);
		}
		
		public function cuestionario_put(){
			$fecha = new DateTime();
			$status = $this->put('status');
			$note = $this->put('notes');
			$id = $this->put('id');
			$pacienteId = $this->put('patientid');
			$user = $this->put('userid');
			
			$this->data->cuestionario_edit($id,array('estado' => $status));
			
			$nuevaActividad = array(
			'usuario' => $user,
			'fecha' => $fecha->format('Y-m-d'),
			'hora' => $fecha->format('h:i:00'),
			'cuestionarioid' => $id,
			'tipoid' => 1,
			'etapaid' => 1,
			'estadoid' => 2,
			'pacienteid' => $pacienteId,
			'nota' => $note);
			$error = $this->paciente->agregar_actividad($nuevaActividad);
			if($error['code'] == 0){
				if($note != ""){
					$this->notificaciones->cuestionario_nota_nueva($id,$pacienteId,$user);
				}
				if($status != 0){
					$this->notificaciones->cuestionario_status($id,$pacienteId,$user);
				}
			}
			$this->response($error);
		}
		
		public function cuestionariofile_post(){
			$fecha = new Datetime();
			$config['encrypt_name'] = true;
			$config['upload_path'] = './files/patients/'.$this->post('patient').'/';
			$config['allowed_types'] = ['jpg','png','pdf'];
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload('file')){
				$error = array(
				'code'=> '1',
				'type'=>'error',
				'title'=>'<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Error',
				'error' => $this->upload->display_errors());
			}else{
				$fileData = $this->upload->data();				
				$archivo = array(
					'usuarioid' => $this->post('user'),
					'pacienteid' => $this->post('patient'),
					'nombre' => $fileData['client_name'],
					'nombreFisico' => $fileData['file_name'],
					'activo' => 1);
				$this->db->insert('paciente_archivos',$archivo);
				$archivoid = $this->db->insert_id();
				
				/*$encabezado = array('archivo' => $archivoid);
				$this->data->cuestionario_edit($id,$encabezado);*/
				$nuevaActividad = array(
				'usuario' => $this->post('user'),
				'fecha' => $fecha->format('Y-m-d'),
				'hora' => $fecha->format('h:i:00'),
				'cuestionarioid' => $this->post('questid'),
				'archivoid' => $archivoid,
				//'ofertaid' => 3,
				'tipoid' => 3,
				'etapaid' => 1,
				'estadoid' => 2,
				'pacienteid' => $this->post('patient'),
				'nota' => '<dl class="dl-horizontal"> 
							<dt>File</dt><dd>'.$fileData['client_name'].'</dd> 
							<dt>Size</dt><dd>'.($fileData['file_size'] / 1000).' MB.</dd> 
							<dt>Type</dt><dd>'.$fileData['file_type'].'</dd> 
						</dl>');
				$error = $this->paciente->agregar_actividad($nuevaActividad);
			}
			$this->response($error);
		}
		
		public function procedureoptions_post(){
			$id = $this->post('id');
			$option = $this->post('option');
			$newOption = array(
				'preguntaid' => $id,
				'opcion' => $option);
			$error = $this->data->procedure_option_add($newOption);
			$this->response($error);
		}
		
		public function procedureoptions_delete(){
			$id = $this->delete('id');
			$error = $this->data->procedure_option_delete($id);
			$this->response($error);
		}
		
		public function surveys_get(){
			$data['data'] = $this->data->surveys_list();
			$this->response($data);
		}
	
	#endregion
	
	#region Precios (fee)
		public function feesched_get(){
			$data['data'] = $this->data->feesched_list();
			$this->response($data);
		}
		
		public function precios_get(){
			$data['data'] = $this->data->precios_lista();
			$this->response($data);
		}
		
		public function precios_put(){
			$id = $this->put('id');
			$amount = $this->put('amount');
			
			$fee = array('Amount' => $amount);
			$data['data'] = $this->data->precios_edit($id,$fee);
			$this->response($data);
		}
		
		public function precios_post(){
			$category = $this->post('category');
			$procedure = $this->post('procedure');
			$amount = $this->post('amount');
			
			$fee = array(
				'FeeSched' => $category,
				'CodeNum' => $procedure,
				'Amount' => $amount);
			//$data['data'] = $this->data->precios_add($id,$fee);
			$this->response($fee);
		}
	#endregion

	#region Origen
		public function origen_get(){
			$activo = $this->get('activo');
			$data['data'] = $this->data->origen_list($activo);
			$this->response($data);
		}	

		public function origen_post(){
			$newOrigen = array(
				'nombre' => $this->post('name'),
				'color' => $this->post('color'),
				'activo' => $this->post('status'));
			$error = $this->data->origen_add($newOrigen);
			$this->response($error);
		}

		public function origen_put(){
			$id = $this->put('id');
			$newOrigen = array(
				'nombre' => $this->put('name'),
				'color' => $this->put('color'),
				'activo' => $this->put('status'));
			$error = $this->data->origen_edit($id,$newOrigen);
			$this->response($error);
		}
		
		public function origendata_get(){
			$data = $this->data->origen_data();
			$this->response($data);
		}
		
	#endregion

	#region Graficas
		public function ofertasdata_get(){
			$data = $this->data->ofertas_data();
			$this->response($data);
		}
		
		public function procedimientosdata_get(){
			$data = $this->data->procedimientos_data();
			$this->response($data);
		}
	
		public function proving_get(){
			
			$data['data'] = $this->data->doctor_ingresos();
			$this->response($data);
		}
	#endregion
}