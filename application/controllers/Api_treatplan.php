<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Api_treatplan extends REST_Controller{
	function __construct(){
        parent::__construct();
		$headers = $this->input->request_headers();
		$dbConfig = json_decode($this->encryption->decrypt($headers['dbconfig']));
		$this->load->database(db_config($dbConfig));
		
		$this->load->model('Treatplan_model','tp');
		$this->load->model('Paciente_model','patient');
	}
	
	#region CRUD
		public function treatplan_post(){
			$status = $this->post('status');
			$fecha = new DateTime();
			$newTP = array(
				'PatNum' => $this->post('patient'),
				'DateTP' => $fecha->format('Y-m-d'),
				'Heading' => $this->post('name'),
				'Complete' => 0,
				'Note' => $this->post('notes'),
				'TPStatus' => $this->post('status'));
			$data['data'] = $this->tp->create($newTP);
			$this->response($data);
		}
		
		public function treatplan_get(){
			$pat = $this->get('patient');
			$data['data'] = $this->tp->listtp($pat);
			$this->response($data);
		}
		
		public function treatplandata_get(){
			$tpid = $this->get('tpid');
			$data['data'] = $this->tp->details($tpid);
			$this->response($data);
		}
		
		public function treatplan_put(){
			$editTP = array(
				'Heading' => $this->put('name'),
				'Note' => $this->put('notes'),
				'Complete' => $this->put('complete'),
				'TPStatus' => $this->put('status'),);
			$id = $this->put('id');
			$error['data'] = $this->tp->edit($editTP,$id);
			$this->response($error);
		}
	#endregion
	
	public function tppdf_get(){
		$tpid = $this->get('treatplan');
		$data = $this->tp->tp_pdf($tpid);
		
		$data['tpid'] = $tpid;
		$data['databusiness'] = data_bussines();
		$procedures = $this->tp->tp_details_procedures($tpid,'all');
		$data['proceduresODT'] =  json_encode($procedures);

		$this->load->view('pdf/treatplan_details',$data);
		
	}
	
	public function patient_get(){
		$id = $this->get('patient');
		$type = $this->get('type');
		
	}
	
	public function tpprocedure_post(){
		$fecha = new DateTime();
		$tpId = $this->post('tp');
		$tooths = $this->post('tooth');
		$tooths = explode(',',$tooths);
		$fee = $this->post('fee');
		
		$tpData = $this->tp->details($tpId);
		if($fee == ''){
			$feePrincipal = $this->tp->fee_principal($this->post('procedure'));
			if(count($feePrincipal) > 0){ $fee = $feePrincipal[0]->Amount;}
			else{ $fee = 0;}
		}
		foreach($tooths as $tooth){
			$provider = $this->patient->provider($tpData[0]->PatNum);
			$addProce = array(
				'PatNum' => $tpData[0]->PatNum,
				//'AptNum' => $this->post(),
				'ProcDate' => $fecha->format('Y-m-d H:i'),
				'ToothRange' => $this->post('toothrange'),
				'Surf' => $this->post('surf'),
				'ProcFee' => $fee,
				'ToothNum' => $tooth,
				'ProcStatus' => $this->post('status'),
				'ProvNum' => $this->post('provi'),
				'CodeNum' => $this->post('procedure'));			
			$error = $this->tp->proc_attach($addProce,$tpId);
		}
		print_r(json_encode($error));
	}
		
	public function tpprocedure_get(){
		$treatplanid = $this->get('treatplan');
		$status = $this->get('status');
		
		$data['data'] = $this->tp->tp_details_procedures($treatplanid,$status);
		$this->response($data);
	}
	
	public function tpprocedure_put(){
		$fecha = '';
		if($this->put('status') == 2){
			$fecha = fechaactual('Y-m-d');
		}
		$editProce = array(
		'ToothNum' => $this->put('tooth'),
		'ProcFee' => $this->put('fee'),
		'Priority' => $this->put('priority'),
		'ProcStatus' => $this->put('status'),
		'Notes' => $this->put('note'),
		'ProvNum' => $this->put('doctor'),
		'DateComplete' => $fecha,
		'Surf' => $this->put('surf'));
		$id = $this->put('id');
		$error = $this->tp->proc_edit($editProce,$id);
		$this->response($error);
	}

}