<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
		
	if(!function_exists('db_config')){
		/*
		* Retorna un arreglo con la configuracion para la conexion a la base de datos
		* "dbname" no tiene que estar encriptado.
		*/
		function db_config($dbConfig){
			$config['hostname'] = 'www.cliwork.com';
			$config['username'] = 'cliwork_kevin';
			$config['password'] = 'GWGjC+E1sZAu';
			$config['database'] = $dbConfig->hostDBNombre;
			$config['dbdriver'] = 'mysqli';
			$config['dbprefix'] = '';
			$config['pconnect'] = FALSE;
			$config['db_debug'] = TRUE;
			$config['cache_on'] = FALSE;
			$config['cachedir'] = '';
			$config['char_set'] = 'utf8';
			$config['dbcollat'] = 'utf8_general_ci';
			return $config;
		}
	}
	
	if(!function_exists('config_system')){
		function config_system($index){
			$data = array(
				'url_files' => "./files/"
			);
			return $data[$index];
		}
	}
	
	if(!function_exists('appt_pattern_time')){
		function appt_pattern_time(){
			$ci =& get_instance();
			$ci->load->database();	
			$ci->db->select('instrucciones');
			$ci->db->from('configuracion_sistema');
			$ci->db->where('id',2);
			$data = $ci->db->get()->result();
			return $data[0]->instrucciones;
		}
	}

	if(!function_exists('data_bussines')){
		function data_bussines(){
			$ci =& get_instance();
			$ci->load->database();	
			$ci->db->select('*');
			$ci->db->from('databusiness');			
			$data = $ci->db->get()->result();
			return $data[0];
		}
	}

	if(!function_exists('segundos_a_tiempo')){
		function segundos_a_tiempo($TotalSegundos){
			$data['horas'] = floor($TotalSegundos / 3600);
			$data['minutos'] = floor(($TotalSegundos % 3600) / 60);
			$data['segundos'] = $TotalSegundos - ($data['horas'] * 3600) - ($data['minutos'] * 60);
			return $data;
		}
	}
	
	if(!function_exists('fechaactual')){
		function fechaactual($format){
			$fecha = new DateTime();
			$fecha = $fecha->format($format);
			return $fecha;
		}
	}

?>