<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Treatment plan - <?=$encabezado[0]->cliente?></title>
<style>
		@font-face {
  font-family: SourceSansPro;
  src: url(SourceSansPro-Regular.ttf);
}

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #0087C3;
  text-decoration: none;
}

body {
  position: relative;
  width: 21cm;  
 
  margin: 0 auto; 
  color: #555555;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 14px; 
  font-family: SourceSansPro;
}

header {
  padding: 10px 0;
  margin-bottom: 20px;
  border-bottom: 1px solid #AAAAAA;
}

#logo {
  float: left;
  margin-top: 8px;
}

#logo img {
  height: 70px;
}

#company {
  float: right;
  text-align: right;
}


#details {
  margin-bottom: 50px;
}

#client {
  padding-left: 6px;
  border-left: 6px solid #0087C3;
  float: left;
}

#client .to {
  color: #777777;
}

h2.name {
  font-size: 1.4em;
  font-weight: normal;
  margin: 0;
}

#invoice {
  float: right;
  text-align: right;
}

#invoice h1 {
  color: #0087C3;
  font-size: 2.4em;
  line-height: 1em;
  font-weight: normal;
  margin: 0  0 10px 0;
}

#invoice .date {
  font-size: 1.1em;
  color: #777777;
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px;
}

table th,
table td {
    /* background: #EEEEEE; */
    text-align: center;
    border: 1px solid #AAAAAA;
}
table th {
	background: #EEEEEE;
}

table th {
  white-space: nowrap;        
  font-weight: normal;
}

table td {
  text-align: left;
}

table td h3{
  color: #57B223;
  font-size: 1.2em;
  font-weight: normal;
  margin: 0 0 0.2em 0;
}

table .no {
  color: #FFFFFF;
  font-size: 1.6em;
  background: #57B223;
}

table .desc {
  text-align: left;
}

table .unit {
  background: #DDDDDD;
}

table .qty {
}

table .total {
  background: #57B223;
  color: #FFFFFF;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table tfoot td {
	/*padding: 10px 20px; */
	background: #FFFFFF;
	border-bottom: none;
	border-left: none;
	border-right: none;
	/* font-size: 1.2em; */
	white-space: nowrap;
	border-top: 1px solid #AAAAAA;
}

table tfoot tr:first-child td {
  border-top: none; 
}



table tfoot tr td:first-child {
  border: none;
}

#thanks{
  font-size: 2em;
  margin-bottom: 50px;
}

#notices{
  padding-left: 6px;
  border-left: 6px solid #0087C3;  
}

#notices .notice {
  font-size: 1.2em;
}

footer {
  color: #777777;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #AAAAAA;
  padding: 8px 0;
  text-align: center;
}
</style>
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
        <img src="<?=base_url('img/logo.png')?>">
      </div>
      <div id="company">
        <h2 class="name">Smile4Ever</h2>
        <div>Dr. Atl 2084, Zona Urbana Rio Tijuana, 22010 Tijuana, B.C.</div>
        <div>619-906-7481</div>
        <div><a href="smile4evermexico.com">smile4evermexico.com</a></div>
      </div>
      </div>
    </header>
    <main>
      <div id="details" class="clearfix">
        <div id="client">
          <div class="to">TREATMENT PLAN TO:</div>
          <h2 class="name"><?=$encabezado[0]->cliente?></h2>
          <div class="address"><?=$encabezado[0]->Address?>,<?=$encabezado[0]->City?>,<?=$encabezado[0]->state?></div>
          <div class="email"><a href=""><?=$encabezado[0]->Email?></a></div>
        </div>
        <div id="invoice">
          <h1>Treatment plan</h1>
          <div class="date">Date of treatmentplan: <?=$encabezado[0]->DateTP?></div>
          <div class="date"></div>
        </div>
      </div>
	  <div style="width:100%; text-align:center;">
		<?php echo file_get_contents(base_url()."img/svg/odontograma.svg"); ?>
	  </div>
      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="desc">Date</th>
            <th class="desc">Description</th>
            <th class="qty">Doctor</th>
            <th class="qty">Tooth</th>
            <th class="qty">Status</th>
            <th class=""></th>
          </tr>
        </thead>
        <tbody>
		<?php $total = 0;?>
		<?php foreach($procedures as $item):?>
		<?php $total = $total + $item->ProcFee?>
			<tr>
				<td><?=$item->dateFormat?></td>
				<td><?=$item->Doctor?></td>
				<td><?=$item->Descript?></td>
				<td><?=$item->ToothNum?></td>
				<td><?=$item->status?></td>
				<td><?=$item->ProcFeeFormat?></td>
			</tr>
		<?php endforeach;?>
         </tbody>
        <tfoot>
          <tr>
            <td colspan="3"></td>
            <td colspan="2">Total</td>
            <td>$<?=number_format($total,2)?></td>
          </tr>
        </tfoot>
      </table>
    </main>    
  </body>
  <script>
	window.print();
	setTimeout(function(){ 	window.close(); }, 3000);
  </script>
</html>