<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Treatment plan - <?=$encabezado[0]->cliente?></title>
<style>
	@font-face {
	  font-family: SourceSansPro;
	  src: url(<?=base_url()?>assets/SourceSansPro.ttf);
	}

	.clearfix:after {
	  content: "";
	  display: table;
	  clear: both;
	}

	a {
	  color: #0087C3;
	  text-decoration: none;
	}

	body {
	  position: relative;
	  width: 100%;  
	 
	  margin: 0 auto; 
	  color: #555555;
	  background: #FFFFFF; 
	  /*font-family: Arial, sans-serif; */
	  font-size: 14px; 
	  font-family: SourceSansPro;
	}

header {
  padding: 10px 0;
  margin-bottom: 20px;
  border-bottom: 1px solid #AAAAAA;
}

#logo {
  float: left;
  margin-top: 8px;
}

#logo img {
  height: 70px;
}

#company {
  float: ;
  text-align: right;
}


#details {
  margin-bottom: 50px;
}

#client {
  padding-left: 6px;
  border-left: 6px solid #0087C3;
  float: left;
}

#client .to {
  color: #777777;
}

h2.name {
  font-size: 1.4em;
  font-weight: normal;
  margin: 0;
}

#invoice {
  float: right;
  text-align: right;
}

#invoice h1 {
  color: #0087C3;
  font-size: 2.4em;
  line-height: 1em;
  font-weight: normal;
  margin: 0  0 10px 0;
}

#invoice .date {
  font-size: 1.1em;
  color: #777777;
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px;
}

table th,
table td {
    /* background: #EEEEEE; */
    text-align: center;
	font-size: 12px; 
    border: 1px solid #AAAAAA;
}
table th {
	background: #EEEEEE;
}

table th {
  white-space: nowrap;        
  font-weight: normal;
}

table td {
  text-align: left;
}

table td h3{
  color: #57B223;
  font-size: 1.2em;
  font-weight: normal;
  margin: 0 0 0.2em 0;
}

table .no {
  color: #FFFFFF;
  font-size: 1.6em;
  background: #57B223;
}

table .desc {
  text-align: left;
}

table .unit {
  background: #DDDDDD;
}

table .qty {
}

table .total {
  background: #57B223;
  color: #FFFFFF;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table tfoot td {
	/*padding: 10px 20px; */
	background: #FFFFFF;
	border-bottom: none;
	border-left: none;
	border-right: none;
	/* font-size: 1.2em; */
	white-space: nowrap;
	border-top: 1px solid #AAAAAA;
}

table tfoot tr:first-child td {
  border-top: none; 
}



table tfoot tr td:first-child {
  border: none;
}

#thanks{
  font-size: 2em;
  margin-bottom: 50px;
}

#notices{
  padding-left: 6px;
  border-left: 6px solid #0087C3;  
}

#notices .notice {
  font-size: 1.2em;
}

footer {
  color: #777777;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #AAAAAA;
  padding: 8px 0;
  text-align: center;
}
</style>
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
        <img src="data:image/png;base64, <?=base64_encode(file_get_contents(base_url().$databusiness->LogoUrl));?>">
      </div>
      <div id="company">
        <h2 class="name"><?=$databusiness->NameEnterprise?></h2>
        <div><?=$databusiness->Address?></div>
        <div><?=$databusiness->Phone?></div>
        <div><a href="<?=$databusiness->Weblink?>"><?=$databusiness->Weblink?></a></div>
      </div>
      </div>
    </header>
    <main>
	  <div style="text-align: center;">
		<b>
			Statement<br>
			<?=$encabezado[0]->cliente?><br>
			<?php $fecha = new DateTime(); echo $fecha->format('Y-m-d');?>
		</b>
	  </div>
	  <h1></h1>
      <!--<div id="details" class="clearfix">
        <div id="client">
          <div class="to">TREATMENT PLAN TO:</div>
          <h2 class="name"><?=$encabezado[0]->cliente?></h2>
          <div class="address"><?=$encabezado[0]->Address?>,<?=$encabezado[0]->City?>,<?=$encabezado[0]->state?></div>
          <div class="email"><a href=""><?=$encabezado[0]->Email?></a></div>
        </div>
        <div id="invoice">
          <h1>Treatment plan</h1>
          <div class="date">Date of treatmentplan: <?=$encabezado[0]->DateTP?></div>
          <div class="date"></div>
        </div>
      </div>-->
      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
			<th class="qty">Done</th>
            <th class="desc">Date Complete</th>
            <th class="qty">Doctor</th>
			<th class="qty">Priority</th>
			<th class="qty">Tooth</th>
			<th class="qty">Surf</th>
			<th class="desc">Code</th>
			<th class="desc">Description</th>
            <th class="">Charges</th>
            <th class="">Credits</th>
            <th class="">Balance</th>
          </tr>
        </thead>
        <tbody>
		<?php $subtotal = $account['total'][0]->amount; ?>
		<?php $total = 0; ?>
		<?php $subtotalBalance = 0; ?>
		<?php $comision = 0; ?>
		<?php $pay = 0; ?>
		<?php $increments = 0; ?>
		<?php $substractions = 0; ?>
		<?php foreach($account['list'] as $item):?>
				<?php if($item->TypeItem == "proc"): ?>
					<tr>
						<td style="text-align:center;">X</td>
						<td><?=$item->DateComplete?></td>
						<td><?=$item->Doctor?></td>
						<td style="text-align:center;"><?=$item->Priority?></td>
						<td style="text-align:center;"><?=$item->ToothNum?></td>
						<td style="text-align:center;"><?=$item->Surf?></td>
						<td><?=$item->ProcCode?></td>
						<td><?=$item->Descript?></td>
						<td><?=$item->ProcFeeFormat?></td>
						<td></td>
						<td></td>
					</tr>
				<?php elseif ($item->TypeItem == "pay"): ?>
				<?php $pay  = $pay + $item->monto;?>
					<tr>
						<td></td>
						<td><?=$item->dateFormat?></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td><?=$item->TypeName?></td>
						<td></td>
						<td><?=$item->montoFormato?></td>
						<td></td>
					</tr>
				<?php elseif ($item->TypeItem == "adjust"): ?>
				<?php 
					if($item->ItemValue == "+"){
						$increments  = $substractions + $item->monto;
					}else{
						$substractions  = $substractions + $item->monto;	
					}
				?>
					<tr>
						<td></td>
						<td><?=$item->dateFormat?></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td><?=$item->ItemName?></td>
						<td><?=$item->montoFormato?></td>
						<td></td>
						<td></td>
					</tr>
				<?php elseif ($item->TypeItem == "Comis"): ?>
				<?php  $comision = $item->comision * $item->deuda/100; ?>
					<tr>
						<td></td>
						<td><?=$item->dateFormat?></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>Comision por plan de pagos: %<?=$item->comision?></td>
						<td><?=$comision?></td>
						<td></td>
						<td></td>
					</tr>
				<?php endif; ?>
		<?php endforeach;?>
			<tr>
				<td colspan="7"></td>
				<td>Subtotal</td>
				<td><b><?php $subtotal = $subtotal + $increments - $substractions + $comision; echo number_format($subtotal,2);?></b></td>
				<td><b><?=$pay?></b></td>
				<td><b><?php  $subtotalBalance =  $subtotal - $pay;  echo $subtotalBalance;?></b></td>
			</tr>
			<?php foreach($account['uncomplete'] as $item):?>
			<?php $total = $item->ProcFee; ?>
				<tr>
					<td></td>
					<td></td>
					<td><?=$item->Doctor?></td>
					<td><?=$item->Priority?></td>
					<td><?=$item->ToothNum?></td>
					<td><?=$item->Surf?></td>
					<td><?=$item->ProcCode?></td>
					<td><?=$item->Descript?></td>
					<td><?=$item->ProcFeeFormat?></td>
					<td></td>
					<td></td>
				</tr>
			<?php endforeach; ?>
			<tr>
				<td colspan="7"></td>
				<td>Total</td>
				<td><b><?=$total?></b></td>
				<td></td>
				<td><b><?=$total + $subtotalBalance?></b></td>
			</tr>
         </tbody>
      </table>
    </main>    
	</body>
  	<script type="text/javascript" src="<?=base_url()?>js/vendor/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>js/vendor/jquery/jquery-ui.min.js"></script>
	<script>
		$(document).ready(function(){
			window.print();
			setTimeout(function(){ 	window.close(); }, 3000);
		});
	</script>
</html>