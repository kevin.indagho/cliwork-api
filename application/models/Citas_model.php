<?php
Class Citas_model extends CI_Model{
	
	#region CRUD
		public function create($newappt,$services){
			$this->db->insert('appointment',$newappt);
			$error = $this->db->error();
			$error['id'] = $this->db->insert_id();
			if($error['code'] == 0){
				$this->add_services($error['id'],$services);
			}
			return $error;
		}
		
		public function read($idappt){
			$this->db->select('appointment.id,
			appointment.PatId,
			appointment.ProvId,
			appointment.AptDateTime,
			appointment.Pattern,
			appointment.reason,
			appointment.ProvHyg,
			appointment.IsNewPatient,
			appointment.Assistant,
			appointment.ProvNum,
			appointment.Note,
			appointment.all_Day,
			appointment.Needs,
			appointment.Confirmed,
			appointment.AptStatus,
			appointment.inClinic,
			appointment.Sedation,
			appointment.SedationNote,
			appointment.AmountApp,
			appointment.Vuelo,
			appointment.VueloFecha,
			DATE_FORMAT(appointment.VueloFecha,"%m/%d/%Y") as VueloFechaFormat,
			appointment.VueloSalida,
			appointment.VueloLlegada,
			appointment.VueloNotas,
			appointment.VueloDocu,
			appointment.IsNewPatient');
			$this->db->from('appointment');
			$this->db->where('appointment.id',$idappt);
			$data = $this->db->get()->result();
			$data[0]->services = $this->procedures_appt($data[0]->PatId);
			$data[0]->reasonproc = $this->reason_appt($data[0]->reason);
			return $data;
		}
		
		public function edit($id,$editAppt){
			$this->db->where('id',$id);
			$this->db->update('appointment',$editAppt);
			$error = $this->db->error();
			return $error;
		}
		
		public function deleted($idappt){
			$this->db->select('procedurelog.Id');
			$this->db->from('procedurelog');
			$this->db->where('procedurelog.AptNum',$idappt);
			$procedures = $this->db->get()->result();
			foreach($procedures as $proce){
				$this->db->where('procedurelog.Id',$proce->Id);
				$this->db->update('procedurelog',array('AptNum' => null));
			}
			
			$this->db->where('appointment.id',$idappt);
			$this->db->delete('appointment');
			$data = $this->db->error();
			return $data;
		}
		
		public function update_duration($id){
			/* Actualizar duracion de la cita con el tiempo de los procedimientos asignados a la cita */
			$this->db->select('SUM(procedurecode.Duration) as total');
			$this->db->from('procedurelog');
			$this->db->join('procedurecode','procedurelog.CodeNum = procedurecode.id','inner');
			$this->db->where('procedurelog.AptNum',$id);
			$data = $this->db->get()->result();
			if(count($data) > 0){
					$this->db->select('appointment.AptDateTime,appointment.AptDateTimeEnd');
					$this->db->from('appointment');
					$this->db->where('appointment.id',$id);
					$appt = $this->db->get()->result();
					if($data[0]->total > 0){
						$fecha = date($appt[0]->AptDateTime);
						$nuevafecha = strtotime('+'.$data[0]->total.' minute',strtotime ($fecha));
						$nuevafecha = date('Y-m-d H:i:s',$nuevafecha);
					}else{
						$fecha = date($appt[0]->AptDateTime);
						$nuevafecha = strtotime('+30 minute',strtotime ($fecha));
						$nuevafecha = date('Y-m-d H:i:s',$nuevafecha);

					}
					
					$this->db->where('appointment.id',$id);
					$this->db->update('appointment',array('appointment.AptDateTimeEnd' => $nuevafecha));
				
			}
		}
	#endregion
	
	public function reason_appt($reason){
		$procedures = explode(',',$reason);
		$this->db->select('procedurecode.id,
		procedurecode.ProcCode,
		procedurecode.Descript');
		$this->db->from('procedurecode');
		$this->db->where_in('procedurecode.id',$procedures);
		$data = $this->db->get()->result();
		return $data;
	}
	
	public function apt_back_deal($aptid){
		$this->db->select('appointment.ofertaId');
		$this->db->from('appointment');
		$this->db->where('appointment.id',$aptid);
		$aptData = $this->db->get()->result();
		
		$this->db->where('appointment.id',$aptid);
		$this->db->update('appointment',array('appointment.AptStatus' => 6));
		
		$this->db->where('ofertas.id',$aptData[0]->ofertaId);
		$this->db->update('ofertas',array('ofertas.vendida' => 0));
		$error = $this->db->error();
		return $error;
	}
	
	public function add_proc_planappt($proceId,$planApptId){
		$this->db->where('Id',$proceId);
		$this->db->update('procedurelog',array('PlannedAptNum' => $plaApptId));
		$error = $this->db->error();
		return $error;
	}
	
	public function procedures_appt($PatId){
		$this->db->select('procedurelog.Id,
		procedurecode.id,
		procedurecode.CodeNum,
		procedurecode.ProcCode,
		procedurecode.Descript,
		procedurecode.AbbrDesc,
		procedurecode.Duration,
		procedurelog.ProcFee,
		procedurelog.PatNum,
		procedurelog.AptNum');
		$this->db->from('procedurelog');
		$this->db->join('procedurecode','procedurelog.CodeNum = procedurecode.id','inner');
		$this->db->where('procedurelog.PatNum',$PatId);
		$data = $this->db->get()->result();
		foreach($data as $proce){
			$proce->ProcFeeFormat = number_format($proce->ProcFee,2);
		}
		return $data;
	}
		
	public function upt_provider($patId,$provId){
		$this->db->where('Id',$patId);
		$this->db->update('patient',array('PriProv' => $provId));
		$error = $this->db->error();
		return $error;
	}
	
	public function add_services($apptid,$services){
		foreach($services as $service){
			$this->db->where('Id',$service);
			$this->db->update('procedurelog',array('AptNum' => $apptid));
			$error = $this->db->error();
		}
		
		$this->db->select('Sum(procedurecode.Duration) as duration');
		$this->db->from('procedurelog');
		$this->db->join('procedurecode','procedurelog.CodeNum = procedurecode.id','inner');
		$this->db->where('procedurelog.AptNum',$apptid);
		$data = $this->db->get()->result();
		
		/*$patternFormat = "";
		$total = ($data[0]->duration/10);
		for($i = 1; $i <= $total; $i++){
			$patternFormat .='//';
		}
		$this->db->where('id',$apptid);
		$this->db->update('appointment',array('Pattern' => $patternFormat));*/
			
			
		$error = $this->db->error();
		return $error;
	}
	
	public function calendario_lista($dateStart,$dateEnd,$status,$inBox,$provider = null){
		$this->db->select('appointment.id,
		appointment.AptNum,
		appointment.AptDateTime,
		appointment.AptDateTimeEnd,
		appointment.ProvId,
		appointment.Pattern,
		appointment.AptStatus,
		appointment.all_Day,
		appointment.IsNewPatient,
		appointment.reason,
		appointment.Note,
		appointment.Needs,
		appointment.Confirmed,
		appointment.Sedation,
		appointment.Vuelo,
		patient.PatNum,
		patient.Id as Patid,
		CONCAT(patient.LName," ",patient.FName) as PatName,
		CONCAT(provider.LName," ",provider.FName) as ProName');
		$this->db->from('appointment');
		$this->db->join('patient','appointment.PatId = patient.Id','inner');
		$this->db->join('provider','appointment.ProvId = provider.id','inner');
		if($provider != "all"){ $this->db->where('appointment.ProvId',$provider); }
		if($inBox != "none"){ $this->db->where('appointment.inBox',$inBox); }
		if($dateStart != "none"){ $this->db->where('appointment.AptDateTime >=',$dateStart); }
		if($dateEnd != "none") { $this->db->where('appointment.AptDateTime <=',$dateEnd); }
		if($status != "none") { $this->db->where_in('appointment.AptStatus',$status); }
		//$this->db->where('appointment.AptStatus',2);
		$this->db->where('appointment.active',1);
		$data = $this->db->get()->result();
		foreach($data as $cita){
			if($cita->AptStatus == 7){
				$cita->allDay = true;
				$cita->backgroundColor = "#ffe6002e";
				$cita->borderColor = "#ffe6002e";
			}else{
				$cita->allDay = false;
			}
			
			$servicios = $this->appointment_services($cita->id);
			$cita->servicios = $servicios;
			$Segundos = (strlen($cita->Pattern)*appt_pattern_time())*60;
			$duracion = segundos_a_tiempo($Segundos);
			$cita->tiempo = segundos_a_tiempo($Segundos);
			/**/
			$fecha = date($cita->AptDateTime);
			$nuevafecha = strtotime('+'.$duracion['horas'].' hours',strtotime($fecha));
			$nuevafecha = date('Y-m-d H:i:s',$nuevafecha);
			$nuevafecha = strtotime ('+'.$duracion['minutos'].' minutes',strtotime($nuevafecha));
			//$cita->end = date('Y-m-d H:i:s',$nuevafecha );
			
			$cita->start = $cita->AptDateTime;
			$cita->end = $cita->AptDateTimeEnd;
			
			/**/
			$cita->resourceId = $cita->ProvId;
			$cita->title = $cita->PatName."<br>";
			$cita->description = '<dl class="text-capitalize">';
			$cita->description .= '<dt><b>characteristics</b></dt>';
			if($cita->IsNewPatient == 1 || $cita->Needs == 1){
				$cita->description .= '<dd>';
				if($cita->Needs == 1){
					$cita->description .= '<span class="text-sm label label-danger">Special needs</span>';
				}
				$cita->description .= '</dd>';
			}
			if($cita->all_Day == 0){
				//$cita->description .= '<dd><span class="text-capitalize text-sm label label-info"><i class="fa fa-clock-o" aria-hidden="true"></i> '.($duracion['horas'] > 0 ? $duracion['horas']." Hrs.":"").' '.($duracion['minutos'] > 0 ? $duracion['minutos']." Min.":"").'</span></dd>';
			}else{
				$cita->description .= '<dd><span class="text-capitalize text-sm label label-warning">ALL DAY</span></dd>';
			}
			$cita->description .= '<dt><b>Note</b></dt><dd>'.$cita->Note.'</dd>';
			if($servicios[0]->services != ''){
				$cita->description .= '<dd><span class="text-sm label label-success">$'.number_format($servicios[0]->Fee,2).'</span></dd>';
				$cita->description .= '<dt><b>Procedures</b></dt><dd>'.$servicios[0]->services.'</dd>';
			}else{
				$cita->description .= '<dt><b>Reason visit</b></dt>';
				$reasonproc = $this->reason_appt($cita->reason);
				foreach($reasonproc as $item){
					$cita->description .= '<dd>'.$item->ProcCode.' - '.$item->Descript.'</dd>';
				}
				
			}
			$cita->description .= '</dl>';
			
		}
		return $data;
	}
	
	public function calendario_lista_box(){
		//Unschedlist
		$this->db->select('appointment.id,
		appointment.AptNum,
		appointment.AptDateTime,
		appointment.ProvId,
		appointment.Pattern,
		appointment.IsNewPatient,
		appointment.Note,
		appointment.all_Day,
		appointment.Needs,
		appointment.Sedation,
		appointment.Vuelo,
		appointment.Confirmed,
		CONCAT(provider.LName," ",provider.FName) as provider,
		patient.PatNum,
		patient.Id as Patid,
		CONCAT(patient.LName," ",patient.FName) as PatName');
		$this->db->from('appointment');
		$this->db->join('patient','appointment.PatId = patient.Id','inner');
		$this->db->join('provider','appointment.ProvId = provider.id','left');
		$this->db->where('appointment.AptStatus',4);
		$data = $this->db->get()->result();
		foreach($data as $cita){
			$servicios = $this->appointment_services($cita->id);
			
			//Modificar tiempo de END(pattenr) a 30 min cada cita del inBox
			//$cita->Pattern = '//////';
			$Segundos = (strlen($cita->Pattern)*appt_pattern_time())*60;
			$duracion = segundos_a_tiempo($Segundos);
			$cita->tiempo = segundos_a_tiempo($Segundos);
			/**/
			$fecha = date($cita->AptDateTime);
			$nuevafecha = strtotime('+'.$duracion['horas'].' hours',strtotime($fecha));
			$nuevafecha = date('Y-m-d H:i:s',$nuevafecha);
			$nuevafecha = strtotime ('+'.$duracion['minutos'].' minutes',strtotime($nuevafecha));
			$cita->end = date('Y-m-d H:i:s',$nuevafecha );
			/**/
			$cita->resourceId = $cita->ProvId;
			$cita->start = $cita->AptDateTime;
			$cita->title = $cita->PatName."<br>";
			$cita->description = '<dl class="text-capitalize">';
			$cita->description .= '<dt><b>characteristics</b></dt>';
			if($cita->IsNewPatient == 1 || $cita->Note != "" || $cita->Needs == 1){
				$cita->description .= '<dd>';
				if($cita->IsNewPatient == 1){
					$cita->description .= '<span class="text-sm label label-danger">NP</span> ';
				}
				if($cita->Needs == 1){
					$cita->description .= '<span class="text-sm label label-danger">SN</span>';
				}
				if($cita->Note != ""){
					$cita->description .= '<span data-toggle="tooltip"data-placement="top" data-original-title="'.$cita->Note.'" class="text-sm label label-danger"><i class="fa fa-sticky-note" aria-hidden="true"></i></span>';
				}
				$cita->description .= '</dd>';
			}
			$cita->description .= '<dd><span class="text-capitalize text-sm label label-info">'.($duracion['horas'] > 0 ? $duracion['horas']." Hrs.":"").' '.($duracion['minutos'] > 0 ? $duracion['minutos']." Min.":"").'</span></dd>';
			$cita->description .= '<dd><span class="text-sm label label-success">$ 100,2650.00</span></dd>';
			if($servicios[0]->services != ''){
				$cita->description .= '<dt><b>Procedures</b></dt><dd>'.$servicios[0]->services.'</dd>';
			}
			$cita->description .= '</dl>';
			
		}
		return $data;
	}
	
	public function appointment_services($apptid){
		$this->db->select('GROUP_CONCAT(procedurecode.Descript) as services,
			SUM(procedurelog.ProcFee) as Fee');
		$this->db->from('procedurecode');
		$this->db->join('procedurelog','procedurelog.CodeNum = procedurecode.id','inner');
		$this->db->where('procedurelog.AptNum',$apptid);
		$data = $this->db->get()->result();
		return $data;
	}
}
?>