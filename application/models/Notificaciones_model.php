<?php
class Notificaciones_model extends CI_Model{
	/*
		Tipos de reporte
		1 : Paciente y Oferta perdida.
		
	*/

	public function leida($id){
		$notificacion = array('notificaciones.leida' => 1);
		$this->db->where('notificaciones.id',$id);
		$this->db->update('notificaciones',$notificacion);
		$error = $this->db->error();
		return $error;
	}
	
	public function cita_nueva($citaid){
		$this->db->select('appointment.PatId,
		appointment.AptDateTime,
		appointment.AptDateTimeEnd,
		patient.LName,
		patient.FName');
		$this->db->from('appointment');
		$this->db->join('patient','appointment.PatId = patient.Id','inner');
		$this->db->where('appointment.id',$citaid);
		$appt = $this->db->get()->result();
		
		$datetime1 = date_create($appt[0]->AptDateTime);
		$datetime2 = date_create($appt[0]->AptDateTimeEnd);
		$duration = date_diff($datetime1, $datetime2);
		$fecha = new DateTime();
		
		$usuarios = $this->leer_usuarios(5);
		foreach($usuarios as $usuario){
			$notificacion = array(
			'usuarioid'=> $usuario->id,
			'leida' => 0,
			'tipoid' => 4,
			'color' => 'info',
			'fecha' => $fecha->format('Y-m-d  H:i'),
			'pacienteid' => $appt[0]->PatId,
			'cuerpo'=> "You have a new appointment for <b>".$appt[0]->FName." ".$appt[0]->LName.
			"</b><br>Date: ".$appt[0]->AptDateTime.
			"<br>Duration: ".($duration->h > 0? $duration->h.' Hr': '')
			.($duration->i > 0? $duration->i.' Mn': ''));
			$this->db->insert('notificaciones',$notificacion);
			$error = $this->db->error();
		}
	}

	public function cita_planeada($citaid){
		$this->db->select('appointment.PatId,
		DATE_FORMAT(appointment.AptDateTime,"%Y-%m-%d") as AptDateTimeFormat,
		appointment.AptDateTime,
		appointment.AptDateTimeEnd,
		patient.LName,
		patient.FName');
		$this->db->from('appointment');
		$this->db->join('patient','appointment.PatId = patient.Id','inner');
		$this->db->where('appointment.id',$citaid);
		$appt = $this->db->get()->result();
		
		$datetime1 = date_create($appt[0]->AptDateTime);
		$datetime2 = date_create($appt[0]->AptDateTimeEnd);
		$duration = date_diff($datetime1, $datetime2);
		$fecha = new DateTime();
		
		$usuarios = $this->leer_usuarios(9);
		foreach($usuarios as $usuario){
			$notificacion = array(
			'usuarioid'=> $usuario->id,
			'leida' => 0,
			'tipoid' => 4,
			'color' => 'info',
			'fecha' => $fecha->format('Y-m-d  H:i'),
			'pacienteid' => $appt[0]->PatId,
			'cuerpo'=> "The appointment planned with <b>".$appt[0]->FName." ".$appt[0]->LName."</b> on <b>".$appt[0]->AptDateTimeFormat."</b>, has not been confirmed or scheduled.");
			$this->db->insert('notificaciones',$notificacion);
			$error = $this->db->error();
		}
	}
	
	public function cita_cancelada($citaid){
		$this->db->select('appointment.PatId,
		appointment.AptDateTime,
		appointment.AptDateTimeEnd,
		patient.LName,
		patient.FName');
		$this->db->from('appointment');
		$this->db->join('patient','appointment.PatId = patient.Id','inner');
		$this->db->where('appointment.id',$citaid);
		$appt = $this->db->get()->result();
		
		$datetime1 = date_create($appt[0]->AptDateTime);
		$datetime2 = date_create($appt[0]->AptDateTimeEnd);
		$duration = date_diff($datetime1, $datetime2);
			
		$fecha = new DateTime();
		$notificacion = array(
			'usuarioid'=>3,
			'leida' => 0,
			'tipoid' => 4,
			'color' => 'danger',
			'fecha' => $fecha->format('Y-m-d H:i'),
			'pacienteid' => $appt[0]->PatId,
			'cuerpo'=> "Your appointment for <b>".$appt[0]->FName." ".$appt[0]->LName.
			"</b> was canceled <br>Date: ".$appt[0]->AptDateTime.
			"<br>Duration: ".($duration->h > 0? $duration->h.' Hr': '')
			.($duration->i > 0? $duration->i.' Mn': ''));
			$this->db->insert('notificaciones',$notificacion);
	}

	#region Ofertas
		public function etapa_expirada($ofertaid,$etapaNombre,$dias){
			$usuarios = $this->leer_usuarios(7);
			foreach($usuarios as $usuario){
				$this->db->select('ofertas.valor,ofertas.pacienteid,
				CONCAT(patient.FName," ",patient.LName) as patient');
				$this->db->join('patient','ofertas.pacienteid = patient.Id','inner');
				$this->db->from('ofertas');
				$this->db->where('ofertas.id',$ofertaid);
				$oferta = $this->db->get()->result();
				
				$fecha = new DateTime();
				$notificacion = array(
				'usuarioid'=> $usuario->id,
				'leida' => 0,
				'tipoid' => 4,
				'ofertaid' => $ofertaid, 
				'pacienteid' => $oferta[0]->pacienteid, 
				'color' => 'info',
				'nueva' => 1,
				'fecha' => $fecha->format('Y-m-d H:i'),
				//'pacienteid' => $pacId,
				'cuerpo'=> 'La venta de <b>'.$oferta[0]->patient.'</b> tiene '.$dias.' dias en la etapa de <b>'.$etapaNombre.'</b>. <br> Value: <b>'.number_format($oferta[0]->valor,2).'</b>');
				$this->db->insert('notificaciones',$notificacion);
				$error = $this->db->error();
				if($error['code'] == 0){
					$pushData = array(
					'id' => $ofertaid,
					'type'=> "information",
					'title'=>"You have new a notifation.",
					"noti" => $notificacion);
					$pusher = $this->ci_pusher->get_pusher();
					$event = $pusher->trigger('notifications',MD5($usuario->id),$pushData);
				}
			}
		}
		
		public function oferta_nueva($id){
			$usuarios = $this->leer_usuarios(6);
			foreach($usuarios as $usuario){
				$this->db->select('ofertas.valor,ofertas.pacienteid,
				CONCAT(patient.FName," ",patient.LName) as patient');
				$this->db->from('ofertas');
				$this->db->join('patient','ofertas.pacienteid = patient.Id','inner');
				$this->db->where('ofertas.id',$id);
				$oferta = $this->db->get()->result();
				
				$fecha = new DateTime();				
				//Notificacion para los usuarios 
				$notificacion = array(
				'usuarioid'=> $usuario->id,
				'leida' => 0,
				'tipoid' => 4,
				'ofertaid' => $id, 
				'pacienteid' => $oferta[0]->pacienteid, 
				'color' => 'info',
				'nueva' => 1,
				'fecha' => $fecha->format('Y-m-d H:i'),
				//'pacienteid' => $pacId,
				'cuerpo'=> 'A new sale was created for <b>'.$oferta[0]->patient.'</b>. <br> Value: <b>'.number_format($oferta[0]->valor,2).'</b>');
				$this->db->insert('notificaciones',$notificacion);
				$error = $this->db->error();
				if($error['code'] == 0){
					$pushData = array(
					'id' => $id,
					'type'=> "information",
					'title'=>"You have new a notifation.",
					"noti" => $notificacion);
					$pusher = $this->ci_pusher->get_pusher();
					$event = $pusher->trigger('notifications',MD5($usuario->id),$pushData);
				}
			}
		}
	#endregion
	
	#region Cuestionarios
		public function cuestionario_nuevo($id,$pacId,$paciente,$procedureid){
			$this->db->select('cuestionario_encabezado.usuario');
			$this->db->from('cuestionario_encabezado');
			$this->db->where('cuestionario_encabezado.id',$id);
			$asesorVenta = $this->db->get()->result();
			
			$this->db->select('procedurecode.ProcCode,procedurecode.Descript');
			$this->db->from('procedurecode');
			$this->db->where('procedurecode.id',$procedureid);
			$procedure = $this->db->get()->result();
			
			$this->db->select('usuario.id');
			$this->db->from('usuario');
			$this->db->where_in('usuario.tipoid',array(1,2,3));
			$usuarios = $this->db->get()->result();
			foreach($usuarios as $usuario){
				$fecha = new DateTime();
				$notificacion = array(
				'usuarioid'=> $usuario->id,
				'leida' => 0,
				'tipoid' => 4,
				'color' => 'info',
				'nueva' => 1,
				'fecha' => $fecha->format('Y-m-d H:i'),
				'pacienteid' => $pacId,
				'cuerpo'=> '<b>'.$paciente."</b> answered the questions for the procedure of <br>".$procedure[0]->ProcCode."<br>".$procedure[0]->Descript);
				$this->db->insert('notificaciones',$notificacion);
			}
		}
		
		public function cuestionario_nota_nueva($id,$pacienteId,$userlogin){
			$this->db->select('CONCAT(usuario.nombre," ",usuario.apellidos) as name');
			$this->db->from('usuario');
			$this->db->where('usuario.id',$userlogin);
			$usuarioLogin = $this->db->get()->result();
			
			$this->db->select('usuario.id');
			$this->db->from('usuario');
			$this->db->where_in('usuario.tipoid',array(1));
			$usuarios = $this->db->get()->result_array();
			
			
			$this->db->select('cuestionario_encabezado.usuario,
			cuestionario_encabezado.doctor,
			CONCAT(patient.LName,patient.FName) as paciente');
			$this->db->from('cuestionario_encabezado');
			$this->db->join('patient','cuestionario_encabezado.patientid = patient.Id','inner');
			$this->db->where('cuestionario_encabezado.id',$id);
			$data = $this->db->get()->result();
			if($data[0]->usuario != ''){ $usuarios[] = array('id' => $data[0]->usuario); }
			if($data[0]->doctor != ''){ $usuarios[] = array('id' => $data[0]->doctor); }
			
			foreach($usuarios as $usuario){
				if($usuario['id'] != $userlogin){
					$fecha = new DateTime();
					$notificacion = array(
					'usuarioid'=> $usuario['id'],
					'leida' => 0,
					'tipoid' => 4,
					'color' => 'info',
					'nueva' => 1,
					'fecha' => $fecha->format('Y-m-d H:i'),
					'pacienteid' => $pacienteId,
					'cuerpo'=> 'The user <b>'.$usuarioLogin[0]->name.'</b> added a new note in the questionnaire of '.$data[0]->paciente.'.');
					$this->db->insert('notificaciones',$notificacion);
				}
			}
		}
		
		public function cuestionario_status($id,$pacienteId,$userlogin){
			$this->db->select('CONCAT(usuario.nombre," ",usuario.apellidos) as name');
			$this->db->from('usuario');
			$this->db->where('usuario.id',$userlogin);
			$usuarioLogin = $this->db->get()->result();
			
			$this->db->select('usuario.id');
			$this->db->from('usuario');
			$this->db->where_in('usuario.tipoid',array(1));
			$usuarios = $this->db->get()->result_array();
			
			
			$this->db->select('cuestionario_encabezado.estado,
			cuestionario_encabezado.usuario,
			cuestionario_encabezado.doctor,
			CONCAT(patient.LName,patient.FName) as paciente');
			$this->db->from('cuestionario_encabezado');
			$this->db->join('patient','cuestionario_encabezado.patientid = patient.Id','inner');
			$this->db->where('cuestionario_encabezado.id',$id);
			$data = $this->db->get()->result();
			if($data[0]->usuario != ''){ $usuarios[] = array('id' => $data[0]->usuario); }
			if($data[0]->doctor != ''){ $usuarios[] = array('id' => $data[0]->doctor); }
			
			if($data[0]->estado == 0){$color = "text-primary"; }
			else if($data[0]->estado == 1){$color = "text-success"; }
			else if($data[0]->estado == 2){$color = "text-warning"; }
			else if($data[0]->estado == 3){$color = "text-danger"; }
			
			foreach($usuarios as $usuario){
				if($usuario['id'] != $userlogin){
					$msg = 'The user <b>'.$usuarioLogin[0]->name.'</b> change status to <i class="'.$color.' fa fa-square" aria-hidden="true"></i> in the questionnaire of '.$data[0]->paciente.'.';
					$fecha = new DateTime();
					$notificacion = array(
					'usuarioid'=> $usuario['id'],
					'leida' => 0,
					'tipoid' => 4,
					'color' => 'info',
					'nueva' => 1,
					'fecha' => $fecha->format('Y-m-d H:i'),
					'pacienteid' => $pacienteId,
					'cuerpo'=> $msg);
					$this->db->insert('notificaciones',$notificacion);
					$id = $this->db->insert_id();
					
					$pushData = array(
						'id' => $id,
						'type'=> "information",
						'title'=>"You have new a notifation.",
						"noti" => $notificacion);
					$pusher = $this->ci_pusher->get_pusher();
					$event = $pusher->trigger('notifications',MD5($usuario['id']),$pushData);
				}
			}
		}
	#endregion
	
	public function crear($reporteid,$mensaje,$color,$paciente = null,$oferta = null,$tipo){
		$fecha = new DateTime();
		$usuarios = $this->leer_usuarios(1);
		foreach($usuarios as $usuario){
			$notificacion = array(
			'usuarioid'=> $usuario->id,
			'leida' => 0,
			'tipoid' => $tipo,
			'color' => $color,
			'fecha' => $fecha->format('Y-m-d H:i'),
			'pacienteid' => $paciente,
			'ofertaid' => $oferta,
			'cuerpo'=>$mensaje);
			$this->db->insert('notificaciones',$notificacion);
			$id = $this->db->insert_id();
			$error = $this->db->error();
			if($error['code'] == 0){
				$pushData = array(
				'id' => $id,
				'type'=> "information",
				'title'=>"You have new a notifation.",
				"noti" => $notificacion);
				$pusher = $this->ci_pusher->get_pusher();
				$event = $pusher->trigger('notifications',MD5($usuario->id),$pushData);
			}
		}
		return $error;
	}

	public function leer_usuarios($reporte_id){
		$this->db->select('usuario.id');
		$this->db->from('notificaciones_reportes_usuarios');
		$this->db->join('usuario_tipo','notificaciones_reportes_usuarios.usuarioTipoid = usuario_tipo.id','inner');
		$this->db->join('usuario','usuario.tipoid = usuario_tipo.id','inner');
		$this->db->where('notificaciones_reportes_usuarios.reporteid',$reporte_id);
		$usuarios = $this->db->get()->result();
		return $usuarios;
	}
}?>