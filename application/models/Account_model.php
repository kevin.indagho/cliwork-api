<?php
Class Account_model extends CI_Model{
	
	public function valid_tp_incomple($patientid){
		$this->db->select('treatplan.PatNum');
		$this->db->from('treatplan');
		$this->db->where('treatplan.TPStatus',1);
		$this->db->where('treatplan.Complete',0);
		$this->db->where('treatplan.PatNum',$patientid);
		$data = $this->db->get()->result();
		return $data;
	}
	
	public function paymentplan($planid){
		$this->db->select('pagos_plan_encabezado.id,
		pagos_plan_encabezado.fecha,
		pagos_plan_encabezado.usuario,
		pagos_plan_encabezado.pacienteid,
		pagos_plan_encabezado.comision,
		pagos_plan_encabezado.deuda,
		pagos_plan_encabezado.pago,
		pagos_plan_encabezado.duracion,
		pagos_plan_encabezado.tipo,
		pagos_plan_encabezado.fechaPago,
		pagos_plan_encabezado.activo');
		$this->db->from('pagos_plan_encabezado');
		$this->db->where('pagos_plan_encabezado.id',$planid);
		$data = $this->db->get()->result();
		foreach($data as $plan){
			$this->db->select('pagos_plan_fechas.id,
			pagos_plan_fechas.encabezadoid,
			pagos_plan_fechas.fecha,
			pagos_plan_fechas.completo,
			IFNULL(Sum(pagos_plan_fechas_split.monto),0) as pagado');
			$this->db->from('pagos_plan_fechas');
			$this->db->join('pagos_plan_fechas_split','pagos_plan_fechas_split.fechaid = pagos_plan_fechas.id','left');
			$this->db->where('pagos_plan_fechas.encabezadoid',$plan->id);
			$this->db->group_by('pagos_plan_fechas.id');
			$plan->fechas = $this->db->get()->result();
		}
		return $data;
	}	
	
	public function playmenta_active($patientid){
		$this->db->select('pagos_plan_encabezado.id,
		pagos_plan_encabezado.fecha,
		pagos_plan_encabezado.usuario,
		pagos_plan_encabezado.pacienteid,
		pagos_plan_encabezado.comision,
		pagos_plan_encabezado.deuda,
		pagos_plan_encabezado.pago,
		pagos_plan_encabezado.duracion,
		pagos_plan_encabezado.tipo,
		pagos_plan_encabezado.fechaPago,
		pagos_plan_encabezado.activo');
		$this->db->from('pagos_plan_encabezado');
		$this->db->where('pagos_plan_encabezado.activo',1);
		$this->db->where('pagos_plan_encabezado.completo',0);
		$this->db->where('pagos_plan_encabezado.pacienteid',$patientid);
		$data = $this->db->get()->result();
		foreach($data as $plan){
			$this->db->select('pagos_plan_fechas.id,
			pagos_plan_fechas.encabezadoid,
			pagos_plan_fechas.fecha,
			pagos_plan_fechas.completo,
			IFNULL(Sum(pagos_plan_fechas_split.monto),0) as pagado');
			$this->db->from('pagos_plan_fechas');
			$this->db->join('pagos_plan_fechas_split','pagos_plan_fechas_split.fechaid = pagos_plan_fechas.id','left');
			$this->db->where('pagos_plan_fechas.encabezadoid',$plan->id);
			$this->db->group_by('pagos_plan_fechas.id');
			$plan->fechas = $this->db->get()->result();
		}
		return $data;
	}
	
	public function playmentplan_complete($planid){
		$this->db->where('id',$planid);
		$this->db->update('pagos_plan_encabezado',array('completo' => 1));
		$error = $this->db->error();
		return $error;
	}
	
	public function playmentplan_add($newPaymentPlan,$fechas,$planid){
		$this->db->insert('pagos_plan_encabezado',$newPaymentPlan);
		$error = $this->db->error();
		$id = $this->db->insert_id();
		if($error['code'] == 0){
			$this->db->where('id',$planid);
			$this->db->update('pagos_plan_encabezado',array('activo'=>0));			
			foreach($fechas as $fecha){
				$fecha = array(
				'encabezadoid' => $id,
				'fecha' => $fecha['fecha'],
				'monto' => $fecha['monto'],
				'completo' => 0);
				$this->db->insert('pagos_plan_fechas',$fecha);
			}
		}
		return $error;
	}
	
	public function playmentplan_details($planid){
		$this->db->select('pagos_plan_encabezado.fecha,
		CONCAT(patient.LName," ",patient.FName) as cliente,
		pagos_plan_encabezado.comision,
		pagos_plan_encabezado.deuda,
		pagos_plan_encabezado.pago,
		pagos_plan_encabezado.tipo,
		pagos_plan_encabezado.id,
		patient.Address,
		patient.City,
		estados.nombre as state,
		patient.Email');
		$this->db->from('pagos_plan_encabezado');
		$this->db->join('patient','pagos_plan_encabezado.pacienteid = patient.Id','inner');
		$this->db->join('estados','estados.id = patient.State','left');
		$this->db->where('pagos_plan_encabezado.id',$planid);
		$data['encabezado'] = $this->db->get()->result();
		
		$this->db->select('pagos_plan_fechas.fecha,
		pagos_plan_fechas.monto,
		Sum(pagos_plan_fechas_split.monto) as pagado');
		$this->db->from('pagos_plan_fechas');
		$this->db->join('pagos_plan_fechas_split','pagos_plan_fechas_split.fechaid = pagos_plan_fechas.id','inner');
		$this->db->where('pagos_plan_fechas.encabezadoid',$planid);
		$this->db->group_by('pagos_plan_fechas.id');
		$data['fechas'] = $this->db->get()->result();
		return $data;
	}
	
	public function adjusment_add($newAdjustment){
		$this->db->insert('ajustes',$newAdjustment);
		$error = $this->db->error();
		return $error;
	}
	
	public function payment_read($paymentid){
		$this->db->select('pagos.fecha as date,
		pagos.id,
		pagos.monto,
		pagos.notas,
		pagos.doctor,
		pagos.tipo,
		pagos.archivo,
		pagos.usuarioid,
		pagos.treatplanid,
		pagos.patientid');
		$this->db->from('pagos');
		$this->db->where('pagos.id',$paymentid);
		$data = $this->db->get()->result();
		return $data;
	}
	
	public function payment_add($newPayment,$patient){
		$this->db->insert('pagos',$newPayment);
		$pagoid = $this->db->insert_id();
		$error = $this->db->error();
		if($error['code'] == 0){
			$this->validate_pay_planpagos($newPayment,$pagoid,$patient);
			$this->db->select('treatplan.id');
			$this->db->from('treatplan');
			$this->db->where('treatplan.TPStatus',1);
			$this->db->where('treatplan.PatNum',$patient);
			$this->db->where('treatplan.Complete',0);
			$tps = $this->db->get()->result();
			foreach($tps as $tp){
				$this->db->insert('treatplan_pagos',array('pagoid' => $pagoid ,'treatplanid' => $tp->id));
			}
		}
		return $error;
	}
	
	public function payment_upd($id,$payment){
		$this->db->where('id',$id);
		$this->db->update('pagos',$payment);
		$error = $this->db->error();
		return $error;
	}
	
	public function account_list($tpid,$patient,$status){
		$itemList = array();
		$this->db->select('pagos_plan_encabezado.id,
		pagos_plan_encabezado.fecha as date,
		DATE_FORMAT(pagos_plan_encabezado.fecha,"%Y-%m-%d") as dateFormat,
		pagos_plan_encabezado.comision,
		pagos_plan_encabezado.deuda,
		CONCAT(usuario.nombre," ",usuario.apellidos) as user');
		$this->db->from('pagos_plan_encabezado');
		$this->db->join('usuario','pagos_plan_encabezado.usuario = usuario.id','inner');
		$this->db->where('pagos_plan_encabezado.activo',1);
		$this->db->where('pagos_plan_encabezado.pacienteid',$patient);
		$planPagos = $this->db->get()->result();
		foreach($planPagos as $plan){
			$plan->TypeItem = "Comis";
		}
		
		$itemList = array_merge($itemList,$planPagos);
		
		$this->db->select('GROUP_CONCAT(treatplan.id) as id');
		$this->db->from('treatplan');
		$this->db->where('treatplan.PatNum',$patient);
		$this->db->where('treatplan.TPStatus',1);
		$this->db->where('treatplan.Complete',0);
		$tp = $this->db->get()->result();
		$tp = explode(',',$tp[0]->id);
		
		$this->db->select('procedurelog.Id,
		procedurelog.ProcDate as date,
		DATE_FORMAT(procedurelog.ProcDate,"%Y-%m-%d") as dateFormat,
		procedurecode.ProcCode,
		procedurecode.Descript,
		procedurecode.AbbrDesc,
		procedurelog.ToothNum,
		procedurelog.ProcFee,
		procedurelog.Pay,
		treatplan.Heading,
		CONCAT(provider.LName," ",provider.FName) as Doctor');
		$this->db->from('procedurelog');
		$this->db->join('treatplanattach','treatplanattach.ProcNum = procedurelog.Id','inner');
		$this->db->join('procedurecode','procedurelog.CodeNum = procedurecode.id','inner');
		$this->db->join('provider','procedurelog.ProvNum = provider.id','inner');
		$this->db->join('treatplan','treatplanattach.TreatPlanNum = treatplan.id','inner');
		$this->db->where_in('treatplanattach.TreatPlanNum',$tp);
		if($status == "2"){ $this->db->where('procedurelog.ProcStatus',2); }
		$procedures = $this->db->get()->result();
		foreach($procedures as $procedure){
			$procedure->TypeItem = "proc";
			$procedure->ProcFeeFormat = number_format($procedure->ProcFee,2);
		}
		$itemList = array_merge($itemList,$procedures);
		
		$this->db->select('pagos.fecha as date,
		DATE_FORMAT(pagos.fecha,"%Y-%m-%d") as dateFormat,
		pagos.id,
		pagos.monto,
		pagos.notas,
		pagos.archivo,
		pagos.usuarioid,
		pagos.treatplanid,
		pagos.patientid,
		definition.ItemName as TypeName,
		CONCAT(usuario.nombre," ",usuario.apellidos) as user');
		$this->db->from('pagos');
		$this->db->join('treatplan_pagos','treatplan_pagos.pagoid = pagos.id','inner');
		$this->db->join('definition','pagos.tipo = definition.DefNum','inner');
		$this->db->join('usuario','pagos.usuarioid = usuario.id','inner');
		$this->db->where_in('treatplan_pagos.treatplanid',$tp);
		$this->db->group_by('pagos.id');
		$paymentsSplit = $this->db->get()->result();
		foreach($paymentsSplit as $paymentSplit){
			$paymentSplit->TypeItem = "pay";
			$paymentSplit->montoFormato = number_format($paymentSplit->monto,2);
			//$payments[] = $paymentSplit;
		}
		$itemList = array_merge($itemList,$paymentsSplit);
		
		$this->db->select('ajustes.id,
			ajustes.fecha as date,
			DATE_FORMAT(ajustes.fecha,"%Y-%m-%d") as dateFormat,
			ajustes.monto,
			ajustes.tipo,
			ajustes.notas,
			ajustes.pacienteid,
			CONCAT(usuario.nombre," ",usuario.apellidos) as user,
			treatplan.Heading,
			definition.ItemName,
			definition.ItemValue');
			$this->db->from('ajustes');
			$this->db->join('usuario','ajustes.usuarioid = usuario.id','inner');
			$this->db->join('definition','ajustes.tipo = definition.DefNum','inner');
			$this->db->join('treatplan','ajustes.treatplanid = treatplan.id','inner');
			$this->db->where_in('ajustes.treatplanid',$tp);			
			$adjustments = $this->db->get()->result();
			foreach($adjustments as $adjustment){
				$adjustment->TypeItem = "adjust";
				$adjustment->montoFormato = number_format($adjustment->monto,2);
			}
		
			$itemList = array_merge($itemList,$adjustments);
			$keys = array_column($itemList, 'date');
			//array_multisort($keys, SORT_ASC, $itemList);			
		return $itemList;
	}

	public function tp_details($tpid,$patient,$status){
		$itemList = array();
		$this->db->select('pagos_plan_encabezado.id,
		pagos_plan_encabezado.fecha as date,
		DATE_FORMAT(pagos_plan_encabezado.fecha,"%Y-%m-%d") as dateFormat,
		pagos_plan_encabezado.comision,
		pagos_plan_encabezado.deuda,
		CONCAT(usuario.nombre," ",usuario.apellidos) as user');
		$this->db->from('pagos_plan_encabezado');
		$this->db->join('usuario','pagos_plan_encabezado.usuario = usuario.id','inner');
		$this->db->where('pagos_plan_encabezado.activo',1);
		$this->db->where('pagos_plan_encabezado.pacienteid',$patient);
		$planPagos = $this->db->get()->result();
		foreach($planPagos as $plan){
			$plan->TypeItem = "Comis";
		}
		
		$itemList = array_merge($itemList,$planPagos);
		
		/*$this->db->select('GROUP_CONCAT(treatplan.id) as id');
		$this->db->from('treatplan');
		$this->db->where('treatplan.PatNum',$patient);
		$this->db->where('treatplan.TPStatus',1);
		$this->db->where('treatplan.Complete',0);
		$tp = $this->db->get()->result();
		$tp = explode(',',$tp[0]->id);*/
		$tp = array($tpid);
		
		$this->db->select('procedurelog.Id,
		procedurelog.ProcDate as date,
		DATE_FORMAT(procedurelog.ProcDate,"%Y-%m-%d") as dateFormat,
		procedurecode.ProcCode,
		procedurecode.Descript,
		procedurecode.AbbrDesc,
		procedurelog.ToothNum,
		procedurelog.ProcFee,
		procedurelog.Pay,
		treatplan.Heading,
		CONCAT(provider.LName," ",provider.FName) as Doctor');
		$this->db->from('procedurelog');
		$this->db->join('treatplanattach','treatplanattach.ProcNum = procedurelog.Id','inner');
		$this->db->join('procedurecode','procedurelog.CodeNum = procedurecode.id','inner');
		$this->db->join('provider','procedurelog.ProvNum = provider.id','LEFT');
		$this->db->join('treatplan','treatplanattach.TreatPlanNum = treatplan.id','inner');
		$this->db->where_in('treatplanattach.TreatPlanNum',$tp);
		//if($status == "2"){ $this->db->where('procedurelog.ProcStatus',2); }
		$procedures = $this->db->get()->result();
		foreach($procedures as $procedure){
			$procedure->TypeItem = "proc";
			$procedure->ProcFeeFormat = number_format($procedure->ProcFee,2);
		}
		$itemList = array_merge($itemList,$procedures);
		
		$this->db->select('pagos.fecha as date,
		DATE_FORMAT(pagos.fecha,"%Y-%m-%d") as dateFormat,
		pagos.id,
		pagos.monto,
		pagos.notas,
		pagos.archivo,
		pagos.usuarioid,
		pagos.treatplanid,
		pagos.patientid,
		definition.ItemName as TypeName,
		CONCAT(usuario.nombre," ",usuario.apellidos) as user');
		$this->db->from('pagos');
		$this->db->join('treatplan_pagos','treatplan_pagos.pagoid = pagos.id','inner');
		$this->db->join('definition','pagos.tipo = definition.DefNum','inner');
		$this->db->join('usuario','pagos.usuarioid = usuario.id','inner');
		$this->db->where_in('treatplan_pagos.treatplanid',$tp);
		$this->db->group_by('pagos.id');
		$paymentsSplit = $this->db->get()->result();
		foreach($paymentsSplit as $paymentSplit){
			$paymentSplit->TypeItem = "pay";
			$paymentSplit->montoFormato = number_format($paymentSplit->monto,2);
			//$payments[] = $paymentSplit;
		}
		$itemList = array_merge($itemList,$paymentsSplit);
		
		$this->db->select('ajustes.id,
			ajustes.fecha as date,
			DATE_FORMAT(ajustes.fecha,"%Y-%m-%d") as dateFormat,
			ajustes.monto,
			ajustes.tipo,
			ajustes.notas,
			ajustes.pacienteid,
			CONCAT(usuario.nombre," ",usuario.apellidos) as user,
			treatplan.Heading,
			definition.ItemName,
			definition.ItemValue');
			$this->db->from('ajustes');
			$this->db->join('usuario','ajustes.usuarioid = usuario.id','inner');
			$this->db->join('definition','ajustes.tipo = definition.DefNum','inner');
			$this->db->join('treatplan','ajustes.treatplanid = treatplan.id','inner');
			$this->db->where_in('ajustes.treatplanid',$tp);			
			$adjustments = $this->db->get()->result();
			foreach($adjustments as $adjustment){
				$adjustment->TypeItem = "adjust";
				$adjustment->montoFormato = number_format($adjustment->monto,2);
			}
		
			$itemList = array_merge($itemList,$adjustments);
			$keys = array_column($itemList, 'date');
			array_multisort($keys, SORT_ASC, $itemList);			
		return $itemList;
	}

	public function account_total($tpid,$patient){
		$data = array(
			'allSubTotal' => 0,
			'allTotal' => 0,
			'allPayments' => 0,
			'alladitions' => 0,
			'alldiscounts' => 0,
			'allDue' => 0,);
			
		$this->db->select('pagos_plan_encabezado.id,
		pagos_plan_encabezado.comision,
		pagos_plan_encabezado.deuda');
		$this->db->from('pagos_plan_encabezado');
		$this->db->where('pagos_plan_encabezado.activo',1);
		$this->db->where('pagos_plan_encabezado.pacienteid',$patient);
		$planPagos = $this->db->get()->result();
		
		$this->db->select('ajustes.monto,definition.ItemValue');
		$this->db->from('ajustes');
		$this->db->join('definition','ajustes.tipo = definition.DefNum','inner');
		$this->db->join('treatplan','ajustes.treatplanid = treatplan.id','inner');
		$this->db->where('treatplan.TPStatus',1);
		$this->db->where('treatplan.Complete',0);
		$this->db->where('treatplan.PatNum',$patient);
		$ajustes = $this->db->get()->result();
		
		foreach($ajustes as $item){
			if($item->ItemValue == "+"){
				$data['alladitions'] = (isset($item->monto) == true ? $item->monto : 0);
			}
			if($item->ItemValue == "-"){
				$data['alldiscounts'] = (isset($item->monto) == true ? $item->monto : 0);
			}
		}
		
		$this->db->select('SUM(procedurelog.ProcFee) as amount');
		$this->db->from('procedurelog');
		$this->db->join('treatplanattach','treatplanattach.ProcNum = procedurelog.Id','inner');
		$this->db->join('treatplan','treatplanattach.TreatPlanNum = treatplan.id','inner');
		$this->db->where('treatplan.TPStatus',1);
		$this->db->where('treatplan.Complete',0);
		$this->db->where('procedurelog.ProcStatus',2);
		$this->db->where('procedurelog.PatNum',$patient);
		$allSubTotal = $this->db->get()->result();
		$allSubTotal = $allSubTotal[0]->amount;
		$data['allSubTotal'] = number_format($allSubTotal,2);
		
		$this->db->select('pagos.monto');
		$this->db->from('treatplan_pagos');
		$this->db->join('pagos','treatplan_pagos.pagoid = pagos.id','inner');
		$this->db->join('treatplan','treatplan_pagos.treatplanid = treatplan.id','inner');
		$this->db->where('treatplan.TPStatus',1);
		$this->db->where('treatplan.Complete',0);
		$this->db->where('treatplan.PatNum',$patient);
		$this->db->group_by('pagos.id');
		$pays = $this->db->get()->result();
		$totalPayment = 0;
		foreach($pays as $pay){
			$totalPayment += $pay->monto;
		}
		
		$data['allPayments'] = number_format($totalPayment,2);
		$data['allPaymentsPercent'] = ($allSubTotal > 0 ? number_format(($totalPayment*100)/$allSubTotal,0) : 0);
		$comision = (count($planPagos) > 0 ? ($planPagos[0]->comision * $planPagos[0]->deuda)/100 : 0);
		$allTotal = $allSubTotal + $data['alladitions'] - $data['alldiscounts'] + $comision;
		$allDue = $allTotal - $totalPayment;
		$data['allTotal'] = number_format($allTotal,2);
		$data['allDue'] = number_format($allDue,2);
		$data['allDuePercent'] = ($allSubTotal > 0 ? str_replace('-','+',number_format(($allDue*100)/$allSubTotal,0)) : 0);
		
		if($allDue == 0){
			$this->db->select('treatplan.id');
			$this->db->from('treatplan');
			$this->db->where('treatplan.PatNum',$patient);
			$this->db->where('treatplan.TPStatus',1);
			$this->db->where('treatplan.Complete',0);
			$tpIncomplete = $this->db->get()->result();
			foreach($tpIncomplete as $tpid){
				$this->db->where('treatplan.id',$tpid->id);
				$this->db->update('treatplan',array('treatplan.Complete'=>1));
			}
		}else{
			$this->db->select('treatplan.id');
			$this->db->from('treatplan');
			$this->db->where('treatplan.PatNum',$patient);
			$this->db->where('treatplan.TPStatus',1);
			$this->db->where('treatplan.Complete',0); 
			$tpIncomplete = $this->db->get()->result();
			foreach($tpIncomplete as $tpid){
				$this->db->where('treatplan.id',$tpid->id);
				$this->db->update('treatplan',array('treatplan.Complete'=> 0));
			}
		}
		
		return $data;
	}

	public function account_balance($patId){
		
		$this->db->select('pagos_plan_encabezado.id,
		pagos_plan_encabezado.comision,
		pagos_plan_encabezado.deuda');
		$this->db->from('pagos_plan_encabezado');
		$this->db->where('pagos_plan_encabezado.activo',1);
		$this->db->where('pagos_plan_encabezado.pacienteid',$patId);
		$planPagos = $this->db->get()->result();
		
		
		$this->db->select('GROUP_CONCAT(treatplan.id) as tp');
		$this->db->from('treatplan');
		$this->db->where('treatplan.PatNum',$patId);
		$this->db->where('treatplan.Complete',0);
		$treatplans = $this->db->get()->result();
		$tp = explode(',',$treatplans[0]->tp);
		
		
		$this->db->select('SUM(procedurelog.ProcFee) as amount');
		$this->db->from('treatplanattach');
		$this->db->join('procedurelog','treatplanattach.ProcNum = procedurelog.Id','inner');
		$this->db->where('procedurelog.ProcStatus',2);
		$this->db->where_in('treatplanattach.TreatPlanNum',$tp);
		$subtotal = $this->db->get()->result();
		$data['subtotal']['value'] = $subtotal[0]->amount;
		$data['subtotal']['format'] = number_format($subtotal[0]->amount,2);
		
		$this->db->select('definition.ItemValue,SUM(ajustes.monto) as amount');
		$this->db->from('ajustes');
		$this->db->join('definition','ajustes.tipo = definition.DefNum','inner');
		$this->db->where_in('ajustes.treatplanid',$tp);
		$this->db->group_by('definition.ItemValue');
		$adjust = $this->db->get()->result();
		
		$data['adjust']['aditions']['value'] = 0;
		$data['adjust']['aditions']['format'] = 0;
		$data['adjust']['discounts']['value'] = 0;
		$data['adjust']['discounts']['format'] = 0;
		foreach($adjust as $item){
			if($item->ItemValue == "+"){
				$data['adjust']['aditions']['value'] = (isset($item->amount) == true ? $item->amount : 0);
				$data['adjust']['aditions']['format'] = (isset($item->amount) == true ? number_format($item->amount,2) : 0);
			}
			if($item->ItemValue == "-"){
				$data['adjust']['discounts']['value'] = (isset($item->amount) == true ? $item->amount : 0);
				$data['adjust']['discounts']['format'] = (isset($item->amount) == true ? number_format($item->amount,2) : 0);
			}
		}
		
		$comision = ($planPagos[0]->comision * $planPagos[0]->deuda)/100;
		$total = $data['subtotal']['value'] + $data['adjust']['aditions']['value'] - $data['adjust']['discounts']['value'] + $comision;
		
		
		$data['comision']['value'] = $comision;
		$data['total']['value'] = $total;
		$data['total']['format'] = number_format($total,2);
		
		$this->db->select('pagos.monto');
		$this->db->from('treatplan_pagos');
		$this->db->join('pagos','treatplan_pagos.pagoid = pagos.id','inner');
		$this->db->where_in('treatplan_pagos.treatplanid',$tp);
		$this->db->group_by('pagos.id');
		$pays = $this->db->get()->result();
		$paytotal = 0;
		foreach($pays as $pay){
			$paytotal += $pay->monto;
		}
		$data['pay']['value'] = $paytotal;
		$data['pay']['format'] = number_format($paytotal,2);
		
		return $data;
	}

	public function validate_pay_planpagos($newPayment,$pagoid,$patient){
		$fecha = new DateTime();
		$pagoMonto = $newPayment['monto'];		
		$this->db->select('pagos_plan_encabezado.id as planid,
		pagos_plan_fechas.fecha,
		pagos_plan_fechas.monto,
		pagos_plan_fechas.completo,
		pagos_plan_fechas.id');
		$this->db->from('pagos_plan_encabezado');
		$this->db->join('pagos_plan_fechas','pagos_plan_fechas.encabezadoid = pagos_plan_encabezado.id','inner');
		$this->db->where('pagos_plan_encabezado.pacienteid',$patient);
		$this->db->where('pagos_plan_encabezado.activo',1);
		//$this->db->where('pagos_plan_fechas.fecha',$fecha->format('Y-m-d'));
		$this->db->where('pagos_plan_fechas.completo',0);
		$fechas = $this->db->get()->result();
		foreach($fechas as $fecha){
			
			$this->db->select('SUM(pagos_plan_fechas_split.monto) as total');
			$this->db->from('pagos_plan_fechas_split');
			$this->db->where('pagos_plan_fechas_split.fechaid',$fecha->id);
			$pagado = $this->db->get()->result();			
			
			$fechaPago = $fecha->monto - $pagado[0]->total;
			$completo = false;
			if($pagoMonto > 0){
				if($pagoMonto > $fechaPago){
					$completo = true;
					$split = $fechaPago;
					$pagoMonto = $pagoMonto - $fechaPago;
				}else if($pagoMonto == $fechaPago){
					$completo = true;
					$split = $fechaPago;
					$pagoMonto = 0;
				}else{
					$split = $pagoMonto;
					$pagoMonto = 0;
				}
				$pagoSplit = array(
				'pagoid' => $pagoid,
				'fechaid' => $fecha->id,
				'monto' => $split);
				$this->db->insert('pagos_plan_fechas_split',$pagoSplit);
			}
			
			if($completo == true){
				$this->db->where('pagos_plan_fechas.id',$fecha->id);
				$this->db->update('pagos_plan_fechas',array('pagos_plan_fechas.completo'=>1));
			}
		}
	}

	public function account_pdf($patient){
		
		$itemList = array();
		$this->db->select('pagos_plan_encabezado.id,
		pagos_plan_encabezado.fecha as date,
		DATE_FORMAT(pagos_plan_encabezado.fecha,"%Y-%m-%d") as dateFormat,
		pagos_plan_encabezado.comision,
		pagos_plan_encabezado.deuda,
		CONCAT(usuario.nombre," ",usuario.apellidos) as user');
		$this->db->from('pagos_plan_encabezado');
		$this->db->join('usuario','pagos_plan_encabezado.usuario = usuario.id','inner');
		$this->db->where('pagos_plan_encabezado.activo',1);
		$this->db->where('pagos_plan_encabezado.pacienteid',$patient);
		$planPagos = $this->db->get()->result();
		foreach($planPagos as $plan){
			$plan->status = "2";
			$plan->TypeItem = "Comis";
		}
		
		$itemList = array_merge($itemList,$planPagos);
		
		$this->db->select('GROUP_CONCAT(treatplan.id) as id');
		$this->db->from('treatplan');
		$this->db->where('treatplan.PatNum',$patient);
		$tp = $this->db->get()->result();
		$tp = explode(',',$tp[0]->id);
		
		$this->db->select('procedurelog.Id,
		procedurelog.DateComplete as date,
		DATE_FORMAT(procedurelog.ProcDate,"%Y-%m-%d") as dateFormat,
		procedurecode.ProcCode,
		procedurecode.Descript,
		procedurecode.AbbrDesc,
		procedurelog.ToothNum,
		procedurelog.Priority,
		procedurelog.DateComplete,
		procedurelog.Surf,
		procedurelog.ProcFee,
		procedurelog.Pay,
		treatplan.Heading,
		CONCAT(provider.LName," ",provider.FName) as Doctor');
		$this->db->from('procedurelog');
		$this->db->join('treatplanattach','treatplanattach.ProcNum = procedurelog.Id','inner');
		$this->db->join('procedurecode','procedurelog.CodeNum = procedurecode.id','inner');
		$this->db->join('provider','procedurelog.ProvNum = provider.id','inner');
		$this->db->join('treatplan','treatplanattach.TreatPlanNum = treatplan.id','inner');
		$this->db->where_in('treatplanattach.TreatPlanNum',$tp);
		$this->db->where('procedurelog.ProcStatus',1);
		$proceduresUnComplete = $this->db->get()->result();
		foreach($proceduresUnComplete as $procedure){
			$procedure->ProcFeeFormat = number_format($procedure->ProcFee,2);
		}
		
		$this->db->select('Sum(procedurelog.ProcFee) as amount');
		$this->db->from('treatplan');
		$this->db->join('treatplanattach','treatplanattach.TreatPlanNum = treatplan.id','inner');
		$this->db->join('procedurelog','treatplanattach.ProcNum = procedurelog.Id','inner');
		$this->db->where('procedurelog.ProcStatus',2);
		$this->db->where_in('treatplan.id',$tp);
		$total = $this->db->get()->result();
		
		$this->db->select('procedurelog.Id,
		procedurelog.DateComplete as date,
		DATE_FORMAT(procedurelog.ProcDate,"%Y-%m-%d") as dateFormat,
		procedurecode.ProcCode,
		procedurecode.Descript,
		procedurecode.AbbrDesc,
		procedurelog.ToothNum,
		procedurelog.Priority,
		procedurelog.DateComplete,
		procedurelog.Surf,
		procedurelog.ProcFee,
		procedurelog.Pay,
		treatplan.Heading,
		CONCAT(provider.LName," ",provider.FName) as Doctor');
		$this->db->from('procedurelog');
		$this->db->join('treatplanattach','treatplanattach.ProcNum = procedurelog.Id','inner');
		$this->db->join('procedurecode','procedurelog.CodeNum = procedurecode.id','inner');
		$this->db->join('provider','procedurelog.ProvNum = provider.id','inner');
		$this->db->join('treatplan','treatplanattach.TreatPlanNum = treatplan.id','inner');
		$this->db->where_in('treatplanattach.TreatPlanNum',$tp);
		$this->db->where('procedurelog.ProcStatus',2);
		$procedures = $this->db->get()->result();
		foreach($procedures as $procedure){
			$procedure->status = "2";
			$procedure->TypeItem = "proc";
			$procedure->ProcFeeFormat = number_format($procedure->ProcFee,2);
		}
		$itemList = array_merge($itemList,$procedures);
		
		$this->db->select('pagos.fecha as date,
		DATE_FORMAT(pagos.fecha,"%Y-%m-%d") as dateFormat,
		pagos.id,
		pagos.monto,
		pagos.notas,
		pagos.archivo,
		pagos.usuarioid,
		pagos.treatplanid,
		pagos.patientid,
		definition.ItemName as TypeName,
		CONCAT(usuario.nombre," ",usuario.apellidos) as user');
		$this->db->from('pagos');
		$this->db->join('treatplan_pagos','treatplan_pagos.pagoid = pagos.id','inner');
		$this->db->join('definition','pagos.tipo = definition.DefNum','inner');
		$this->db->join('usuario','pagos.usuarioid = usuario.id','inner');
		$this->db->where_in('treatplan_pagos.treatplanid',$tp);
		$this->db->group_by('pagos.id');
		$paymentsSplit = $this->db->get()->result();
		foreach($paymentsSplit as $paymentSplit){
			$paymentSplit->status = "2";
			$paymentSplit->TypeItem = "pay";
			$paymentSplit->montoFormato = number_format($paymentSplit->monto,2);
			//$payments[] = $paymentSplit;
		}
		$itemList = array_merge($itemList,$paymentsSplit);
		
		$this->db->select('ajustes.id,
			ajustes.fecha as date,
			DATE_FORMAT(ajustes.fecha,"%Y-%m-%d") as dateFormat,
			ajustes.monto,
			ajustes.tipo,
			ajustes.notas,
			ajustes.pacienteid,
			CONCAT(usuario.nombre," ",usuario.apellidos) as user,
			treatplan.Heading,
			definition.ItemName,
			definition.ItemValue');
			$this->db->from('ajustes');
			$this->db->join('usuario','ajustes.usuarioid = usuario.id','inner');
			$this->db->join('definition','ajustes.tipo = definition.DefNum','inner');
			$this->db->join('treatplan','ajustes.treatplanid = treatplan.id','inner');
			$this->db->where_in('ajustes.treatplanid',$tp);			
			$adjustments = $this->db->get()->result();
			foreach($adjustments as $adjustment){
				$adjustment->status = "2";
				$adjustment->TypeItem = "adjust";
				$adjustment->montoFormato = number_format($adjustment->monto,2);
			}
		
			$itemList = array_merge($itemList,$adjustments);
			$keys = array_column($itemList, 'date');
			array_multisort($keys, SORT_ASC, $itemList);
			$data['list'] = $itemList;
			$data['total'] = $total;
			$data['uncomplete'] = $proceduresUnComplete;
		return $data;
	}

	function sort_by_orden ($a, $b) {
		return $a['date'] - $b['date'];
	}
}
?>