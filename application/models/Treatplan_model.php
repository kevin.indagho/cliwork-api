<?php
Class Treatplan_model extends CI_Model{
	
	#region CRUD
		public function create($newTP){
			$this->db->insert('treatplan',$newTP);
			$error = $this->db->error();
			return $error;
		}
		
		public function details($tpid){
			$this->db->select('treatplan.id,
			treatplan.TreatPlanNum,
			treatplan.PatNum,
			treatplan.DateTP,
			treatplan.Heading,
			treatplan.Note');
			$this->db->from('treatplan');
			$this->db->where('treatplan.id',$tpid);
			$data = $this->db->get()->result();
			foreach($data as $tp){
				$contador = 0;
				$this->db->select('procedurelog.Id,
				procedurelog.PlannedAptNum,
				procedurelog.AptNum');
				$this->db->from('procedurelog');
				$this->db->join('treatplanattach','treatplanattach.ProcNum = procedurelog.Id','inner');
				$this->db->where('treatplanattach.TreatPlanNum',$tp->id);
				$procedures = $this->db->get()->result();
				foreach($procedures as $proce){
					if($proce->AptNum == ""){
						$contador += 1;
					}
				}
				$tp->ProcAptPlanPendi = $contador;
			}
			return $data;
		}
		
		public function edit($tp,$id){
			$this->db->where('id',$id);
			$this->db->update('treatplan',$tp);
			$error = $this->db->error();
			return $error;
		}
		
		public function listtp($pat){
			$this->db->select('treatplan.id,
			treatplan.TreatPlanNum,
			treatplan.PatNum,
			treatplan.DateTP,
			treatplan.Complete,
			treatplan.Heading,
			treatplan.Note,
			treatplan.TPStatus');
			$this->db->from('treatplan');
			if($pat != 'none'){
				$this->db->where('treatplan.PatNum',$pat);
			}
			$data = $this->db->get()->result();
			foreach($data as $tp){
				$total = 0;
				$this->db->select('Sum(procedurelog.ProcFee) as mount');
				$this->db->from('treatplanattach');
				$this->db->join('procedurelog','treatplanattach.ProcNum = procedurelog.Id','inner');
				$this->db->where('treatplanattach.TreatPlanNum',$tp->id);
				$Subtotal = $this->db->get()->result();
				$tp->Subtotal = $Subtotal[0]->mount;
				$total = $Subtotal[0]->mount;
				$tp->SubtotalFormat = '<b class="text-warning">'.number_format($Subtotal[0]->mount,2).'</b>';
				
				
				$tp->payments = 'TEST';
				$tp->paymentsFormat = '<b class="text-success">TEST</b>';
		
				$this->db->select('Sum(ajustes.monto) as amount,definition.ItemValue');
				$this->db->from('ajustes');
				$this->db->join('definition','ajustes.tipo = definition.DefNum','inner');
				$this->db->where('ajustes.treatplanid',$tp->id);
				$this->db->group_by('definition.ItemValue');
				$adjusments = $this->db->get()->result();
				$tp->adjusments = $adjusments;
				$tp->adjusmentsFormat = '<b class="text-success">+0.00</b>/<b class="text-danger">-0.00</b>';
				
				foreach($adjusments as $i => $adj){
					if($i > 0){ $tp->adjusmentsFormat .= " / "; }
					
					if($adj->ItemValue == "+"){
						$total = $total + floatval($adj->amount);
						$clase = "text-success"; }
					else if($adj->ItemValue == "-"){ 
					$total = $total - floatval($adj->amount);
						$clase = "text-danger"; }
					else{ 
						$clase = "text-default"; 
					}
					$tp->adjusmentsFormat = '<b class="'.$clase.'">'.$adj->ItemValue."".number_format($adj->amount,2).'</b>';
				}
				$tp->total = $total;
				$tp->totalFormat = '<b class="text-info">'.number_format($total,2).'</b>';
		
			}
			return $data;
		}
	#endregion
	
	public function tp_pdf($tpid){
		$this->db->select('CONCAT(patient.LName," ",patient.FName) as cliente,
		patient.Address,
		patient.City,
		patient.Email,
		estados.nombre as state,
		treatplan.Heading,
		treatplan.DateTP');
		$this->db->from('treatplan');
		$this->db->join('patient','treatplan.PatNum = patient.Id','inner');
		$this->db->join('estados','patient.State = estados.id','left');
		$this->db->where('treatplan.id',$tpid);
		$data['encabezado'] = $this->db->get()->result();

		$this->db->select('procedurecode.CodeNum,
		procedurecode.ProcCode,
		procedurecode.Descript,
		procedurelog.DateComplete,
		procedurelog.ProcDate,
		DATE_FORMAT(procedurelog.ProcDate,"%Y-%m-%d") as dateFormat,
		procedurelog.ProcFee,
		procedurelog.Surf,
		procedurelog.ToothNum,
		procedurelog.Priority,
		procedurelog.ProcStatus,
		treatplanattach.TreatPlanNum,
		CONCAT(provider.LName," ",provider.FName) as Doctor');
		$this->db->from('treatplanattach');
		$this->db->join('procedurelog','treatplanattach.ProcNum = procedurelog.Id','left');
		$this->db->join('procedurecode','procedurelog.CodeNum = procedurecode.id','left');
		$this->db->join('provider','procedurelog.ProvNum = provider.id','left');
		$this->db->where('treatplanattach.TreatPlanNum',$tpid);
		$procedures = $this->db->get()->result();
		foreach($procedures as $item){
			
			$item->status = ($item->ProcStatus == 2? "X" : "");
			$item->ProcFeeFormat = number_format($item->ProcFee,2);
		}
		$data['procedures'] = $procedures;
		return $data;
	}
	
	public function tp_details_procedures($tp,$status){
		$total = 0;
		$this->db->select('procedurelog.Id,
		procedurecode.id as Proceid,
		procedurecode.CodeNum,
		procedurecode.ProcCode,
		procedurecode.Descript,
		procedurelog.ToothRange,
		procedurelog.ProcDate,
		procedurelog.ProcFee,
		procedurelog.ProcStatus,
		procedurelog.Surf,
		procedurelog.ToothNum,
		procedurelog.Notes,
		procedurelog.ProvNum,
		procedurelog.AptNum,
		procedurelog.Priority,
		procedurecode.isSurf,
		procedurecode.isCrown,
		procedurecode.isImplant,
		procedurecode.isQuarter,
		CONCAT(provider.LName," ",provider.FName) as provider');
		$this->db->from('procedurelog');
		$this->db->join('treatplanattach','treatplanattach.ProcNum = procedurelog.Id','inner');
		$this->db->join('procedurecode','procedurelog.CodeNum = procedurecode.id','inner');
		$this->db->join('provider','procedurelog.ProvNum = provider.id','LEFT');
		$this->db->where('treatplanattach.TreatPlanNum',$tp);
		if($status != "all"){
			$this->db->where('procedurelog.ProcStatus',$status);
		}		
		$data = $this->db->get()->result();
		foreach($data as $procedure){
			$procedure->Done = '';
			if($procedure->ProcStatus == 2){
				$procedure->Done = 'X';	
			}
			$this->db->select('feesched.Description,fee.Amount');
			$this->db->from('feesched');
			$this->db->join('fee','fee.FeeSched = feesched.id','inner');
			$this->db->where('fee.CodeNum',$procedure->Proceid);
			$procedure->fees = $this->db->get()->result();
		}
		return $data;
	}
	
	#region Procedimientos CRUD
		public function proc_attach($addProce,$tpid){
		$this->db->insert('procedurelog',$addProce);
		$error = $this->db->error();
		$id = $this->db->insert_id();
		$error['id'] = $id;
		
		$newAttach = array(
			'TreatPlanNum' => $tpid,
			'ProcNum' => $id);
		$this->db->insert('treatplanattach',$newAttach);
		
		return $error;
	}
		
		public function proc_edit($edit,$id){
			$this->db->where('Id',$id);
			$this->db->update('procedurelog',$edit);
			$error = $this->db->error();
			return $error;
		}
	
		public function proc_details($proceId){
			
		}
	#endregion
		
	public function procedure_add_appt($services,$AptId,$procupd){
		$this->db->select('procedurelog.Id');
		$this->db->from('procedurelog');
		$this->db->where('procedurelog.AptNum',$AptId);
		$procedures = $this->db->get()->result();
		foreach($procedures as $procedure){
			$this->db->where('Id',$procedure->Id);
			$this->db->update('procedurelog',array('AptNum'=> null));
		}
		if($services != ''){
			foreach($services as $service){
				$this->db->where('Id',$service);
				$this->db->update('procedurelog',array('AptNum'=>$AptId));
			}
		}
		if($procupd == 1){
			$this->db->select('Sum(procedurecode.Duration) as duration');
			$this->db->from('procedurelog');
			$this->db->join('procedurecode','procedurelog.CodeNum = procedurecode.id','inner');
			$this->db->where('procedurelog.AptNum',$AptId);
			$data = $this->db->get()->result();
			
			$patternFormat = "";
			$total = ($data[0]->duration/10);
			for($i = 1; $i <= $total; $i++){
				$patternFormat .='//';
			}
			$this->db->where('id',$AptId);
			$this->db->update('appointment',array('Pattern' => $patternFormat));
		}
		$error = $this->db->error();
		return $error;
	}
	
	public function tp_active($patient){
		$this->db->select('treatplan.id,treatplan.Heading,treatplan.Note,');
		$this->db->from('treatplan');
		$this->db->where('treatplan.TPStatus',1);
		$this->db->where('treatplan.PatNum',$patient);
		$data = $this->db->get()->result();
		return $data;		
	}

	public function fee_principal($proceid){
		$this->db->select('fee.Amount');
		$this->db->from('fee');
		$this->db->join('feesched','fee.FeeSched = feesched.id','inner');
		$this->db->where('feesched.principal',1);
		$this->db->where('fee.CodeNum',$proceid);
		$data = $this->db->get()->result();
		return $data;
	}
}
?>