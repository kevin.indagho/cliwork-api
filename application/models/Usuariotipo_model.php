<?php
class Usuariotipo_model extends CI_Model{

	public function lista($activo){
		$this->db->select('usuario_tipo.id,
		usuario_tipo.`name`,
		usuario_tipo.nombre,
		usuario_tipo.activo');
		$this->db->from('usuario_tipo');
		if($activo != 'none'){
			$this->db->where('activo',$activo);
		}
		$data = $this->db->get()->result();
		return $data;
	}
}
