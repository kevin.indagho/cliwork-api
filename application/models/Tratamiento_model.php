<?php 
class Tratamiento_model extends CI_Model{

	public function lista($activo){
		$this->db->select('tratamiento.id,
		tratamiento.`name`,
		tratamiento.nombre,
		tratamiento.activo,
		tratamiento.costo');
		$this->db->from('tratamiento');
		if($activo != "none"){
			$this->db->where('tratamiento.activo',$activo);
		}
		$data = $this->db->get()->result();
		return $data;
	}
	
	public function totalcosto($tratamientos){
		$this->db->select('SUM(tratamiento.costo) as total');
		$this->db->from('tratamiento');
		$this->db->where_in('tratamiento.id',$tratamientos);
		$data = $this->db->get()->result();
		//$data = $this->db->get_compiled_select();
		$data[0]->totalFormat = number_format($data[0]->total,2);
		return $data;
	}
}
?>