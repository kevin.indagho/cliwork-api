<?php
class Data_model extends CI_Model{
	
	#region pais
		public function  pais_lista($activo){
			$this->db->select('pais.id,
			pais.nombre,
			pais.activo');
			$this->db->from('pais');
			if($activo != 'none'){
				$this->db->where('pais.activo',$activo);	
			}
			$data = $this->db->get()->result();
			
			return $data;
		}
		
		public function  pais_estados_lista($pais){
			$this->db->select('estados.id,
			estados.nombre');
			$this->db->from('estados');
			$this->db->where('estados.ciudadid',$pais);
			$data = $this->db->get()->result();
			return $data;
		}
	#endregion
	
	#region patient
		public function asistentes(){
			$this->db->select('employee.id,
			employee.EmployeeNum,
			employee.LName,
			employee.FName');
			$this->db->from('employee');
			$data = $this->db->get()->result();
			return $data;
		}
	#endregion
	
	#region patient
		public function patient_read($id){
			$this->db->select('patient.Id,patient.LName,patient.FName');
			$this->db->from('patient');
			$this->db->where('patient.Id',$id);
			$data = $this->db->get()->result();
			return $data;
		}
		
		public function patsearch($text){
			$this->db->select('patient.Id,patient.LName,patient.FName');
			$this->db->from('patient');
			$this->db->like('CONCAT(patient.LName," ",patient.FName)',rtrim($text));
			$data = $this->db->get()->result();
			return $data;
		}
	#endregion
	
	#region Procedures
		public function procedures_lista(){
			$this->db->select('procedurecode.id,
			procedurecode.ProcCode,
			procedurecode.Descript,
			procedurecode.Active,
			procedurecode.Duration,
			procedurecode.ReasonVisit,
			procedurecode.AbbrDesc,
			procedurecode.isSurf,
			procedurecode.isImplant,
			procedurecode.isQuarter,
			procedurecode.toothRange,
			procedurecode.isCrown');
			$this->db->from('procedurecode');
			$this->db->where('procedurecode.delete',0);
			$data = $this->db->get()->result();			
			return $data;
		}
		
		public function procedures_reasonvisit(){
			$this->db->select('procedurecode.id,
			procedurecode.ProcCode,
			procedurecode.Descript,
			procedurecode.Active,
			procedurecode.Duration,
			procedurecode.ReasonVisit,
			procedurecode.AbbrDesc');
			$this->db->from('procedurecode');
			$this->db->where('procedurecode.Active',1);
			$this->db->where('procedurecode.ReasonVisit',1);
			$data = $this->db->get()->result();
			return $data;
		}
		
		public function procedures_search($limit,$by,$text){
			$this->db->select('procedurecode.id,
			procedurecode.ProcCode,
			procedurecode.Descript,
			procedurecode.AbbrDesc,
			fee.Amount');
			$this->db->from('procedurecode');
			$this->db->join('fee','fee.CodeNum = procedurecode.id','left');
			$this->db->join('feesched','fee.FeeSched = feesched.id','left');
			$this->db->where('feesched.principal',1);
			$this->db->like($by,$text);
			if($limit != 'none'){
				$this->db->limit($limit);
			}
			$data = $this->db->get()->result();
			return $data;
		}
	
		public function procedure_add($procedure){
			$this->db->insert('procedurecode',$procedure);
			$error = $this->db->error();
			return $error;
		}
		
		public function procedure_edit($id,$procedure){
			$this->db->where('id',$id);
			$this->db->update('procedurecode',$procedure);
			$error = $this->db->error();
			return $error;
		}
		
		public function procedure_delete($id){
			$this->db->where('id',$id);
			$this->db->update('procedurecode',array('delete'=>1));
			$error = $this->db->error();
			return $error;
		}
	
		public function procedure_reminders_add($data){
			$this->db->insert('ofertas_recordatorios_procedure',$data);
			$error = $this->db->error();
			return $error;
		}
		
		public function procedure_reminders_edit($id,$data){
			$this->db->where('id',$id);
			$this->db->update('ofertas_recordatorios_procedure',$data);
			$error = $this->db->error();
			return $error;
		}
		
		public function procedure_reminders_delete($id){
			$this->db->where('id',$id);
			$this->db->delete('ofertas_recordatorios_procedure');
			$error = $this->db->error();
			return $error;
		}
		
		public function procedure_reminders(){
			$this->db->select('procedurecode.ProcCode,
			procedurecode.Descript,
			ofertas_recordatorios_procedure.id,
			ofertas_recordatorios_procedure.procedureid,
			ofertas_recordatorios_procedure.mensaje,
			ofertas_recordatorios_procedure.archivo,
			ofertas_recordatorios_procedure.tipo,
			ofertas_recordatorios_procedure.dias');
			$this->db->from('procedurecode');
			$this->db->join('ofertas_recordatorios_procedure','ofertas_recordatorios_procedure.procedureid = procedurecode.id','inner');
			$data = $this->db->get()->result();
			return $data;
		}
	#endregion
	
	#region Configuracion
		public function confignoti_edit($id,$config,$usertype){
			$this->db->where('id',$id);
			$this->db->update('notificaciones_reportes',$config);
			$error = $this->db->error();
			if($error['code'] == 0){
				$this->db->where('notificaciones_reportes_usuarios.reporteid',$id);
				$this->db->delete('notificaciones_reportes_usuarios');
				if($usertype != ''){
					foreach($usertype as $type){
						$this->db->insert('notificaciones_reportes_usuarios',array('notificaciones_reportes_usuarios.reporteid'=>$id,'usuarioTipoid' => $type));
					}
				}
			}
			return $error;
			
		}
		
		public function noti_config_list(){
			$this->db->select('notificaciones_reportes.id,
			notificaciones_reportes.nombre,
			notificaciones_reportes.`name`,
			notificaciones_reportes.descripcion,
			notificaciones_reportes.instrucciones,
			notificaciones_reportes.usuarios,
			GROUP_CONCAT(notificaciones_reportes_usuarios.usuarioTipoid) as tipos,
			GROUP_CONCAT(usuario_tipo.name) as tiposName');
			$this->db->from('notificaciones_reportes');
			$this->db->join('notificaciones_reportes_usuarios','notificaciones_reportes_usuarios.reporteid = notificaciones_reportes.id','left');
			$this->db->join('usuario_tipo','notificaciones_reportes_usuarios.usuarioTipoid = usuario_tipo.id','left');
			$this->db->group_by('notificaciones_reportes.id');
			$data = $this->db->get()->result(); 
			return $data;
			
		}
		
		public function reporte_editar($reporteid,$instrucciones){
			$this->db->where('id',$reporteid);
			$this->db->update('notificaciones_reportes',array('instrucciones' => $instrucciones));
			$error = $this->db->error();
			return $error;
		}
		
		public function reporte_usuarios($reporteid){
			$this->db->select('notificaciones_reportes_usuarios.usuarioid');
			$this->db->from('notificaciones_reportes_usuarios');
			$this->db->where('notificaciones_reportes_usuarios.reporteid',$reporteid);
			$data = $this->db->get()->result();
			return $data;
			
		}
		
		public function configuracion($configid){
			$this->db->select('configuracion_sistema.id,
			configuracion_sistema.nombre,
			configuracion_sistema.descripcion,
			configuracion_sistema.instrucciones');
			$this->db->from('configuracion_sistema');
			if($configid != "none"){
				$this->db->where('configuracion_sistema.id',$configid);
			}
			$data = $this->db->get()->result();
			return $data;
		}
		
		public function configuracion_edit($configid,$config){
			$this->db->where('configuracion_sistema.id',$configid);
			$this->db->update('configuracion_sistema',$config);
			$data = $this->db->error();
			return $data;
		}
	#endregion
	
	#region Providers
		function providers_lista($IsHidden = "none",$specialty){
			$this->db->select('provider.id,
			provider.ProvNum,
			provider.Abbr,
			provider.Color,
			provider.LName,
			provider.FName,
			provider.IsHidden,
			provider.ProvStatus,
			provider.Specialty,
			provider.horario');
			$this->db->from('provider');
			if($specialty != "none"){
				$this->db->where('Specialty',$specialty);
			}
			if($IsHidden != "none"){
				$this->db->where('IsHidden',$IsHidden);
			}
			$data = $this->db->get()->result();
			return $data; 
		}
		
		public function provider_add($provider){
			$this->db->insert('provider',$provider);
			$error = $this->db->error();
			return $error;
		}
		
		public function provider_edit($id,$provider){
			$this->db->where('id',$id);
			$this->db->update('provider',$provider);
			$error = $this->db->error();
			return $error;
		}
		
		public function provider_delete($id,$provider){
			$this->db->where('id',$id);
			$this->db->update('provider',$provider);
			$error = $this->db->error();
			return $error;
		}
	#endregion
	
	#region Citas(Appointment) Status
		public function status_cita_lista($activo){
			$this->db->select('appointment_status.id,
			appointment_status.nombre,
			appointment_status.descripcion,
			appointment_status.activo');
			$this->db->from('appointment_status');
			if($activo != 'none'){
				$this->db->where('activo',$activo); 
			}
			$data = $this->db->get()->result();
			return $data;
		}
	#endregion
	
	#region Etapas
		public function etapas($activo){
			$this->db->select('etapa.id,
			etapa.color,
			etapa.name,
			etapa.dias,
			etapa.nombre,
			etapa.activo');
			$this->db->from('etapa');
			if($activo != 'none'){
				$this->db->where('etapa.activo',$activo);
			}
			$data = $this->db->get()->result();
			return $data;
		}
		
		public function etapas_add($etapa){
			$this->db->insert('etapa',$etapa);
			$data = $this->db->error();
			return $data;
		}
		
		public function etapas_editar($id,$etapa){
		$this->db->where('id',$id);
		$this->db->update('etapa',$etapa);
		$data = $this->db->error();
		return $data;
	}
	#endregion
	
	#region Archivos
		public function archivos_add($archivo){
			$this->db->insert('archivos',$archivo);
			$error = $this->db->error();
			return $error;
		}
		
		public function archivos_update($id,$archivo){
			$this->db->where('id',$id);
			$this->db->update('archivos',$archivo);
			$error = $this->db->error();
			return $error;
		}

		public function archivos_delete($id,$archivo){
			$this->db->where('id',$id);
			$this->db->update('archivos',$archivo);
			$error = $this->db->error();
			return $error;
		}
		
		public function archivostipo_detalle($id){
			$this->db->select('archivos_tipo.nombre,
			archivos_tipo.carpeta');
			$this->db->from('archivos_tipo');
			$this->db->where('archivos_tipo.id',$id);
			$data = $this->db->get()->result();
			return $data;
		}
		
		public function archivostipo_list(){
			$this->db->select('archivos_tipo.id,
			archivos_tipo.nombre,
			archivos_tipo.carpeta');
			$this->db->from('archivos_tipo');
			$data = $this->db->get()->result();
			return $data;
		}
		
		public function archivos($tipo = "none",$activo,$delete = 0){
			$this->db->select('archivos.id,
			archivos.fisico,
			archivos.nombre,
			archivos.descripcion,
			archivos.tipo,
			archivos_tipo.nombre as tipoNombre,
			archivos.activo');
			$this->db->from('archivos');
			$this->db->join('archivos_tipo','archivos.tipo = archivos_tipo.id','inner');
			$this->db->where('archivos.delete',$delete);
			if($tipo != "none"){
				$this->db->where('archivos.tipo',$tipo);
			}
			if($activo != "none"){
				$this->db->where('archivos.activo',$activo);
			}
			$data = $this->db->get()->result();
			return $data;
		}
	#endregion

	#region procedimientos botones
		public function proce_botones(){
			$this->db->select('procbuttonquick.Id,
			procbuttonquick.Description');
			$this->db->from('procbuttonquick');
			$this->db->where('procbuttonquick.IsLabel',1);
			$data = $this->db->get()->result();
			foreach($data as $item){
				$this->db->select('procbuttonquick.Id,
				procbuttonquick.ProcButtonQuickNum,
				procbuttonquick.Description,
				procbuttonquick.CodeValue,
				procbuttonquick.active,
				procbuttonquick.Surf,
				procbuttonquick.YPos,
				procbuttonquick.ItemOrder,
				procbuttonquick.IsLabel');
				$this->db->from('procbuttonquick');
				$this->db->where('procbuttonquick.categoryid',$item->Id);
				$this->db->where('procbuttonquick.active',1);
				$item->data = $this->db->get()->result();
			}
			return $data;
		}
		
		public function proce_botones_category(){
			$this->db->select('procbuttonquick.Id,procbuttonquick.Description');
			$this->db->from('procbuttonquick');
			$this->db->where('procbuttonquick.IsLabel',1);
			$data = $this->db->get()->result();
			return $data;
		}
		
		public function proce_botones_list(){
			$this->db->select('procbuttonquick.Id,
			procbuttonquick.ProcButtonQuickNum,
			procbuttonquick.Description,
			procbuttonquick.CodeValue,
			procbuttonquick.Surf,
			procbuttonquick.YPos,
			procbuttonquick.active,
			procbuttonquick.ItemOrder,
			procbuttonquick.categoryid,
			procbuttonquick.IsLabel,
			CONCAT(procedurecode.ProcCode,"<br>",procedurecode.Descript) as ProcedureName');
			$this->db->from('procbuttonquick');
			$this->db->where('procbuttonquick.IsLabel',0);
			$this->db->join('procedurecode','procbuttonquick.CodeValue = procedurecode.id','inner');
			$data = $this->db->get()->result();
			foreach($data as $button){
				$this->db->select('procbuttonquick.Id,procbuttonquick.Description');
				$this->db->from('procbuttonquick');
				$this->db->where('procbuttonquick.Id',$button->categoryid);
				$category = $this->db->get()->result();
				$button->categoryName = $category[0]->Description;
			}
			return $data;
		}
	
		public function proce_botones_add($button){
			$this->db->insert('procbuttonquick',$button);
			$error = $this->db->error();
			return $error;
		}
		
		public function proce_botones_edit($id,$button){
			$this->db->where('procbuttonquick.Id',$id);
			$this->db->update('procbuttonquick',$button);
			$error = $this->db->error();
			return $error;
		}
	#endregion
	
	#region Documentos
	 public function document_list($patientid){
		 $this->db->select('paciente_archivos.usuarioid,,
		paciente_archivos.fecha,
		paciente_archivos.nombre,
		paciente_archivos.nombreFisico,
		paciente_archivos.descripcion,
		paciente_archivos.activo,
		CONCAT(usuario.nombre," ",usuario.apellidos) as usuario,
		CONCAT(patient.LName," ",patient.FName) as Patient');
		$this->db->from('paciente_archivos');
		$this->db->where('paciente_archivos.pacienteid',$patientid);
		$this->db->join('usuario','paciente_archivos.usuarioid = usuario.id','inner');
		$this->db->join('patient','paciente_archivos.pacienteid = patient.Id','inner');
		$data = $this->db->get()->result();
		return $data;
	 }
	#endregion
	
	#region Definiciones
		public function definition_by_type($type){
			$this->db->select('definition.DefNum,
			definition.ItemName,
			definition.ItemValue,
			definition.IsHidden');
			$this->db->from('definition');
			$this->db->where('definition.Category',$type);
			$this->db->where('definition.IsHidden',0);
			$this->db->order_by('definition.ItemValue',"ASC");
			$data = $this->db->get()->result();
			return $data;
		}
	#endregion
	
	#region Cuestionario
		
		public function cuestionario_notas($encuestaid){
			$this->db->select('historial_actividades.nota,
			historial_actividades.fecha,
			historial_actividades.hora,
			CONCAT(usuario.nombre," ",usuario.apellidos) as user');
			$this->db->from('historial_actividades');
			$this->db->join('usuario','historial_actividades.usuario = usuario.id','inner');
			$this->db->where('historial_actividades.cuestionarioid',$encuestaid);
			$this->db->where('historial_actividades.tipoid',1);
			$this->db->order_by('historial_actividades.fecha','DESC');
			$this->db->order_by('historial_actividades.hora','DESC');
			$notas = $this->db->get()->result();
			return $notas;
		}
		
		public function cuestionario_informes(){
			$this->db->select('COUNT(cuestionario_encabezado.id) as total');
			$this->db->from('cuestionario_encabezado');
			$this->db->where('cuestionario_encabezado.usuario is null');
			$total = $this->db->get()->result();
			
			$data['totalFree'] = $total[0]->total;
			return $data;
		}
		
		public function proce_preguntas($id){
			$this->db->select('procedure_preguntas.id,
			procedure_preguntas.procedureid,
			procedure_preguntas.nombre,
			procedure_preguntas.descripcion,
			procedure_preguntas.tipo,
			procedure_preguntas.activo,
			procedurecode.Descript,
			procedurecode.AbbrDesc');
			$this->db->from('procedure_preguntas');
			$this->db->join('procedurecode','procedure_preguntas.procedureid = procedurecode.id','inner');
			$this->db->where('procedure_preguntas.activo',1);
			$this->db->where('procedure_preguntas.procedureid',$id);
			$this->db->order_by('procedure_preguntas.orden','ASC');
			$data = $this->db->get()->result();
			foreach($data as $item){
				$item->control = "";
				if($item->tipo == 1){
					$control = array(
					'name'  => 'preguntas['.$item->id.']',
					'class' => 'form-control');
					$item->control = form_input($control);
				}
				if($item->tipo == 2){
					$this->db->select('procedure_pregunta_opciones.id,
					procedure_pregunta_opciones.opcion');
					$this->db->from('procedure_pregunta_opciones');
					$this->db->where('procedure_pregunta_opciones.preguntaid',$item->id);
					$opciones = $this->db->get()->result();
					
					foreach($opciones as $opcion){
						$control = array(
						'name' => 'preguntas['.$item->id.']',
						'value' => $opcion->id,
						'data-validation' => 'required',
						'data-validation-error-msg' => 'You did not enter a valid e-mail',
						'data-validation-error-msg-container'=> '#email-error-dialog',
						'checked' => false);
						$item->control .= '<div class="app-radio roun inline"><label>'.form_radio($control).''.$opcion->opcion.'</label></div>';
					}
				}
				if($item->tipo == 3){
					$this->db->select('procedure_pregunta_opciones.id,
					procedure_pregunta_opciones.opcion');
					$this->db->from('procedure_pregunta_opciones');
					$this->db->where('procedure_pregunta_opciones.preguntaid',$item->id);
					$opciones = $this->db->get()->result();
					
					foreach($opciones as $opcion){
						$attr = array(
						'name' => 'preguntas['.$item->id.'][]',
						'value' => $opcion->id,
						'data-validation' => 'required',
						'checked' => false);
						$item->control .= '<div class="app-checkbox margin-top-5"><label>'.form_checkbox($attr).''.$opcion->opcion.'</label></div>';
					}
				}
			}
			return $data;
		}
		
		public function proce_preguntas_lista(){
			$this->db->select('procedure_preguntas.id,
			procedure_preguntas.procedureid,
			procedure_preguntas.nombre,
			procedure_preguntas.descripcion,
			procedure_preguntas.tipo,
			procedure_preguntas.activo,
			procedurecode.Descript,
			procedurecode.AbbrDesc');
			$this->db->from('procedure_preguntas');
			$this->db->join('procedurecode','procedure_preguntas.procedureid = procedurecode.id','inner');
			$this->db->where('procedure_preguntas.activo',1);
			$data = $this->db->get()->result();
			foreach($data as $item){
				$this->db->select('procedure_pregunta_opciones.id,procedure_pregunta_opciones.opcion');
				$this->db->from('procedure_pregunta_opciones');
				$this->db->where('procedure_pregunta_opciones.preguntaid',$item->id);
				$this->db->where('procedure_pregunta_opciones.delete',0);
				$item->opciones = $this->db->get()->result();
			}
			return $data;
		}
		
		public function proce_pregunta_agregar($pregunta){
			$this->db->insert('procedure_preguntas',$pregunta);
			$error = $this->db->error();
			return $error;
		}
		
		public function proce_pregunta_delete($id){
			$this->db->where('id',$id);
			$this->db->update('procedure_preguntas',array('activo' => 0));
			$error = $this->db->error();
			return $error;
		}
		
		public function proce_pregunta_editar($id,$pregunta){
			$this->db->where('procedure_preguntas.id',$id);
			$this->db->update('procedure_preguntas',$pregunta);
			$error = $this->db->error();
			return $error;
		}
		
		public function cuestionario_add($encabezado,$preguntas){
			$fecha = new DateTime();
			$this->db->trans_start();
			$this->db->insert('cuestionario_encabezado',$encabezado);
			$encabezadoid = $this->db->insert_id();
			foreach($preguntas as $id => $respuesta){
				if($id == 4){
					$respuesta = implode(',',$respuesta);
				}
				$detalle = array(
					'encabezadoid' => $encabezadoid,
					'preguntaid' => $id,
					'respuesta' => $respuesta);
				$this->db->insert('cuestionario_detalle',$detalle);
			}
			$this->db->trans_complete();
			$error = $this->db->error();
			$error['id'] = $encabezadoid;
			if($error['code'] == 0){
				//$this->correo_cuestionario_complete(); 
			}
			return $error;
		}
	
		public function cuestionario_edit($id,$encabezado){
			$this->db->where('cuestionario_encabezado.id',$id);
			$this->db->update('cuestionario_encabezado',$encabezado);
			$error = $this->db->error();
			return $error;
		}
		
		public function cuestionario_list($filter,$user,$tipo){
			$this->db->select('cuestionario_encabezado.id,
			patient.Id as patientid,
			patient.LName,
			patient.FName,
			patient.HmPhone,
			patient.WirelessPhone,
			patient.Email,
			cuestionario_encabezado.fecha,
			cuestionario_encabezado.nombres,
			cuestionario_encabezado.apellidos,
			cuestionario_encabezado.correo,
			cuestionario_encabezado.telefono,
			cuestionario_encabezado.estado,
			procedurecode.Descript,
			procedurecode.AbbrDesc,
			CONCAT(usuario.nombre," ",usuario.apellidos) as usuario,
			CONCAT(doctor.nombre," ",doctor.apellidos) as doctor');
			$this->db->from('cuestionario_encabezado');
			$this->db->join('procedurecode','cuestionario_encabezado.procedureid = procedurecode.id','inner');
			$this->db->join('patient','cuestionario_encabezado.patientid = patient.Id','inner');
			$this->db->join('usuario','cuestionario_encabezado.usuario = usuario.id','left');
			$this->db->join('usuario as doctor','cuestionario_encabezado.doctor = doctor.id','left');
			if($filter == 1){
				$this->db->where('cuestionario_encabezado.doctor',$user);
			}else{
				$this->db->where('cuestionario_encabezado.doctor is null');
			}
			$data = $this->db->get()->result();
			foreach($data as $item){
				
				$this->db->select('COUNT(cuestionario_encabezado.id) AS total');
				$this->db->from('historial_actividades');
				$this->db->join('cuestionario_encabezado','historial_actividades.cuestionarioid = cuestionario_encabezado.id','inner');
				$this->db->join('paciente_archivos','historial_actividades.archivoid = paciente_archivos.id','inner');
				$this->db->where('cuestionario_encabezado.id',$item->id);
				$archivos = $this->db->get()->result();
				$item->archivos = '<div class="btn-group" role="group"><button class="files btn btn-xs btn-info "><i class="fa fa-cloud-upload"></i></button> ';
				if($archivos[0]->total > 0){
					$item->archivos .= '<button class="btn btn-xs btn-success "><i class="fa fa-check"></i></button>';
				}else{
					$item->archivos .= '<button class="btn btn-xs btn-danger"><i class="fa fa-times"></i></button>';
				}
				$item->archivos .= '</div>';
				
				$this->db->select('CONCAT(historial_actividades.fecha," ",historial_actividades.hora) as date');
				$this->db->from('historial_actividades');
				$this->db->where('historial_actividades.cuestionarioid',$item->id);
				$this->db->where('historial_actividades.tipoid',1);
				$this->db->order_by('historial_actividades.fecha','DESC');
				$this->db->order_by('historial_actividades.hora','DESC');
				$this->db->limit('1');
				$lastNote = $this->db->get()->result();
				if(count($lastNote) > 0){
					$item->lastnote = $lastNote[0]->date;
				}else{
					$item->lastnote = "";
				}
				
				if($filter == 0){
					if($tipo == 2){
						$item->boton = '<button class="open takedoc tbtn btn-xs btn-warning"><i class=" fa fa-hand-paper-o" aria-hidden="true"></i></button>';
					}else if($tipo == 1){
						$item->boton = '<button class="open takeven btn btn-xs btn-warning"><i class="fa fa-hand-paper-o" aria-hidden="true"></i></button>';
					}else{
						$item->boton = '<button class="open btn btn-xs btn-warning"><i class="fa fa-hand-paper-o" aria-hidden="true"></i></button>';
					}
				}else{
					$item->boton = '<button class="open btn btn-xs btn-info"><i class=" fa fa-eye" aria-hidden="true"></i></button>';
				}
			}
			return $data;
		}
		
		public function cuestionario_detalle($id){
			$this->db->select('cuestionario_encabezado.id,
			patient.Id as patientid,
			patient.LName,
			patient.FName,
			patient.HmPhone,
			patient.WirelessPhone,
			patient.Email,
			cuestionario_encabezado.fecha,
			cuestionario_encabezado.nombres,
			cuestionario_encabezado.apellidos,
			cuestionario_encabezado.correo,
			cuestionario_encabezado.telefono,
			cuestionario_encabezado.notyOpen,
			cuestionario_encabezado.archivo,
			cuestionario_encabezado.estado,
			procedurecode.Descript,
			procedurecode.AbbrDesc,
			paciente_archivos.nombre as ArchivoNombre,
			paciente_archivos.nombreFisico as ArchivoNombreFisico');
			$this->db->from('cuestionario_encabezado');
			$this->db->join('procedurecode','cuestionario_encabezado.procedureid = procedurecode.id','inner');
			$this->db->join('patient','cuestionario_encabezado.patientid = patient.Id','inner');
			$this->db->join('paciente_archivos','cuestionario_encabezado.archivo = paciente_archivos.id','LEFT');
			$this->db->where('cuestionario_encabezado.id',$id);
			$data = $this->db->get()->result();
			foreach($data as $encuesta){
				$this->db->select('paciente_archivos.nombre,
				paciente_archivos.nombreFisico');
				$this->db->from('historial_actividades');
				$this->db->join('paciente_archivos','historial_actividades.archivoid = paciente_archivos.id','inner');
				$this->db->where('historial_actividades.cuestionarioid',$encuesta->id);
				$encuesta->archivos = $this->db->get()->result();
				
				$this->db->select('cuestionario_detalle.id,
				cuestionario_detalle.encabezadoid,
				cuestionario_detalle.preguntaid,
				cuestionario_detalle.respuesta,
				procedure_preguntas.tipo,
				procedure_preguntas.nombre,
				procedure_preguntas.descripcion');
				$this->db->from('cuestionario_detalle');
				$this->db->join('procedure_preguntas','cuestionario_detalle.preguntaid = procedure_preguntas.id','inner');
				$this->db->where('cuestionario_detalle.encabezadoid',$encuesta->id);
				$preguntas = $this->db->get()->result();
				foreach($preguntas as $item){
					$item->control = "";
					if($item->tipo == 1){
						$attr = array(
						'value' => $item->respuesta,
						'disabled' => true,
						'class' => 'form-control input-sm');
						$item->control = form_input($attr);
					}
					if($item->tipo == 2){
						$this->db->select('procedure_pregunta_opciones.id,
						procedure_pregunta_opciones.opcion');
						$this->db->from('procedure_pregunta_opciones');
						$this->db->where('procedure_pregunta_opciones.preguntaid',$item->preguntaid);
						$this->db->where('procedure_pregunta_opciones.delete',0);
						$opciones = $this->db->get()->result();
						
						foreach($opciones as $opcion){
							$control = array(
							'disabled' => true,
							'checked' => ($opcion->id == $item->respuesta ? true : false));
							$item->control .= '<div class="app-radio roun inline"><label>'.form_radio($control).''.$opcion->opcion.'</label></div>';
						}
					}
					if($item->tipo == 3){
						$respuestas = explode(',',$item->respuesta);
						$this->db->select('procedure_pregunta_opciones.id,
						procedure_pregunta_opciones.opcion');
						$this->db->from('procedure_pregunta_opciones');
						$this->db->where('procedure_pregunta_opciones.preguntaid',$item->preguntaid);
						$this->db->where('procedure_pregunta_opciones.delete',0);
						$opciones = $this->db->get()->result();
						
						foreach($opciones as $opcion){
							$attr = array(
								'disabled' => true,
								'checked' => (in_array($opcion->id,$respuestas)? true : false)
							);
							$item->control .= '<div class="app-checkbox margin-top-5"><label>'.form_checkbox($attr).''.$opcion->opcion.'</label></div>';
						}
					}
				}
				$encuesta->preguntas = $preguntas;
				if($encuesta->notyOpen == 0){ $this->correo_cuestionario_open(); }
			}
			
			return $data;
		}
	
		public function procedure_option_add($newOption){
			$this->db->insert('procedure_pregunta_opciones',$newOption);
			$error = $this->db->error();
			return $error;
		}
		
		public function procedure_option_delete($id){
			$this->db->where('id',$id);
			$this->db->update('procedure_pregunta_opciones',array('delete' => 1));
			$error = $this->db->error();
			return $error;
		}
	
		public function correo_cuestionario_complete(){
			$config = $this->configcode->emailconfig();
			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			
			$this->email->from('system@cliwork.com','Cliwork');
			$this->email->to('kevin.edua@live.com.mx,miguel@indagho.com');
			$this->email->subject('SMILE4EVER | Thank you for answer');
			$this->email->message($this->load->view('correo_plantillas/cuestionario_echo',null,true));
			$error = $this->email->send();
			//$error = $this->email->print_debugger(array('headers'));
			return $error;			
		}
		
		public function correo_cuestionario_open(){
			$config = $this->configcode->emailconfig();
			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			
			$this->email->from('system@cliwork.com','Cliwork');
			$this->email->to('kevin.edua@live.com.mx');
			$this->email->subject('SMILE4EVER | Thank you for waiting');
			$this->email->message($this->load->view('correo_plantillas/cuestionario_echo',null,true));
			$this->email->send();
			$error = $this->email->print_debugger(array('headers'));
			return $error;			
		}
		
		public function surveys_list(){
			$this->db->select('procedurecode.id,
			procedurecode.CodeNum,
			procedurecode.ProcCode,
			procedurecode.Descript');
			$this->db->from('procedurecode');
			$this->db->join('procedure_preguntas','procedure_preguntas.procedureid = procedurecode.id','inner');
			$this->db->group_by('procedurecode.id');
			$data = $this->db->get()->result();
			return $data;
		}
	#endregion

	#region Precios (fee)
		public function feesched_list(){
			$this->db->select('feesched.id,
			feesched.Description');
			$this->db->from('feesched');
			$data = $this->db->get()->result();
			return $data;
		}
		
		public function precios_edit($id,$fee){
			$this->db->where('id',$id);
			$this->db->update('fee',$fee);
			$error = $this->db->error();
			return $error;
		}
		
		public function precios_lista(){
			$this->db->select('fee.id,
			feesched.Description,
			fee.Amount,
			procedurecode.Descript,
			procedurecode.AbbrDesc');
			$this->db->from('feesched');
			$this->db->join('fee','fee.FeeSched = feesched.id','inner');
			$this->db->join('procedurecode','fee.CodeNum = procedurecode.id','inner');
			$data = $this->db->get()->result();
			foreach($data as $fee){
				$fee->AmountFormat = number_format($fee->Amount,2);
			}
			return $data;
		}
	#endregion

	#region Origen

		public function origen_edit($id,$origen){
			$this->db->where('id',$id);
			$this->db->update('origen',$origen);
			$error = $this->db->error();
			return $error;
		}
		
		public function origen_add($origen){
			$this->db->insert('origen',$origen);
			$error = $this->db->error();
			return $error;
		}
		
		public function origen_list($activo){
			$this->db->select('origen.id,
			origen.color,
			origen.nombre,
			origen.activo');
			$this->db->from('origen');
			if($activo != 'none'){
				$this->db->where('activo',$activo);
			}
			$data = $this->db->get()->result();
			return $data;
		}
		
		public function origen_data(){
			$Datenow = new DateTime();
			$this->db->select('origen.id,
			origen.color,
			origen.nombre,
			origen.activo');
			$this->db->from('origen');
			$this->db->where('activo',1);
			$origenes = $this->db->get()->result();
			foreach($origenes as $origen){
				$this->db->select('DATE_FORMAT(patient.SecDateEntry,"%Y-%m") as date,Count(patient.Id) as number');
				$this->db->from('patient');
				$this->db->where('patient.Origen',$origen->id);
				$this->db->group_by('DATE_FORMAT(patient.SecDateEntry,"%Y-%m")');
				$this->db->where('DATE_FORMAT(patient.SecDateEntry,"%Y")',$Datenow->format('Y'));
				$data = $this->db->get()->result();
				foreach($data as $item ){
					$origen->data[] = $item->number;
				}
			}
			return $origenes;
		}
	#endregion
	
	#region Graficas
		public function ofertas_data(){
			$this->db->select('ofertas_estado.id,
			ofertas_estado.name');
			$this->db->from('ofertas_estado');
			$this->db->where_in('ofertas_estado.id',[1,3,7]);
			$estados = $this->db->get()->result();
			foreach($estados as $estado){
				$this->db->select('SUM(ofertas.valor) as number,COUNT(ofertas.id) as count');
				$this->db->from('ofertas');
				$this->db->where('ofertas.estadoid',$estado->id);
				$this->db->where('DATE_FORMAT(ofertas.fecha, "%Y") =',fechaactual('Y'));
				$this->db->group_by('DATE_FORMAT(ofertas.fecha,"%Y-%m")');
				$data = $this->db->get()->result();
				foreach($data as $item ){
					$estado->data[] = $item->number;
				}
			}
			return $estados;
		}
	
		public function procedimientos_data(){
			$this->db->select('procedurecode.Descript,
			Sum(procedurelog.ProcFee) as amount,
			Count(procedurelog.Id) as number');
			$this->db->from('procedurelog');
			$this->db->join('procedurecode','procedurelog.CodeNum = procedurecode.id','inner');
			$this->db->where('procedurelog.ProcStatus',2);
			$this->db->where('DATE_FORMAT(procedurelog.DateComplete, "%Y") =',fechaactual('Y'));
			$this->db->group_by('procedurecode.id');
			$this->db->order_by('number','DESC');
			$this->db->limit('10');
			$data['number'] = $this->db->get()->result();
			
			$this->db->select('procedurecode.Descript,
			Sum(procedurelog.ProcFee) as amount,
			Count(procedurelog.Id) as number');
			$this->db->from('procedurelog');
			$this->db->join('procedurecode','procedurelog.CodeNum = procedurecode.id','inner');
			$this->db->where('DATE_FORMAT(procedurelog.DateComplete, "%Y") =',fechaactual('Y'));
			$this->db->where('procedurelog.ProcStatus',2);
			$this->db->group_by('procedurecode.id');
			$this->db->order_by('amount','DESC');
			$this->db->limit('10');
			$data['amount'] = $this->db->get()->result();
			return $data;
		}
	
		public function doctor_ingresos(){
			$this->db->select('provider.id,
			CONCAT(provider.LName," ",provider.FName) as nombre,
			provider.Color');
			$this->db->from('provider');
			$this->db->where('provider.IsHidden',0);
			$doctores = $this->db->get()->result();
			foreach($doctores as $doctor){
				for($i = 1;$i <= 12;$i++){
					$this->db->select("Sum(pagos.monto) as total,pagos.fecha");
					$this->db->from('pagos');
					$this->db->join('provider','pagos.doctor = provider.id','inner');
					$this->db->where('DATE_FORMAT(pagos.fecha,"%Y-%m")',fechaactual('Y')."-".$i);
					$this->db->where('provider.id',$doctor->id);
					$datos = $this->db->get()->result();
					$doctor->fechas[] = $datos[0]->total;
				}				
			}
			return $doctores;
		}
	#endregion
}