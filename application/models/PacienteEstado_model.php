<?php 
class PacienteEstado_model extends CI_Model{
	
	public function listapacientes_ofertas($activo,$vendido,$type = "none",$user = null){
		
		$this->db->select('etapa.id,
		etapa.name,
		etapa.nombre,
		etapa.activo');
		$this->db->from('etapa');
		if($activo != 'none'){
			$this->db->where('etapa.activo',$activo);
		}
		$data = $this->db->get()->result();
		foreach($data as $Etapa){
			$this->db->select('SUM(ofertas.valor) as valor');
			$this->db->from('ofertas');
			$this->db->join('patient','ofertas.pacienteid = patient.Id','inner');
			//$this->db->where('patient.EtapaId',$Etapa->id);
			$this->db->where('ofertas.activo',1);
			$this->db->where('ofertas.vendida',$vendido);
			$this->db->where('ofertas.etapaid',$Etapa->id);
			if($type != 1){
				$this->db->where('ofertas.usuarioid',$user);
			}
			$total = $this->db->get()->result();
			$Etapa->total = number_format($total[0]->valor,2);
			
			$this->db->select('ofertas.id as ofertaid,
			ofertas.fecha,
			ofertas.titulo,
			ofertas.valor,
			ofertas.usuarioid,
			ofertas.fechaCierre,
			ofertas.pacienteid,
			ofertas.etapaId,
			patient.Id as patientid,
			patient.PatNum,
			patient.LName,
			patient.FName,
			ofertas_estado.name as EstadoNombre,
			ofertas_estado.color as Estado');
			$this->db->from('ofertas');
			$this->db->join('patient','ofertas.pacienteid = patient.Id','inner');
			$this->db->join('ofertas_estado','ofertas.estadoid = ofertas_estado.id','inner');
			$this->db->where('ofertas.activo',1);
			$this->db->where('ofertas.vendida',$vendido);
			$this->db->where('ofertas.etapaid',$Etapa->id);
			if($type != 1){
				$this->db->where('ofertas.usuarioid',$user);
			}
			$Etapa->ofertas = $this->db->get()->result();
			foreach($Etapa->ofertas as $oferta){
				
				$this->db->select('COUNT(historial_actividades.id) as total');
				$this->db->from('historial_actividades');
				$this->db->where('historial_actividades.tipoid',2);
				$this->db->where('historial_actividades.estadoid',1);
				$this->db->where('historial_actividades.ofertaid',$oferta->ofertaid);
				$actividades = $this->db->get()->result();
				
				$this->db->select('COUNT(historial_actividades.id) as total');
				$this->db->from('historial_actividades');
				$this->db->where('historial_actividades.tipoid',2);
				$this->db->where('historial_actividades.estadoid',1);
				$this->db->where('historial_actividades.ofertaid',$oferta->ofertaid);
				$this->db->where('historial_actividades.fechaInicio',fechaActual('Y-m-d'));
				$actividadPendiente = $this->db->get()->result();
				
				$this->db->select('COUNT(historial_actividades.id) as total');
				$this->db->from('historial_actividades');
				$this->db->where('historial_actividades.tipoid',2);
				$this->db->where('historial_actividades.estadoid',3);
				$this->db->where('historial_actividades.ofertaid',$oferta->ofertaid);
				$actividadVencida = $this->db->get()->result();
				
				if($actividades[0]->total > 0){ $oferta->Estado = "default"; }
				else{ $oferta->Estado = "warning"; }
				
				if($actividadPendiente[0]->total > 0){ $oferta->Estado = "success"; }
				if($actividadVencida[0]->total > 0){ $oferta->Estado = "danger"; }
				
				$oferta->valorFormato = number_format($oferta->valor,2);
				$oferta->tratamientos = array();
				$oferta->tratamientosNames = "";
				$oferta->tratamientosNombres = "";
				$oferta->fechaestado = $this->calcular_fecha_estado($oferta->fechaCierre);
				
				$this->db->select('ofertas_tratamientos.id,
				procedurecode.Descript,
				procedurecode.AbbrDesc,
				procedurecode.ProcCode');
				$this->db->from('ofertas_tratamientos');
				$this->db->join('procedurecode','ofertas_tratamientos.tratamientoid = procedurecode.id','inner');
				$this->db->where('ofertaid',$oferta->ofertaid);
				$tratamientos = $this->db->get()->result();
				foreach($tratamientos as $tratamiento){
					$oferta->tratamientosNames .= $tratamiento->AbbrDesc.",";
					
					array_push($oferta->tratamientos,$tratamiento->id);
				}
				
			}
		}
		return $data;
	}
	
	public function calcular_estatus_oferta($ofertaid,$EtapaId,$usuarioid){
		
		$this->db->select('COUNT(historial_actividades.id ) as cantidad');
		$this->db->from('historial_actividades');
		$this->db->where('historial_actividades.ofertaid',$ofertaid);
		$this->db->where('historial_actividades.etapaid',$EtapaId);
		$this->db->where('historial_actividades.tipoid',2);
		$this->db->where('historial_actividades.usuario',$usuarioid);
		$this->db->where('historial_actividades.estadoid',1);
		$pendientes = $this->db->get()->result();
		
		$this->db->select('COUNT(historial_actividades.id ) as cantidad');
		$this->db->from('historial_actividades');
		$this->db->where('historial_actividades.ofertaid',$ofertaid);
		$this->db->where('historial_actividades.etapaid',$EtapaId);
		$this->db->where('historial_actividades.tipoid',2);
		$this->db->where('historial_actividades.usuario',$usuarioid);
		$this->db->where('historial_actividades.estadoid',6);
		$porvencer = $this->db->get()->result();
		
		$this->db->select('COUNT(historial_actividades.id ) as cantidad');
		$this->db->from('historial_actividades');
		$this->db->where('historial_actividades.ofertaid',$ofertaid);
		$this->db->where('historial_actividades.etapaid',$EtapaId);
		$this->db->where('historial_actividades.tipoid',2);
		$this->db->where('historial_actividades.usuario',$usuarioid);
		$this->db->where('historial_actividades.estadoid',3);
		$vencidas = $this->db->get()->result();
		if($vencidas[0]->cantidad  > $porvencer[0]->cantidad){
			$estado = 'danger';
		}else{
			if($porvencer[0]->cantidad  > $pendientes[0]->cantidad){
				$estado = 'danger';	
			}else{
				if($pendientes[0]->cantidad > 0){
					$estado = 'success';
				}else{
					$estado = 'primary';
				}
			}
		}
		return $estado;
	}
	
	public function calcular_fecha_estado($fechaf){
		$fechaf = new dateTime($fechaf);
		$fechaActual = new DateTime();
		$difrencia = date_diff($fechaActual,$fechaf);
		
		if($fechaActual >= $fechaf){
			$estado = '<span class="text-danger"><i class="fa fa-calendar"></i> '.$difrencia->days." past due</span>";
		}else{
			$estado = ($difrencia->days == 0 ? '<b class="text-danger">Today expired</b>' : $difrencia->days." Days to expired");
		}
		return $estado;
	}
	
	public function lista($activo){
		$this->db->select('etapa.id,
		etapa.name,
		etapa.nombre,
		etapa.activo');
		$this->db->from('etapa');
		if($activo != 'none'){
			$this->db->where('etapa.activo',$activo);
		}
		$data = $this->db->get()->result();
		return $data;
	}
}
?>