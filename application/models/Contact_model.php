<?php
Class Contact_model extends CI_Model{
	
	#region CRUD
		public function create($contact){
            $this->db->insert("contact",$contact);
            $error = $this->db->error();
			return $error;
        }

        public function delete($contact_id){
            $this->db->where("id",$contact_id);
            $this->db->update("contact",array("delete" => 1));
            $error = $this->db->error();
            return $error;
        }

        public function list_table(){
            $this->db->select("id,name,lastname,phone,email,comment,
            DATE_FORMAT(date,'%Y-%d-%m %H:%i') as date");
            $this->db->from("contact");
            $this->db->where("delete",0);
            $data = $this->db->get()->result();
            return $data;
        }
    }
?>