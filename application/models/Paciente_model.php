<?php 
class Paciente_model extends CI_Model{

	public function agregar($nuevoPaciente){
		$this->db->insert('patient',$nuevoPaciente);
		$error = $this->db->error();
		$error['id'] = $this->db->insert_id();
		if($error['code'] == 0){
			if(!is_dir('files/patients/'.$error['id'])){
				mkdir('files/patients/'.$error['id'], 0777, TRUE);
			}
		}
		return $error;
	}
	
	public function editar($paciente,$pacienteid){
		$this->db->where('patient.id',$pacienteid);
		$this->db->update('patient',$paciente);
		//$data = $this->db->get_compiled_update();
		$error = $this->db->error();
		return $error;
	}
	
	public function editarestado($estado,$pacienteid){
		$this->db->where('id',$pacienteid);
		$this->db->update('patient',$estado);
		$error = $this->db->error();		
		//$data = $this->db->get_compiled_update();
		return $error;
	}
	
	public function lista(){
		$this->db->select('CONCAT(LName," ",FName) as nombre,
		patient.Id,
		patient.PatNum,
		patient.LName,
		patient.FName,
		patient.Gender,
		patient.Email,
		patient.EtapaId,
		patient.HmPhone,
		patient.WkPhone,
		patient.PerdidaFecha,
		patient.PerdidaRazon,
		patient.WirelessPhone,
		patient.State,
		patient.Country,
		patient.City,
		patient.Zip,
		patient.Origen,
		patient.Address,
		patient.foto,
		patient.Birthdate,
		DATE_FORMAT(patient.Birthdate,"%m/%d/%y") as BirthdateFormat,
		etapa.color as etapaColor,
		etapa.name as etapaName,
		etapa.nombre as etapaNombre');
		$this->db->from('patient');
		$this->db->join('etapa','patient.EtapaId = etapa.id','LEFT');
		$data = $this->db->get()->result();
		
		foreach($data as $patient){
			if($patient->EtapaId == 5){
				$patient->oferta = array();
			}			
		}
		
		return $data;
	}
	
	public function pacientes_estado($EtapaId){
		$this->db->select('patient.Id,
		patient.PatNum,
		patient.LName,
		patient.FName,
		patient.EtapaId');
		$this->db->from('patient');
		$this->db->where('patient.EtapaId',$EtapaId);
		$data = $this->db->get()->result();
		return $data;
	}
	
	public function listaestados($activo){
		$this->db->select('etapa.id,
		etapa.name,
		etapa.nombre,
		etapa.activo');
		$this->db->from('etapa');
		if($activo != 'none'){
			$this->db->where('etapa.activo',$activo);
		}
		$data = $this->db->get()->result();
		return $data;
	}

	public function detallestatus($id){
		$this->db->select('patient.Id,
		patient.LName,
		patient.FName,
		patient.EtapaId');
		$this->db->from('patient');
		$this->db->where('patient.Id',$id);
		$data = $this->db->get()->result();
		return $data;
	}
	
	public function detalle($id){
		$this->db->select('patient.Id,
		patient.Address,
		patient.HmPhone,
		patient.WkPhone,
		patient.WirelessPhone,
		patient.Email,
		patient.LName,
		patient.FName,
		patient.foto,
		patient.EtapaId');
		$this->db->from('patient');
		$this->db->where('patient.Id',$id);
		$data = $this->db->get()->result();
		foreach($data as $paciente){
			$this->db->select('treatplan.id,treatplan.Heading');
			$this->db->from('treatplan');
			$this->db->where('treatplan.PatNum',$paciente->Id);
			$paciente->treatplan = $this->db->get()->result();
		}
		return $data;
	}
	
	public function buscador($nombre){
		$this->db->select('patient.Id,
		patient.LName,
		patient.FName');
		$this->db->from('patient');
		$this->db->where('CONCAT(patient.LName," ",patient.FName) like "%'.$nombre.'%"');
		$data = $this->db->get()->result();
		//$data = $this->db->get_compiled_select();
		return $data;
	}

	public function notas_paciente($pacienteid){
		$this->db->select('historial_actividades.id,
		historial_actividades.fecha,
		historial_actividades.hora,
		historial_actividades.nota,
		CONCAT(usuario.nombre," ",usuario.apellidos) as usuario');
		$this->db->from('historial_actividades');
		$this->db->join('usuario','historial_actividades.usuario = usuario.id','left');
		$this->db->where('historial_actividades.pacienteid',$pacienteid);
		$this->db->where('historial_actividades.tipoid',1);
		$data = $this->db->get()->result();
		return $data;
	}
	
	public function procedimientos($pacienteid){
		$this->db->select('procedurelog.Id,
		procedurecode.id,
		procedurecode.CodeNum,
		procedurecode.Duration,
		procedurecode.ProcCode,
		procedurecode.Descript,
		procedurecode.AbbrDesc,
		procedurelog.PatNum,
		procedurelog.AptNum');
		$this->db->from('procedurecode');
		$this->db->join('procedurelog','procedurelog.CodeNum = procedurecode.id','inner');
		$this->db->where('procedurelog.PatNum',$pacienteid);
		$data = $this->db->get()->result();
		return $data;
	}
	
	public function archivos_lista($pacienteid){
		$this->db->select("historial_actividades.ofertaid,
		paciente_archivos.nombre,
		paciente_archivos.nombreFisico,
		paciente_archivos.descripcion");
		$this->db->from("historial_actividades");
		$this->db->join("paciente_archivos","historial_actividades.archivoid = paciente_archivos.id","inner");
		$data = $this->db->get()->result();
		return $data;
	}
	#region OFERTAS
	
		public function prov_com_ingresos($origen){
			$fecha = new DateTime();
			$this->db->select('provider.id,
			CONCAT(provider.LName," ",provider.FName) as name');
			$this->db->from('provider');
			$providers = $this->db->get()->result();
			foreach($providers as $provider){
				$this->db->select('Sum(pagos.monto) as monto');
				$this->db->from('pagos');
				$this->db->join('provider','pagos.doctor = provider.id','inner');
				$this->db->join('patient','pagos.patientid = patient.Id','inner');
				$this->db->where('DATE_FORMAT(pagos.fecha,"%Y")',$fecha->format('Y'));
				$this->db->where('provider.id',$provider->id);
				if($origen != "none"){
					$this->db->where_in('patient.Origen',$origen);
				}
				$monto = $this->db->get()->result();
				$provider->amount = number_format($monto[0]->monto,2);
			}
			return $providers;
		}
		
		public function editar_oferta_etapa($ofertaid,$etapa,$oldetapa){
			$this->db->where('id',$ofertaid);
			$this->db->update('ofertas',$etapa);
			$error = $this->db->error();
			if($error['code'] == 0){
				$fecha = new DateTime();
				$this->db->select('etapa_control.id');
				$this->db->from('etapa_control');
				$this->db->where('etapa_control.terminado',0);
				$this->db->where('etapa_control.ofertaid',$ofertaid);
				$this->db->where('etapa_control.etapaid',$oldetapa);
				$control = $this->db->get()->result();
				if(count($control) > 0){
				$this->db->where('etapa_control.id',$control[0]->id);
				$this->db->update('etapa_control',array('etapa_control.terminado'=>1,'etapa_control.fechaFin' => $fecha->format('Y-m-d H:i')));
				}
				
				$controlI = array(
					'ofertaid' => $ofertaid,
					'etapaid' => $etapa['etapaid'],
					'fechaInicio' => $fecha->format('Y-m-d H:i'),
					'terminado' => 0);
				$this->db->insert('etapa_control',$controlI);
			}
			//$data = $this->db->get_compiled_update();
			return $error;
		}
		
		public function agregar_oferta($data,$tratamientos){
			$this->db->insert('ofertas',$data);
			$error = $this->db->error();
			if($error['code'] == 0){
				$id = $this->db->insert_id();
				$error['id'] = $id;
				if(is_array($tratamientos) == true){
					foreach($tratamientos as $tratamiento){
						$detalle = array('ofertaid' => $id,
						'tratamientoid' => $tratamiento);
						$this->db->insert('ofertas_tratamientos',$detalle);
					}
				}
			}
			return $error;
			
		}
		
		public function oferta_estado($id,$data){
			$this->db->where('id',$id);
			$this->db->update('ofertas',$data);
			$error = $this->db->error();
			return $error;
			
		}
		
		public function editar_oferta($id,$data,$procedel = array() ,$proceadd = array()){
			$this->db->where('id',$id);
			$this->db->update('ofertas',$data);
			$error = $this->db->error();
			
			if($error['code'] == 0){
				if(isset($_POST['proceadd']) == true){
					foreach($proceadd as $procedure){
						$detalle = array('ofertaid' => $id,
						'tratamientoid' => $procedure);
						$this->db->insert('ofertas_tratamientos',$detalle);
					}
				}
				
				if(isset($_POST['procedel']) == true){
					foreach($procedel as $procedure){
						$this->db->where('id',$procedure);
						$this->db->delete('ofertas_tratamientos');
					}
				}
			}
			
			$this->db->select('SUM(fee.Amount) as total');
			$this->db->from('ofertas_tratamientos');
			$this->db->join('procedurecode','ofertas_tratamientos.tratamientoid = procedurecode.id','inner');
			$this->db->join('fee','fee.CodeNum = procedurecode.id','inner');
			$this->db->join('feesched',' fee.FeeSched = feesched.id','inner');
			$this->db->where('feesched.principal',1);
			$this->db->where('ofertas_tratamientos.ofertaid',$id);
			$value = $this->db->get()->result();
			
			$this->db->where('ofertas.id',$id);
			$this->db->update('ofertas',array('ofertas.valor' => $value[0]->total));
			
			return $error;
			
		}
		
		public function detalle_oferta($ofertaid){
			$this->db->select('patient.Id as pacienteid,
			patient.LName,
			patient.FName,
			patient.EtapaId,
			patient.Address,
			patient.HmPhone,
			patient.WkPhone,
			patient.WirelessPhone,
			patient.Email,
			ofertas.id,
			ofertas.doctorid,
			ofertas.fecha,
			ofertas.perdidaFecha,
			ofertas.perdidaRazon,
			ofertas.titulo,
			ofertas.valor,
			DATE_FORMAT(ofertas.fechaCierre,"%m/%d/%Y") as fechaCierre,
			ofertas.pacienteid,
			etapa.name as EtapaName,
			etapa.id as EtapaId');
			$this->db->from('ofertas');
			$this->db->join('patient','ofertas.pacienteid = patient.Id','inner');
			$this->db->join('etapa','ofertas.etapaid = etapa.id','inner');
			$this->db->where('ofertas.id',$ofertaid);
			$ofertas = $this->db->get()->result();
			foreach($ofertas as $oferta){
					
					$oferta->valorFormato = number_format($oferta->valor,2);
					$oferta->tratamientos = array();
					$oferta->tratamientosNames = "";
					$oferta->tratamientosNombres = "";
					$this->db->select('procedurecode.Descript,
					procedurecode.AbbrDesc,
					procedurecode.ProcCode,
					ofertas_tratamientos.id,
					fee.Amount');
					$this->db->from('ofertas_tratamientos');
					$this->db->join('procedurecode','ofertas_tratamientos.tratamientoid = procedurecode.id','inner');
					$this->db->join('fee','fee.CodeNum = procedurecode.id','left');
					$this->db->join('feesched','fee.FeeSched = feesched.id','left');
					$this->db->where('feesched.principal',1);
					$this->db->where('ofertaid',$ofertaid);
					$tratamientos = $this->db->get()->result();
					$oferta->procedimientos = $tratamientos;
					
					foreach($tratamientos as $tratamiento){
						$oferta->tratamientosNames .= $tratamiento->ProcCode."<br>".$tratamiento->Descript." <br><br>";
						array_push($oferta->tratamientos,$tratamiento->id);
					}
					
					/*$this->db->select('historial_actividades.fecha,
					historial_actividades.ofertaid,
					historial_actividades.inicioEtapa,
					historial_actividades.etapaid');
					$this->db->from('historial_actividades');
					$this->db->join('ofertas','historial_actividades.ofertaid = ofertas.id','inner')
					$this->db->join('etapa','historial_actividades.etapaid = etapa.id','inner')
					$this->db->where('historial_actividades.inicioEtapa',1);*/
					
				}
				
			return $ofertas;
		}
		
		public function ofertas($estadoid,$pacienteid){
			$this->db->select('ofertas.fecha as date,
			CONCAT(patient.LName," ",patient.FName) as patient,
			ofertas.id,
			ofertas.valor as value,
			ofertas.estadoid,
			CONCAT(usuario.nombre," ",usuario.apellidos) as user');
			$this->db->from('ofertas');
			$this->db->join('patient','ofertas.pacienteid = patient.Id','inner');
			$this->db->join('usuario','ofertas.usuarioid = usuario.id','inner');
			if($estadoid != "none"){ $this->db->where('ofertas.estadoid',$estadoid); }
			if($pacienteid != "none"){ $this->db->where('ofertas.pacienteid',$pacienteid); }
			$data = $this->db->get()->result();
			foreach($data as $oferta){
				
				$oferta->value = number_format($oferta->value,2);
				
				$this->db->select('GROUP_CONCAT(tratamiento.name) AS ing,
				GROUP_CONCAT(tratamiento.nombre) AS esp,');
				$this->db->from('ofertas_tratamientos');
				$this->db->join('tratamiento','ofertas_tratamientos.tratamientoid = tratamiento.id','inner');
				$this->db->where('ofertas_tratamientos.ofertaid',$oferta->id);
				$oferta->treatments = $this->db->get()->result();
			}
			return $data;
		}

		public function ofertas_vendidas(){
		$this->db->select('ofertas.fecha as date,
		CONCAT(patient.LName," ",patient.FName) as patient,
		ofertas.id,
		ofertas.valor as value,
		ofertas.estadoid,
		CONCAT(usuario.nombre," ",usuario.apellidos) as user');
		$this->db->from('ofertas');
		$this->db->join('patient','ofertas.pacienteid = patient.Id','inner');
		$this->db->join('usuario','ofertas.usuarioid = usuario.id','inner');
		if($estadoid != "none"){ $this->db->where('ofertas.estadoid',$estadoid); }
		if($pacienteid != "none"){ $this->db->where('ofertas.pacienteid',$pacienteid); }
		$data = $this->db->get()->result();
		foreach($data as $oferta){
			$oferta->value = number_format($oferta->value,2);
			$this->db->select('GROUP_CONCAT(tratamiento.name) AS ing,
			GROUP_CONCAT(tratamiento.nombre) AS esp,');
			$this->db->from('ofertas_tratamientos');
			$this->db->join('tratamiento','ofertas_tratamientos.tratamientoid = tratamiento.id','inner');
			$this->db->where('ofertas_tratamientos.ofertaid',$oferta->id);
			$oferta->treatments = $this->db->get()->result();
		}
		return $data;
	}
	
		public function ofertas_perdidas(){
			$this->db->select('ofertas.fecha as date,
			CONCAT(patient.LName," ",patient.FName) as patient,
			ofertas.id,
			ofertas.valor as value,
			ofertas.estadoid,
			ofertas.perdidaFecha,
			ofertas.perdidaRazon,
			ofertas.estadoid,
			CONCAT(usuario.nombre," ",usuario.apellidos) as user');
			$this->db->from('ofertas');
			$this->db->join('patient','ofertas.pacienteid = patient.Id','inner');
			$this->db->join('usuario','ofertas.usuarioid = usuario.id','inner');
			$this->db->where('ofertas.activo',0);
			$data = $this->db->get()->result();
			foreach($data as $oferta){
				$oferta->value = number_format($oferta->value,2);
				$this->db->select('GROUP_CONCAT(tratamiento.name) AS ing,
				GROUP_CONCAT(tratamiento.nombre) AS esp,');
				$this->db->from('ofertas_tratamientos');
				$this->db->join('tratamiento','ofertas_tratamientos.tratamientoid = tratamiento.id','inner');
				$this->db->where('ofertas_tratamientos.ofertaid',$oferta->id);
				$oferta->treatments = $this->db->get()->result();
			}
			return $data;
		}
	
		public function ofertas_perdidas_informes(){
			$this->db->select('SUM(ofertas.valor) as total');
			$this->db->from('ofertas');
			$this->db->where('ofertas.activo',0);
			$lost = $this->db->get()->result();
			$data['totalLost'] = $lost[0]->total;
			
			return $data;
		}
	#endregion
	
	#region PERFIL
		public function details_perfil($patid){
			$this->db->select('COUNT(historial_actividades.id) as total');
			$this->db->from('historial_actividades');
			$this->db->where('historial_actividades.pacienteid',$patid);
			$this->db->where('historial_actividades.tipoid',1);
			$notes = $this->db->get()->result();
			$data['notes'] = $notes[0]->total;
			
			$this->db->select('COUNT(appointment.id) as total');
			$this->db->from('appointment');
			$this->db->where('appointment.PatId',$patid);
			$this->db->where('appointment.AptStatus',2);
			$visits = $this->db->get()->result();
			$data['visits'] = $visits[0]->total;
			
			$this->db->select('appointment.id,
			DATE_FORMAT(appointment.AptDateTime,"%Y-%m-%d") as fecha');
			$this->db->from('appointment');
			$this->db->where('appointment.PatId',$patid);
			//$this->db->where('appointment.AptStatus',2);
			$this->db->where('DATE_FORMAT(appointment.AptDateTime,"%Y-%m-%d") <=',fechaactual('Y-m-d'));
			$this->db->limit('1');
			$lastappt = $this->db->get()->result();
			$data['lastappt'] = $lastappt;
			return $data;
		}
		
		public function nextapptlimit($patid,$limit){
			$this->db->select('appointment.id,
			appointment.Note,
			appointment.AptDateTime,
			appointment.AptDateTimeEnd,
			DATE_FORMAT(appointment.AptDateTime,"%Y-%m-%d %H:%i") as date');
			$this->db->from('appointment');
			$this->db->where('appointment.PatId',$patid);
			$this->db->where('DATE_FORMAT(appointment.AptDateTime,"%Y-%m-%d") >=',fechaactual('Y-m-d'));
			$this->db->order_by('appointment.AptDateTime','ASC');
			$this->db->limit($limit);
			$data = $this->db->get()->result();
				
			foreach($data as $appt){
				$datetime1 = date_create($appt->AptDateTime);
				$datetime2 = date_create($appt->AptDateTimeEnd);
				$appt->duration = date_diff($datetime1, $datetime2);
	
				$this->db->select('SUM(procedurelog.ProcFee) as total');
				$this->db->from('procedurelog');
				$this->db->where('procedurelog.AptNum',$appt->id);
				$proceduresTotal = $this->db->get()->result();
				$appt->total = number_format($proceduresTotal[0]->total,2);
			
				$this->db->select('procedurecode.Descript,
				CONCAT(provider.LName," ",provider.FName) as provider,
				procedurelog.ProcFee');
				$this->db->from('procedurelog');
				$this->db->join('procedurecode','procedurelog.CodeNum = procedurecode.id','inner');
				$this->db->join('provider','procedurelog.ProvNum = provider.id','inner');
				$this->db->where('procedurelog.AptNum',$appt->id);
				$procedures = $this->db->get()->result();
				
				$appt->procedures = $this->createUL($procedures);
			}
			return $data;
		}
	#endregion
	
	#region Plan de pagos
		public function paymentplan_list($patid){
			$this->db->select('pagos_plan_encabezado.id,
			pagos_plan_encabezado.fecha,
			pagos_plan_encabezado.usuario,
			pagos_plan_encabezado.pacienteid,
			CONCAT(pagos_plan_encabezado.comision," %") as comision,
			pagos_plan_encabezado.deuda,
			pagos_plan_encabezado.pago,
			pagos_plan_encabezado.duracion,
			pagos_plan_encabezado.tipo,
			pagos_plan_encabezado.activo,
			pagos_plan_encabezado.fechaPago,
			pagos_plan_encabezado.planAnterior,
			CONCAT(usuario.nombre,usuario.apellidos) as usuario');
			$this->db->from('pagos_plan_encabezado');
			$this->db->join('usuario','pagos_plan_encabezado.usuario = usuario.id','inner');
			$this->db->where('pagos_plan_encabezado.pacienteid',$patid);
			$data = $this->db->get()->result();
			foreach($data as $item){
				$item->deudaFormato = number_format($item->deuda,2);
			}
			return $data;
		}
	#endregion
	
	#region Pagos
		public function patient_payment($patid){
			$this->db->select('pagos.fecha,
			DATE_FORMAT(pagos.fecha,"%m-%d-%Y") as fechaFormato,
			pagos.monto,
			pagos.notas,
			pagos.archivo,
			CONCAT(provider.LName," ",provider.FName) as provider,
			definition.ItemName');
			$this->db->from('pagos');
			$this->db->join('provider','pagos.doctor = provider.id','inner');
			$this->db->join('definition','pagos.tipo = definition.DefNum','inner');
			$this->db->where('pagos.patientid',$patid);
			$data = $this->db->get()->result();
			foreach($data as $item){
				$item->montoFormato = number_format($item->monto,2);
			}
			return $data;
		}
	#endregion
		
	/*actividades*/
	public function editar_nota($id,$nota){
		$actividad = array('nota' => $nota);
		$this->db->where('id',$id);
		$this->db->update('historial_actividades',$actividad);
		$error = $this->db->error();
		return $error;
	}
	
	public function editar_actividad($id,$nota,$tipo){
		$actividad = array(
		'nota' => $nota,
		'actividadtipo' => $tipo,
		);
		$this->db->where('id',$id);
		$this->db->update('historial_actividades',$actividad);
		$error = $this->db->error();
		return $error;
	}
	
	public function actividad_estado($id,$estadoid){
		if($estadoid == 2){
			$actividad = array('estadoid' => $estadoid,'fechaCompletado' => fechaactual('Y-m-d'));
		}else{
			$actividad = array('estadoid' => $estadoid,'fechaCompletado' => null);
		}
		$this->db->where('id',$id);
		$this->db->update('historial_actividades',$actividad);
		$error = $this->db->error();
		return $error;
	}
	
	public function detalle_actividad($id){
		$this->db->select('historial_actividades.id,
		historial_actividades.fecha,
		historial_actividades.hora,
		historial_actividades.usuario,
		historial_actividades.pacienteid,
		historial_actividades.tipoid,
		historial_actividades.nota,
		historial_actividades.etapaid,
		historial_actividades.estadoid,
		historial_actividades.ofertaid,
		historial_actividades.fechaInicio,
		historial_actividades.horaInicio,
		historial_actividades.horaTerminado,
		historial_actividades.actividadTipo,
		historial_actividades.archivoid');
		$this->db->from('historial_actividades');
		$this->db->where('historial_actividades.id',$id);
		$data = $this->db->get()->result();
		return $data;
	}
		
	public function lista_actividades($pacienteid,$ofertaid = "none",$etapaid = "none"){
		$this->db->select('historial_actividades.id,
		historial_actividades.inicioEtapa,
		historial_actividades.finEtapa,
		historial_actividades.fecha,
		historial_actividades.hora,
		historial_actividades.fechaInicio,		
		historial_actividades.horaInicio,
		historial_actividades.usuario,
		historial_actividades.pacienteid,
		historial_actividades.tipoid,
		historial_actividades.nota,
		historial_actividades.actividadTipo,
		historial_actividades.archivoid,
		actividades_tipo.nombre,
		actividad_estado.name as EstadoName,
		actividad_estado.nombre as EstadoNombre,
		actividad_estado.color as EstadoColor,
		usuario.nombre as usuarioNombre,
		usuario.apellidos as usuarioApellido,
		usuario.correo as usuarioCorreo');
		$this->db->from('historial_actividades');
		$this->db->join('actividades_tipo','historial_actividades.tipoid = actividades_tipo.id','left');
		$this->db->join('actividad_estado','historial_actividades.estadoid = actividad_estado.id','left');
		$this->db->join('usuario','historial_actividades.usuario = usuario.id','left');
		$this->db->where('historial_actividades.pacienteid',$pacienteid);
		if($ofertaid != 'none'){
			$this->db->where('historial_actividades.ofertaid',$ofertaid);
			$this->db->where('historial_actividades.etapaid',$etapaid);
		}
		$this->db->order_by('historial_actividades.id','DESC');
		//$this->db->order_by('historial_actividades.hora','DESC');
		$data = $this->db->get()->result();
		
		foreach($data as $actividad){
			if($actividad->tipoid == 3){
				$this->db->select('paciente_archivos.id,
				paciente_archivos.usuarioid,
				paciente_archivos.pacienteid,
				paciente_archivos.nombre,
				paciente_archivos.nombreFisico,
				paciente_archivos.descripcion,
				paciente_archivos.activo');
				$this->db->from('paciente_archivos');
				$this->db->where('paciente_archivos.id',$actividad->archivoid);
				$actividad->archivo = $this->db->get()->result();
			}else{
				$actividad->archivo = 'sin archivo';
			}
		}
		return $data;
	}
	
	public function lista_actividades_pendientes($pacienteid,$ofertaid = "none",$etapaid = "none"){
		$this->db->select('historial_actividades.id,
		historial_actividades.fecha,
		historial_actividades.hora,
		historial_actividades.fechaInicio,		
		historial_actividades.horaInicio,
		historial_actividades.usuario,
		historial_actividades.pacienteid,
		historial_actividades.tipoid,
		historial_actividades.nota,
		historial_actividades.actividadTipo,
		historial_actividades.archivoid,
		actividades_tipo.nombre,
		actividad_estado.name as EstadoName,
		actividad_estado.nombre as EstadoNombre,
		actividad_estado.color as EstadoColor,
		usuario.nombre as usuarioNombre,
		usuario.apellidos as usuarioApellido,
		usuario.correo as usuarioCorreo');
		$this->db->from('historial_actividades');
		$this->db->join('actividades_tipo','historial_actividades.tipoid = actividades_tipo.id','inner');
		$this->db->join('actividad_estado','historial_actividades.estadoid = actividad_estado.id','inner');
		$this->db->join('usuario','historial_actividades.usuario = usuario.id','inner');
		$this->db->where('historial_actividades.pacienteid',$pacienteid);
		if($ofertaid != 'none'){
			$this->db->where('historial_actividades.ofertaid',$ofertaid);
			$this->db->where('historial_actividades.etapaid',$etapaid);
		}
		$this->db->where('historial_actividades.tipoid',2);
		$this->db->where('historial_actividades.estadoid <>',2);
		$this->db->order_by('historial_actividades.fecha','DESC');
		$this->db->order_by('historial_actividades.hora','DESC');
		$data = $this->db->get()->result();
		
		foreach($data as $actividad){
			$fechai = new DateTime($actividad->fechaInicio);
			$fechaActual = new DateTime();
			$diferencia = date_diff($fechaActual,$fechai);
			$actividad->dias = $diferencia->days;
		}
		return $data;
	}
	
	public function agregar_actividad($nuevaActividad){
		$this->db->insert('historial_actividades',$nuevaActividad);
		//$data = $this->db->get_compiled_update();
		$error = $this->db->error();
		return $error;
	}
	
	public function provider($patid){
		$this->db->select('patient.PriProv');
		$this->db->from('patient');
		$this->db->where('patient.Id',$patid);
		$data = $this->db->get()->result();
		return $data;
		
	}

	private function createUL($items){
		$ul = '<ul class="list-group">';
		foreach($items as $item){			
			$ul .= '<li class="list-group-item padding-5"><small class="text-info ">'.$item->provider.'</small> <small class="pull-right text-success">$'.number_format($item->ProcFee,2).'</small><br>'.$item->Descript.'</li>';
		}
		$ul .= '</ul>';
		return $ul;
	}
}
?>