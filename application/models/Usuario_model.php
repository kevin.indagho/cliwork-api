<?php
class Usuario_model extends CI_Model{
	
	public function agregar($nuevoUsuario){
		$this->db->insert('usuario',$nuevoUsuario);
		$error = $this->db->error();
		return $error;
	}
	
	public function editar($usuario,$usuarioid){
		$this->db->where('usuario.id',$usuarioid);
		$this->db->update('usuario',$usuario);
		$error = $this->db->error();
		return $error;
	}
	
	public function detalle($usuarioid){
		$this->db->select('usuario.id,
		usuario.nombre,
		usuario.apellidos,
		usuario.correo,k
		usuario.contrasena,
		usuario.tipoid,
		usuario.activo,
		usuario.imagen');
		$this->db->from('usuario');
		$this->db->where('usuario.id',$usuarioid);
		$data = $this->db->get()->result();
		return $data;
	}
	
	public function lista($activo,$tipo = 'none'){
		$this->db->select(
		'usuario.id,
		usuario.nombre,
		usuario.apellidos,
		usuario.correo,
		usuario.tipoid,
		usuario.activo');
		$this->db->from('usuario');
		$this->db->where('oculto',0);
		if($activo != 'none'){ $this->db->where('usuario.activo',$activo); }
		if($tipo != 'none'){ 
			$tipos = explode(',',$tipo);
			$this->db->where_in('usuario.tipoid',$tipos); 
		}
		$data = $this->db->get()->result_array();
		return $data;
	}
	
	public function agenda($id){
		$this->db->select('historial_actividades.id,
		historial_actividades.fecha,
		historial_actividades.hora,
		historial_actividades.usuario,
		historial_actividades.tipoid,
		historial_actividades.nota as description,
		DATE_FORMAT(historial_actividades.fechaInicio,"%m/%d/%Y") as fechaInicio,
		historial_actividades.horaInicio,
		CONCAT(historial_actividades.fechaInicio," ",historial_actividades.horaInicio) as start,
		CONCAT(historial_actividades.fechaInicio," ",historial_actividades.horaTerminado) as end,
		historial_actividades.horaTerminado,		
		historial_actividades.actividadTipo');
		$this->db->from('historial_actividades');
		$this->db->where('historial_actividades.usuario',$id);
		$this->db->where('historial_actividades.tipoid',2);
		$data = $this->db->get()->result();
		foreach($data as $actividad){
			/*
			 aqua = 23ccef33;
			 orange = #ffa53433;
			 red = #fb404b33;
			 blue = #447df733
			 green = #87cb1633
			*/
			$actividad->backgroundColor = "#447df733";
			//$actividad->textColor = "#FFFFFF";
			if($actividad->actividadTipo == 0){ 
				$actividad->icon  = '<i class="fa fa-sticky-note"></i>';
				$actividad->title = 'Note'; 
			}
			if($actividad->actividadTipo == 1){ 
				$actividad->icon  = '<i class="fa fa-phone"></i>';
				$actividad->title = 'Call'; 
			}
			if($actividad->actividadTipo == 2){ 
				$actividad->title = '<i class="fa fa-users"></i>'; 
				$actividad->title = 'Meeting'; 
			}
			if($actividad->actividadTipo == 3){ 
				$actividad->title = '<i class="fa fa-clock-o"></i>';
				$actividad->title = 'Task';
			}
			if($actividad->actividadTipo == 4){ 
				$actividad->title = '<i class="fa fa-flag"></i>';
				$actividad->title = 'Deadline';
			}
			if($actividad->actividadTipo == 5){ 
				$actividad->title = '<i class="fa fa-envelope"></i>'; 
				$actividad->title = 'Email'; 
			}
			if($actividad->actividadTipo == 6){ 
				$actividad->title = '<i class="fa fa-cutlery"></i>';
				$actividad->title = 'Lunch';
			}
		}
		return $data;
	}
	
	public function notificaciones($usuarioid,$new){
		/**
		 * Leer las notificaciones que el usuario no ah marcado como leido
		 */
		$this->db->select('notificaciones.usuarioid,
		notificaciones.cuerpo,
		notificaciones.color,
		notificaciones.nueva,
		notificaciones.leida,
		notificaciones.fecha,
		notificaciones.tipoid,
		notificaciones.pacienteid,
		notificaciones.ofertaid,
		notificaciones.id');
		$this->db->from('notificaciones');
		$this->db->where('notificaciones.usuarioid',$usuarioid);
		if($new != 'none'){
			/**
			 * Unicamente leer las notificaciones nuevas, que no han sido mostradas al usuario
			 * Campo: notificaciones.nueva
			 */
			 $this->db->where('notificaciones.nueva',$new);	 
		}
		$this->db->order_by('notificaciones.fecha','DESC');
		$this->db->where('notificaciones.leida',0);
		$data['notificaciones'] = $this->db->get()->result();
		
		/**
		 * Contar las notifiaciones para mostrar en el badge
		 */
		$this->db->select('COUNT(notificaciones.leida) as cantidad');
		$this->db->from('notificaciones');
		$this->db->where('notificaciones.usuarioid',$usuarioid);
		if($new != 'none'){ $this->db->where('notificaciones.nueva',$new);	 }
		$this->db->where('notificaciones.leida',0);
		$data['cantidad'] = $this->db->get()->result();
		
		foreach($data['notificaciones'] as $noti){
			/**
			 * Cambiara el campo "notificaciones.nueva" a 0 
			 * para indicar que la notificacion ya ah sido mostrada al usuario.
			 */
			$this->db->where('notificaciones.id',$noti->id);
			$this->db->update('notificaciones',array('nueva' => 0));
		}
		return $data;
	}
		
	public function permisos($tipoid){
		$this->db->select('modulos.id,
		modulos.menu,
		modulos.submenu,
		modulos.nombre,
		modulos.description,
		modulos.descripcion,
		modulos_permisos.id as permisoid,
		modulos_permisos.permitido,
		modulos_permisos.usuariotipid');
		$this->db->from('modulos_permisos');
		$this->db->join('modulos','modulos_permisos.moduloid = modulos.id','inner');
		$this->db->where('modulos_permisos.usuariotipid',$tipoid);
		$permisos = $this->db->get()->result_array();
		return $permisos;
	}
	
	public function buscador($nombre,$tipo){
		$this->db->select('usuario.id,
		usuario.nombre,
		usuario.apellidos,');
		$this->db->from('usuario');
		$this->db->where('LOWER(CONCAT(usuario.nombre," ",usuario.apellidos)) like "%'.$nombre.'%"');
		$this->db->where('usuario.activo',1);
		$this->db->where('usuario.oculto',0);
		$this->db->where('usuario.tipoid',$tipo);		
		$data = $this->db->get()->result();
		//$data = $this->db->get_compiled_select();
		return $data;
	}
	
	/* inicio de sesion */
	public function autenticar($correo,$contrasenamd5){
		$this->db->select(
		'usuario.id,
		usuario.nombre,
		usuario.apellidos,
		usuario.tipoid,
		usuario.correo,
		usuario_tipo.name as tiponombre');
		$this->db->from('usuario');
		$this->db->join('usuario_tipo','usuario.tipoid = usuario_tipo.id','inner');
		$this->db->where('correo',$correo);
		$this->db->where('contrasena',$contrasenamd5);
		$data = $this->db->get();
		//print_r();
		//->row_array();
		
		//$data = $this->db->get_compiled_select();
		if($data->conn_id->affected_rows > 0){
			$data = $data->row_array();
			
			$permisos = $this->permisos($data['tipoid']);
			foreach($permisos as $permiso){
				$permisoArray[$permiso['id']] = $permiso;
			}
			$data['permisos'] = $permisoArray;
			$error['data'] = $data;
			$error['status'] = true;		
		}else{
			$error['status'] = false;
		}
		return $error;
	}
	
	/* PERMISOS */
	public function permiso_editar($permisoid,$tipoid,$permitido){
		$permiso = array('modulos_permisos.permitido'=> $permitido);
		$this->db->where('modulos_permisos.id',$permisoid);
		$this->db->update('modulos_permisos',$permiso);
		$error = $this->db->error();
		$error['permitido'] = boolval($permitido);
		if($permitido == 'true'){
			$error['message'] = 'Permission Enable';
		}else{
			$error['message'] = 'Permission Disable';
		}
		return $error;
	}	
	/***********/
}?>