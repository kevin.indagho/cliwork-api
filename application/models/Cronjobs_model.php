<?php 
Class Cronjobs_model extends CI_Model{
	
	public function account_list($patient){
		
		$itemList = array();
		$this->db->select('pagos_plan_encabezado.id,
		pagos_plan_encabezado.fecha as date,
		DATE_FORMAT(pagos_plan_encabezado.fecha,"%Y-%m-%d") as dateFormat,
		pagos_plan_encabezado.comision,
		pagos_plan_encabezado.deuda,
		CONCAT(usuario.nombre," ",usuario.apellidos) as user');
		$this->db->from('pagos_plan_encabezado');
		$this->db->join('usuario','pagos_plan_encabezado.usuario = usuario.id','inner');
		$this->db->where('pagos_plan_encabezado.activo',1);
		$this->db->where('pagos_plan_encabezado.pacienteid',$patient);
		$planPagos = $this->db->get()->result();
		foreach($planPagos as $plan){
			$plan->status = "2";
			$plan->TypeItem = "Comis";
		}
		
		$itemList = array_merge($itemList,$planPagos);
		
		$this->db->select('GROUP_CONCAT(treatplan.id) as id');
		$this->db->from('treatplan');
		$this->db->where('treatplan.PatNum',$patient);
		$tp = $this->db->get()->result();
		$tp = explode(',',$tp[0]->id);
		
		$this->db->select('procedurelog.Id,
		procedurelog.DateComplete as date,
		DATE_FORMAT(procedurelog.ProcDate,"%Y-%m-%d") as dateFormat,
		procedurecode.ProcCode,
		procedurecode.Descript,
		procedurecode.AbbrDesc,
		procedurelog.ToothNum,
		procedurelog.Priority,
		procedurelog.DateComplete,
		procedurelog.Surf,
		procedurelog.ProcFee,
		procedurelog.Pay,
		treatplan.Heading,
		CONCAT(provider.LName," ",provider.FName) as Doctor');
		$this->db->from('procedurelog');
		$this->db->join('treatplanattach','treatplanattach.ProcNum = procedurelog.Id','inner');
		$this->db->join('procedurecode','procedurelog.CodeNum = procedurecode.id','inner');
		$this->db->join('provider','procedurelog.ProvNum = provider.id','inner');
		$this->db->join('treatplan','treatplanattach.TreatPlanNum = treatplan.id','inner');
		$this->db->where_in('treatplanattach.TreatPlanNum',$tp);
		$this->db->where('procedurelog.ProcStatus',1);
		$proceduresUnComplete = $this->db->get()->result();
		foreach($proceduresUnComplete as $procedure){
			$procedure->ProcFeeFormat = number_format($procedure->ProcFee,2);
		}
		
		$this->db->select('Sum(procedurelog.ProcFee) as amount');
		$this->db->from('treatplan');
		$this->db->join('treatplanattach','treatplanattach.TreatPlanNum = treatplan.id','inner');
		$this->db->join('procedurelog','treatplanattach.ProcNum = procedurelog.Id','inner');
		$this->db->where('procedurelog.ProcStatus',2);
		$this->db->where_in('treatplan.id',$tp);
		$total = $this->db->get()->result();
		
		$this->db->select('procedurelog.Id,
		procedurelog.DateComplete as date,
		DATE_FORMAT(procedurelog.ProcDate,"%Y-%m-%d") as dateFormat,
		procedurecode.ProcCode,
		procedurecode.Descript,
		procedurecode.AbbrDesc,
		procedurelog.ToothNum,
		procedurelog.Priority,
		procedurelog.DateComplete,
		procedurelog.Surf,
		procedurelog.ProcFee,
		procedurelog.Pay,
		treatplan.Heading,
		CONCAT(provider.LName," ",provider.FName) as Doctor');
		$this->db->from('procedurelog');
		$this->db->join('treatplanattach','treatplanattach.ProcNum = procedurelog.Id','inner');
		$this->db->join('procedurecode','procedurelog.CodeNum = procedurecode.id','inner');
		$this->db->join('provider','procedurelog.ProvNum = provider.id','inner');
		$this->db->join('treatplan','treatplanattach.TreatPlanNum = treatplan.id','inner');
		$this->db->where_in('treatplanattach.TreatPlanNum',$tp);
		$this->db->where('procedurelog.ProcStatus',2);
		$procedures = $this->db->get()->result();
		foreach($procedures as $procedure){
			$procedure->status = "2";
			$procedure->TypeItem = "proc";
			$procedure->ProcFeeFormat = number_format($procedure->ProcFee,2);
		}
		$itemList = array_merge($itemList,$procedures);
		
		$this->db->select('pagos.fecha as date,
		DATE_FORMAT(pagos.fecha,"%Y-%m-%d") as dateFormat,
		pagos.id,
		pagos.monto,
		pagos.notas,
		pagos.archivo,
		pagos.usuarioid,
		pagos.treatplanid,
		pagos.patientid,
		definition.ItemName as TypeName,
		CONCAT(usuario.nombre," ",usuario.apellidos) as user');
		$this->db->from('pagos');
		$this->db->join('treatplan_pagos','treatplan_pagos.pagoid = pagos.id','inner');
		$this->db->join('definition','pagos.tipo = definition.DefNum','inner');
		$this->db->join('usuario','pagos.usuarioid = usuario.id','inner');
		$this->db->where_in('treatplan_pagos.treatplanid',$tp);
		$this->db->group_by('pagos.id');
		$paymentsSplit = $this->db->get()->result();
		foreach($paymentsSplit as $paymentSplit){
			$paymentSplit->status = "2";
			$paymentSplit->TypeItem = "pay";
			$paymentSplit->montoFormato = number_format($paymentSplit->monto,2);
			//$payments[] = $paymentSplit;
		}
		$itemList = array_merge($itemList,$paymentsSplit);
		
		$this->db->select('ajustes.id,
			ajustes.fecha as date,
			DATE_FORMAT(ajustes.fecha,"%Y-%m-%d") as dateFormat,
			ajustes.monto,
			ajustes.tipo,
			ajustes.notas,
			ajustes.pacienteid,
			CONCAT(usuario.nombre," ",usuario.apellidos) as user,
			treatplan.Heading,
			definition.ItemName,
			definition.ItemValue');
			$this->db->from('ajustes');
			$this->db->join('usuario','ajustes.usuarioid = usuario.id','inner');
			$this->db->join('definition','ajustes.tipo = definition.DefNum','inner');
			$this->db->join('treatplan','ajustes.treatplanid = treatplan.id','inner');
			$this->db->where_in('ajustes.treatplanid',$tp);			
			$adjustments = $this->db->get()->result();
			foreach($adjustments as $adjustment){
				$adjustment->status = "2";
				$adjustment->TypeItem = "adjust";
				$adjustment->montoFormato = number_format($adjustment->monto,2);
			}
		
			$itemList = array_merge($itemList,$adjustments);
			$keys = array_column($itemList, 'date');
			array_multisort($keys, SORT_ASC, $itemList);
			$data['list'] = $itemList;
			$data['total'] = $total;
			$data['uncomplete'] = $proceduresUnComplete;
		return $data;
	}	
	
}