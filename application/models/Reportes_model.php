<?php
Class Reportes_model extends CI_Model{
	
	public function ventas_activas(){
		$fecha = new DateTime();
		$this->db->select('ofertas.id,
		ofertas.fecha,
		ofertas.pacienteid,
		CONCAT(patient.LName," ",patient.FName) AS patient,
		CONCAT(usuario.nombre," ",usuario.apellidos) AS usuario,
		ofertas.valor,
		ofertas.estadoid,
		etapa.name as StepName');
		$this->db->from('ofertas');
		$this->db->join('patient','ofertas.pacienteid = patient.Id','inner');
		$this->db->join('usuario','ofertas.usuarioid = usuario.id','inner');
		$this->db->join('etapa','ofertas.etapaid = etapa.id','inner');
		$this->db->where('ofertas.activo',1);
		$data = $this->db->get()->result();
		foreach($data as $oferta){
			$oferta->valorFormat = '<b class="text-success">$ '.number_format($oferta->valor,2).'</b>';
			$datetime1 = date_create($fecha->format('Y-m-d'));
			$datetime2 = date_create($oferta->fecha);
			$duration = date_diff($datetime1, $datetime2);
			$oferta->days = $duration->days;
			$this->db->select('historial_actividades.fecha,
			actividades_tipo.nombre,
			historial_actividades.ofertaid,
			actividad_estado.color as EstadoColor,
			actividad_estado.icon as EstadoIcon');
			$this->db->from('historial_actividades');
			$this->db->join('actividades_tipo','historial_actividades.tipoid = actividades_tipo.id','inner');
			$this->db->join('actividad_estado','historial_actividades.estadoid = actividad_estado.id','inner');
			$this->db->where('historial_actividades.ofertaid',$oferta->id);
			$this->db->order_by('historial_actividades.fecha','DESC');
			$this->db->limit(1);
			$oferta->lastact = $this->db->get()->result();
			foreach($oferta->lastact as $act){
				$datetime1 = date_create($fecha->format('Y-m-d'));
				$datetime2 = date_create($act->fecha);
				$duration = date_diff($datetime1, $datetime2);
				$act->days = $duration->days;
			}
			
			$this->db->select('etapa_control.id,
			etapa_control.etapaid,
			etapa_control.ofertaid,
			etapa_control.fechaInicio,
			etapa_control.fechaFin,
			etapa_control.terminado,
			etapa.name as EtapaNombre,
			etapa.dias');
			$this->db->from('etapa_control');
			$this->db->join('etapa','etapa_control.etapaid = etapa.id','inner');
			$this->db->where('etapa_control.ofertaid',$oferta->id);
			$etapaControl = $this->db->get()->result();
			foreach($etapaControl as $etapa){
				$fechaI = date_create($etapa->fechaInicio);
				$fechaF = date_create($etapa->fechaFin);
				$etapa->duration = date_diff($fechaI,$fechaF);
			}
			$oferta->etapasControl = $etapaControl;
		}
		return $data;
	}
	
	public function ventas_informes(){
		$this->db->select('SUM(ofertas.valor) as total');
		$this->db->from('ofertas');
		$this->db->where('ofertas.activo',1);
		$total = $this->db->get()->result();
		
		$data['total'] = $total[0]->total;
		return $data;
	}
	
	public function actividades_vencidas(){
		$this->db->select('ofertas.fecha as date,
		CONCAT(patient.LName," ",patient.FName) as patient,
		ofertas.id,
		ofertas.valor as value,
		ofertas.estadoid,
		patient.id as patientid,
		CONCAT(usuario.nombre," ",usuario.apellidos) as user');
		$this->db->from('ofertas');
		$this->db->join('patient','ofertas.pacienteid = patient.Id','inner');
		$this->db->join('usuario','ofertas.usuarioid = usuario.id','inner');
		$this->db->join('historial_actividades','historial_actividades.ofertaid = ofertas.id','inner');
		$this->db->where('historial_actividades.estadoid',1);
		$this->db->group_by('ofertas.id');
		$data = $this->db->get()->result();
		foreach($data as $oferta){
			
			$oferta->value = number_format($oferta->value,2);
			
			$this->db->select('GROUP_CONCAT(tratamiento.name) AS ing,
			GROUP_CONCAT(tratamiento.nombre) AS esp,');
			$this->db->from('ofertas_tratamientos');
			$this->db->join('tratamiento','ofertas_tratamientos.tratamientoid = tratamiento.id','inner');
			$this->db->where('ofertas_tratamientos.ofertaid',$oferta->id);
			$oferta->treatments = $this->db->get()->result();
			
			$this->db->select('historial_actividades.id,
			historial_actividades.fecha,
			historial_actividades.hora,
			historial_actividades.usuario,
			historial_actividades.pacienteid,
			historial_actividades.tipoid,
			historial_actividades.nota,
			historial_actividades.etapaid,
			historial_actividades.estadoid,
			historial_actividades.ofertaid,
			historial_actividades.fechaInicio,
			historial_actividades.horaInicio,
			historial_actividades.horaTerminado,
			historial_actividades.actividadTipo,
			historial_actividades.archivoid');
			$this->db->from('historial_actividades');
			$this->db->where('historial_actividades.ofertaid',$oferta->id);
			$this->db->where('historial_actividades.tipoid',2);
			$this->db->order_by('fechaInicio','ASC');
			$oferta->activities = $this->db->get()->result();
			foreach($oferta->activities as $actividad){
				$fechaInicio  = new DateTime($actividad->fechaInicio);
				$fechaActual  = new DateTime();
				$intervalo = date_diff($fechaInicio,$fechaActual);
				$actividad->dias = $intervalo->days;
			}
		}
		return $data;
	}
}
?>