var dtnotifications;
$(document).ready(function(){
	
	$.ajax({
		url: urlapi+"Api_usuariotipo/tipos",
		Type:"GET",
		dataType:"json",
		data: { activo:"1"},
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,item){
				$('#form_edit [name="type[]"]').append('<option value="'+item.id+'">'+item.name+'</option>');
			});
			$('.bs-select').val('default').selectpicker('refresh');
		}
	});
	
	dtconfig = $("#dtconfig").DataTable({
		"ajax": {
			url: urlapi+"Api_data/confignoti",
			type: "get",
			beforeSend: function (xhr){
				xhr.setRequestHeader('Authorization', auth);
			},
			data: {activo: "none"},
		},
		columns: [
			{data: 'nombre', class:"padding-5"},
			{data: 'descripcion', class:"padding-5"},
			{data: 'instrucciones', class:"padding-5"},
			{data: 'tiposName', class:"padding-5"},
			{defaultContent: '<button class="edit btn btn-xs btn-warning"><i class="fa fa-pencil"></i> </button>', class:"padding-5"}
		],
		"drawCallback": function( settings ){ $("[data-toggle='tooltip']").tooltip(); },
		"rowCallback": function( row, data ){
			//$('td:eq(2)',row).html(genero);
		}
	});
	
	$.validate({
		form : '#form_edit',
		modules : 'file,logic',
		validateOnBlur : true,
		errorMessagePosition : 'top',
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form) {
			
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.code == 0){
						apptools.sweetNoty('success','Complete','Update configuration');
						dtconfig.ajax.reload();
					}else{
						apptools.sweetNoty('error','Error','');
					}
				}
			});
		  return false; // Will stop the submission of the form
		}
	});
	
	$('#dtconfig tbody').on('click','.edit',function(){
		var data = dtconfig.row($(this).parents('tr')).data();
		
		if(data.tipos != '' && data.tipos != null){
			tipos = data.tipos.split(',');
			$('#form_edit [name="type[]"]').val(tipos).trigger('change');
			//$('#form_edit [name="type[]"]').prop('disabled',false);
		}else{
		}
		
		$('#form_edit [name="configid"]').val(data.id);
		$('#form_edit [name="name"]').val(data.nombre);
		$('#form_edit [name="description"]').val(data.descripcion);
		$('#form_edit [name="configuration"]').val(data.instrucciones);
		$('#modal_edit').modal();
	});
	
});