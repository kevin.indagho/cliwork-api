var options = {
	users: []
};

$(document).ready(function(){
	$(".mask").mask('99:99');	
	$(".timepicker").datetimepicker({
		stepping: 5,
		format: "hh:mm A",
		//minDate: {hour: 14, minute: 30},
		useCurrent: false
	});
	
	
	dtconfig = $('#dtconfig').DataTable({
		"ajax": {
			url: urlapi+"Api_data/config",
			type: "get",
			beforeSend: function (xhr){
				xhr.setRequestHeader('Authorization', auth);
			},
			data: {id: "none"},
		},
		columns: [
			{data: 'nombre'},
			{data: 'descripcion'},
			{data: 'instrucciones'},
			{defaultContent: '<button class="edit btn btn-xs btn-warning"><i class="fa fa-pencil"></i></button>',width:"15px"}
		],
		"drawCallback": function( settings ){ $("[data-toggle='tooltip']").tooltip(); },
		"rowCallback": function( row, data ){ }
	});
	
	$('#dtconfig tbody').on('click','.edit',function(){
		var data = dtconfig.row($(this).parents('tr')).data();
		$('#bodyconfig').empty();
		if(data.id == 1){
			$('#form_config [name="configid"]').val(data.id);
			$('#bodyconfig').append('<div class="form-group">'
					+'<label class="col-md-12 control-label">'+data.nombre+'</label>'
					+'<div class="col-md-12">'
						+'<input name="instrucciones" type="text" class="form-control input-sm" data-validation="required" value="'+data.instrucciones+'">'
						+'<span class="help-block">'+data.descripcion+'</span>'
					+'</div>'
				+'</div>');
			$('#modal_config').modal();
		}
		else if(data.id == 2){
			$('#form_config [name="configid"]').val(data.id);
			$('#bodyconfig').append('<div class="form-group">'
					+'<label class="col-md-12 control-label">'+data.nombre+'</label>'
					+'<div class="col-md-12">'
						+'<input name="instrucciones" type="text" class="form-control input-sm" data-validation="required" value="'+data.instrucciones+'">'
						+'<span class="help-block">'+data.descripcion+'</span>'
					+'</div>'
				+'</div>');
			$('#modal_config').modal();
		}
		else if(data.id == 3){
			time = data.instrucciones;
			time = time.split("-");
			$('#form_config_timeclinic [name="configid"]').val(data.id);
			$('#form_config_timeclinic [name="starthour"]').val(time[0]);
			$('#form_config_timeclinic [name="finishhour"]').val(time[1]);
			$('#modal_config_timeclinic').modal();
		}
		else if(data.id == 4){
			$('#form_config [name="configid"]').val(data.id);
			$('#bodyconfig').append('<div class="form-group">'
					+'<label class="col-md-12 control-label">'+data.nombre+'</label>'
					+'<div class="col-md-12">'
						+'<input name="instrucciones" type="text" class="form-control input-sm" data-validation="required" value="'+data.instrucciones+'">'
						+'<span class="help-block">'+data.descripcion+'</span>'
					+'</div>'
				+'</div>');
			$('#modal_config').modal();
		}
	});



	$.ajax({
		url: urlapi+"Api_usuario/lista",
		type: "GET",
		data: {activo: 1},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,usuario){
				$('#form_config_users [name="emails"]').append($('<option>',{ text: usuario.nombre+" "+usuario.apellidos, value: parseInt(usuario.id)}));
			});
			$('.multiselect').multiSelect('refresh');
		}
	});
	
	$('#timeclinic').on('click',function(){
		$.ajax({
			url: urlapi+"Api_data/config",
			type: "GET",
			data: {id: 3},
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
			success: function(response){
				time = response.data[0].instrucciones;
				time = time.split("-");
				$('#form_config_timeclinic [name="starthour"]').val(time[0]);
				$('#form_config_timeclinic [name="finishhour"]').val(time[1]);
			},
			complete: function(){
				$('#modal_config_timeclinic').modal();
			}
		});
	});


	$.validate({
		form : '#form_config_timeclinic',
		modules : 'logic',
		validateOnBlur : true, 
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onError : function($form){
			//apptools.notyCustom('error','fields Required','Complete All inputs');
		},
		onSuccess : function($form){
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			var data = $form.serializeArray();
			//data.push({name: 'id', value: patientid});
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.data.code == 0){
						apptools.sweetNoty('success','Complete','Update time clinic');
						dtconfig.ajax.reload();
						$('#modal_config_timeclinic').modal('hide');
					}else{
						notyError(response.message);
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	$.validate({
		form : '#form_config',
		modules : 'logic',
		validateOnBlur : true, 
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onError : function($form){
			//apptools.notyCustom('error','fields Required','Complete All inputs');
		},
		onSuccess : function($form){
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			var data = $form.serializeArray();
			//data.push({name: 'id', value: patientid});
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.data.code == 0){
						apptools.sweetNoty('success','Complete','Update config');
						dtconfig.ajax.reload();
						$('#modal_config_timeclinic').modal('hide');
					}else{
						notyError(response.message);
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
});