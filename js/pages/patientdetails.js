var ofertaid = $('#ofertaid').val();
var patientid = $('#pacienteid').val();
var EtapaId;
var dateEvent;
var hourEvent;
var stages = [];
var EtapaAcual = "";

Dropzone.options.formaddfile = {
	url: urlapi+"Api_paciente/agregararchivos",
	addRemoveLinks : true,
	paramName: "file", 
	maxFilesize: 2, // MB
	headers: {'Authorization': auth},
	acceptedFiles: ".jpg,.png,application/pdf",
	params: {
		"patient": patientid,
		"user": apptools.userlogin,
		"ofertaid": ofertaid,
	},
	accept: function(file, done){
		done();
	},success: function(file, message) {
		if(message.code == 1){
			$(file.previewElement).addClass("dz-error").find('.dz-error-message').text(message.error);
		}else{
			$(file.previewElement).addClass("dz-success");
		}
		notyCustom(message.type,message.title,message.error);
		actividades();
		
	},error: function(file, message) {
		$(file.previewElement).addClass("dz-error").find('.dz-error-message').text(message.error);
	}
};

$(document).ready(function(){
	
	$(".timepicker").datetimepicker({
		stepping: 5,
		format: "hh:mm A",
		//minDate: {hour: 14, minute: 30},
		useCurrent: false
	});
	$(".datetimepicker").datetimepicker({stepping: 5,format: "YYYY-MM-DD", useCurrent: false});
	/*$("#form_addact [name='finishhour']").datetimepicker({
		stepping: 5,
		format: "LT",
		//minDate: {hour: 14, minute: 30},
		useCurrent: false
	});*/
	
	
	$.ajax({
		url: urlapi+"Api_paciente/detalle",
		type: "GET",
		data: {id: patientid},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			patient = response.data;
			$('#conHmPhone')[0].innerHTML = patient[0].HmPhone;
			$('#conWkPhone')[0].innerHTML = patient[0].WkPhone;
			$('#conWiPhone')[0].innerHTML = patient[0].WirelessPhone;
			$('#conEmail').text(patient[0].Email);
			$('#conAddress').text(patient[0].Address);
			$('#patname').text(patient[0].FName+" "+patient[0].LName);
			$('#pataddress').text(patient[0].Address);
			//$('#form_sendemail [name="to"]').val(patient[0].Email);
			$('#form_sendemail [name="to"]').tagsinput('add', patient[0].Email);
			//$('#form_sendemail [name="to"]').tagsinput('add', {id:patient[0].Email, label:patient[0].Email});
			loadStage();
		}
	});	
	
	$.ajax({
		url: urlapi+"Api_usuario/lista",
		type: "GET",
		data: {activo: 1},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,user){
				$('#form_addact [name="user"]').append($('<option>',{
					value: user.id,
					text: user.nombre+" "+user.apellidos
				}));
			});
			$('#form_addact [name="user"]').val(apptools.userlogin).trigger('change');
		}
	});
	
	$.ajax({
		url: urlapi+"Api_data/archivos",
		type: "GET",
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,file){
				$('#form_sendemail [name="templates[]"]').append($('<option>',{
					value: file.fisico,
					text: file.nombre
				}));
			});
			$('.bs-select').selectpicker('refresh');
		}
	});	
	
	var calendar = $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: ''
        },
		height: 650,
		minTime: "08:00:00",
		maxTime: "19:00:00",
		axisFormat: 'LT',
		timeFormat: 'LT',
		slotDuration: '00:05:00',
		slotLabelInterval : "00:05:00",
		slotLabelFormat : "h:mm a",
		defaultView: 'agendaDay',
		validRange: function(nowDate) {
			return {
			  start: nowDate,
			  end: nowDate.clone().add(1, 'months')
			};
		},
        buttonIcons: {
            prev: 'icon-chevron-left',
            next: 'icon-chevron-right',
            prevYear: 'icon-chevron-left',
            nextYear: 'icon-chevron-right'
        },
        eventRender: function(event, element) {
			element.popover({
				html: true,
				title: event.icon+" "+event.title,
				content: event.description,
				trigger: 'hover',
				placement: 'top',
				container: 'body'
			  });
		},
		editable: false,
        droppable: false,
        selectable: false,
        selectHelper: false,
    });
	
	$('.summernote').summernote({
		height: 250,
		toolbar: [
			// [groupName, [list of button]]
			['style', ['bold', 'italic', 'underline', 'clear']],
			['para', ['ul', 'ol', 'paragraph']],
			['fontsize', ['fontsize']],
			['color', ['color']]
		]
	});
	
	$.validate({
		form : '#form_addact',
		onError : function($form) {
			notyFormRequired();
		},
		onSuccess : function($form) {
			var isEmpty = $('#form_addact [name="note"]').summernote('isEmpty');
			if(isEmpty == false){
				var data = $form.serializeArray();
				data.push({name: 'id', value: patientid});
				data.push({name: 'userid', value: apptools.userlogin});
				data.push({name: 'ofertaid', value: "none"});
				$.ajax({
					url: urlapi+$form.attr('action'),
					type: $form.attr('method'),
					data: data,
					dataType: "json",
					beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
					success: function(response){
						if(response.code == 0){
							calendar.fullCalendar('refetchEvents');
							resetForm("form_addact");
							$('#form_addact [name="note"]').summernote('reset');
							actividades();
							notyFormSuccess();
						}else{
							notyError(response.message);
						}
					}
				});
			}else{
				notyCustom('information','<i class="fa fa-info-circle"></i> Description required','This field is required.');
			}
		  return false; // Will stop the submission of the form
		},
	});
	
	$.validate({
		form : '#form_addnote',
		onError : function($form) {
			notyFormRequired();
		},
		onSuccess : function($form) {
			var isEmpty = $('#form_addnote [name="note"]').summernote('isEmpty');
			if(isEmpty == false){
				var data = $form.serializeArray();
				data.push({name: 'id', value: patientid});
				data.push({name: 'ofertaid', value: ofertaid});
				data.push({name: "userid", value: apptools.userlogin })
				$.ajax({
					url: urlapi+$form.attr('action'),
					type: $form.attr('method'),
					data: data,
					dataType: "json",
					beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
					success: function(response){
						if(response.code == 0){
							$('#form_addnote [name="note"]').summernote('reset');
							actividades();
							notyFormSuccess();
						}else{
							notyError(response.message);
						}
					}
				});
			}else{
				notyCustom('information','<i class="fa fa-info-circle"></i> Note required','This field is required.');
			}
		  return false; // Will stop the submission of the form
		},
	});
	
	$.validate({
		form : '#form_sendemail',
		onError : function($form) {
			notyFormRequired();
		},
		onSuccess : function($form){	
			var isEmpty = $('#form_sendemail [name="body"]').summernote('isEmpty');
			if(isEmpty == false){
				var data = new FormData($form[0]);
				data.append("id",patientid);
				data.append("userid",apptools.userlogin);
				data.append("ofertaid",ofertaid);
				$.ajax({
					url: urlapi+$form.attr('action'),
					type: $form.attr('method'),
					data: data,
					enctype: 'multipart/form-data',
					processData: false,
					contentType: false,
					cache: false,
					//dataType: "json",
					beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
					success: function(response){
						if(response.code == 0){
							notyCustom('success','Complete','Email sended and saved.');
							$('#form_sendemail').get(0).reset();
							$('#form_sendemail [name="body"]').summernote('reset');
							actividades();
						}else{
							notyError(response.msg);
						}
					}
				});
			}else{
				notyCustom('information','<i class="fa fa-info-circle"></i> Body required','This field is required.');
			}
		  return false; // Will stop the submission of the form
		},
	});
	
	$.validate({
		form : '#form_editnote',
		onError : function($form) {
			notyFormRequired();
		},
		onSuccess : function($form) {
			var isEmpty = $('#form_editnote [name="note"]').summernote('isEmpty');
			if(isEmpty == false){
				var data = new FormData($form[0]);
				data.append("patientid",patientid);
				data.append("userid",apptools.userlogin);
				$.ajax({
					url: urlapi+$form.attr('action'),
					type: $form.attr('method'),
					data: data,
					enctype: 'multipart/form-data',
					processData: false,
					contentType: false,
					cache: false,
					//dataType: "json",
					beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
					success: function(response){
						console.log(response);
						if(response.code == 0){
							notyCustom('success','Complete','Note update.');
						}else{
							notyError(response.msg);
						}
					}
				});
			}else{
				notyCustom('information','<i class="fa fa-info-circle"></i> Body required','This field is required.');
			}
		  return false; // Will stop the submission of the form
		},
	});
	
	$.validate({
		form : '#form_editact',
		onError : function($form) {
			notyFormRequired();
		},
		onSuccess : function($form) {
			var isEmpty = $('#form_editact [name="note"]').summernote('isEmpty');
			if(isEmpty == false){
				var data = new FormData($form[0]);
				data.append("patientid",patientid);
				data.append("userid",apptools.userlogin);
				$.ajax({
					url: urlapi+$form.attr('action'),
					type: $form.attr('method'),
					data: data,
					enctype: 'multipart/form-data',
					processData: false,
					contentType: false,
					cache: false,
					//dataType: "json",
					beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
					success: function(response){
						console.log(response);
						if(response.code == 0){
							notyCustom('success','Complete','Note update.');
						}else{
							notyError(response.msg);
						}
					}
				});
			}else{
				notyCustom('information','<i class="fa fa-info-circle"></i> Body required','This field is required.');
			}
		  return false; // Will stop the submission of the form
		},
	});

	$("#form_addact [name='startdate']").on("dp.change",function(e){
		fecha = $(this).val();
		$('#calendar').fullCalendar('gotoDate',fecha);
		$('#form_addact [name="starthour"]').prop('disabled',false);
		$('#form_addact [name="finishhour"]').prop('disabled',false);
	});
	
	$(".timepicker").on("dp.change",function(e){
		validarActividad();
	});
	
	$('#form_addact [name="user"]').on('change',function(){
		var iduser = $(this).val();
		$.ajax({
			url: urlapi+"Api_usuario/agenda",
			type: "GET",
			data: {id: iduser},
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
			success: function(response){
				$('#form_addact [name="startdate"]').prop('disabled',false);
				calendar.fullCalendar("removeEvents");
				calendar.fullCalendar('addEventSource', response);
				calendar.fullCalendar('refetchEvents');
			}
		});
	});
	
	$('#form_sendemail [name="files[]"]').on('change',function(){
		$('#fileinputs').append('<label class="col-md-2">File :</label><div class="col-md-10"><input name="files[]" type="file" class="form-control"multiple /></div>');
	});
	
	$('#form_addfiles [name="files[]"]').on('change',function(){
		$('#form_addfiles [name="name"]').val($(this)[0].files[0].name);
	});
	
	actividades();
});

function loadStage(){
	/*Cargar etapa actual del paciente*/
	$.ajax({
		url: urlapi+"Api_data/etapas",
		type: "GET",
		dataType: "json",
		data:{activo: 1},
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,item){
				$('#smartwizard ul').append('<li><a data-id="'+item.id+'">'+item.name+'<br /><small>This is step description</small></a></li>');
				stages.push(item.name);
			});
			
			index = stages.findIndex(checkAdult);
			
			 $('#smartwizard').smartWizard({
					selected: index,
					autoAdjustHeight: true,
					theme: 'arrows',
					transitionEffect:'fade',
					showStepURLhash: false,
					anchorSettings: {
						anchorClickable: true, // Enable/Disable anchor navigation
						enableAllAnchors: true, // Activates all anchors clickable all times
						markDoneStep: false, // Add done css
						markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
						removeDoneStepOnNavigateBack: false, // While navigate back done step after active step will be cleared
						enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
					}
			});
			
			$("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection) {
				EtapaId = anchorObject.data('id');
				$.ajax({
					url: urlapi+"Api_paciente/editaretapaoferta",
					type:"POST",
					dataType:"json",
					data:{
						etapa: EtapaId, 
						idoferta: ofertaid,
						userid: apptools.userlogin,
						patientid: patientid
					},
					beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
					success: function(response){
						if(response.code == 0){
							apptools.shownoty("information",'<i class="fa fa-refresh fa-spin fa-fw"></i> Stages Update',"Updating list of stages");
							actividades();
						}
					}							
				});
			});
			
			
			$("#ise_custom_values").ionRangeSlider({
				grid: true,
				disable: false,
				from: index,
				//from_fixed: true,
				//to_fixed: false,
				values: stages,
				onFinish: function(data){
					console.log(data);
				}
			});
		}
	});
}

function checkAdult(stages){
	return stages == EtapaAcual;
}

function editnote(actid){
	$.ajax({
		url: urlapi+"Api_paciente/actividaddetalle",
		type: "GET",
		data: {id: actid},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			data = response;
			console.log(data);
			$("#form_editnote [name='id']").val(response[0].id);
			$("#form_editnote [name='note']").summernote("code", response[0].nota);
		}
	});
	$('#modal_editnote').modal();
}

function editact(actid){
	$.ajax({
		url: urlapi+"Api_paciente/actividaddetalle",
		type: "GET",
		data: {id: actid},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			data = response;
			console.log(data);
			$("#form_editact [name='id']").val(response[0].id);
			$("#form_editact [name='type'][value='"+response[0].actividadTipo+"']").attr('checked',true);
			$("#form_editact [name='type'][value='"+response[0].actividadTipo+"']").parents('label').addClass('active');
			$("#form_editact [name='note']").summernote("code", response[0].nota);
		}
	});
	$('#modal_editact').modal();
}

function validarActividad(){
	msg = "";	
	fechaOriginal = $("#form_addact [name='startdate']").val();
	horai = $("#form_addact [name='starthour']").val();
	horaf = $("#form_addact [name='finishhour']").val();
	
	fechaInicio = fechaOriginal+" "+horai;
	fechaInicio = moment(fechaInicio).format('MM/DD/YYYY HH:mm');
	
	fechaFin = fechaOriginal+" "+horaf;
	fechaFin = moment(fechaFin).format('MM/DD/YYYY HH:mm');
	
	$('#calendar').fullCalendar('clientEvents', function(events){
		if(fechaOriginal == events.fechaInicio){
			eventoInicio = moment(events.start).format('MM/DD/YYYY HH:mm');
			eventoFin = moment(events.end).format('MM/DD/YYYY HH:mm');
			
			if(fechaInicio >= eventoInicio && fechaInicio < eventoFin){
				ocupadoI = true;
				msg  = "hour Start: Not available";
			}else{
				ocupadoI = false;
			}
			
			if(fechaFin >= eventoInicio && fechaFin <= eventoFin){
				ocupadoF = true;
				msg ="Hour Finish: Not available ";
			}else{
				ocupadoF = false;
			}
			
			if(ocupadoI == true && ocupadoF == true){
				msg = "hour Start: Not available <br> Hour Finish: Not available ";
			}
			
			if(ocupadoI == true || ocupadoF == true){
				$.noty.closeAll();
				apptools.shownoty('warning','Not Available',msg);
				$('#form_addact button[type="submit"]').prop('disabled',true);
			}else{
				$('#form_addact button[type="submit"]').prop('disabled',false);
			}
			
		}
	});
	
}

function actividades(){
	$.ajax({
		url: urlapi+"Api_paciente/listaactividades",
		type: "GET",
		data: {id: patientid,ofertaid: 'none',etapaid: 'none'},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$('#activitiestimeline').empty();
			$.each(response.data,function(i,actividad){
				var editar = '';
				var vencimiento = '';
				var documentlink = '';
				var tipo = '';
				if(actividad.tipoid == 1){ editar = '<a onclick="editnote('+actividad.id+');" class="text-muted margin-right-10"><span class="fa fa-pencil"></span> Edit</a>'; }
				if(actividad.tipoid == 2){ 
					editar = '<a onclick="editact('+actividad.id+');" class="text-muted margin-right-10"><span class="fa fa-pencil"></span> Edit</a>'; 
					vencimiento = '<span class="text-danger"> Due Date: <b>'+actividad.fechaInicio+' '+actividad.horaInicio+'</b></span>'; 
				}
				if(actividad.tipoid == 3){
					tipo = '<i class="fa fa fa-paperclip"></i>';
					documentlink = '<a href="'+pathapi+''+actividad.archivo[0].nombreFisico+'" class="text-muted margin-right-10" download><span class="fa fa-cloud-download"></span> download file</a>'; 
				}
				if(actividad.actividadTipo == 0){ tipo = '<i class="fa fa-sticky-note"></i>'; }
				if(actividad.actividadTipo == 1){ tipo = '<i class="fa fa-phone"></i>'; }
				if(actividad.actividadTipo == 2){ tipo = '<i class="fa fa-users"></i>'; }
				if(actividad.actividadTipo == 3){ tipo = '<i class="fa fa-clock-o"></i>'; }
				if(actividad.actividadTipo == 4){ tipo = '<i class="fa fa-flag"></i>'; }
				if(actividad.actividadTipo == 5){ tipo = '<i class="fa fa-envelope"></i>'; }
				if(actividad.actividadTipo == 6){ tipo = '<i class="fa fa-cutlery"></i>'; }
				
				$('#activitiestimeline').append('<div class="app-timeline-item">'
					+'<div class="dot dot-'+actividad.EstadoColor+'"></div>'
					+'<div id="act'+actividad.id+'" class="content">'
						+'<div class="title">'+tipo+' <strong><a href="#">'+actividad.usuarioNombre+' '+actividad.usuarioApellido+'</a></strong>'+vencimiento+'</div>'
						+'<div class="scrollCustom" style="max-height: 240px;">'
						+actividad.nota
						+'</div>'
						+'<p>'
							+ '<a onclick="expandir('+actividad.id+');" class="text-muted margin-right-10"><span class="fa fa-expand"></span> Expand</a>'
							+ editar
							+ documentlink
							/*+'<a href="#" class="text-muted margin-right-10"><span class="fa fa-comment"></span> Comments</a>'*/
							/*+'<a href="#" class="text-muted"><span class="fa fa-bullhorn"></span> Report</a>'*/
							+'<span class="pull-right text-muted"> <i class="fa fa-calendar"></i> '+actividad.fecha+'</span>'
						+'</p>'
					+'</div>'
				+'</div>');
			});
			$('#activitiestimeline').append('<div class="app-timeline-more"><a href="#">...</a></div>');
			$(".scrollCustom").mCustomScrollbar({axis:"y", autoHideScrollbar: true, scrollInertia: 200, advanced: {autoScrollOnFocus: false}});
		}
	});
}

function expandir(id){
	$("#act"+id).toggleClass("fullscreen");
	valid = $("#act"+id).hasClass('fullscreen');
	$(".scrollCustom").css('max-height',(valid == true ? '90%' : '250px'));
	$(".scrollCustom").mCustomScrollbar("update");
}