var dtquestions;
$(document).ready(function(){	
	$('#form_addquestion [name="procedure"]').selectpicker({size:10,liveSearch: true}).ajaxSelectPicker({
		ajax : {
			url: urlapi+"Api_data/proceduressearch",
			type: "GET",
			// Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
			// automatically replace it with the value of the search query.
			data : { by:'AbbrDesc',name: '{{{q}}}' },
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); }
        },
        locale : { emptyTitle: 'Nothing selected' },
        log : 3,
        preprocessData: function (response){
            var l = response.data.length, array = [];			
            if (l){
				$.each(response.data,function(i,item){
					array.push($.extend(true,i,{
						text : item.AbbrDesc,
						value: item.id,
						data : { subtext: item.Descript }
					}));
				});
            }
            // You must always return a valid array when processing data. The
            // data argument passed is a clone and cannot be modified directly.
            return array;
        }
	});
	
	dtquestions = $("#dtquestions").DataTable({
		dom: '<"col-md-6"B><"col-md-6"f>rt<"col-md-6 hidden-mobile"i><"col-md-6"p>',
		buttons: [
			{
				text: '<span class="fa fa-plus"></span> Add',
				className: 'btn btn-success btn-sm btn-icon-fixed',
				attr:{
					"data-backdrop": "static",
					"data-toggle": "modal", 
					"data-target": "#modal_addquestion"
					}
			},
			{
				text: '<span class="fa fa-pencil"></span> Edit',
				className: 'btn btn-warning btn-sm btn-icon-fixed',
				attr: { "disabled": "",id: 'edit' }
				
			},
			{
				text: '<span class="fa fa-trash"></span> Delete',
				className: 'btn btn-danger btn-sm btn-icon-fixed',
				attr: { "disabled": "",id: 'delete' }
			}
		],
		"ajax": {
			url: urlapi+"Api_data/precedure_preguntas",
			type: "get",
			beforeSend: function (xhr){
				xhr.setRequestHeader('Authorization', auth);
			},
		},
		columns: [
			{data: 'Descript'},
			{data: 'nombre'},
			{data: 'descripcion'},
		],
		"drawCallback": function( settings ){ $("[data-toggle='tooltip']").tooltip(); },
		"rowCallback": function( row, data ){
		}
	});
	
	$('#dtquestions tbody').on( 'click', 'tr', function () {
        if($(this).hasClass('selection')){
            $(this).removeClass('selection');
			$('#delete').prop('disabled',true);
			$('#edit').prop('disabled',true);
        }else{
			$('#delete').prop('disabled',false);
			$('#edit').prop('disabled',false);
            dtquestions.$('tr.selection').removeClass('selection');
            $(this).addClass('selection');
        }
    });
	
	$('#dtquestions tbody').on('dblclick','tr',function(e){
		dtquestions.$('tr.selection').removeClass('selection');
        $(this).addClass('selection');
		$('#edit').trigger('click');
	});
	
	$('#edit').on( 'click',function(){
		var data = dtquestions.row($('.selection')).data();
		$('#listOptions').empty();
		$.each(data.opciones,function(i,item){
			$('#listOptions').append('<li id="btnDelOption" data-id="'+item.id+'" class="list-group-item">'+item.opcion+'</li>');
		});		
		$('#form_editquestion [name="id"]').val(data.id);
		$('#form_editquestion [name="procedure"]').val(data.Descript);
		$('#form_editquestion [name="name"]').val(data.nombre);
		$('#form_editquestion [name="description"]').val(data.descripcion);
		$('#form_editquestion [name="type"]').val(data.tipo);
		if(data.tipo == 2 || data.tipo == 3){
			$('#divoptions').show();
		}else{
			$('#divoptions').hide();
		}
		$('.bs-select').selectpicker('refresh');
		$(".scroll").mCustomScrollbar({axis:"y", autoHideScrollbar: true, scrollInertia: 200, advanced: {autoScrollOnFocus: false}});
		$("#modal_editquestion").modal("show");
	});
	
	$('#delete').on( 'click',function(){
		var data = dtquestions.row($('.selection')).data();
		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this question!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Yes, I am sure!',
			cancelButtonText: "No, cancel it!",
			closeOnConfirm: false,
			closeOnCancel: false
		 },
		 function(isConfirm){
		   if (isConfirm){
				$.ajax({
					url: urlapi+"Api_data/precedure_preguntas",
					type: "delete",
					data: { "id": data.id},
					dataType: "json",
					beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
					success: function(response){
						if(response.data.code == 0){
							dtquestions.ajax.reload();
							swal("Complete", "Question delete", "success");
							
						}else{
							swal("Cancelled", "error", "error");
						}
					}
				});
			}else{
				swal("Cancelled", "", "error");
				e.preventDefault();
			}
		 });
	});
	
	$.validate({
		form : '#form_addquestion',
		onError : function($form) {
			noty({
				layout: 'top',
				theme: 'defaultTheme',
				timeout: 2000,
				type: 'error', // success, error, warning, information, notification
				text: '<strong>ERROR</strong>Complete All inputs',
			});
		},
		onSuccess : function($form) {
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.code == 0){
						noty({
							layout: 'top',
							theme: 'defaultTheme',
							timeout: 2000,
							type: 'success',
							text: '<strong>Complete</strong> new user added',
						});
						$('#form_addquestion').get(0).reset();
						$('#form_addquestion').modal('hide');
						dtquestions.ajax.reload();
					}else{
						noty({
							layout: 'top',
							theme: 'defaultTheme',
							type: 'error',
							text: '<strong>Error</strong>'+response.message,
						});
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	$.validate({
		form : '#form_editquestion',
		onError : function($form) {
			noty({
				layout: 'top',
				theme: 'defaultTheme',
				timeout: 2000,
				type: 'error', // success, error, warning, information, notification
				text: '<strong>ERROR</strong>Complete All inputs',
			});
		},
		onSuccess : function($form) {
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.code == 0){
						noty({
							layout: 'top',
							theme: 'defaultTheme',
							timeout: 2000,
							type: 'success',
							text: '<strong>Complete</strong> new user added',
						});
						$('#form_editquestion').get(0).reset();
						$('#modal_editquestion').modal('hide');
						dtquestions.ajax.reload();
					}else{
						noty({
							layout: 'top',
							theme: 'defaultTheme',
							type: 'error',
							text: '<strong>Error</strong>'+response.message,
						});
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	$('#btnAddOption').on('click',function(){
		var id = $('#form_editquestion [name="id"]').val();
		var newOption = $('#form_editquestion [name="newOption"]').val();
		
		$.ajax({
			url: urlapi+"Api_data/procedureoptions",
			type: "post",
			data: { id: id,"option": newOption},
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
			success: function(response){
				if(response.code == 0){
					noty({
						layout: 'top',
						theme: 'defaultTheme',
						timeout: 2000,
						type: 'success',
						text: '<strong>Complete</strong> New option add',
					});
				}else{
					noty({
						layout: 'top',
						theme: 'defaultTheme',
						type: 'error',
						text: '<strong>Error</strong>'+response.message,
					});
				}
			}
		});
	});

	$('#listOptions').on('click','li',function(){
		var id = $(this).data("id");
		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this option!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Yes, I am sure!',
			cancelButtonText: "No, cancel it!",
			closeOnConfirm: false,
			closeOnCancel: false
		 },
		 function(isConfirm){
		   if (isConfirm){
				$.ajax({
					url: urlapi+"Api_data/procedureoptions",
					type: "delete",
					data: { "id": id},
					dataType: "json",
					beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
					success: function(response){
						if(response.code == 0){
							swal("Complete", "Option delete", "success");
						}else{
							swal("Cancelled", "error", "error");
						}
					}
				});
			}else{
				swal("Cancelled", "", "error");
				e.preventDefault();
			}
		 });
	});
});