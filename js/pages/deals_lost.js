var dt_deallost;
$(document).ready(function(){
	$.ajax({
		url: urlapi+"Api_paciente/lostinformer",
		type: "GET",
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			console.log(response);
			$('#totalLostInformer').html(response.data.totalLost);
		}
	});
		
	dt_deallost = $("#dt_deallost").DataTable({
		dom: '<"col-md-6"B><"col-md-6"f>rt<"col-md-6 hidden-mobile"i><"col-md-6"p>',
		buttons: [
			{
				text: '<span class="fa fa-eye"></span> Open',
				className: 'btn btn-info btn-sm btn-icon-fixed',
				attr: { "disabled": "",id: 'ofertaopen' }
				
			}
		],
		"ajax": {
			url: urlapi+"Api_paciente/ofertaslost",
			type: "get",
			beforeSend: function (xhr){
				xhr.setRequestHeader('Authorization', auth);
			},
			data: {activo: "none"},
		},
		columns: [
			{data: 'date'},
			{data: 'patient'},
			{data: 'user'},
			{data: 'perdidaFecha'},
			{data: 'perdidaRazon'},
			{data: 'value'}
		],
		"drawCallback": function( settings ){ $("[data-toggle='tooltip']").tooltip(); },
		"rowCallback": function( row, data ){
			//$('td:eq(2)',row).html(genero);
		}
	});
	
	$('#dt_deallost tbody').on( 'click', 'tr', function () {
        if($(this).hasClass('selection')){
            $(this).removeClass('selection');
			$('#ofertaopen').prop('disabled',true);
        }else{
			$('#ofertaopen').prop('disabled',false);
            dt_deallost.$('tr.selection').removeClass('selection');
            $(this).addClass('selection');
        }
    });
	
	$('#dt_deallost tbody').on('dblclick','tr',function(e){
		dt_deallost.$('tr.selection').removeClass('selection');
        $(this).addClass('selection');
		$('#ofertaopen').trigger('click');
	});
	
	$('#ofertaopen').on('click',function(){
		var data = dt_deallost.row($('.selection')).data();
		$.ajax({
			url: urlapi+"Api_paciente/detalleoferta",
			type: "GET",
			data: {id: data.id},
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
			success: function(response){
				$('#form_deal [name="id"]').val(response[0].id);
				$('#form_deal [name="patient"]').val(response[0].FName+" "+response[0].LName);
				$('#form_deal [name="value"]').val(response[0].valorFormato);
				$('#form_deal [name="closedate"]').val(response[0].fechaCierre);
				$('#form_deal [name="email"]').val(response[0].Email);
				$('#form_deal [name="phome"]').val(response[0].HmPhone);
				$('#form_deal [name="pwork"]').val(response[0].WkPhone);
				$('#form_deal [name="pmobile"]').val(response[0].WirelessPhone);
				$('#form_deal [name="treatment[]"]').val(response[0].tratamientos).trigger('change');
				$('#form_deal [name="treatment[]"]').multiSelect("refresh");
				$('#listProcedures').empty();
				$.each(response[0].procedimientos,function(i,item){
					$('#listProcedures').append('<li class="list-group-item">'+item.Descript+' <span class="badge badge-success">$ '+item.Amount+'</span></li>');
				});
			},complete: function(){
				$(".scrollProcedures").mCustomScrollbar({axis:"y", autoHideScrollbar: true, scrollInertia: 200, advanced: {autoScrollOnFocus: false}});	
				$('#modal_opendeal').modal('show');
			}
		});
	});
});