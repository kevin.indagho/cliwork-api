var formulario;
var id = $('#id').val();

$('.app-container').css('background-color','#FFFFFF');

$(document).ready(function(){	  
	$('#formulario [name="birthdate"]').datetimepicker({viewMode: 'years',format: "YYYY-MM-DD"});
	$.ajax({
		url: urlapi+"Api_data/propre_all",
		type: "get",
		data: {id: id},
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth);},
		success: function(response){
			$.each(response.data,function(i,item){
				$('#formulario_contenido').append('<div class="col-md-12 heading-line-below"><div class="form-group"><label class="control-label col-xs-12 col-md-6">'+item.nombre+'</label>'
				+'<div class="col-md-6">'+item.control+'</div>'
				+'</div></div>');
			});
			$(".app-checkbox label, .app-radio label").each(function(){$(this).append("<span></span>");});
		}
	});
	
	$.validate({
		form : '#formulario',
		modules : 'logic',
		validateOnBlur : false, 
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onError : function($form){
			//apptools.notyCustom('error','fields Required','Complete All inputs');
		},
		onSuccess : function($form){
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			$form.find('button[type="submit"]').prop('disabled',true);
			$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-spinner fa-pulse fa-fw');
			
			var data = new FormData($form[0]);
			
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				enctype: 'multipart/form-data',
				processData: false,
				contentType: false,
				cache: false,
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					$form.find('button[type="submit"]').prop('disabled',false);
					$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-floppy-o');
					if(response.code == 0){
						//apptools.sweetNoty('success','Complete','Add new patient');
						sweetAlert({
							title:'Complete',
							text: 'Complete survey',
							type:'success'
					  },function(isConfirm){
							window.location = "http://www.tijuanadentalcenter.com/";
					  });
					}else{
						apptools.sweetNoty('error','Error','');
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
});