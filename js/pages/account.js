var patientid = 0;
var selecttp;
var totaltp = 0;
var fechas;

$(document).ready(function(){
	$(".splitAdd").mCustomScrollbar({axis:"y", autoHideScrollbar: true, scrollInertia: 200, advanced: {autoScrollOnFocus: false}});
	
	$(".popover-hover").on("mouseenter",function(){
		$(this).popover('show');
	}).on("mouseleave",function(){
		$(this).popover('hide');
	});
		
	$('#patientsearch').selectpicker({liveSearch: true}).ajaxSelectPicker({
        ajax : {
			url: urlapi+"Api_data/patsearch",
			type: "GET",
			// Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
			// automatically replace it with the value of the search query.
			data : { name: '{{{q}}}' },
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); }
        },
        locale : { emptyTitle: 'Nothing selected' },
        log : 3,
        preprocessData: function (response){
            var l = response.data.length, array = [];			
            if (l){
				$.each(response.data,function(i,item){
					array.push($.extend(true,i,{
						text : item.FName+" "+item.LName,
						value: item.Id,
						//data : { subtext: "Sub Text" }
					}));
				});
            }
            // You must always return a valid array when processing data. The
            // data argument passed is a clone and cannot be modified directly.
            return array;
        }
    });

	$.ajax({
		url: urlapi+"Api_data/definitions",
		Type:"GET",
		dataType:"json",
		data: { type:"10"},
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,item){
				$('#form_addpayment [name="type"]').append('<option value="'+item.DefNum+'">'+item.ItemName+'</option>');
				$('#form_editpayment [name="type"]').append('<option value="'+item.DefNum+'">'+item.ItemName+'</option>');
			});
			$('.bs-select').val('default').selectpicker('refresh');
		}
	});
	
	$.ajax({
		url: urlapi+"Api_data/definitions",
		Type:"GET",
		dataType:"json",
		data: { type:"1"},
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			optgroupAdd =  $('<optgroup></optgroup>',{label: "Additions"});
			optgroupSub =  $('<optgroup></optgroup>',{label: "Substractions"});
			
			$.each(response.data,function(i,item){
				if(item.ItemValue == "+"){
					optgroupAdd.append('<option value="'+item.DefNum+'">'+item.ItemName+'</option>');
				}
				if(item.ItemValue == "-"){
					optgroupSub.append('<option value="'+item.DefNum+'">'+item.ItemName+'</option>');
				}
			});
			$('#form_addadjusment [name="type"]').append(optgroupAdd);
			$('#form_addadjusment [name="type"]').append(optgroupSub);
			$('.bs-select').val('default').selectpicker('refresh');
		}
	});
	
	$.ajax({
		url: urlapi+"Api_data/providers",
		type: "GET",
		data: {IsHidden: 0,specialty: "none"},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$('#form_addpayment [name="provider"]').append('<option></option>');
			$('#form_editpayment [name="provider"]').append('<option></option>');
			$.each(response.data,function(i,item){
				$('#form_addpayment [name="provider"]').append($('<option>',{text: item.FName+" "+item.LName,value: item.id}));
				$('#form_editpayment [name="provider"]').append($('<option>',{text: item.FName+" "+item.LName,value: item.id}));
			});
			$('.bs-select').val('default').selectpicker('refresh');
		}
	});
	
	/*FORM VALIDATE*/
	$.validate({
		form : '#form_addpayment',
		modules : 'file,logic',
		validateOnBlur : true,
		errorMessagePosition : 'top',
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form) {
			var data = new FormData($form[0]);
			data.append("patient",patientid);
			data.append("userid",apptools.userlogin);
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				enctype: 'multipart/form-data',
				processData: false,
				contentType: false,
				cache: false,
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.code == 0){
						$('#patientsearch').trigger('change');
						$('#modal_payment').modal('hide');
						apptools.sweetNoty('success','Complete','Payment save');
						$('#treatplan').trigger('change');
					}else{
						apptools.sweetNoty('error','Error','');
					}
				}
			});
		  return false; // Will stop the submission of the form
		}
	});
	
	$.validate({
		form : '#form_editpayment',
		modules : 'file,logic',
		validateOnBlur : true,
		errorMessagePosition : 'top',
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form) {
			var data = new FormData($form[0]);
			data.append("patient",patientid);
			data.append("userid",apptools.userlogin);
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				enctype: 'multipart/form-data',
				processData: false,
				contentType: false,
				cache: false,
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.code == 0){
						$('#modal_editpayment').modal('hide');
						apptools.sweetNoty('success','Complete','Payment save');
						$('#treatplan').trigger('change');
					}else{
						apptools.sweetNoty('error','Error','');
					}
				}
			});
		  return false; // Will stop the submission of the form
		}
	});

	$.validate({
		form : '#form_addadjusment',
		modules : 'file,logic',
		validateOnBlur : true,
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form) {
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			$form.find('button[type="submit"]').prop('disabled',true);
			$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-spinner fa-pulse fa-fw');
			var data = new FormData($form[0]);
			data.append("patient",patientid);
			data.append("userid",apptools.userlogin);
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				enctype: 'multipart/form-data',
				processData: false,
				contentType: false,
				cache: false,
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					$form.find('button[type="submit"]').prop('disabled',false);
					$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-floppy-o');
					if(response.code == 0){
						$('#modal_payment').modal('hide');
						$('#patientsearch').trigger('change');
						apptools.sweetNoty('success','Complete','Payment save');
						$('#treatplan').trigger('change');
					}else{
						apptools.sweetNoty('error','Error','');
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	$.validate({
		form : '#form_addpaymentplan',
		modules : 'file,logic',
		validateOnBlur : true,
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form) {
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			var data = new FormData($form[0]);
			data.append("userid",apptools.userlogin);
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				enctype: 'multipart/form-data',
				processData: false,
				contentType: false,
				cache: false,
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.code == 0){
						pay = $('#form_addpaymentplan [name="pay"]').val();
						paytotal = $('#form_addpaymentplan [name="paytotal"]').val();
						$('#tbl_dates tbody').empty();
						$('#divbtnplayplanview').show();
						$('#divbtnplayplan').hide();
						$('#modal_payplan').modal('hide');
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	
	/* Actions */
	$('#form_addpayment [name="amount"]').on('input',function(){
		total = $('#form_addpayment [name="total"]').data('value');
		pay = $('#form_addpayment [name="pay"]').data('value');
		amount = ($(this).val() != '' ? parseFloat($(this).val()) : 0);
		pay += amount;
		due = total - pay;
		$('#form_addpayment [name="pay"]').val(pay);
		$('#form_addpayment [name="due"]').val(due);
	});
	
	$('#patientsearch').on('changed.bs.select',function(){
		patientid = $(this).val();
		if(patientid != "" && patientid != 0){
			patientid = $(this).val();
			app.block.loading.start("#block-account");
			$.ajax({
				url: urlapi+"Api_account/valid",
				Type:"GET",
				dataType:"json",
				data: { patient: patientid},
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.length == 0){
						apptools.notyCustom('information','No treatplan active or procedures','');
						$('#btnPayPlan').prop('disabled',true);
						$('#btnPayment').prop('disabled',true);
						$('#btnAdjustment').prop('disabled',true);
					}else{
						$('#btnPayPlan').prop('disabled',false);
						$('#btnPayment').prop('disabled',false);
						$('#btnAdjustment').prop('disabled',false);
					}
				}
			});
			
			
			
			
			//selecttp = $('#treatplan').val();
			//$('#form_addadjusment [name="treatplan"]').val(selecttp);
			//$('.selecttreatplan').html($('#treatplan')[0].selectedOptions[0].text);
			gettotal();
			$.ajax({
				url: urlapi+"Api_account/account",
				Type:"GET",
				dataType:"json",
				data: { patient: patientid,treatplan:"all",status: 2},
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					$("#dt_account tbody").empty();
					total = 0;
					$.each(response.data,function(i,item){
							if(item.TypeItem == "proc"){
								total = total + parseFloat(item.ProcFee);
								$("#dt_account tbody").append('<tr id="'+item.Id+'">'
								+'<td class="padding-5">'+item.dateFormat+'</td>'
								+'<td class="padding-5">'+item.Doctor+'</td>'
								+'<td class="padding-5">'+item.Heading+'</td>'
								+'<td class="padding-5">'+item.Descript+'</td>'
								+'<td class="padding-5 text-center">'+item.ToothNum+'</td>'
								+'<td class="padding-5 text-right">'+item.ProcFeeFormat+'</td>'
								+'</tr>');
							}else if(item.TypeItem == "pay"){
								total = total - parseFloat(item.monto);
								$("#dt_account tbody").append('<tr data-id="'+item.id+'" data-split="'+item.paySplit+'" class="editpay text-success pointer">'
									+'<td class="padding-5">'+item.dateFormat+'</td>'
									+'<td class="padding-5">'+item.user+'</td>'
									+'<td class="padding-5"></td>'
									+'<td class="padding-5">'								
										+(item.notas != "" ? '<i class="fa fa-info-circle popover-hover" aria-hidden="true" data-placement="top" data-container="body" data-trigger="click" data-content="'+item.notas+'"></i> ': "")
										+ item.TypeName
										+(item.archivo != "" ? ' <a href="'+pathapi+'files/patients/'+patientid+'/'+item.archivo+'">File</a>' : "")
									+'</td>'
									+'<td class="padding-5 text-center"></td>'
									+'<td class="padding-5 text-right">'+item.montoFormato+'</td>'
								+'</tr>');
							}else if(item.TypeItem == "adjust"){
								if(item.ItemValue == "+"){
									total = total + parseFloat(item.monto);
								}else{
									total = total - parseFloat(item.monto);
								}
								$("#dt_account tbody").append('<tr data-id="'+item.id+'" class="editaddj '+(item.ItemValue == "+" ? "text-info" : "text-danger")+'">'
									+'<td class="padding-5">'+item.dateFormat+'</td>'
									+'<td class="padding-5">'+item.user+'</td>'
									+'<td class="padding-5">'+item.Heading+'</td>'
									+'<td class="padding-5">'
										+(item.notas != "" ? '<i class="fa fa-info-circle popover-hover" aria-hidden="true" data-placement="top" data-container="body" data-trigger="click" data-content="'+item.notas+'"></i> ': "")
										+ item.ItemName
									+'</td>'
									+'<td class="padding-5 text-center"></td>'
									+'<td class="padding-5 text-right">'+item.ItemValue+''+item.montoFormato+'</td>'
								+'</tr>');
							}else if(item.TypeItem == "Comis"){
								comision = (item.comision * item.deuda)/100;
								total = total + parseFloat(comision);
								$("#dt_account tbody").append('<tr data-id="'+item.id+'" class="editaddj text-warning">'
									+'<td class="padding-5">'+item.dateFormat+'</td>'
									+'<td class="padding-5">'+item.user+'</td>'
									+'<td class="padding-5"></td>'
									+'<td class="padding-5">Comision por plan de pagos: '+item.comision+'%</td>'
									+'<td class="padding-5 text-center"></td>'
									+'<td class="padding-5 text-right">'+comision+'</td>'
								+'</tr>');
							}
						$("#dt_account tfoot").empty();
						$("#dt_account tfoot").append('<tr>'
							+'<td class="text-center pading-5" colspan="4"></td>'
							+'<td class="text-center pading-5">Balance</td>'
							+'<td class="text-right pading-5">'+total+'</td>'
						+'</tr>');
					});
				},
				complete: function(){
					$(".scrollCustom").mCustomScrollbar({axis:"y", autoHideScrollbar: true, scrollInertia: 200, advanced: {autoScrollOnFocus: false}});
					app.block.loading.finish("#block-account");
					$('[data-toggle="popover"]').popover()
					
					$("#dt_account .popover-hover").on("mouseenter",function(){
						$(this).popover('show');
					}).on("mouseleave",function(){
						$(this).popover('hide');
					});
					
					$("#dt_account tbody").on('mouseenter','.editpay',function(){
						tr = $(this);
						paysplit = tr.data('split');
						if(typeof paysplit == 'number'){
							$('#'+paysplit).addClass('selection');
						}else{
							paysplit = paysplit.split(',');
							$.each(paysplit,function(i,item){
								$('#'+item).addClass('selection');
							});
						}
					});
					
					$("#dt_account tbody").on('mouseleave','.editpay',function(){
						$('#dt_account tbody .selection').removeClass('selection');
					});
				
					$("#dt_account tbody").on('dblclick','.editpay',function(){
						tr = $(this);
						getPayment(tr.data('id'))
						$('.bs-select').val('default').selectpicker('refresh');
						$('#modal_editpayment').modal('show');
					});
					
					$("#dt_account tbody").on('click','.editaddj',function(){
						tr = $(this);
						$('#modal_editpayment').modal('show');
					});
				}
			});
			
			
			$.ajax({
				url: urlapi+"Api_treatplan/treatplan",
				Type:"GET",
				dataType:"json",
				data: { patient: patientid,treatplan: selecttp },
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					$('#form_addpayment [name="treatplan"]').empty();
					$('#form_editpayment [name="treatplan"]').empty();
					$('#form_addadjusment [name="treatplan"]').empty();
					$.each(response.data,function(i,tp){
						$('#form_addpayment [name="treatplan"]').append('<option value="'+tp.id+'">'+tp.Heading+'</option>');
						$('#form_editpayment [name="treatplan"]').append('<option value="'+tp.id+'">'+tp.Heading+'</option>');
						$('#form_addadjusment [name="treatplan"]').append('<option value="'+tp.id+'">'+tp.Heading+'</option>');
					});
					$('.bs-select').selectpicker('refresh');
				}
			});
			/*Get Payplan Active*/
			$.ajax({
				url: urlapi+"Api_account/paymentplanactive",
				Type:"GET",
				dataType:"json",
				data: { patient: patientid},
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					//response[0].id
					if(response.data.length == 1){
						$('#form_editpaymentplan [name="planid"]').val(response.data[0].id);
						//$('#form_editpaymentplan [name=""]').val();
						$('#divbtnplayplanview').show();
						$('#divbtnplayplan').hide();
					}else{
						$('#divbtnplayplan').show();
						$('#divbtnplayplanview').hide();
					}
				},complete: function(){
					$('#btnPayPlanView').on('click',function(){
						getPaymentPlanActive();
						$('#modal_payplanedit').modal();
					});
				}
			});
		}
		$('#patientsearch').val('default').selectpicker("refresh");
	});

	$('#form_addpaymentplan [name="quantity"]').on('change',function(){
		value = $('#form_addpaymentplan [name="value"]').val();
		quantity = $(this).val();
		pay = parseFloat(value) / parseFloat(quantity);
		$('#form_addpaymentplan [name="pay"]').val(pay);
	});
	
	$('#form_addpaymentplan [name="profit"]').on('change',function(){
		pay = $('#form_addpaymentplan [name="pay"]').val();
		quantity = $('#form_addpaymentplan [name="quantity"]').val();
		profit = $(this).val();
		
		paytotal = parseFloat(profit) * parseFloat(pay)/100;
		$('#form_addpaymentplan [name="paytotal"]').val(paytotal);
	});
	
	$('#form_editpaymentplan [name="quantity"]').on('change',function(){
		value = $('#form_editpaymentplan [name="value"]').val();
		quantity = $(this).val();
		pay = parseFloat(value) / parseFloat(quantity);
		$('#form_editpaymentplan [name="pay"]').val(pay);
	});
	
	$('#form_editpaymentplan [name="profit"]').on('change',function(){
		pay = $('#form_editpaymentplan [name="pay"]').val();
		quantity = $('#form_editpaymentplan [name="quantity"]').val();
		profit = $(this).val();
		
		paytotal = parseFloat(profit) * parseFloat(pay)/100;
		$('#form_editpaymentplan [name="paytotal"]').val(paytotal);
		//$('#form_editpaymentplan [name="datestart"]').trigger('dp.change');
	});
	
	$('#btnPayPlan').on('click',function(){
		$('#msgUpdate').hide();
		patient = $('#patientsearch');
		value = $("#dt_account tfoot")[0].rows[0].cells[2].innerText;
		$('#form_addpaymentplan [name="patientid"]').val(patient.val());
		$('#form_addpaymentplan [name="patient"]').val(patient[0].selectedOptions[0].outerText);
		$('#form_addpaymentplan [name="value"]').val(value);
		$('#modal_payplan').modal('show');
	});
	
	$('#btnPayment').on('click',function(){
		$.ajax({
			url: urlapi+"Api_account/balance",
			Type:"GET",
			dataType:"json",
			data: { patient: patientid,treatplan:'all',status: 2},
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
			success: function(response){
				total = response.data.subtotal.value - response.data.adjust.discounts.value + response.data.adjust.aditions.value;
				$('#form_addpayment [name="subtotal"]').attr('data-value',response.data.subtotal.value);
				$('#form_addpayment [name="subtotal"]').val(response.data.subtotal.format);
				$('#form_addpayment [name="additions"]').attr('data-value',response.data.adjust.aditions.value);
				$('#form_addpayment [name="additions"]').val(response.data.adjust.aditions.format);
				$('#form_addpayment [name="discounts"]').attr('data-value',response.data.adjust.discounts.value);
				$('#form_addpayment [name="discounts"]').val(response.data.adjust.discounts.format);
				$('#form_addpayment [name="total"]').attr('data-value',response.data.total.value);
				$('#form_addpayment [name="total"]').val(response.data.total.format);
				$('#form_addpayment [name="pay"]').attr('data-value',response.data.pay.value);
				$('#form_addpayment [name="pay"]').val(response.data.pay.format);
			},complete:function(){
				total = $('#form_addpayment [name="total"]').data('value');
				pay = $('#form_addpayment [name="pay"]').data('value');
				due = total - pay;
				$('#form_addpayment [name="pay"]').val(pay);
				$('#form_addpayment [name="due"]').val(due);
				$('#modal_payment').modal();
			}
		});
		
	});

	$('#form_addpaymentplan [name="datestart"]').on('dp.change',function(){
		$('#tbl_dates tbody').empty();
		$('#tbl_dates tfoot').empty();
		datestart = 	$('#form_addpaymentplan [name="datestart"]').val();
		type = 	$('#form_addpaymentplan [name="type"]').val();
		quantity = 	$('#form_addpaymentplan [name="quantity"]').val();
		pay = 	$('#form_addpaymentplan [name="pay"]').val();
		payTotal = 	$('#form_addpaymentplan [name="paytotal"]').val();
		total = 0;
		$.ajax({
			url: urlapi+"Api_account/paycalculate",
			type:"post",
			data:{datestart: datestart,quantity: quantity, type: type},
			dataType:"json",
			beforeSend:function(xhr){xhr.setRequestHeader('Authorization',auth);},
			success:function(response){
				$.each(response,function(i,date){
					payc = parseFloat(pay)+parseFloat(paytotal);
					total += payc;
					$('#tbl_dates tbody').append('<tr>'
						+'<td><input type="hidden" name="fechas['+i+'][fecha]" value="'+date+'">'+date+'</td>'
						+'<td class="text-right"><input type="hidden" name="fechas['+i+'][monto]" value="'+payc+'">'+payc+'</td>'
					+'</tr>');
				});
				$('#tbl_dates tfoot').append('<tr>'
					+'<td></td>'
					+'<td class="text-right"><b>Total:</b> '+total+'</td>'
				+'</tr>');
			}
		});
	});
	
	$('#completeplan').on('click',function(){
		id = $('#form_editpaymentplan [name="planid"]').val();
		swal({
		  title: "Payment plan complete",
		  text: "?",
		  type: "warning",
		  showConfirmButton: 1,
		  showCancelButton: 1,
		  dangerMode: true,},
		  function(isConfirm){
			if(isConfirm){
				$.ajax({
					url: urlapi+"Api_account/paymentplan",
					type:"PUT",
					dataType:"json",
					data: { planid: id},
					beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
					success: function(response){
						if(response.code == 0){
							$('#modal_payplanedit').modal('hide');
							apptools.sweetNoty('success','Complete','Payment plan complete');
							$('#patientsearch').trigger('change');
						}
					}
				});
			}
		  });
	});
	
	$('#updateplan').on('click',function(){
		
		$('#modal_payplanedit').modal('hide');
		//setTimeout(function(){ alert("Hello"); }, 3000);
		//$('#modal_payplanedit').on('hidden.bs.modal',function(){
		setTimeout(function(){
			$.ajax({
				url: urlapi+"Api_account/paymentplanactive",
				Type:"GET",
				dataType:"json",
				data: { patient: patientid},
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					pplan = response.data[0];
					$('#form_addpaymentplan [name="planid"]').val(pplan.id);
					$('#form_addpaymentplan [name="date"]').val(pplan.fecha);
					$('#form_addpaymentplan [name="value"]').val(pplan.deuda);
					$('#form_addpaymentplan [name="quantity"]').val(pplan.duracion);
					$('#form_addpaymentplan [name="type"]').val(pplan.deuda);
					$('#form_addpaymentplan [name="pay"]').val(pplan.pago);
					$('#form_addpaymentplan [name="profit"]').val(pplan.comision).trigger('change');
					pay = 	$('#form_addpaymentplan [name="pay"]').val();
					payTotal = 	$('#form_addpaymentplan [name="paytotal"]').val();
					payc = parseFloat(pay)+parseFloat(payTotal);
					$('#tbl_dates tbody').empty();
					$.each(pplan.fechas,function(i,item){
						$('#tbl_dates tbody').append('<tr>'
								+'<td>'+item.fecha+'</td>'
								+'<td class="text-right">'+payc+'</td>'
							+'</tr>');
					});
				},complete: function(){
					$('#msgUpdate').show();
					$('#btnPayPlan').trigger('click');
				}
			});
		},500);
	});
	
});

function getPaymentPlanActive(){
	$.ajax({
		url: urlapi+"Api_account/paymentplanactive",
		Type:"GET",
		dataType:"json",
		data: { patient: patientid},
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			pplan = response.data[0];
			$('#form_addpaymentplan [name="planid"]').val(pplan.id);
			$('#form_editpaymentplan [name="date"]').val(pplan.fecha);
			$('#form_editpaymentplan [name="value"]').val(pplan.deuda);
			$('#form_editpaymentplan [name="quantity"]').val(pplan.duracion);
			$('#form_editpaymentplan [name="type"]').val(pplan.deuda);
			$('#form_editpaymentplan [name="pay"]').val(pplan.pago);
			$('#form_editpaymentplan [name="profit"]').val(pplan.comision).trigger('change');
			pay = 	$('#form_editpaymentplan [name="pay"]').val();
			payTotal = 	$('#form_editpaymentplan [name="paytotal"]').val();
			payc = parseFloat(pay)+parseFloat(payTotal);
			$('#tbl_dates_edit tbody').empty();
			$.each(pplan.fechas,function(i,item){
				$('#tbl_dates_edit tbody').append('<tr class="'+(item.completo == 1? 'success' : '')+'">'
						+'<td>'+item.fecha+'</td>'
						+'<td class="text-right">'+payc+'</td>'
						+'<td class="text-right">'+item.pagado+'</td>'
					+'</tr>');
			});
		},complete: function(){}
	});
}

function calculate_split(){
	payment = $('#form_addpayment [name="amount"]').val();
	rows = $('#dt_proce_pay .selection');
	selections = $('#dt_proce_pay .selection');
	for(i = 1; i <= selections.length; i++){
		row = $('#dt_proce_pay tr[number="'+i+'"]');
		number = $(row).data('number');
		value = $(row).data('value');
		if(payment > value){
			payment = payment - value;
			due = 0;
		}else{
			due = value - payment;
			payment = 0;
		}
		$(row)[0].cells[3].innerHTML = due;
	}
	$('#form_addpayment [name="paymentbalance"]').val(payment);
}

function getPayment(id){
	$.ajax({
		url: urlapi+"Api_account/payment",
		Type:"GET",
		dataType:"json",
		data: { payment: id},
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			payment = response.data[0];
			$('#form_editpayment [name="id"]').val(id);
			$('#form_editpayment [name="provider"]').val(payment.doctor).trigger('change');
			$('#form_editpayment [name="type"]').val(payment.treatplanid);
			$('#form_editpayment [name="type"]').val(payment.tipo);
			$('#form_editpayment [name="amount"]').val(payment.monto);
			$('#form_editpayment [name="notes"]').val(payment.notas);
			
		},
		complete: function(){
		}
	});
}

function gettotal(){
	$.ajax({
		url: urlapi+"Api_account/accounttotal",
		Type:"GET",
		dataType:"json",
		data: { patient: patientid,treatplan: selecttp },
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$('#allSubTotal').html(response.data.allSubTotal);
			$('#allAdditions').html(response.data.alladitions);
			$('#allSubstractions').html(response.data.alldiscounts);
			$('#allTotal').html(response.data.allTotal);
			$('#allPayments').html(response.data.allPayments);
			$('#allPaymentsPercent').html(response.data.allPaymentsPercent+" %");
			$('#allDue').html(response.data.allDue);
			$('#allDuePercent').html(response.data.allDuePercent+" %");
			
			/*$('#tptotal').html(response.data.total);
			$('#tppayments').html(response.data.Payments);
			$('#tpdue').html(response.data.Due);
			$('#duepercent').html(response.data.DuePercent+" %");
			$('#paymentspercent').html(response.data.PaymentsPercent+" %");*/
		}
	});
}

function donwload(){
	if(patientid != 0){
		patSelect = $('#patientsearch')[0].selectedOptions[0].innerText;
		$.ajax({
			url: urlapi+"Api_account/pdf_tp_account",
			Type:"GET",
			data: { patient: patientid,treatplan: selecttp,status:2 },
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
			xhrFields: { responseType: 'blob' },
			success: function(data){
				var a = document.createElement('a');
				var url = window.URL.createObjectURL(data);
				a.href = url;
				a.download = 'statement - '+patSelect+'.pdf';
				document.body.append(a);
				a.click();
				a.remove();
				window.URL.revokeObjectURL(url);
			}
		});
	}else{
		apptools.sweetNoty('info','Not select patient','Seleccione un paciente para descargar');
	}
}


