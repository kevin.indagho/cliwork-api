var tpselect = $('#treatplanid').val();
var patient;
var dt_tpproc_data = {treatplan: tpselect, status: 1};
var form_tratplan;
  

  
$(document).ready(function(){
	loadOdt();
	
	$.ajax({
		url: urlapi+"Api_treatplan/treatplandata",
		type: "GET",
		data: {tpid: tpselect},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			patient = response.data[0].PatNum;
			if(response.data[0].ProcAptPlanPendi > 0){
				$('#msgprocplanapt').show();
			}else{
				$('#msgprocplanapt').hide();
			}
			$('#form_newappointment [name="patient[id]"]').val(response.data[0].PatNum);
		}
	});
	
	$.ajax({
		url: urlapi+"Api_data/providers",
		type: "GET",
		data: {IsHidden: 0,specialty: "none"},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,item){
				$('#form_newappointment [name="doctor"]').append($('<option>',{text: item.FName+" "+item.LName,value: item.id}));
				$('#form_editappointment [name="doctor"]').append($('<option>',{text: item.FName+" "+item.LName,value: item.id}));
				$('#form_procedure_edit [name="doctor"]').append($('<option>',{text: item.FName+" "+item.LName,value: item.id}));
				$('#form_procedure [name="doctor"]').append($('<option>',{text: item.FName+" "+item.LName,value: item.id}));
				$('#doctorproclist').append($('<option>',{text: item.FName+" "+item.LName,value: item.id}));
			});
			$('.bs-select').val('default').selectpicker('refresh');
		}
	});
	
	$.ajax({
		url: urlapi+"Api_data/providers",
		type: "GET",
		data: {IsHidden: 0,specialty: 266},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,item){
				$('#form_newappointment [name="hygienis"]').append($('<option>',{text: item.FName+" "+item.LName,value: item.id}));
				$('#form_editappointment [name="hygienis"]').append($('<option>',{text: item.FName+" "+item.LName,value: item.id}));
			});
			$('.bs-select').val('default').selectpicker('refresh');
		}
	});
	
	$.ajax({
		url: urlapi+"Api_data/assistant",
		type: "GET",
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,item){
				$('#form_newappointment [name="assistant[]"]').append($('<option>',{text: item.FName+" "+item.LName,value: item.id}));
				$('#form_editappointment [name="assistant[]"]').append($('<option>',{text: item.FName+" "+item.LName,value: item.id}));
			});
			$('.bs-select').val('default').selectpicker('refresh');
		}
	});
	
	
	$.ajax({
		url: urlapi+"Api_data/procbtn",
		type:"GET",
		dataType:"json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			
			var labelnow = null;
			$.each(response.data,function(i,item){
				description = item.Description.replace(" ","");
				$('#quickbuttons').append('<label class="col-xs-12 control-label">'+item.Description+'</label><div id="btncate'+item.Id+'" class="col-xs-12"></div>');
				$.each(item.data,function(i,btn){	
					$('#btncate'+item.Id).append('<button data-toggle="tooltip" data-placement="top" title="" data-original-title="Tooltip on top" data-id="'+btn.id+'" data-proccode="'+btn.CodeValue+'" data-surf="'+btn.Surf+'" type="button" class="btnquick btn btn-xs btn-default margin-0">'+btn.Description+'</button>');
				});
			});
			//$("[data-toggle='tooltip']").tooltip();
		} 
	});
		
	dt_plaappt = $("#dt_plaappt").DataTable({
		"bStateSave": false,
		dom: '<"col-md-6"B><"col-md-6"f>rt<"col-md-6 hidden-mobile"i><"col-md-6"p>',
		buttons: [
			{
				text: '<span class="fa fa-plus"></span> New',
				className: 'btn btn-success btn-sm btn-icon-fixed',
				action: function (e,dt,node,config){ load_procedures_appt(); newApptPlan();},
				
			},
			{
				text: '<span class="fa fa-pencil"></span> Edit',
				className: 'btn btn-warning btn-sm btn-icon-fixed',
				attr: { "disabled": "",id: 'treatedit' }
				
			}
		],
		"ajax": {
			url: urlapi+"Api_cita/calendario",
			Type:"GET",
			data: {dateStart: "none", dateEnd:"none", "status": [7],inbox:"none"},
			dataType:"json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		},
		columns: [
			{data: 'PatName'},
			{data: 'ProName'},
			{data: 'servicios[, ].services'},
			{defaultContent: '<button class="delete btn btn-xs btn-danger"><i class="fa fa-times"></i></button>'},
		]
	});
	
	dt_procedurelist = $("#dt_procedurelist").DataTable({
		"bStateSave": false,
		"ajax": {
			url: urlapi+"Api_data/procedures",
			Type:"GET",
			dataType:"json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		},
		columns: [
			{data: 'ProcCode'},
			{data: 'ProcCode'},
			{data: 'Descript'},
			{data: 'AbbrDesc'},
			{data: 'AbbrDesc'},
		]
	});
	
	dt_tpproc = $("#dt_tpproc").DataTable({
		"bStateSave": false,
		"ajax": {
			url: urlapi+"Api_treatplan/tpprocedure",
			Type:"GET",
			dataType:"json",
			data: function(d){
				return  $.extend(d, dt_tpproc_data);
            },
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		},
		columns: [
			{data: 'Done'},
			{data: 'ProcDate'},
			{data: 'Priority'},
			{data: 'ToothNum',width:"15px"},
			{data: 'Surf',width:"15px"},
			{data: 'Descript'},
			{data: 'provider'},
			{data: 'ProcFee',class:"text-right"},
		],
		"rowCallback": function(row,data){
			$(row).addClass('proc'+data.Id);
			if(data.ProcStatus == 1){
				$(row).addClass('text-danger');
			}
			
			if(data.ProcStatus == 2){
				$(row).addClass('text-info');
			}
			
			if(data.ProcStatus == 3 || data.ProcStatus == 4){
				$(row).addClass('text-success');
			}
			
			if(data.isSurf == 1){
				if(data.Surf == '' || data.Surf == null){
					$(row).addClass('danger');
				}
			}
			
		},
		"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data; 
            // Remove the formatting to get integer data for summation
            var intVal = function(i){
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
			//total = api.column(6).data().reduce(function(a,b){return intVal(a) + intVal(b);},0);
			//$(api.column(6).footer()).html(total);
        }
	});
   
   $('#dt_tpproc tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = dt_tpproc.row( tr ); 
        if(row.child.isShown()){
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }else{
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });
	
	dt_treatplans = $("#dt_treatplans").DataTable({
		"bStateSave": false,
		"ajax": {
			url: urlapi+"Api_treatplan/treatplan",
			Type:"GET",
			dataType:"json",
			data:{patient: selectpat},
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		},
		dom: '<"col-md-6"B><"col-md-6"f>rt<"col-md-6 hidden-mobile"i><"col-md-6"p>',
		buttons: [
			{
				text: '<span class="fa fa-plus"></span> New',
				className: 'btn btn-success btn-sm btn-icon-fixed',
				attr:{
					"data-backdrop": "static",
					"data-toggle": "modal", 
					"data-target": "#modal_newtreatplan"
					}
			},
			{
				text: '<span class="fa fa-pencil"></span> Edit',
				className: 'btn btn-warning btn-sm btn-icon-fixed',
				attr: { "disabled": "",id: 'treatedit' }
				
			}
		],
		columns: [
			{data: 'DateTP'},
			{data: 'Heading'},
			{data: 'TPStatus'},
		],
		"rowCallback": function( row, data ){
			if(data.TPStatus == 1){ 
				$('td:eq(2)',row).html('<span class="label label-success">Active</span>');
			}else{
				$('td:eq(2)',row).html('<span class="label label-danger">Inactive</span>');
			}
		}
	});
	
	$('#dt_treatplans tbody').on( 'click', 'tr', function () {
        if($(this).hasClass('selection')){
            $(this).removeClass('selection');
			$('#treatedit').prop('disabled',true);
        }else{
			$('#treatedit').prop('disabled',false);
            dt_treatplans.$('tr.selection').removeClass('selection');
            $(this).addClass('selection');
        }
    });
	
	$('#dt_treatplans tbody').on('dblclick','tr',function(e){
		dt_treatplans.$('tr.selection').removeClass('selection');
		$('#treatedit').prop('disabled',false);
        $(this).addClass('selection');
		$('#treatedit').trigger('click');
	});
	
	$("#dt_procedurelist tbody").on('dblclick','tr',function(){
		procedure = dt_procedurelist.row($(this)).data();
		var selectTooth = $('.select');
		if(selectTooth.length > 0){
			//pat = $('#frmsearchpat [name="patient"]').val();
			estatus = $('#form_procedure [name="status"]').val();
			data = [];		
			tooths = [];
			$.each(selectTooth,function(i,item){
				tooths.push($(item).data('tooth'));
			});	
				
			data.push({name: 'provi', value: $('#doctorproclist').val()});
			data.push({name: 'tp', value: tpselect});
			data.push({name: 'tooth', value: tooths});
			data.push({name: 'status', value: estatus});
			//data.push({name: 'patient', value: pat});
			data.push({name: 'procedure', value: procedure.id});
			data.push({name: 'surf', value: ""});
			$.ajax({
				url: urlapi+"Api_treatplan/tpprocedure",
				type:"POST",
				data: data,
				dataType:"json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.code == 0){
						dt_tpproc.ajax.reload(function(){
							$('#dt_tpproc .proc'+response.id).trigger('dblclick');
							loadOdt();
						});
					}
				} 
			});
			$('#modal_procedures').modal('hide');
		}else{
			apptools.sweetNoty('warning','Nothing selected','You need selection a tooth.');
		}
	});
	
	$("#dt_tpproc tbody").on('dblclick','tr',function(){
		procedure = dt_tpproc.row($(this)).data();
		
		$('#form_procedure_edit [name="feesched"]').empty();
		$.each(procedure.fees,function(i,item){
			$('#form_procedure_edit [name="feesched"]').append('<option value="'+item.Amount+'">'+item.Description+'</option>');
		});
		$('#form_procedure_edit [name="feesched"]').val('default');
		$('#form_procedure_edit [name="doctor"]').val(procedure.ProvNum);
		$('#form_procedure_edit [name="id"]').val(procedure.Id);
		$('#form_procedure_edit [name="status"]').val(procedure.ProcStatus);
		$('#form_procedure_edit [name="code"]').val(procedure.ProcCode);
		$('#form_procedure_edit [name="description"]').val(procedure.Descript);
		$('#form_procedure_edit [name="tooth"]').val(procedure.ToothNum);
		$('#form_procedure_edit [name="surf"]').val(procedure.Surf);
		$('#form_procedure_edit [name="fee"]').val(procedure.ProcFee);
		$('#form_procedure_edit [name="note"]').val(procedure.Notes);
		if(procedure.isSurf == 1){
			$('#form_procedure_edit [name="surf"]').prop('disabled',false);
		}else{
			$('#form_procedure_edit [name="surf"]').prop('disabled',true);
		}
		
		$('.bs-select').selectpicker('refresh');
		$('#modal_procedures_edit').modal('show');
	});
	
	$('#form_procedure [name="doctor"]').on('changed.bs.select',function(){
		id = $(this).val();
		$('#doctorproclist').val(id).trigger('change');
	});
	
	$('#form_procedure_edit [name="feesched"]').on('changed.bs.select',function(){
		fee = $(this).val();
		$('#form_procedure_edit [name="fee"]').val(fee);
	});
		
	
	$('#tp_planappt_proce tbody').on('click','tr',function(){
		$(this).toggleClass('selection');
		InputCheck = $(this).find('input[type="checkbox"]');
		if(InputCheck[0].checked == false){
			InputCheck.prop('checked',true);
		}else{
			InputCheck.prop('checked',false);
		}
    });
		
	/* ODONTOGRAMA */
	$('#quickbuttons').on('click','.btnquick',function(){
		var selectTooth = $('.select');
		if(selectTooth.length > 0){
			tooths = [];
			$.each(selectTooth,function(i,item){
				tooths.push($(item).data('tooth'));
			});			
			//pat = $('#frmsearchpat [name="patient"]').val();
			estatus = $('#form_procedure [name="status"]').val();
			data = [];
			
			data.push({name: 'tp', value: tpselect});
			data.push({name: 'tooth', value: tooths});
			data.push({name: 'status', value: estatus});
			//data.push({name: 'patient', value: pat});
			data.push({name: 'procedure', value: $(this).data('proccode')});
			data.push({name: 'surf', value: $(this).data('surf')});
			$.ajax({
				url: urlapi+"Api_treatplan/tpprocedure",
				type:"POST",
				data: data,
				dataType:"json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					$('.odttooth').removeClass('select');
					dt_tpproc.ajax.reload(function(){ loadOdt(); });
				} 
			});
		}else{
			apptools.sweetNoty('warning','Nothing selected','You need selection a tooth.');
		}
		//if(selecttooth.length > 0){ $.each(selecttooth,function(i,item){ $(item).addClass('mesial oclusal'); }); }
	});
	
	$('#postcomsite').on('click','.mo',function(){ 
		var selecttooth = $('.select');
		if(selecttooth.length > 0){ $.each(selecttooth,function(i,item){ $(item).addClass('mesial oclusal'); }); }
	});
	$('#postcomsite').on('click','.mod',function(){
		//D2393
		var selecttooth = $('.select');
		if(selecttooth.length > 0){ $.each(selecttooth,function(i,item){ $(item).addClass('mesial oclusal distal'); }); }
	});
	$('#postcomsite').on('click','.o',function(){ 
		var selecttooth = $('.select');
		if(selecttooth.length > 0){ $.each(selecttooth,function(i,item){ $(item).addClass('oclusal'); }); }
	});
	$('#postcomsite').on('click','.do',function(){ 
		var selecttooth = $('.select');
		if(selecttooth.length > 0){ $.each(selecttooth,function(i,item){ $(item).addClass('distal oclusal'); }); }
	});
	$('#postcomsite').on('click','.ol',function(){ 
		var selecttooth = $('.select');
		if(selecttooth.length > 0){ $.each(selecttooth,function(i,item){ $(item).addClass('oclusal lingual'); }); }
	});
	$('#postcomsite').on('click','.ob',function(){ 
		var selecttooth = $('.select');
		if(selecttooth.length > 0){ $.each(selecttooth,function(i,item){ $(item).addClass('oclusal buccal'); }); }
	});
	$('#postcomsite').on('click','.modl',function(){ 
		var selecttooth = $('.select');
		if(selecttooth.length > 0){ $.each(selecttooth,function(i,item){ $(item).addClass('mesial oclusal distal lingual'); }); }
	});
	$('#postcomsite').on('click','.modb',function(){ 
		var selecttooth = $('.select');
		if(selecttooth.length > 0){ $.each(selecttooth,function(i,item){ $(item).addClass('mesial oclusal distal buccal'); }); }
	});
	
	$('.selection').on('click',function(){
		$(this).toggleClass('select');
	});
	
	/* ********** */
	$.validate({
		form : '#form_newappointment',
		modules : 'logic',
		validateOnBlur : true, 
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form){
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			//$form.find('button[type="submit"]').prop('disabled',true);
			$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-spinner fa-pulse fa-fw');
			
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					//$form.find('button[type="submit"]').prop('disabled',false);
					$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-floppy-o');
					if(response.code == 0){
						apptools.notysuccess();
						$('#form_newappointment').get(0).reset();
						$('#modal_add_planappt').modal('hide');
						dt_plaappt.ajax.reload();
					}else{
						apptools.notyerror();
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	$.validate({
		form : '#form_newtreatplan',
		modules : 'logic',
		validateOnBlur : true, 
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form){
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			$form.find('button[type="submit"]').prop('disabled',true);
			$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-spinner fa-pulse fa-fw');
			var data = $form.serializeArray();
			data.push({name: 'patient', value: selectpat});
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.data.code == 0){
						$('#form_newtreatplan').get(0).reset();
						$('#modal_newtreatplan').modal('hide');
						apptools.sweetNoty('success','Complete','New Treat Plant create');
						dt_treatplans.ajax.reload();
					}else{
						
					}
				}
			});
			
		  return false;
		},
	});
	
	$.validate({
		form : '#form_edittreatplan',
		modules : 'logic',
		validateOnBlur : true, 
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form){
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			$form.find('button[type="submit"]').prop('disabled',true);
			$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-spinner fa-pulse fa-fw');
			var data = $form.serializeArray();
			data.push({name: 'patient', value: selectpat});
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.data.code == 0){
						$('#form_newtreatplan').get(0).reset();
						$('#modal_newtreatplan').modal('hide');
						apptools.sweetNoty('success','Complete','New Treat Plant create');
						dt_treatplans.ajax.reload();
					}else{
						
					}
				}
			});
			
		  return false;
		},
	});
	
	$.validate({
		form : '#form_procedure_edit',
		modules : 'logic',
		validateOnBlur : false, 
		errorMessagePosition : 'top',
		scrollToTopOnError : false,
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form){
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			$form.find('button[type="submit"]').prop('disabled',true);
			$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-spinner fa-pulse fa-fw');
			
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					$form.find('button[type="submit"]').prop('disabled',false);
					$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-floppy-o');
					if(response.code == 0){
						apptools.notysuccess();
						dt_tpproc.ajax.reload();
						$('#form_procedure_edit').get(0).reset();
						$('#modal_procedures_edit').modal('hide');
						$(".modal-backdrop.in").hide();
					}else{
						apptools.notyerror();
					}
				}
			});
		  return false;
		},
	});
	
	$('#treatedit').on('click',function(){
		if($('.selection').length == 0){ apptools.notyCustom('information','<i class="fa fa-info"></i> Not Patient','Selected a patient from the list'); }
		else{
			var data = dt_treatplans.row($('.selection')).data();
			$('#form_edittreatplan [name="id"]').val(data.id);
			$('#form_edittreatplan [name="name"]').val(data.Heading);
			$('#form_edittreatplan [name="notes"]').val(data.Note);
			$('#form_edittreatplan [name="status"]').prop('checked',parseInt(data.TPStatus));
			$('#modal_edittreatplan').modal('show');
		}
	});
	
	$('#dt_plaappt tbody').on('click','.delete',function(){
		var data = dt_plaappt.row($(this).parents('tr')).data();
		swal({
			title: "Are you sure delete planned appointment?",
			text: data.title+"<br> <b>Doctor:</b> "+data.ProName,
			type: "warning",
			showConfirmButton: 1,
			showCancelButton: 1,
			html:true,
			dangerMode: true,},
			function(isConfirm){
				if(isConfirm){
					$.ajax({
						url: urlapi+"Api_cita/citas",
						type:"delete",
						data: {id: data.id},
						dataType:"json",
						beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
						success: function(response){
							dt_plaappt.ajax.reload();
						} 
					});
				}
			});
		});
	
	$('.tooth16').on('click',function(){
		alert('d');
	});
	
	$('#moldarbtn').on('click',function(){
		
		$('#modal_molar').modal('show');
	});
	
	$('#implant').on('click',function(){
		checks = $('.toothchartcheck:checked');
		$.each(checks,function(i,item){
			toothnumber = $(item).val();
			$(item).parent('label');
			$(item).parent('label').find('svg').addClass('implant');
		});
	});
	
	$('#endo').on('click',function(){
		checks = $('.toothchartcheck:checked');
		$.each(checks,function(i,item){
			toothnumber = $(item).val();
			$(item).parent('label');
			$(item).parent('label').find('svg').addClass('endo');
		});
	});
	
	$('#carieleft').on('click',function(){
		checks = $('.toothchartcheck:checked');
		$.each(checks,function(i,item){
			toothnumber = $(item).val();
			$(item).parent('label');
			$(item).parent('label').find('svg').addClass('carieleft');
		});
	});
	
	$('#crackedright').on('click',function(){
		checks = $('.toothchartcheck:checked');
		$.each(checks,function(i,item){
			toothnumber = $(item).val();
			$(item).parent('label');
			$(item).parent('label').find('svg').addClass('crackedright');
		});
	});
	
	$('.toothchartcheck').on('change',function(){
		if($(this)[0].checked == true){
			$('#toothbadge'+$(this).val()).removeClass("badge-default");
			$('#toothbadge'+$(this).val()).addClass("badge-danger");
		}else{
			$('#toothbadge'+$(this).val()).removeClass(" badge-danger");
			$('#toothbadge'+$(this).val()).addClass("badge-default");
		}
	});

	$('#proceshow').on('change',function(){
		dt_tpproc_data.status = $(this).val();
		dt_tpproc.ajax.reload();
		loadOdt();
	});
	
	$('#tp_appt_proce_add tbody').on('click','tr',function(){
		$('#form_newappointment [name="procupd"]').prop('checked',true);
		$(this).toggleClass('selection');
		InputCheck = $(this).find('input[type="checkbox"]');
		if(InputCheck[0].checked == false){
			InputCheck.prop('checked',true);
		}else{
			InputCheck.prop('checked',false);
		}
    });
});

function load_procedures_appt(){
	$('#tp_appt_proce_add tbody').empty();
	$.ajax({
		url: urlapi+"Api_paciente/procedimientos",
		type: "GET",
		data : { id: patient},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,item){				
				$('#tp_appt_proce_add tbody').append('<tr>'
				+'<td class="padding-5 padding-top-0 padding-bottom-0">'+item.Duration+'</td>'
				+'<td class="padding-5 padding-top-0 padding-bottom-0">'
					+'<input style="display:none;" type="checkbox" name="procedures[]" value="'+item.Id+'">'
					+item.ProcCode+'</td>'
				+'<td class="padding-5 padding-top-0 padding-bottom-0">'+item.Descript+'</td>'
				+'</tr>');
			});
		}
	});	
}

function newApptPlan(){		
		val = 89;
		$.ajax({
			url: urlapi+"Api_paciente/detalle",
			type: "GET",
			data : { id: val},
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
			success: function(response){
				$('#form_newappointment [name="patient[nombre]"]').val(response.data[0].FName);
				$('#form_newappointment [name="patient[apellidos]"]').val(response.data[0].LName);
				$('#form_newappointment [name="patient[apellidos]"]').val(response.data[0].LName);
				$('#form_newappointment [name="patient[correo]"]').val(response.data[0].Email);
				$('#form_newappointment [name="patient[casatelefono]"]').val(response.data[0].HmPhone);
				$('#form_newappointment [name="patient[moviltelefono]"]').val(response.data[0].WirelessPhone);
				$('#form_newappointment [name="patient[trabajotelefono]"]').val(response.data[0].WkPhone);
			},
			complete: function(){
				$('#modal_add_planappt').modal();
			}
		});
		
		$.ajax({
			url: urlapi+"Api_paciente/procedimientos",
			type: "GET",
			data : { id: val},
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
			success: function(response){
				$('#tp_appt_proce_add tbody').empty();
				$.each(response.data,function(i,item){
					if(item.AptNum == ''  || item.AptNum == null){
						$('#tp_appt_proce_add tbody').append('<tr>'
						+'<td class="padding-5 padding-top-0 padding-bottom-0"><input style="display:none;" type="checkbox" name="procedures[]" value="'+item.Id+'" data-validation="checkbox_group" data-validation-qty="min1">'+item.Duration+'</td>'
						+'<td class="padding-5 padding-top-0 padding-bottom-0">'+item.ProcCode+'</td>'
						+'<td class="padding-5 padding-top-0 padding-bottom-0">'+item.Descript+'</td>'
						+'</tr>');
					}
				});
			}
		});
	};
	
function clear_odt(){
	selection = $('#mouth .selection');
	selection.removeClass('select');
	
	crown = $('#mouthdown [data-type="crown"]');
	implant = $('#mouthdown [data-type="implant"]');
	suface = $('#mouthdown [data-type="surf"]');
	tooths = $('#mouthdown [data-type="tooth"]');
	tooths.removeClass().attr('display','block');	
	suface.removeClass().attr('display','none');	
	implant.removeClass().attr('display','none');
	crown.removeClass().attr('display','none');
	
	crown = $('#mouthup [data-type="crown"]');
	implant = $('#mouthup [data-type="implant"]');
	suface = $('#mouthup [data-type="surf"]');
	tooths = $('#mouthup [data-type="tooth"]');
	tooths.removeClass().attr('display','block');	
	suface.removeClass().attr('display','none');	
	implant.removeClass().attr('display','none');
	crown.removeClass().attr('display','none');
}	
	
function loadOdt(){	
	clear_odt();
	$.ajax({
		url: urlapi+"Api_treatplan/tpprocedure",
		Type:"GET",
		dataType:"json",
		data: dt_tpproc_data,
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,item){
				console.log(item);
				if(item.isSurf == 1){
					var strg = item.Surf;
					for(indx in strg){
						status = "";
						if(item.ProcStatus == 1){ status = "incomplete"; }
						if(item.ProcStatus == 2){ status = "complete"; }
						if(item.ProcStatus == 3){ status = "exisother"; }
						$('#teeth'+item.ToothNum+' g[id="'+strg[indx]+'"]').attr('display',"block")
						$('#teeth'+item.ToothNum+' g[id="'+strg[indx]+'"]').addClass(status);
					}
				}
				if(item.isCrown == 1){
					status = "";
					if(item.ProcStatus == 1){ status = "incomplete"; }
					if(item.ProcStatus == 2){ status = "complete"; }
					if(item.ProcStatus == 3){ status = "exisother"; }
					
					//$('#tooth'+item.ToothNum).attr('display',"none");
					$('#teeth'+item.ToothNum+' g[id="crown"]').attr('display','block');
					$('#teeth'+item.ToothNum+' g[id="crown"]').addClass(status);
				}
				if(item.isImplant == 1){
					$('#tooth'+item.ToothNum).attr('display',"none");
					$('#implant'+item.ToothNum).attr('display',"block");
				}
			});
		}
	});
}

function format(d){
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
            '<td>SF:</td>'+
            '<td></td>'+
        '</tr>'+
        '<tr>'+
            '<td>DX:</td>'+
            '<td></td>'+
        '</tr>'+
        '<tr>'+
            '<td>Extra info:</td>'+
            '<td>And any further details here (images etc)...</td>'+
        '</tr>'+
    '</table>';
}