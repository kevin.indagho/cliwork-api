var usertype;
var dtcuest;
var patientselect,questselect;
var dtfilter = $('#dtfilter').val();

Dropzone.options.formaddfile = {
	url: urlapi+"Api_data/cuestionariofile",
	addRemoveLinks : true,
	paramName: "file", 
	maxFilesize: 2, // MB
	headers: {'Authorization': auth},
	acceptedFiles: ".jpg,.png,application/pdf",
	params: {
		"user": apptools.userlogin,
	},
	accept: function(file, done){
		done();
	},
	sending: function(file, xhr, formData){
        formData.append('patient', patientselect);
        formData.append('questid', questselect);
    },
	success: function(file, message) {
		if(message.code == 1){
			$(file.previewElement).addClass("dz-error").find('.dz-error-message').text(message.error);
		}else{
			$(file.previewElement).addClass("dz-success");
		}
		//notyCustom(message.type,message.title,message.error);
		
	},error: function(file, message) {
		$(file.previewElement).addClass("dz-error").find('.dz-error-message').text(message.error);
	}
};

$(document).ready(function(){
	usertype = $('#usertype').val();
	
	$.ajax({
		url: urlapi+"Api_data/cuesinfo",
		type: "GET",
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			console.log(response);
			$('#totalInformer').html(response.totalFree);
		}
	});
		
	$.validate({
		form : '#form_details',
		modules : 'logic',
		validateOnBlur : true, 
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onError : function($form){
			//apptools.notyCustom('error','fields Required','Complete All inputs');
		},
		onSuccess : function($form){
			var data = $form.serializeArray();
			data.push({name: "userid", value: apptools.userlogin})
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.code == 0){
						apptools.sweetNoty('success','Complete','Update questionnaire');
						id = $('#form_details [name="id"]').val();
						notes(id);
						dtcuest.ajax.reload();
					}else{
						apptools.sweetNoty('error','Error','');
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	dtcuest = $("#dtcuest").DataTable({
		"ajax": {
			url: urlapi+"Api_data/cuestionarios/"+dtfilter+"/"+apptools.userlogin+'/'+usertype,
			type: "get",
			beforeSend: function (xhr){
				xhr.setRequestHeader('Authorization', auth);
			},
		},
		columns:[
			{data: 'fecha'},
			{data: 'doctor'},
			{data: 'usuario'},
			{data: 'Descript'},
			{data: 'FName'},
			{data: 'LName'},
			{data: null},
			{data: 'lastnote'},
			{data: 'archivos',width: "60px"},
			{data: 'boton',width: "15px"},
		],
		"drawCallback": function( settings ){ $("[data-toggle='tooltip']").tooltip(); },
		"rowCallback": function( row, data ){
			if(data.estado == 1){
				$('td:eq(6)',row).html('<span class="label label-success">Approved</span>');
			}else if(data.estado == 2){
				$('td:eq(6)',row).html('<span class="label label-warning">Checking</span>');
			}else if(data.estado == 3){
				$('td:eq(6)',row).html('<span class="label label-danger">Cancelled</span>');
			}else{
				$('td:eq(6)',row).html('<span class="label label-primary">Pending</span>');
			}
			
		}
	});
	
	dt_procesurvey = $("#dt_procesurvey").DataTable({
		"ajax": {
			url: urlapi+"Api_data/surveys/",
			type: "get",
			beforeSend: function (xhr){xhr.setRequestHeader('Authorization', auth);},
		},
		columns:[
			{data: 'Descript'},
			{data: null},
		],
		"drawCallback": function( settings ){ $("[data-toggle='tooltip']").tooltip(); },
		"rowCallback": function( row, data ){
			$('td:eq(1)',row).html('<a href="'+base_url+'survey/'+data.id+'" target="_blank">Open</a>');
		}
	});
	
	$("#dtcuest tbody").on('click','.files',function(){
		var data = dtcuest.row($(this).parents('tr')).data();
		patientselect = data.patientid
		questselect = data.id
		$('#modal_files').modal('show');
	});
	
	$("#dtcuest tbody").on('click','.open',function(){
		var data = dtcuest.row($(this).parents('tr')).data();
		$.ajax({
			url: urlapi+"Api_data/cuestionario",
			type: "get",
			data:{id: data.id},
			beforeSend: function (xhr){
				xhr.setRequestHeader('Authorization', auth);
			},
			success: function(response){
				data = response.data;
				$('#form_details [name="id"]').val(data[0].id);
				$('#form_details [name="patientid"]').val(data[0].patientid);
				$('#form_details [name="fname"]').val(data[0].FName);
				$('#form_details [name="lname"]').val(data[0].LName);
				$('#form_details [name="cell"]').val(data[0].WirelessPhone);
				$('#form_details [name="phone"]').val(data[0].HmPhone);
				$('#form_details [name="email"]').val(data[0].Email);
				$('#form_details [name="status"][value="'+data[0].estado+'"]').prop('checked',true);
				$('#formulario_contenido').empty();
				$.each(data[0].preguntas,function(i,item){
					$('#formulario_contenido').append('<div class="col-md-12 heading-line-below"><div class="form-group"><label class="control-label  col-xs-12 col-md-6">'+item.nombre+'</label>'
					+'<div class="col-md-6">'+item.control+'<div>'
					+'</div></div>');
				});
				$(".app-checkbox label, .app-radio label").each(function(){$(this).append("<span></span>");});
				$('#filegallery').empty();
				$.each(data[0].archivos,function(i,item){
					$('#filegallery').append('<div class="col-md-3 col-ms-4 grid-element filter-business">'
							+'<div class="tile-basic">'
								+'<a href="#" class="tile-image tile-image-padding tile-image-hover-grayscale preview" data-preview-image="'+pathapi+'files/patients/'+data[0].patientid+'/'+item.nombreFisico+'" data-preview-size="modal-lg">'
									+'<img src="'+pathapi+'files/patients/'+data[0].patientid+'/'+item.nombreFisico+'" alt="">'
								+'</a>'
							+'</div>'
						+'</div>');
				});
				notes(data[0].id);
			},
			complete : function(){
				$('#modal_details').modal('show');
				$(".scrollCustom").mCustomScrollbar({axis:"y", autoHideScrollbar: true, scrollInertia: 200, advanced: {autoScrollOnFocus: false}});
				$("#timelineList").mCustomScrollbar({axis:"y", autoHideScrollbar: true, scrollInertia: 200, advanced: {autoScrollOnFocus: false}});
				/*setTimeout(function(){
					$('#filegallery').isotope({
						itemSelector: ".grid-element",
						layoutMode: "fitRows",
						percentPosition: true
					}); 
				}, 3000);*/
				/*var $grid = $('#filegallery').isotope({
					itemSelector: ".grid-element",
					layoutMode: "fitRows",
					percentPosition: true
				});*/
				
			
			}
		});
	});
	
	$("#dtcuest tbody").on('click','.takedoc',function(){
		var data = dtcuest.row($(this).parents('tr')).data();
		$.ajax({
			url: urlapi+"Api_data/cuestionario",
			type: "PATCH",
			data: {doctor:apptools.userlogin,id: data.id},
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
			success: function(response){
				console.log(response);
			}
		});
		
	});
	
	$("#dtcuest tbody").on('click','.takeven',function(){
		var data = dtcuest.row($(this).parents('tr')).data();
		$.ajax({
			url: urlapi+"Api_data/cuestionario",
			type: "PATCH",
			data: {usuario:apptools.userlogin,id: data.id},
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
			success: function(response){
				console.log(response);
			}
		});
	});

	$('#dtfilter').on('change',function(){
		dtfilter = $(this).val();
		dtcuest.ajax.url(urlapi+"Api_data/cuestionarios/"+dtfilter+"/"+apptools.userlogin+'/'+usertype).load();
	});
});

function notes(id){
	$('#timelineList .app-timeline').empty();
	$.ajax({
		url: urlapi+"Api_data/cuestionarionotas",
		type: "GET",
		dataType: "json",
		data: {id:id},
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,note){
					$('#timelineList .app-timeline').append('<div class="app-timeline-item">'
						+'<div class="dot dot-'+note.EstadoColor+'"></div>'
						+'<div class="content">'
							+'<div class="title"><strong><a href="#">'+note.user+'</a></strong></div>'
							+'<div class="scrollCustom" style="max-height: 240px;">'
							+ note.nota
							+'</div>'
							+'<p>'
								+'<span class="pull-right text-muted"> <i class="fa fa-calendar"></i> '+note.fecha+'</span>'
							+'</p>'
						+'</div>'
					+'</div>');
				});
		}
	});
	
}