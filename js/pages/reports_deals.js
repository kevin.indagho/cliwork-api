var dtdeals;
$(document).ready(function(){
	$("#tabStage").mCustomScrollbar({axis:"y", autoHideScrollbar: true, scrollInertia: 200, advanced: {autoScrollOnFocus: false}});
	
	$.ajax({
		url: urlapi+"Api_reportes/ventasinformes",
		type: "GET",
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$('#totalInformer').html(response.data.total);
		}
	});
	
	dtdeals = $("#dtdeals").DataTable({
		dom: '<"col-md-6"B><"col-md-6"f>rt<"col-md-6 hidden-mobile"i><"col-md-6"p>',
		buttons: [
			{
				text: '<span class="fa fa-eye"></span> Open',
				className: 'btn btn-info btn-sm btn-icon-fixed',
				attr: { "disabled": "",id: 'ofertaopen' }
				
			}
		],
		"ajax":{
			url: urlapi+"Api_reportes/ventas",
			Type:"GET",
			dataType:"json",
			//data:{patient: patientid},
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		},
		columns:[
			/*{
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },*/
			{data: 'fecha'},
			{data: 'days'},
			{data: 'usuario'},
			{data: 'patient'},
			{data: 'StepName'},
			{data: 'valorFormat'},
			{data: 'lastact[].nombre'},
			{data: 'lastact[].days'},
		],
		"rowCallback": function( row, data ) {
			if(data.lastact.length > 0){
				$('td:eq(6)', row).html('<i class="text-'+data.lastact[0].EstadoColor+' fa fa-'+data.lastact[0].EstadoIcon+'"></i> '+data.lastact[0].nombre);
			}
		}
	});
	
	$('#dtdeals tbody').on( 'click', 'tr', function () {
        if($(this).hasClass('selection')){
            $(this).removeClass('selection');
			$('#ofertaopen').prop('disabled',true);
        }else{
			$('#ofertaopen').prop('disabled',false);
            dtdeals.$('tr.selection').removeClass('selection');
            $(this).addClass('selection');
        }
    });
	
	$('#dtdeals tbody').on('dblclick','tr',function(e){
		$('#ofertaopen').prop('disabled',false);
		dtdeals.$('tr.selection').removeClass('selection');
        $(this).addClass('selection');
		$('#ofertaopen').trigger('click');
	});
	
	$('.dealdetails').on('click',function(){
		var data = dtdeals.row($('.selection')).data();
		window.open(base_url+"deals/details/"+data.id+"/"+data.pacienteid, '_blank');
	});
	
	$('#ofertaopen').on('click',function(){
		var data = dtdeals.row($('.selection')).data();
		$('#dtstagecontrol tbody').empty();
		$.each(data.etapasControl,function(i,item){
			$('#dtstagecontrol tbody').append('<tr>'
				+'<td>'+item.EtapaNombre+'</td>'
				+'<td>'+item.fechaInicio+'</td>'
				+'<td>'+item.fechaFin+'</td>'
				+'<td>'+item.duration.days+'</td>'
				+'<td>'+item.dias+'</td>'
			+'</tr>');
		});
		
		//updateAct(pacienteid,ofertaid,etapaid);
		$.ajax({
			url: urlapi+"Api_paciente/detalleoferta",
			type: "GET",
			data: {id: data.id},
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
			success: function(response){
				$('#form_deal [name="id"]').val(response[0].id);
				$('#form_deal [name="patient"]').val(response[0].FName+" "+response[0].LName);
				$('#form_deal [name="value"]').val(response[0].valorFormato);
				$('#form_deal [name="closedate"]').val(response[0].fechaCierre);
				$('#form_deal [name="email"]').val(response[0].Email);
				$('#form_deal [name="phome"]').val(response[0].HmPhone);
				$('#form_deal [name="pwork"]').val(response[0].WkPhone);
				$('#form_deal [name="pmobile"]').val(response[0].WirelessPhone);
				$('#form_deal [name="treatment[]"]').val(response[0].tratamientos).trigger('change');
				$('#form_deal [name="treatment[]"]').multiSelect("refresh");
				$('#listProcedures').empty();
				$.each(response[0].procedimientos,function(i,item){
					$('#listProcedures').append('<li class="list-group-item">'+item.Descript+' <span class="badge badge-success">$ '+item.Amount+'</span></li>');
				});
			},complete: function(){
				$('#modal_opendeal').modal('show');
				$(".scrollCustom").mCustomScrollbar('destroy');
				$(".scrollCustom").mCustomScrollbar({axis:"y", autoHideScrollbar: true, scrollInertia: 200, advanced: {autoScrollOnFocus: false}});	
			}
		});
	});

	/*$('#dtdeals tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = dtdeals.row(tr);
        if ( row.child.isShown()){
            row.child.hide();
            tr.removeClass('shown');
        }else{
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });*/
});

function format ( d ) {
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
            '<td>Last Activity:</td>'+
            '<td></td>'+
        '</tr>'+
    '</table>';
}