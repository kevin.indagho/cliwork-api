var calendar;
var resourceData;
var calconfig;
$(document).ready(function(){
	window.chartColors = {
		red: "rgb(239, 64, 67)",
		orange: "rgb(246, 159, 0)",
		yellow: "rgb(242, 255, 37)",
		green: "rgb(118, 171, 60)",
		blue: "rgb(79, 181, 221)",
		purple: "rgb(153, 102, 255)",
		grey: "rgb(231,233,237)"
	}, window.randomScalingFactor = function() {
		return (Math.random() > .5 ? 1 : -1) * Math.round(100 * Math.random())
            };
			
			
	$.ajax({
		url: urlapi+"Api_data/origendata",
		Type:"GET",
		dataType:"json",
		data: { type:"10"},
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			let divelement = document.getElementById("g_origins").getContext("2d");
			new Chart(divelement,{
				type: "line",
				data: {
					labels: ["January", "February", "March", "April", "May", "June", "July","August","September","October","November","December"],
					datasets: [{
						label: "System",
						backgroundColor: window.chartColors.green,
						borderColor: window.chartColors.green,
						data: response[0].data,
						datalabels: { align: 'top', anchor: 'end' },
						fill: !1
					},{
						label: "Facebook",
						backgroundColor: window.chartColors.blue,
						borderColor: window.chartColors.blue,
						data: response[1].data,
						datalabels: { align: 'top', anchor: 'end' },
						fill: !1
					},{
						label: "Instagram",
						fill: !1,
						backgroundColor: window.chartColors.purple,
						borderColor: window.chartColors.purple,
						data: response[2].data,
						datalabels: { align: 'top', anchor: 'end' }
					},{
						label: "Youtube",
						fill: !1,
						backgroundColor: window.chartColors.red,
						borderColor: window.chartColors.red,
						data: response[3].data,
						datalabels: { align: 'top', anchor: 'end' }
					},{
						label: "Email",
						fill: !1,
						backgroundColor: window.chartColors.grey,
						borderColor: window.chartColors.grey,
						data: response[4].data,
						datalabels: { align: 'top', anchor: 'end' }
					},{
						label: "Referido",
						fill: !1,
						backgroundColor: window.chartColors.orange,
						borderColor: window.chartColors.orange,
						data: response[5].data,
						datalabels: { align: 'top', anchor: 'end' }
					}]
				},
				options: {
					plugins: {
						datalabels: {
							backgroundColor: function(context) {
								return context.dataset.backgroundColor;
							},
							borderRadius: 4,
							color: 'white',
							font: {
								weight: 'bold'
							},
							formatter: Math.round
						}
					},
					responsive: true,
					maintainAspectRatio: false,
					title: {
						display: !0,
						text: "Origenes de los clientes"
					},
					tooltips: {
						mode: "index",
						intersect: !1
					},
					hover: {
						mode: "nearest",
						intersect: !0
					},
					scales: {
						xAxes: [{
							display: !0,
							scaleLabel: { display:false, labelString: "Month" }
						}],
						yAxes: [{
							ticks: { beginAtZero: true },
							display: !0,
							scaleLabel: {
								display: !0,
								labelString: "Value"
							}
						}]
					}
				}
			});
		}
	});
	
	$.ajax({
		url: urlapi+"Api_data/ofertasdata",
		Type:"GET",
		dataType:"json",
		data: { type:"10"},
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			var o = document.getElementById("chartjs_bar").getContext("2d");
			new Chart(o, {
				type: "bar",
				data: {
					labels: ["January", "February", "March", "April", "May", "June", "July","August","September","October","November","December"],
					datasets: [{
						label: "Pendientes",
						backgroundColor: window.chartColors.blue,
						borderColor: window.chartColors.blue,
						borderWidth: 1,
						data: response[0].data,
						datalabels: { align: 'top', anchor: 'end' }
					}, {
						label: "Cerradas",
						backgroundColor: window.chartColors.green,
						borderColor: window.chartColors.green,
						borderWidth: 1,
						data: response[2].data,
						datalabels: { align: 'top', anchor: 'end' }
					}, {
						label: "Perdidas",
						backgroundColor: window.chartColors.red,
						borderColor: window.chartColors.red,
						borderWidth: 1,
						data: response[1].data,
						datalabels: { align: 'top', anchor: 'end' }
					}]
				},
				options: {
					 scales: {
						yAxes: [
							{
								ticks: { beginAtZero: true }
							}
						]
					},
					plugins: { datalabels: {color: 'white',backgroundColor: function(context) { return context.dataset.backgroundColor; }, borderRadius: 4, font: { weight: 'bold' } } },
					responsive: true,
					maintainAspectRatio: false,
					legend: {
						position: "top"
					},
					title: {
						display: !0,
						text: "Ofertas $"
					}
				}
			});
		}
	});

	$.ajax({
		url: urlapi+"Api_data/procedimientosdata",
		Type:"GET",
		dataType:"json",
		data: { type:"10"},
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			nombres = [];
			data = [];
			$.each(response.number,function(i,item){
				nombres.push(item.Descript);
				data.push(item.number);
			});
			var o = document.getElementById("g_procedimientos").getContext("2d");
			new Chart(o, {
				type: "bar",
				data: {
					labels: nombres,
					datasets: [{
						label: "Number",
						backgroundColor: window.chartColors.blue,
						borderColor: window.chartColors.blue,
						borderWidth: 1,
						data: data,
						datalabels: { align: 'top', anchor: 'end' }
					}]
				},
				options: {
					plugins: { datalabels: {color: 'white',backgroundColor: function(context) { return context.dataset.backgroundColor; }, borderRadius: 4, font: { weight: 'bold' } } },
					responsive: true,
					scales: {
						yAxes: [
							{
								ticks: { beginAtZero: true }
							}
						]
					},
					showAllTooltips: true,
					maintainAspectRatio: false,
					legend: { display: false, position: "top" },
					title: { display: !0, text: "Procedimientos mas vendidos" }
				}
			});
		}
	});

	$.ajax({
		url: urlapi+"Api_data/procedimientosdata",
		Type:"GET",
		dataType:"json",
		data: { type:"10"},
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			nombres = [];
			data = [];
			$.each(response.amount,function(i,item){
				nombres.push(item.Descript);
				data.push(item.amount);
			});
			var o = document.getElementById("g_procedimientosmoney").getContext("2d");
			new Chart(o, {
				type: "bar",
				data: {
					labels: nombres,
					datasets: [{
						label: "Amount",
						backgroundColor: window.chartColors.blue,
						borderColor: window.chartColors.blue,
						borderWidth: 1,
						data: data,
						datalabels: { align: 'top', anchor: 'end' }
					}]
				},
				options: {
					 scales: {
						yAxes: [
							{
								ticks: { beginAtZero: true }
							}
						]
					},
					plugins: { datalabels: {color: 'white',backgroundColor: function(context) { return context.dataset.backgroundColor; }, borderRadius: 4, font: { weight: 'bold' } } },
					responsive: true,
					showAllTooltips: true,
					maintainAspectRatio: false,
					legend: { display: false, position: "top" },
					title: { display: !0, text: "Procedimientos con mas ingresos" },
					scales: {
						yAxes: [{
							ticks: {
								fixedStepSize: 1
							}
						}],
					}
				}
			});
		}
	});

	$.ajax({
		url: urlapi+"Api_data/proving",
		Type:"GET",
		dataType:"json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			var dataSets = [];
			$.each(response.data,function(i,item){
				dataSets.push({
						label: item.nombre,
						backgroundColor: item.Color,
						borderColor: item.Color,
						borderWidth: 1,
						data: item.fechas,
						datalabels: { align: 'top', anchor: 'end' }
					});
			});
			var o = document.getElementById("g_provingresos").getContext("2d");
			new Chart(o, {
				type: "bar",
				data: {
					labels: ["January", "February", "March", "April", "May", "June", "July","August","September","October","November","December"],
					datasets: dataSets,
				},
				options: {
					scales: {
						yAxes: [
							{
								ticks: { beginAtZero: true }
							}
						]
					},
					plugins: { datalabels: { display :false, color: 'white',backgroundColor: function(context) { return context.dataset.backgroundColor; }, borderRadius: 4, font: { weight: 'bold' } } },
					responsive: true,
					maintainAspectRatio: false,
					legend: { position: "top" },
					title: { display: !0, text: "Ingresos generados por doctor" }
				}
			});
		}
	});

});