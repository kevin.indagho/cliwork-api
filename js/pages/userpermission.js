var dttypes;

$(document).ready(function(){
	dttypes = $("#dttypes").DataTable({
		"ajax": {
			url: urlapi+"Api_usuariotipo/tipos",
			type: "get",
			data: {activo: "none"},
			beforeSend: function (xhr){
				xhr.setRequestHeader('Authorization', auth);
			},
			data: {activo: "none"},
		},
		columns: [
			{data: 'name'},
			{data: null},
			{defaultContent: '<button type="button" class="editar btn btn-warning btn-xs"><i class="fa fa-pencil"></i></button>'},
		],
		"rowCallback": function(row,data){
			
			if(data.id == 1){ $('td:eq(2)',row).html("No Edit"); }
			if(data.activo == 1){ active = '<span class="label label-success">Active</span>'}
			else{ active = '<span class="label label-danger">Inactive</span>';}
			$('td:eq(1)',row).html(active);
		}
	});
	
	dtpermissions = $("#dtpermissions").DataTable({
		"ajax": {
			url: urlapi+"Api_usuario/permisos",
			type: "get",
			data: {tipo: 1},
			beforeSend: function (xhr){
				xhr.setRequestHeader('Authorization', auth);
			},
		},
		columns: [
			{data: 'menu', class:"padding-5"},
			{data: 'submenu', class:"padding-5"},
			{data: null, class:"padding-5"},
		],
		"rowCallback": function( row, data ){
			permitido = "";
			if(data.permitido == 1){ permitido = checked="checked"; }
			active ='<label id="t'+data.id+'" data-toggle="tooltip" data-placement="right" class="switch switch-sm switch-cube">'
					+'<input data-id="'+data.permisoid+'" type="checkbox" class="checkbox" name="permisos['+data.id+'][permitido]" value="1" '+permitido+'>'
					+'<span></span>'
				+'</label>';
			$('td:eq(2)',row).html(active);
		}
	});
	
	$('#form_editpermission').on('change','.checkbox',function(){
		$('[data-toggle="tooltip"]').tooltip()
		permisoid = $(this).data('id');
		typeid = $('#form_editpermission [name="id"]').val();
		checked = this.checked;		
		$.ajax({
			url: urlapi+"Api_usuario/permisoedit",
			type: "POST",
			data: {id:permisoid,tipo:typeid,permitido:checked},
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
			success: function(response){
				console.log(response);
				$('#t'+permisoid).attr('data-original-title',response.message);
				$('#t'+permisoid).tooltip('show');
			}
		});
	});
	
	$('#dttypes tbody').on('click','.editar',function(){
		var data = dttypes.row($(this).parents('tr')).data();
		$('#form_editpermission [name="id"]').val(data.id);
		$('#form_edittype [name="id"]').val(data.id);
		$('#form_edittype [name="nombre"]').val(data.name);
		$('#form_edittype [name="activo"]').prop('checked',parseInt(data.activo));
		$('#modal_editpermissions').modal('show');
	});

	/*Export DataTable*/
	$('.hc').on('input',function(){
		var column = dttypes.column($(this).data('column'));
        column.visible(!column.visible());
	});
	$('#btnEXC').click(function(){ $('#dttypes').tableExport({type:'excel',escape:'false'}); });
	$('#btnPDF').click(function(){ $('#dttypes').tableExport({type:'pdf',escape:'false'}); });
});