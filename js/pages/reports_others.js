$(document).ready(function(){
	$("#patlist").mCustomScrollbar({axis:"y", autoHideScrollbar: true, scrollInertia: 200, advanced: {autoScrollOnFocus: false}});


	$('#form_rp_anual [name="date"]').mask('9999');
	
	$('#form_rp_envelope_template [name="patient"]').selectpicker({size:10,liveSearch: true}).ajaxSelectPicker({
		ajax : {
			url: urlapi+"Api_data/patsearch",
			type: "GET",
			// Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
			// automatically replace it with the value of the search query.
			data : { by:'AbbrDesc',name: '{{{q}}}' },
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); }
        },
        locale : { emptyTitle: 'Nothing selected' },
        log : 3,
        preprocessData: function (response){
            var l = response.data.length, array = [];			
            if (l){
				$.each(response.data,function(i,item){
					array.push($.extend(true,i,{
						text : item.FName+" "+item.LName,
						value: item.Id,
						//data : { subtext: "Sub Text" }
					}));
				});
            }
            // You must always return a valid array when processing data. The
            // data argument passed is a clone and cannot be modified directly.
            return array;
        }
	});
	
	$('#form_rp_envelope_template [name="patient"]').on('changed.bs.select',function(e){
		val = $(this).val();
		if(val != ""){
			patname = $(this)[0].selectedOptions[0].label;
			$('#ulpatlist').append('<li class="list-group-item"> <input type="hidden" name="patients[]" value="'+val+'">'+patname+'</li>');
			$('#form_rp_envelope_template [name="patient"]').val('').trigger('change');
		}
	});
	
	$('#btn_clr_list').on('click',function(){
		$('#ulpatlist').empty();
	});
	
	$.validate({
		form : '#form_rp_patdue',
		modules : 'file,logic',
		validateOnBlur : true,
		errorMessagePosition : 'top',
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form) {
			date = $('#form_rp_anual [name="date"]').val();
			$.ajax({
				url: urlapi+"cronjobs/pacientes_deuda",
				Type:"GET",
				//data: $($form).serialize(),
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				//xhrFields: { responseType: 'blob' },
				success: function(data){
					
					/*var a = document.createElement('a');
					var url = window.URL.createObjectURL(data);
					a.href = url;
					a.download = 'patients - '+date+'.pdf';
					document.body.append(a);
					a.click();
					a.remove();
					window.URL.revokeObjectURL(url);*/
				}
			});
		  return false; // Will stop the submission of the form
		}
	});

	$.validate({
		form : '#form_rp_anual',
		modules : 'file,logic',
		validateOnBlur : true,
		errorMessagePosition : 'top',
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form) {
			date = $('#form_rp_anual [name="date"]').val();
			$.ajax({
				url: urlapi+"cronjobs/reporte_anual",
				Type:"GET",
				data: $($form).serialize(),
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				xhrFields: { responseType: 'blob' },
				success: function(data){
					var a = document.createElement('a');
					var url = window.URL.createObjectURL(data);
					a.href = url;
					a.download = 'report anual - '+date+'.pdf';
					document.body.append(a);
					a.click();
					a.remove();
					window.URL.revokeObjectURL(url);
				}
			});
		  return false; // Will stop the submission of the form
		}
	});
	
	$.validate({
		form : '#form_rp_production',
		modules : 'file,logic',
		validateOnBlur : true,
		errorMessagePosition : 'top',
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form) {
			date = $('#form_rp_production [name="date"]').val();
			$.ajax({
				url: urlapi+"cronjobs/reporte_produccion",
				Type:"GET",
				data: $($form).serialize(),
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				xhrFields: { responseType: 'blob' },
				success: function(data){
					var a = document.createElement('a');
					var url = window.URL.createObjectURL(data);
					a.href = url;
					a.download = 'report production.pdf';
					document.body.append(a);
					a.click();
					a.remove();
					window.URL.revokeObjectURL(url);
				}
			});
		  return false; // Will stop the submission of the form
		}
	});

	$.validate({
		form : '#form_rp_daily_payments',
		modules : 'file,logic',
		validateOnBlur : true,
		errorMessagePosition : 'top',
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form) {
			date = $('#form_rp_daily_payments [name="date"]').val();
			$.ajax({
				url: urlapi+"cronjobs/reporte_pagos",
				Type:"GET",
				data: $($form).serialize(),
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				xhrFields: { responseType: 'blob' },
				success: function(data){
					var a = document.createElement('a');
					var url = window.URL.createObjectURL(data);
					a.href = url;
					a.download = 'report production.pdf';
					document.body.append(a);
					a.click();
					a.remove();
					window.URL.revokeObjectURL(url);
				}
			});
		  return false; // Will stop the submission of the form
		}
	});
	
	$.validate({
		form : '#form_rp_daily_procedures',
		modules : 'file,logic',
		validateOnBlur : true,
		errorMessagePosition : 'top',
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form) {
			date = $('#form_rp_daily_procedures [name="date"]').val();
			$.ajax({
				url: urlapi+"cronjobs/reporte_procedures",
				Type:"GET",
				data: $($form).serialize(),
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				xhrFields: { responseType: 'blob' },
				success: function(data){
					var a = document.createElement('a');
					var url = window.URL.createObjectURL(data);
					a.href = url;
					a.download = 'report procedures.pdf';
					document.body.append(a);
					a.click();
					a.remove();
					window.URL.revokeObjectURL(url);
				}
			});
		  return false; // Will stop the submission of the form
		}
	});

	$.validate({
		form : '#form_rp_production_procedures',
		modules : 'file,logic',
		validateOnBlur : true,
		errorMessagePosition : 'top',
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form) {
			date = $('#form_rp_production_procedures [name="date"]').val();
			$.ajax({
				url: urlapi+"cronjobs/reporte_procedures_production",
				Type:"GET",
				data: $($form).serialize(),
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				xhrFields: { responseType: 'blob' },
				success: function(data){
					var a = document.createElement('a');
					var url = window.URL.createObjectURL(data);
					a.href = url;
					a.download = 'report production procedures.pdf';
					document.body.append(a);
					a.click();
					a.remove();
					window.URL.revokeObjectURL(url);
				}
			});
		  return false; // Will stop the submission of the form
		}
	});

	$.validate({
		form : '#form_rp_envelope_template',
		modules : 'file,logic',
		validateOnBlur : true,
		errorMessagePosition : 'top',
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form) {
			date = $('#form_rp_envelope_template [name="date"]').val();
			$.ajax({
				url: urlapi+"cronjobs/envelope_template",
				Type:"GET",
				data: $($form).serialize(),
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				xhrFields: { responseType: 'blob' },
				success: function(data){
					var a = document.createElement('a');
					var url = window.URL.createObjectURL(data);
					a.href = url;
					a.download = 'envelope template.pdf';
					document.body.append(a);
					a.click();
					a.remove();
					window.URL.revokeObjectURL(url);
				}
			});
		  return false; // Will stop the submission of the form
		}
	});
	
	
});