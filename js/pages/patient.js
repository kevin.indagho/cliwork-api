var dtpatient;
$(document).ready(function(){
	$(".birthday_picker").datetimepicker({format: "MM/DD/YYYY",viewMode: 'years'});

	$('#pictureadd').attr('src',apptools.pathapi+'files/patients/defaultuser.png');
	
	$.ajax({
		url: urlapi+"Api_data/pais",
		type: "GET",
		data: {activo: 1},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,item){
				$('#form_editpatient [name="country"]').append('<option value="'+item.id+'">'+item.nombre+'</option>');
				$('#form_newpatient [name="country"]').append('<option value="'+item.id+'">'+item.nombre+'</option>');
			});
			$('.bs-select').selectpicker('refresh');
		}
	});
	
	$.ajax({
		url: urlapi+"Api_pacienteestados/lista",
		type: "GET",
		data: {activo: 'none'},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,etapa){
				$('#form_editpatient [name="etapa"]').append($('<option>',{
					text: etapa.name,
					value: etapa.id
				}));
				
				$('#form_newpatient [name="etapa"]').append($('<option>',{
					text: etapa.name,
					value: etapa.id
				}));
			});
			$('.bs-select').selectpicker('refresh');
		}
	});
	
	$.ajax({
		url: urlapi+"Api_data/origen",
		type: "GET",
		data: {activo: 1},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,etapa){
				$('#form_newpatient [name="origin"]').append($('<option>',{
					text: etapa.nombre,
					value: etapa.id
				}));
				$('#form_editpatient [name="origin"]').append($('<option>',{
					text: etapa.nombre,
					value: etapa.id
				}));
			});
			$('.bs-select').selectpicker('refresh');
		}
	});
			
	$.validate({
		form : '#form_newpatient',
		modules : 'logic',
		validateOnBlur : true, 
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onError : function($form){
			//apptools.notyCustom('error','fields Required','Complete All inputs');
		},
		onSuccess : function($form){
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			$form.find('button[type="submit"]').prop('disabled',true);
			$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-spinner fa-pulse fa-fw');
			
			var data = new FormData($form[0]);
			
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				enctype: 'multipart/form-data',
				processData: false,
				contentType: false,
				cache: false,
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					$form.find('button[type="submit"]').prop('disabled',false);
					$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-floppy-o');
					if(response.code == 0){
						apptools.sweetNoty('success','Complete','Add new patient');
						$('#form_newpatient').get(0).reset();
						$('#modal_newpatient').modal('hide');
						dtpatient.ajax.reload();
					}else{
						apptools.sweetNoty('error','Error','');
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	$.validate({
		form : '#form_editpatient',
		modules : 'file,logic',
		validateOnBlur : true,
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form) {
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			$form.find('button[type="submit"]').prop('disabled',true);
			$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-spinner fa-pulse fa-fw');
			var data = new FormData($form[0]);
			data.append("userid",apptools.userlogin);
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				enctype: 'multipart/form-data',
				processData: false,
				contentType: false,
				cache: false,
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					$form.find('button[type="submit"]').prop('disabled',false);
					$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-floppy-o');
					if(response.code == 0){
						$('#modal_editpatient').modal('hide');
						dtpatient.ajax.reload();
						apptools.sweetNoty('success','Complete','Update patient information');
						$(this).hasClass('selection').removeClass('selection');
					}else{
						apptools.sweetNoty('error','Error','');
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	dtpatient = $("#dtpatient").DataTable({
		dom: '<"col-md-6"B><"col-md-6"f>rt<"col-md-6 hidden-mobile"i><"col-md-6"p>',
		buttons: [
			{
				text: '<span class="fa fa-plus"></span> Add',
				className: 'btn btn-success btn-sm btn-icon-fixed',
				attr:{
					"data-backdrop": "static",
					"data-toggle": "modal", 
					"data-target": "#modal_newpatient"
					}
			},
			{
				text: '<span class="fa fa-pencil"></span> Edit',
				className: 'btn btn-warning btn-sm btn-icon-fixed',
				attr: { "disabled": "",id: 'patedit' }
				
			},
			{
				text: '<span class="fa fa-trash"></span> Delete',
				className: 'btn btn-danger btn-sm btn-icon-fixed',
				attr: { "disabled": "",id: 'patdel' }
			}
		],
		"ajax": {
			url: urlapi+"Api_paciente/lista",
			type: "get",
			beforeSend: function (xhr){
				xhr.setRequestHeader('Authorization', auth);
			},
			data: {activo: "none"},
		},
		columns: [
			{data: 'nombre'},
			{data: 'Email', class:"hidden-mobile"},
			{data: null, class:"hidden-mobile"},
			{data: null, class:"hidden-mobile"}
		],
		"drawCallback": function( settings ){ $("[data-toggle='tooltip']").tooltip(); },
		"rowCallback": function( row, data ){
			if(data.EtapaId != null){ estado = '<span class="label label-'+data.etapaColor+' label-bordered label-ghost">'+data.etapaName+'</span>'}
			else{ estado = '';}
			$('td:eq(3)',row).html(estado);
			
			if(data.Gender == 1){ genero = 'Female'}
			else if(data.Gender == 2){ genero = "Male";}
			else if(data.Gender == 3){ genero = "Gender-Neutral";}
			else{ genero = '';}
			$('td:eq(2)',row).html(genero);
		}
	});
	
	$('#dtpatient tbody').on( 'click', 'tr', function () {
        if($(this).hasClass('selection')){
            $(this).removeClass('selection');
			$('#patdel').prop('disabled',true);
			$('#patedit').prop('disabled',true);
        }else{
			$('#patdel').prop('disabled',false);
			$('#patedit').prop('disabled',false);
            dtpatient.$('tr.selection').removeClass('selection');
            $(this).addClass('selection');
        }
    });
	
	$('#dtpatient tbody').on('dblclick','tr',function(e){
		dtpatient.$('tr.selection').removeClass('selection');
        $(this).addClass('selection');
		$('#patedit').trigger('click');
	});
	
	$('#form_editpatient [name="foto"]').on('change',function(e){
		let reader = new FileReader();
		reader.readAsDataURL(e.target.files[0]);
		reader.onload = function(){
			let preview = document.getElementById('pictureedit');
			preview.src = reader.result;
		};
	});
	
	$('#form_editpatient').on('click','.details',function(){
		id = $('#form_editpatient [name="id"]').val();
		window.open(apptools.base_url+'patient/perfil/'+id);
	});
	
	$('#patdel').on('click',function(){
		if($('.selection').length == 0){ apptools.notyCustom('information','<i class="fa fa-info"></i> Not Patient','Selected a patient from the list'); }
		else{
			var data = dtpatient.row($('.selection')).data();
			swal({
				title: "Are you sure delete this patient ?",
				text: data.FName +" "+data.LName ,
				type: "warning",
				showConfirmButton: 1,
				showCancelButton: 1,
				html:true,
				dangerMode: true,},
				function(isConfirm){
					if(isConfirm){
						
					}else{
					}
			});
		}
	});
	
	$('#patedit').on('click',function(){
		if($('.selection').length == 0){ apptools.notyCustom('information','<i class="fa fa-info"></i> Not Patient','Selected a patient from the list'); }
		else{
			var data = dtpatient.row($('.selection')).data();
			$('#form_editpatient [name="country"]').val(data.Country).trigger('change');
			$('#linkpreview').attr('data-preview-image',apptools.pathapi+'files/patients/'+(data.foto != null ? data.foto : 'defaultuser.png'));
			$('#pictureedit').attr('src',apptools.pathapi+'files/patients/'+(data.foto != null ? data.Id+"/"+data.foto : 'defaultuser.png'));
			$('#form_editpatient [name="oldpicture"]').val(data.foto);		
			$('#form_editpatient [name="id"]').val(data.Id);
			$('#form_editpatient [name="nombre"]').val(data.FName);
			$('#form_editpatient [name="apellidos"]').val(data.LName);
			$('#form_editpatient [name="correo"]').val(data.Email);
			$('#form_editpatient [name="moviltelefono"]').val(data.WirelessPhone);
			$('#form_editpatient [name="trabajotelefono"]').val(data.WkPhone);
			$('#form_editpatient [name="casatelefono"]').val(data.HmPhone);
			$('#form_editpatient [name="etapa"]').val(data.EtapaId).trigger('change');
			$('#form_editpatient [name="genero"]').val(data.Gender).trigger('change');
			$('#form_editpatient [name="birthdate"]').val(data.BirthdateFormat).trigger('change');
			$('#form_editpatient [name="city"]').val(data.City);
			$('#form_editpatient [name="zip"]').val(data.Zip);
			$('#form_editpatient [name="address"]').val(data.Address);
			setTimeout(function(){
				$('#form_editpatient [name="state"]').val(data.State).trigger('change'); 
			}, 2000);
			if(data.EtapaId == 5){
				$('#tabiLost').show();
				//$('#tabiLost').hide();
				$('#form_editpatient [name="lostdate"]').val(data.PerdidaFecha);
				$('#form_editpatient [name="lostreason"]').val(data.PerdidaRazon);
			}else{
				$('#tabiLostEdit').hide();
			}
			$('#modal_editpatient').modal('show');
		}
	});
	
	$('#form_configpatient [name="nombre"]').on('change',function(){
		var column = dtpatient.column($(this).val());
        column.visible( ! column.visible() );
	});
	
	$('#form_newpatient [name="birthdate"]').on('dp.change',function(){
		var date = $(this).val()
		$.ajax({
			url: urlapi+"Tools/calculate_age",
			type: "POST",
			data: {date: date},
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
			success: function(response){
				$('#age')[0].innerHTML = "Age: "+response;
			}
		});
	});
	
	$('#form_editpatient [name="birthdate"]').on('dp.change',function(){
		var date = $(this).val()
		$.ajax({
			url: urlapi+"Tools/calculate_age",
			type: "POST",
			data: {date: date},
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
			success: function(response){
				$('#ageEdit')[0].innerHTML = "Age: "+response;
			}
		});
	});
	
	$('#form_editpatient [name="country"]').on('change',function(){
		id = $(this).val();
		$('#form_editpatient [name="state"]').empty();
		$('#form_editpatient [name="state"]').append('<option></option>');
		$.ajax({
			url: urlapi+"Api_data/paisestado",
			type: "GET",
			data: {activo: 1,pais: id},
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
			success: function(response){
				$.each(response.data,function(i,item){
					$('#form_editpatient [name="state"]').append('<option value="'+item.id+'">'+item.nombre+'</option>');
				});
				$('.bs-select').selectpicker('refresh');
			}
		});
	});
	
	$('#form_newpatient [name="country"]').on('change',function(){
		id = $(this).val();
		$('#form_newpatient [name="state"]').empty();
		$('#form_newpatient [name="state"]').append('<option></option>');
		$.ajax({
			url: urlapi+"Api_data/paisestado",
			type: "GET",
			data: {activo: 1,pais: id},
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
			success: function(response){
				$.each(response.data,function(i,item){
					$('#form_newpatient [name="state"]').append('<option value="'+item.id+'">'+item.nombre+'</option>');
				});
				$('.bs-select').selectpicker('refresh');
			}
		});
	});
	
	
	/*Export Datatable*/
	$('.hc').on('input',function(){
		var column = dtpatient.column($(this).data('column'));
        column.visible(!column.visible());
	});
	$('#btnEXC').click(function(){ $('#dtpatient').tableExport({type:'excel',escape:'false'}); });
	$('#btnPDF').click(function(){ $('#dtpatient').tableExport({type:'pdf',escape:'false'}); });
});
