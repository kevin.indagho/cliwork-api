var dtactivities;
$(document).ready(function(){
		$.ajax({
		url: urlapi+"Api_reportes/actvencidas",
		type: "GET",
		data: {estado: 2,paciente: 59},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			console.log(response);
		}
	});
	
	
	dtactivities = $("#dtactivities").DataTable({
		"ajax": {
			url: urlapi+"Api_reportes/actvencidas",
			type: "get",
			beforeSend: function (xhr){
				xhr.setRequestHeader('Authorization', auth);
			},
			data: {estado: 2,paciente: 59},
		},
		columns: [
			{
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
			{data: 'activities.0.dias'},
			{data: 'patient'},
			{data: 'treatments[].ing'},
			{data: 'value'},
			{data: 'user'},
			{defaultContent: '<button type="button" class="ver btn btn-info btn-xs"><i class="fa fa-eye"></i></button>'},
		]
	});
	
	$('#dtactivities tbody').on('click', 'td.details-control', function(){
        var tr = $(this).closest('tr');
        var row = dtactivities.row( tr );
        if ( row.child.isShown() ) {
            row.child.hide();
            tr.removeClass('shown');
        } else {
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
	
	$('#dtactivities tbody').on('click','.ver',function(){
		data = dtactivities.row($(this).parents('tr')).data();
		console.log(data);
		window.open(base_url+"deals/details/"+data.id+"/"+data.patientid, '_blank');
		//http://localhost:8080/clienteopendental/deals/details/1/57
	});
	
	
	function detailtdeals(){
		//http://localhost:8080/clienteopendental/deals/details/1/57
	}
	
	function format(d){
		var ul = '';
		$.each(d.activities,function(i,act){
			ul += '<tr><td><div class="alert alert-default" role="alert">'+act.nota+'<p><a class="text-muted margin-right-10 text-danger"><span class="fa fa-calendar"></span> '+act.dias+' Days expired</a> <span class="pull-right text-muted">'+act.fecha+' <i class="fa fa-clock-o"></i> 5 min ago</span></p></div></td></tr>';		});
		
		return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
			'<tr>'+
				'<td>'+ul+'</td>'+
			'</tr>'+
		'</table>';
	}
});