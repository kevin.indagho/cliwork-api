var calendar = null;
var tiempo = "";
resourcesdata = [];

$(document).ready(function(){
	//window.location.assign("appurl:lastname:Cerda/firstname:Kevin/id:90");
	$("#editappproce").mCustomScrollbar({axis:"y", autoHideScrollbar: true, scrollInertia: 200, advanced: {autoScrollOnFocus: false}});
	
	$('#form_newappointment [name="patient[id]"]').selectpicker({liveSearch: true}).ajaxSelectPicker({
        ajax : {
			url: urlapi+"Api_data/patsearch",
			type: "GET",
			// Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
			// automatically replace it with the value of the search query.
			data : { name: '{{{q}}}' },
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); }
        },
        locale : { emptyTitle: 'Nothing selected' },
        log : 3,
        preprocessData: function (response){
            var l = response.data.length, array = [];			
            if (l){
				$.each(response.data,function(i,item){
					array.push($.extend(true,i,{
						text : item.FName+" "+item.LName,
						value: item.Id,
						//data : { subtext: "Sub Text" }
					}));
				});
            }
            // You must always return a valid array when processing data. The
            // data argument passed is a clone and cannot be modified directly.
            return array;
        }
    });
	
	$('#form_newappointment [name="reason"]').selectpicker({liveSearch: true}).ajaxSelectPicker({
        ajax : {
			url: urlapi+"Api_data/proceduressearch",
			type: "GET",
			// Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
			// automatically replace it with the value of the search query.
			data : {limit:20, by:"Descript", name: '{{{q}}}' },
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); }
        },
        locale : { emptyTitle: 'Nothing selected' },
        log : 3,
        preprocessData: function (response){
            var l = response.data.length, array = [];			
            if (l){
				$.each(response.data,function(i,item){
					array.push($.extend(true,i,{
						text : item.Descript,
						value: item.id,
						data : { amount: item.Amount  }
					}));
				});
            }
            return array;
        }
    });
	
	$('#form_editappointment [name="reason"]').selectpicker({liveSearch: true}).ajaxSelectPicker({
        ajax : {
			url: urlapi+"Api_data/proceduressearch",
			type: "GET",
			// Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
			// automatically replace it with the value of the search query.
			data : {limit:20, by:"Descript", name: '{{{q}}}' },
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); }
        },
        locale : { emptyTitle: 'Nothing selected' },
        log : 3,
        preprocessData: function (response){
            var l = response.data.length, array = [];			
            if (l){
				$.each(response.data,function(i,item){
					array.push($.extend(true,i,{
						text : item.Descript,
						value: item.id,
						data : { amount: item.Amount  }
					}));
				});
            }
            return array;
        }
    });
	
	$('#form_editappointment [name="reason"]').on('changed.bs.select',function(){
		ul = $('#listProcedures li');
		amount = $(this)[0].selectedOptions[0].dataset.amount;
		text = $(this)[0].selectedOptions[0].label;
		val = $(this).val();
		if(val != ''){
			$('#listProcedures').append('<li class="list-group-item"><label>'
						+'<input type="checkbox" name="reason[]" value="'+val+'" checked="checked"> '+text+'</label></li>');
		}
		$('#form_editappointment [name="reason"]').val('default').selectpicker("refresh");
	});
	
	$('#form_newappointment [name="reason"]').on('changed.bs.select',function(){
		ul = $('#listProceduresadd li');
		amount = $(this)[0].selectedOptions[0].dataset.amount;
		text = $(this)[0].selectedOptions[0].label;
		val = $(this).val();
		if(val != ''){
			$('#listProceduresadd').append('<li class="list-group-item"><label>'
						+'<input type="checkbox" name="reason[]" value="'+val+'" checked="checked"> '+text+'</label></li>');
		}
		$('#form_newappointment [name="reason"]').val('default').selectpicker("refresh");
	});
	
	upt_appt_box();
	
	/*$.ajax({
		url: urlapi+"Api_data/procereasonvisit",
		type: "GET",
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,item){
				$('#form_newappointment [name="reason"]').append($('<option>',{text: item.Descript,value: item.id}));
				$('#form_editappointment [name="reason"]').append($('<option>',{text: item.Descript,value: item.id}));
			});
			$('.bs-select').val('default').selectpicker('refresh');
		}
	});*/
	
	$.ajax({
		url: urlapi+"Api_data/appstatus",
		type: "GET",
		data: {activo: 1},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,item){
				$('#form_newappointment [name="status"]').append($('<option>',{text: item.nombre,value: item.id}));
				$('#form_editappointment [name="status"]').append($('<option>',{text: item.nombre,value: item.id}));
			});
			$('.bs-select').val('default').selectpicker('refresh');
		}
	});	
		
	$.ajax({
		url: urlapi+"Api_data/providers",
		type: "GET",
		data: {IsHidden: 0,specialty: "none"},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,item){
				horario = item.horario.split('-');
				resourcesdata.push({
					id: item.id,
					eventColor: item.Color,
					businessHours: { 
						start: horario[0],
						end: horario[1]
					},
					title: item.FName+" "+item.LName
				});
				$('#form_newappointment [name="doctor"]').append($('<option>',{text: item.FName+" "+item.LName,value: item.id}));
				$('#form_editappointment [name="doctor"]').append($('<option>',{text: item.FName+" "+item.LName,value: item.id}));
			});
			$('.bs-select').val('default').selectpicker('refresh');
			initCalendar();
		}
	});
	
	$.ajax({
		url: urlapi+"Api_data/providers",
		type: "GET",
		data: {IsHidden: 0,specialty: 266},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,item){
				$('#form_newappointment [name="hygienis"]').append($('<option>',{text: item.FName+" "+item.LName,value: item.id}));
				$('#form_editappointment [name="hygienis"]').append($('<option>',{text: item.FName+" "+item.LName,value: item.id}));
			});
			$('.bs-select').val('default').selectpicker('refresh');
		}
	});
	
	$.ajax({
		url: urlapi+"Api_data/assistant",
		type: "GET",
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,item){
				$('#form_newappointment [name="assistant[]"]').append($('<option>',{text: item.FName+" "+item.LName,value: item.id}));
				$('#form_editappointment [name="assistant[]"]').append($('<option>',{text: item.FName+" "+item.LName,value: item.id}));
			});
			$('.bs-select').val('default').selectpicker('refresh');
		}
	});
	
	$.ajax({
		url: urlapi+"Api_paciente/ofertasvendidas",
		type: "GET",
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,item){
				console.log(item);
			});
		}
	});	

	$.validate({
		form : '#form_newappointment',
		modules : 'logic',
		validateOnBlur : true, 
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form){
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			//$form.find('button[type="submit"]').prop('disabled',true);
			$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-spinner fa-pulse fa-fw');
			
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					//$form.find('button[type="submit"]').prop('disabled',false);
					$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-floppy-o');
					if(response.code == 0){
						apptools.notysuccess();
						$('#form_newappointment').get(0).reset();
						$('#modal_newappointment').modal('hide');
						calendar.fullCalendar('refetchEvents');
					}else{
						apptools.notyerror();
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	$.validate({
		form : '#form_editappointment',
		modules : 'logic',
		validateOnBlur : true, 
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form){
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			//$form.find('button[type="submit"]').prop('disabled',true);
			$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-spinner fa-pulse fa-fw');
			var data = $form.serializeArray();
			time = apptools.clinicTime[0]+"-"+apptools.clinicTime[1];
			data.push({name: 'timeAllDay', value: time});
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					//$form.find('button[type="submit"]').prop('disabled',false);
					$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-floppy-o');
					if(response.code == 0){
						apptools.notysuccess();
						$('#modal_editappointment').modal('hide');
						unschedlist = $('#form_editappointment [name="status"]').val();
						$('#form_editappointment').get(0).reset();
						if(unschedlist == 4){
							upt_appt_box();
						}
						calendar.fullCalendar('refetchEvents');
					}else{
						apptools.notyerror();
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	$.validate({
		form : '#form_appproce',
		modules : 'logic',
		validateOnBlur : true, 
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form){
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			//$form.find('button[type="submit"]').prop('disabled',true);
			$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-spinner fa-pulse fa-fw');
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					//$form.find('button[type="submit"]').prop('disabled',false);
					$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-floppy-o');
					if(response.code == 0){
						apptools.notysuccess();
						$('#modal_editappointment').modal('hide');
						calendar.fullCalendar('refetchEvents');
					}else{
						apptools.notyerror();
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});

	$.validate({
		form : '#form_edit_flight',
		modules : 'logic',
		validateOnBlur : true, 
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form){
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			//$form.find('button[type="submit"]').prop('disabled',true);
			$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-spinner fa-pulse fa-fw');
			var data = new FormData($form[0]);
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				enctype: 'multipart/form-data',
				processData: false,
				contentType: false,
				cache: false,
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					//$form.find('button[type="submit"]').prop('disabled',false);
					$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-floppy-o');
					if(response.code == 0){
						apptools.notysuccess();
						$('#modal_editappointment').modal('hide');
						calendar.fullCalendar('refetchEvents');
					}else{
						apptools.notyerror();
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	/*$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	  var target = $(e.target).attr("href")
	  if(target == '#tabaappt'){
		$('#modal_newappointment .modal-dialog').addClass('modal-fw');
	  }else{
		$('#modal_newappointment .modal-dialog').removeClass('modal-fw');  
	  }
	  
	  if(target == '#tabaappte'){
		$('#modal_editappointment .modal-dialog').addClass('modal-fw');
	  }else{
		$('#modal_editappointment .modal-dialog').removeClass('modal-fw');  
	  }
	});*/
	
	$('#form_editappointment [name="status"]').on('changed.bs.select',function(){
		val = $(this).val();
		if(val == 4){
			$('#unschedlistnoty').show();
		}else{
			$('#unschedlistnoty').hide();
		}
	});
	
	$('#form_newappointment [name="patient[id]"]').on('changed.bs.select',function(){
		val = $(this).val();
		if(val != ''){
			$.ajax({
				url: urlapi+"Api_paciente/detalle",
				type: "GET",
				data : { id: val},
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					$('#form_newappointment [name="patient[nombre]"]').val(response.data[0].FName);
					$('#form_newappointment [name="patient[apellidos]"]').val(response.data[0].LName);
					$('#form_newappointment [name="patient[apellidos]"]').val(response.data[0].LName);
					$('#form_newappointment [name="patient[correo]"]').val(response.data[0].Email);
					$('#form_newappointment [name="patient[casatelefono]"]').val(response.data[0].HmPhone);
					$('#form_newappointment [name="patient[moviltelefono]"]').val(response.data[0].WirelessPhone);
					$('#form_newappointment [name="patient[trabajotelefono]"]').val(response.data[0].WkPhone);
				}
			});
			
			$.ajax({
				url: urlapi+"Api_paciente/procedimientos",
				type: "GET",
				data : { id: val},
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					$('#tp_appt_proce_add tbody').empty();
					$.each(response.data,function(i,item){
					$('#tp_appt_proce_add tbody').append('<tr>'
						+'<td class="padding-5 padding-top-0 padding-bottom-0">'+item.Duration+'</td>'
						+'<td class="padding-5 padding-top-0 padding-bottom-0">'
							+'<input style="display:none;" type="checkbox" name="procedures[]" value="'+item.Id+'">'
							+item.ProcCode+'</td>'
						+'<td class="padding-5 padding-top-0 padding-bottom-0">'+item.Descript+'</td>'
						+'</tr>');
					});
				}
			});
		}
	});
	
	$('#form_editappointment [name="needs"]').on('change',function(){
		check = $(this)[0].checked;
		if(check == true){
			$('#form_editappointment [name="notes"]').attr('data-validation','required');
		}else{
			$('#form_editappointment [name="notes"]').removeAttr('data-validation');
		}
	});
	
	$('#form_editappointment [name="allday"]').on('change',function(){
		/*console.log($(this));
		console.log(apptools.clinicTime[0]);
		console.log(apptools.clinicTime[1]);*/
	});
	
	$('#form_newappointment [name="new"]').on('change',function(){
		$('#form_newappointment [name="patient[id]"]').prop('disabled',$(this)[0].checked);
	});
	
	$('#listProcedures').on('click','a',function(){
		console.log($(this));
		$(this).remove()
	});
	
	$('#tp_appt_proce_add tbody').on('click','tr',function(){
		$('#form_newappointment [name="procupd"]').prop('checked',true);
		$(this).toggleClass('selection');
		InputCheck = $(this).find('input[type="checkbox"]');
		if(InputCheck[0].checked == false){
			InputCheck.prop('checked',true);
		}else{
			InputCheck.prop('checked',false);
		}
    });
	
	$('#tp_appt_proce_edit_del tbody').on('click','tr',function(){
		$('#form_editappointment [name="procupd"]').prop('checked',true);
		$(this).toggleClass('selection-delete');
		InputCheck = $(this).find('input[type="checkbox"]');
		if(InputCheck[0].checked == false){
			InputCheck.prop('checked',true);
		}else{
			InputCheck.prop('checked',false);
		}
    });
	
	$('#tp_appt_proce_edit tbody').on('click','tr',function(){
		if($(this).hasClass('disabled') == false){
			$('#form_editappointment [name="procupd"]').prop('checked',true);
			$(this).toggleClass('selection');
			InputCheck = $(this).find('input[type="checkbox"]');
			if(InputCheck[0].checked == false){
				InputCheck.prop('checked',true);
			}else{
				InputCheck.prop('checked',false);
			}
			calculate_total_apptedit();
		}
    });
	
	$('#cancelAppt').on('click',function(){
		swal({
			title: "Are you sure cancel this appointment ?",
			text: "",
			type: "warning",
			showConfirmButton: 1,
			showCancelButton: 1,
			html:true,
			dangerMode: true,},
			function(isConfirm){
				if(isConfirm){
					id = $('#form_editappointment [name="id"]').val();
					$.ajax({
						url: urlapi+"Api_cita/citas/"+id,
						type: "patch",
						data: {active: 0, },
						dataType: "json",
						beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
						success: function(response){
							if(response.code == 0){
								apptools.sweetNoty('success','Complete','appointment cancel');
								calendar.fullCalendar('refetchEvents');
							}else{
								apptools.sweetNoty('error','Error','');
							}
						}
					});
				}else{ }
		});
	});
	
	$('#backdeal').on('click',function(){
		swal({
		  title: "Regresar a ventas",
		  text: "La cita se eliminara y se regresara a ventas",
		  type: "warning",
		  showConfirmButton: 1,
		  showCancelButton: 1,
		  dangerMode: true,},
		  function(isConfirm){
			if(isConfirm){
				let id = $('#form_editappointment [name="id"]').val();
				$.ajax({
					url: urlapi+"Api_cita/aptback",
					type: "POST",
					dataType: "json",
					data: {aptid: id},
					beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
					success: function(response){
						console.log(response);
						calendar.fullCalendar('refetchEvents');
					}
				});
			}
		  });
	});
});

function deletereazon(id){
	$('#addli'+id).remove();
}

function calculate_total_apptedit(){
	total = 0;
	rows = $('#tp_appt_proce_edit .selection');
	$.each(rows,function(i,row){
		total += $(row).data('fee');
	});
	tfoot = $('#tp_appt_proce_edit tfoot');
	th = tfoot[0].children[0].children[1];
	$(th)[0].innerHTML = total;
	
}

function upt_appt_box(){
	$.ajax({
	  url: urlapi+"Api_cita/calendariobox",
	  Type:"GET",
	  dataType:"json",
	  beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
	  success: function(response){
		  $('#external-events').empty();
		  $.each(response.data,function(i,item){
			var element = document.createElement('a');
			$(element).addClass('list-group-item external-event ui-draggable ui-draggable-handle');
			$(element)[0].innerHTML = ''
			+ (item.Sedation == 1 ? '<i><img style="margin-right: 2px;" src="http://localhost:8080/clienteopendental/img/icons/sleep.png"></i>' : '')
			+ (item.Vuelo != null ? '<i style="font-size: small;" class="fa fa-plane text-purple-500" aria-hidden="true"></i>' : '')
			+ (item.IsNewPatient == 1 ? '<i style="font-size: small;" class="fa fa-star text-yellow-600" aria-hidden="true"></i>' : '')
			+ (item.Confirmed == 1 ? '<i style="font-size: small;" class="fa fa-check text-green-400" aria-hidden="true"></i>' : '<i style="font-size: small;" class="fa fa-times text-orange-500" aria-hidden="true"></i>')
			+ (item.Note != '' ? '<i class="text-danger fa fa-sticky-note" aria-hidden="true"></i>' : '')
			/*+ (item.Needs == 1 ? '<i class="text-warning fa fa-exclamation-triangle" aria-hidden="true"></i>' : '')*/
			+"<br>"+item.PatName;
			
			//.find('.popinfo')
			$(element).popover({
              animation:true,
			  container: "body",
			 // placement: "top",
			  html: true,
              content: item.description,
              trigger: 'hover'
			});
			$('#external-events').append(element);
			$(element).data('eventObject',item);
			$(element).draggable({
				zIndex: 999,
				revert: true,
				revertDuration: 0,
				start: function(){ $(this).popover('hide'); },
				drag: function(){ $(this).popover('hide'); },
				stop: function(){ $(this).popover('hide'); }
			});
		  });
	  }
	});
}

function initCalendar(){
	calendar = $('#calendar').fullCalendar({
		defaultView: 'agendaFourDay',
		customButtons: {
			refresh: {
			  text: '<i class="fa fa-refresh" aria-hidden="true"></i>',
			  icon: 'fa fa-refresh',
			  click: function(){ calendar.fullCalendar('refetchEvents'); }
			}
		},
		header:{
			left: 'prev,next refresh',
			center: 'title',
			right: 'month,agendaFourDay'
		},
		minTime: apptools.clinicTime[0],
		maxTime: apptools.clinicTime[1],
		axisFormat: 'LT',
		timeFormat: 'LT',
		slotDuration: '00:10:00',
		slotLabelInterval : "00:10:00",
		slotLabelFormat : "h:mm a",
		allDaySlot: true,
		allDayText: "Plan Appointment",
		editable: true,
		droppable: true,
		selectable: true,
		selectHelper: false,
		views: {
			agendaFourDay: {
			  type: 'agenda',
			  groupByResource: true,//// uncomment this line to group by day FIRST with resources underneath
			  //groupByDateAndResource: true
			  duration: { days: 1 }
			}
		  },
		resources: resourcesdata,
		eventAfterRender: function(event, element){
			element.popover({
				placement: 'right',
				html: true,
				trigger: 'manual',
				title: "MANUAL",
				content: 'd',
				container : "body",
				//template: '<div class="panel panel-primary popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
			});			
			
			/*element.mousedown(function(){
				element.toggleClass('animated pulse');
				//cursor: grabbing;
			});*/
			
			if(event.Confirmed == 0){
				squareColor = 'fa-times text-orange-500';
			}else{
				squareColor = 'fa-check text-green-400';
			}
			element.find('.fc-time').prepend('<i style="font-size: small;" class="fa '+squareColor+'" aria-hidden="true"></i> <i style="font-size: small;" class="text-info popinfo fa fa-info-circle" aria-hidden="true"></i> ');

			if(event.IsNewPatient == 1){
				element.find('.fc-time').prepend('<i style="font-size: small;" class="fa fa-star text-yellow-100" aria-hidden="true"></i>');
			}
			
			if(event.Vuelo != "" && event.Vuelo != null ){ element.find('.fc-time').prepend('<i style="font-size: small;" class="fa fa-plane text-purple-500" aria-hidden="true"></i>'); }
			if(event.Sedation == 1){ element.find('.fc-time').prepend('<img style="margin-right: 2px;" src="'+base_url+'img/icons/sleep.png"></img>'); }
			
			element.find('.popinfo').popover({
              animation:true,
			  container: "body",
			 // placement: "top",
			  html: true,
              content: event.description,
              trigger: 'hover'
			});
		  
			//$('[data-toggle="tooltip"]').tooltip({title: "Titulo",container: "body"}); 
		},
		eventAfterAllRender: function(){ $('[data-toggle="tooltip"]').tooltip({container: "body"}); },
		dayClick: function(date, allDay, jsEvent, view){
			if(allDay){
				date = date.format('YYYY-MM-DD');
				$('#calendar').fullCalendar('changeView','agendaFourDay')
				$('#calendar').fullCalendar('gotoDate',date);
			}
		},
		eventClick: function(info){
			if(info.allDay == false){
				date = info.start.format("dddd, MMMM Do YYYY");
				Start = info.start.format("h:mm:ss a");
				End = info.end.format("h:mm:ss a");
			}else{
				date = info.start.format("dddd, MMMM Do YYYY");
				Start = "";
				End = "";
			}
			$('#form_editappointment [name="date"]').val(date);
			$('#form_editappointment [name="time"]').val(Start+" - "+End);
			$.ajax({
				url: urlapi+"Api_paciente/detalle",
				type: "GET",
				data: {id: info.Patid},
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					$('#form_patappt [name="patient[nombre]"]').val(response.data[0].FName+" "+response.data[0].LName);
					$('#form_patappt [name="patient[correo]"]').val(response.data[0].Email);
					$('#form_patappt [name="patient[casatelefono]"]').val(response.data[0].HmPhone);
					$('#form_patappt [name="patient[trabajotelefono]"]').val(response.data[0].WkPhone);
					$('#form_patappt [name="patient[moviltelefono]"]').val(response.data[0].WirelessPhone);
				}
			});
			
			$.ajax({
				url: urlapi+"Api_cita/citas",
				type: "GET",
				data: {id: info.id},
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					cita = response.data[0];
					console.log(cita);
					$('#itinerarioPreview').attr('data-preview-image',apptools.pathapi+'files/patients/'+cita.PatId+'/'+cita.VueloDocu);
					$('#form_appproce [name="id"]').val(cita.id);
					$('#form_editappointment [name="id"]').val(cita.id);
					$('#form_editappointment [name="status"]').val(cita.AptStatus).selectpicker('refresh');
					$('#form_editappointment [name="new"][value="'+cita.IsNewPatient+'"]').prop('checked',true);
					$('#form_editappointment [name="needs"][value="'+cita.Needs+'"]').prop('checked',true);
					$('#form_editappointment [name="alldaytime"][value="'+cita.all_Day+'"]').prop('checked',true);
					$('#form_editappointment [name="confirmed"][value="1"]').prop('checked',parseInt(cita.Confirmed));
					$('#form_editappointment [name="sedation"][value="1"]').prop('checked',parseInt(cita.Sedation));
					$('#form_editappointment [name="sedationnote"]').val(cita.SedationNote);
					$('#form_editappointment [name="notes"]').val(cita.Note);
					//$('#form_editappointment [name="reason"]').val(cita.reason);
					$('#form_editappointment [name="doctor"]').val(cita.ProvId);
					$('#form_editappointment [name="hygienis"]').val(cita.ProvHyg);
					
					$('#form_edit_flight [name="id"]').val(cita.id);
					$('#form_edit_flight [name="patient"]').val(cita.PatId);
					$('#form_edit_flight [name="flight"]').val(cita.Vuelo);
					$('#form_edit_flight [name="date"]').val(cita.VueloFecha);
					$('#form_edit_flight [name="horas"]').val(cita.VueloSalida);
					$('#form_edit_flight [name="horal"]').val(cita.VueloLlegada);
					$('#form_edit_flight [name="note"]').val(cita.VueloNotas);
					$('#tp_appt_proce_edit tbody').empty();
					$.each(cita.services,function(i,item){
						if(item.AptNum == null || item.AptNum == "" || item.AptNum == cita.id)
						$('#tp_appt_proce_edit tbody').append('<tr '+(info.id == item.AptNum ? 'class="selection"': '')+' data-fee="'+item.ProcFee+'">'
						+'<td class="padding-5 padding-top-0 padding-bottom-0">'+item.Duration+'</td>'
						+'<td class="padding-5 padding-top-0 padding-bottom-0">'
							+'<input style="display:none;" type="checkbox" name="procedures[]" value="'+item.Id+'" '+(info.id == item.AptNum ? 'checked': '')+'>'
							+item.ProcCode+'</td>'
						+'<td class="padding-5 padding-top-0 padding-bottom-0">'+item.Descript+'</td>'
						+'<td class="padding-5 padding-top-0 padding-bottom-0 text-right">'+item.ProcFeeFormat+'</td>'
						+'</tr>');
					});
					
					$('#listProcedures').empty();
					$.each(cita.reasonproc,function(i,item){
						$('#listProcedures').append('<li class="list-group-item"><label>'
						+'<input type="checkbox" name="reason[]" value="'+item.id+'" checked="checked"> '+item.Descript+'</label></li>');
					});
					
					calculate_total_apptedit();
				},
				complete : function(){
					$('.bs-select').selectpicker('refresh');
					$("#listProceduresScrol").mCustomScrollbar('destroy');
					$("#listProceduresScrol").mCustomScrollbar({axis:"y", autoHideScrollbar: true, scrollInertia: 200, advanced: {autoScrollOnFocus: false}});
					$('#modal_editappointment').modal('show');
				}
			});
		},
		select: function (start, end, allDay,view,jsEvent){
			if(view.name == "agendaFourDay"){
				date = start.format("dddd, MMMM Do YYYY");
				Start = start.format("h:mm:ss a");
				End = end.format("h:mm:ss a");
				$('#form_newappointment [name="doctor"]').val(jsEvent.id).trigger('change');;
				$('#form_newappointment [name="date"]').val(date);
				$('#form_newappointment [name="time"]').val(Start+" - "+End);
				$('#modal_newappointment').modal();
			}
		},
		drop: function (date, jsEvent, ui, resourceId ){
			/*cita inBox se pasa al calendario*/
			var newResource = $('#calendar').fullCalendar('getResourceById',resourceId);
			//CUANDO SE AGREGA UN ELEMENTO AL CALENDARIO
			var originalEventObject = $(this).data('eventObject');
			var copiedEventObject = $.extend({}, originalEventObject);
			copiedEventObject.start = date.format('YYYY-MM-DD HH:mm');
			copiedEventObject.resourceId = resourceId;
			copiedEventObject.end = date.add(copiedEventObject.tiempo.minutos,'minutes').format('YYYY-MM-DD HH:mm');
			swal({
				title: "Are you sure about this change?",
				text: copiedEventObject.title+ "<br>"+copiedEventObject.start+"<br> <b>Doctor:</b> "+newResource.title,
				type: "warning",
				showConfirmButton: 1,
				showCancelButton: 1,
				html:true,
				dangerMode: true,},
				function(isConfirm){
					if(isConfirm){
						$.ajax({
							url: urlapi+"Api_cita/citas/"+originalEventObject.id,
							type: "PATCH",
							data: {
								"resourceId":resourceId,
								"status":2,
								"pattern":originalEventObject.Pattern,
								"date":copiedEventObject.start},
							dataType: "json",
							beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
							success: function(response){
								if(response.code == 0){
									upt_appt_box();
									apptools.notyCustom('success','<i class="fa fa-check"></i> Success','Appointment time change')
									calendar.fullCalendar('refetchEvents');
								}else{
									apptools.notyCustom('error','<i class="fa fa-times"></i> Error',response.message)
								}
							}
						});
					}else{
						/*$(this).remove();
						var element = document.createElement('a');
						$(element).addClass('list-group-item external-event ui-draggable ui-draggable-handle');
						$(element)[0].innerHTML = '<i style="font-size: small;" class="text-info popinfo fa fa-info-circle" aria-hidden="true"></i> '+originalEventObject.PatName+'<span class="label label-success pull-right">New</span>';
						$(element).popover({
						  animation:true,
						  container: "body",
						 // placement: "top",
						  html: true,
						  content: originalEventObject.description,
						  trigger: 'hover'
						});
						
						$('#external-events').append(element);			
						$(element).data('eventObject',originalEventObject);
						$(element).draggable({
							zIndex: 999,
							revert: true,
							revertDuration: 0,
							start: function(){ $(this).popover('hide'); },
							drag: function(){ $(this).popover('hide'); },
							stop: function(){ $(this).popover('hide'); }
						});*/
					}
			});
			
			if ($('#drop-remove').is(':checked')) {
				$(this).remove();
			}
		},
		eventDrop: function( event, delta, revertFunc, jsEvent, ui, view ){
			var newResource = $('#calendar').fullCalendar('getResourceById', event.resourceId);
			swal({
				title: "Are you sure about this change?",
				text: event.title+" "+event.start.format("YYYY-MM-DD hh:mm a")+" <br> <b>Doctor:</b> "+newResource.title,
				type: "warning",
				showConfirmButton: 1,
				showCancelButton: 1,
				html:true,
				dangerMode: true,},
				function(isConfirm){
					if(isConfirm){
						date = event.start.format("YYYY-MM-DD HH:mm");
						$.ajax({
							url: urlapi+"Api_cita/citas/"+event.id,
							type: "PATCH",
							data: {
								"resourceId" : newResource.id, 
								"planapt": event.allDay,
								"date": date},
							dataType: "json",
							beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
							success: function(response){
								if(response.code == 0){
									apptools.notyCustom('success','<i class="fa fa-check"></i> Success','Appointment time change')
									calendar.fullCalendar('refetchEvents');
								}else{
									apptools.notyCustom('error','<i class="fa fa-times"></i> Error',response.message)
								}
							}
						});
					}else{ revertFunc(); }
			});
		},
		eventResize: function(event, delta, revertFunc){
			hourStart = moment(event.start).format('YYYY-MM-DD hh:mm a');
			hourEnd = moment(event.end).format('YYYY-MM-DD H:mm');
			$.ajax({
				url: urlapi+"Api_cita/citas/"+event.id,
				type: "PATCH",
				data: {"apptdateEnd": hourEnd},
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.code == 0){
						apptools.notyCustom('information','<i class="fa fa-check"></i> Success','Appointment time change')
					}else{
						apptools.notyCustom('error','<i class="fa fa-times"></i> Error',response.message)
					}
				}
			});
	  },
		events: function(start, end, timezone, callback){
			start = start.format('YYYY-MM-DD')
			end = end.format('YYYY-MM-DD');
			$.ajax({
			  url: urlapi+"Api_cita/calendario",
			  Type:"GET",
			  data: {dateStart: start, dateEnd: end,"status":[2,7],inbox: 0 },
			  dataType:"json",
			  beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
			  success: function(response){ callback(response.data); } 
			});
		  }
	  /*,events: [
        { id: '1', resourceId: '4', start: '2019-03-01', end: '2018-03-02', title: 'event 1' },
        { id: '2', resourceId: '4', start: '2019-03-01 09:00:00', end: '2019-03-01 10:00:00',  title: 'event 2' },
      ]*/
    });
}