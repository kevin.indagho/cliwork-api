var patientid = $('#patientid').val();
var dt_treatplans,selecttp;
var dt_account_data = {patient:patientid,treatplan: selecttp,status: "all"};
var dt_notes_data = {id: patientid};

Dropzone.options.formaddfile = {
	url: urlapi+"Api_paciente/agregararchivos",
	addRemoveLinks : true,
	paramName: "file", 
	maxFilesize: 2, // MB
	headers: {'Authorization': auth},
	acceptedFiles: ".jpg,.png,application/pdf",
	params: {
		"patient": patientid,
		"user": apptools.userlogin,
		//"ofertaid": ofertaid
	},
	accept: function(file, done){
		done();
	},
	sending: function(file, xhr, formData){
        //formData.append('etapaId', EtapaId);
    },
	success: function(file, message) {
		if(message.code == 1){
			$(file.previewElement).addClass("dz-error").find('.dz-error-message').text(message.error);
		}else{
			$(file.previewElement).addClass("dz-success");
		}
		notyCustom(message.type,message.title,message.error);
		dt_documents.ajax.reload();
		actividades();
		
	},error: function(file, message) {
		$(file.previewElement).addClass("dz-error").find('.dz-error-message').text(message.error);
	}
};

$(document).ready(function(){
	actividades();
	
	$.ajax({
		url: urlapi+"Api_paciente/detalle",
		type: "GET",
		data: {id: patientid},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			pat = response.data;
			if(pat[0].foto != '' && pat[0].foto != null){
				$('#ProfileImage').prop('src',pathapi+'files/patients/'+patientid+"/"+pat[0].foto);
			}			
			$('#FullName').html(pat[0].FName+" "+pat[0].LName);
			$('#firstname').val(pat[0].FName);
			$('#lastname').val(pat[0].LName);
			$('#email').val(pat[0].Email);
			$('#hphone').val(pat[0].HmPhone);
			$('#wphone').val(pat[0].WkPhone);
			$('#mphone').val(pat[0].WirelessPhone);
		}
	});
	
	$.ajax({
		url: urlapi+"Api_paciente/pdetails",
		type: "GET",
		data: {id: patientid},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$('#noteCount').html(response.notes);
			$('#visitsCount').html(response.visits);
			if(response.lastappt.length > 0){
				$('#LastAppt').html('<i class="fa fa-calendar"></i> Last Appt: '+response.lastappt[0].fecha);
			}
		}
	});
	
	$.ajax({
		url: urlapi+"Api_paciente/nextapptlimit",
		type: "GET",
		data: {pat: patientid,limit: "3"},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response,function(i,item){
				if(i == 1){
					$('#NextAppt').html('Next Appt: '+item.date);
				}
				$('#widgetAppt').append('<li>'
						+'<div class="app-widget-tile">'
							+'<div class="line">'
								+'<div class="title"><span class="label label-info label-ghost label-bordered">'+(item.duration.h > 0 ? item.duration.h+' Hr':'')+' '+(item.duration.i > 0 ? item.duration.i+' Min':'')+'</span></div>'
								+'<span class="pull-right badge badge-info">'+(i+1)+'</span>'
							+'</div>'
							+'<div class="row"><div class="col-sm-4"><div class="icon icon-lg"><span class="fa fa-calendar"></span></div></div>'
								+'<div class="col-sm-8"><div class="line"><div class="title">Appointment</div></div>'
									+'<div class="intval text-left">'+item.date+'</div>'
								+'</div>'
							+'</div>'
							+'<div class="line scrollCustom" style="height:250px;">'
							+ '<h4 class="text-uppercase text-bold text-rg heading-line-middle">&nbsp;<span>Balance</span></h4>'
							+ '<b class="text-success">$ '+item.total+'</b>'
							+ '<h4 class="text-uppercase text-bold text-rg heading-line-middle">&nbsp;<span>Notes</span></h4>'
							+ '<p>'+item.Note+'</p>'
							+ '<h4 class="text-uppercase text-bold text-rg heading-line-middle">&nbsp;<span>Procedures</span></h4>'
							+ item.procedures
							+'</div>'
						+'</div>'
					+'</li>');
			});
			if(response.length > 0){
				$('#aptdisplay').remove();
			}				
		},
		complete : function(){
			$(".scrollCustom").mCustomScrollbar({axis:"y", autoHideScrollbar: true, scrollInertia: 200, advanced: {autoScrollOnFocus: false}});
		}
	});
	
	dt_documents = $("#dt_documents").DataTable({
		"bStateSave": false,
		"ajax":{
			url: urlapi+"Api_data/documentslist",
			Type:"GET",
			dataType:"json",
			data:{patient: patientid},
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		},
		dom: '<"col-md-6"B><"col-md-6"f>rt<"col-md-6 hidden-mobile"i><"col-md-6"p>',
		buttons: [
			{
				text: '<span class="fa fa-plus"></span> New Document',
				className: 'btn btn-success btn-sm btn-icon-fixed',
				attr:{
					"data-backdrop": "static",
					"data-toggle": "modal", 
					"data-target": "#modal_newdocument"
					}
			}
		],
		columns:[
			{data: 'fecha'},
			{data: 'usuario'},
			{data: 'nombre'},
			{defaultContent: '<button class="open btn btn-xs btn-info"><i class="fa fa-eye"></i></button>',width:"15px"},
		]
	});

	dt_payments = $("#dt_payments").DataTable({
		"bStateSave": false,
		"ajax":{
			url: urlapi+"Api_paciente/payment",
			Type:"GET",
			dataType:"json",
			data:{patient: patientid},
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		},
		columns:[
			{data: 'fechaFormato'},
			{data: 'provider'},
			{data: 'ItemName'},
			{data: 'montoFormato'},
		],
		"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data; 
            // Remove the formatting to get integer data for summation
            var intVal = function(i){
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api.column(3).data().reduce(function(a,b){return intVal(a) + intVal(b);},0);
            $(api.column(3).footer()).html(total);
        }
	});
	
	dt_treatplans = $("#dt_treatplans").DataTable({
		"bStateSave": false,
		"ajax": {
			url: urlapi+"Api_treatplan/treatplan",
			Type:"GET",
			dataType:"json",
			data:{patient: patientid},
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		},
		dom: '<"col-md-6"B><"col-md-6"f>rt<"col-md-6 hidden-mobile"i><"col-md-6"p>',
		buttons: [
			{
				text: '<span class="fa fa-plus"></span> New Treatplan',
				className: 'btn btn-success btn-sm btn-icon-fixed',
				attr:{
					"data-backdrop": "static",
					"data-toggle": "modal", 
					"data-target": "#modal_newtreatplan"
					}
			},
			{
				text: '<span class="fa fa-pencil"></span> Edit',
				className: 'btn btn-warning btn-sm btn-icon-fixed',
				attr: { "disabled": "",id: 'treatedit' }
				
			}
		],
		columns: [
			{data: 'DateTP'},
			{data: 'Heading'},
			{data: 'SubtotalFormat'},
			{data: 'adjusmentsFormat'},
			{data: 'totalFormat'},
			{data: 'TPStatus',width:"15px"},
			{data: 'Complete',width:"15px"},
			{defaultContent: '<button class="tp btn btn-xs btn-danger"><img src="'+base_url+'img/icons/tooth.png"></img></button>',width:"15px",class:"text-center"},
			{defaultContent: '<button class="pdf btn btn-xs btn-default"><i class="fa fa-file-pdf-o"></i></button>',width:"15px",class:"text-center"},
		],
		"rowCallback": function( row, data ){
			if(data.TPStatus == 1){ 
				$('td:eq(5)',row).html('<span class="label label-success">Active</span>');
			}else{
				$('td:eq(5)',row).html('<span class="label label-danger">Inactive</span>');
			}
			
			if(data.Complete == 1){ 
				$('td:eq(6)',row).html('<span class="label label-success">Yes</span>');
			}else{
				$('td:eq(6)',row).html('<span class="label label-danger">No</span>');
			}
		}
	});
	
	dt_paymentplan = $("#dt_paymentplan").DataTable({
		"bStateSave": false,
		"ajax": {
			url: urlapi+"Api_paciente/paymentplan",
			Type:"GET",
			dataType:"json",
			data:{patient: patientid},
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		},
		columns: [
			{data: 'fecha'},
			{data: 'usuario'},
			{data: 'comision'},
			{data: 'deudaFormato'},
			{defaultContent: ""},
			{defaultContent: '<button type="button" class="open btn btn-xs btn-info"><i class="fa fa-eye"></i></button',width:"15px"},
			{defaultContent: '<button class="pdf btn btn-xs btn-default"><i class="fa fa-file-pdf-o"></i></button>',width:"15px",class:"text-center"}
		],
		"rowCallback": function( row, data ){
			if(data.activo == 1){ 
				$('td:eq(4)',row).html('<span class="label label-success">Active</span>');
			}else{
				$('td:eq(4)',row).html('<span class="label label-danger">Inactive</span>');
			}
		}
	});

	dt_account = $("#dt_accoun").DataTable({
		"bStateSave": false,
		"ajax": {
			url: urlapi+"Api_account/account",
			Type:"GET",
			dataType:"json",
			data: function(d){ return  $.extend(d, dt_account_data); },
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		},
		columns: [
			{data: 'DateComplete',width:"70px"},
			{data: 'Descript'},
			{data: 'ProcFeeFormat',width:"20px",class:"text-right"},
		],
		"rowCallback": function( row, data ){
			$('td:eq(2)',row).html('<b>'+data.ProcFeeFormat+'</b>');
		},
		"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data; 
            // Remove the formatting to get integer data for summation
            var intVal = function(i){
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api.column(2).data().reduce(function(a,b){return intVal(a) + intVal(b);},0);
            $(api.column(2).footer()).html(total);
        }
	});
	
	dt_notes = $("#dt_notes").DataTable({
		"bStateSave": false,
		"ajax": {
			url: urlapi+"Api_paciente/notes",
			Type:"GET",
			dataType:"json",
			data: function(d){ return  $.extend(d, dt_notes_data); },
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		},
		order: [[ 0, "desc" ]],
		ordering: true,
		columns: [
			{data: 'fecha',width:"60px"},
			{data: 'usuario'},
			{data: 'nota'},
		]
	});


	/*DT Actions */
	$('#dt_paymentplan tbody').on('click','.open',function(){
		var data = dt_paymentplan.row($(this).parents('tr')).data();
		$.ajax({
			url: urlapi+"Api_account/paymentplan",
			Type:"GET",
			dataType:"json",
			data: { planid: data.id},
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
			success: function(response){
				pplan = response.data[0];
				paytotal = (pplan.comision * pplan.pago)/100;
				$('#form_editpaymentplan [name="date"]').val(pplan.fecha);
				$('#form_editpaymentplan [name="value"]').val(pplan.deuda);
				$('#form_editpaymentplan [name="quantity"]').val(pplan.duracion);
				$('#form_editpaymentplan [name="type"]').val(pplan.deuda);
				$('#form_editpaymentplan [name="pay"]').val(pplan.pago);
				$('#form_editpaymentplan [name="profit"]').val(pplan.comision);
				$('#form_editpaymentplan [name="paytotal"]').val(paytotal);
				
				pay = 	$('#form_editpaymentplan [name="pay"]').val();
				payTotal = 	$('#form_editpaymentplan [name="paytotal"]').val();
				payc = parseFloat(pay)+parseFloat(payTotal);
				$('#tbl_dates_edit tbody').empty();
				$.each(pplan.fechas,function(i,item){
					$('#tbl_dates_edit tbody').append('<tr class="'+(item.completo == 1? 'success' : '')+'">'
							+'<td>'+item.fecha+'</td>'
							+'<td class="text-right">'+payc+'</td>'
							+'<td class="text-right">'+item.pagado+'</td>'
						+'</tr>');
				});
			},complete: function(){
				$('#modal_payplanedit').modal();
			}
		});
	});
	
	$('#dt_documents tbody').on('click','.open',function(){
		var data = dt_documents.row($(this).parents('tr')).data();
		window.open(pathapi+'files/patients/'+patientid+'/'+data.nombreFisico,"","toolbar=0,titlebar=0,fullscreen=0");
	});
	
	$('#dt_treatplans tbody').on('click','tr',function(){
        if($(this).hasClass('selection')){
            $(this).removeClass('selection');
			$('#treatedit').prop('disabled',true);
        }else{
			$('#treatedit').prop('disabled',false);
            dt_treatplans.$('tr.selection').removeClass('selection');
            $(this).addClass('selection');
			
			var data = dt_treatplans.row($(this)).data();
			app.block.loading.start("#block-account");
			dt_account_data.treatplan = data.id;			
			update_tblproc();
        }
		
		var data = dt_treatplans.row($(this)).data();
		$('#accTpName').html(data.Heading);
    });
	
	$('#dt_treatplans tbody').on('dblclick','tr',function(e){
		dt_treatplans.$('tr.selection').removeClass('selection');
		$('#treatedit').prop('disabled',false);
        $(this).addClass('selection');
		$('#treatedit').trigger('click');
	});
	
	$('#dt_account tbody').on('click','.showfile',function(){
		var preview = $("#preview"),
		dialog  = preview.find(".modal-dialog"),
		content = preview.find(".modal-body"); 
		content.html("");
		dialog.removeClass("modal-lg modal-sm modal-fw");
		if($(this).data("preview-image"))
			content.append(app.features.preview.build.image($(this).data("preview-image")));
									
		if($(this).data("preview-video"))
			content.append(app.features.preview.build.video($(this).data("preview-video")));
		
		if($(this).data("preview-href")){                        
			content.html(app.features.preview.build.href($(this).data("preview-href")));
			app_plugins.loaded();
		}
		
		if($(this).data("preview-size"))
			dialog.addClass($(this).data("preview-size"));                                                           
		
		if($(this).data("preview-title") && $(this).data("preview-description"))
			content.prepend(app.features.preview.build.text($(this).data("preview-title"),$(this).data("preview-description")));                    
		
		preview.modal("show");
	});
	
	$('#openNotes').on('click',function(){
		$.ajax({
		url: urlapi+"Api_paciente/notes",
		type: "GET",
		data: {id: patientid},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
		}
	});
		$('#modal_notes').modal('show');
	});
	
	/* Form Validation*/
	$.validate({
		form : '#form_newtreatplan',
		modules : 'logic',
		validateOnBlur : true, 
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form){
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			$form.find('button[type="submit"]').prop('disabled',true);
			$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-spinner fa-pulse fa-fw');
			var data = $form.serializeArray();
			data.push({name: 'patient', value: patientid});
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.data.code == 0){
						$('#form_newtreatplan').get(0).reset();
						$('#modal_newtreatplan').modal('hide');
						apptools.sweetNoty('success','Complete','New Treat Plant create');
						dt_treatplans.ajax.reload();
					}else{
						
					}
				}
			});
			
		  return false;
		},
	});
	
	$.validate({
		form : '#form_edittreatplan',
		modules : 'logic',
		validateOnBlur : true, 
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form){
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			$form.find('button[type="submit"]').prop('disabled',true);
			$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-spinner fa-pulse fa-fw');
			var data = $form.serializeArray();
			data.push({name: 'patient', value: selectpat});
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.data.code == 0){
						$('#form_newtreatplan').get(0).reset();
						$('#modal_newtreatplan').modal('hide');
						apptools.sweetNoty('success','Complete','New Treat Plant create');
						dt_treatplans.ajax.reload();
					}else{
						
					}
				}
			});
			
		  return false;
		},
	});
	
	/* Actions buttons*/
	$('#dt_treatplans tbody').on('click','.tp',function(){
		var data  = dt_treatplans.row($(this).parents('tr')).data();
		window.open(base_url+'treatplan/'+data.id,'_blank');
	});
	
	$('#dt_treatplans tbody').on('click','.pdf',function(){
		var data  = dt_treatplans.row($(this).parents('tr')).data();
		$.ajax({
			url: urlapi+"Api_treatplan/tppdf?treatplan="+data.id,
			Type:"GET",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
			//xhrFields: { responseType: 'blob' },
			success: function(data){
				apptools.openPrint(data);
			}
		});
	});

	$('#dt_paymentplan tbody').on('click','.pdf',function(){
		var data  = dt_paymentplan.row($(this).parents('tr')).data();
		$.ajax({
			url: urlapi+"Api_account/paymentplanpdf",
			Type:"GET",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
			data: {planid: data.id},
			success: function(data){
				apptools.openPrint(data);
			}
		});
	});
	
	$('#treatedit').on('click',function(){
		if($('.selection').length == 0){ apptools.notyCustom('information','<i class="fa fa-info"></i> Not Patient','Selected a patient from the list'); }
		else{
			var data = dt_treatplans.row($('.selection')).data();
			$('#form_edittreatplan [name="id"]').val(data.id);
			$('#form_edittreatplan [name="name"]').val(data.Heading);
			$('#form_edittreatplan [name="notes"]').val(data.Note);
			$('#form_edittreatplan [name="status"]').prop('checked',parseInt(data.TPStatus));
			$('#form_edittreatplan [name="complete"]').prop('checked',parseInt(data.Complete));
			$('#modal_edittreatplan').modal('show');
		}
	});

	$('#filter_log').on('click','button',function(){
		type = $(this).data('type');
		if(type == "all"){
			$('#activitiestimeline .app-timeline-item').show();
		}else{
			$('#activitiestimeline .app-timeline-item').hide();
			$('#activitiestimeline [data-type="'+type+'"]').show();
		}
	});
});

function actividades(){
	$.ajax({
		url: urlapi+"Api_paciente/listaactividades",
		type: "GET",
		data: {id: patientid,ofertaid: "none",etapaid: "none"},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$('#activitiestimeline').empty();
			$.each(response.data,function(i,actividad){
				var editar = '';
				var vencimiento = '';
				var documentlink = '';
				var tipo = '';
				if(actividad.tipoid == 1){ editar = '<a onclick="editnote('+actividad.id+');" class="text-muted margin-right-10"><span class="fa fa-pencil"></span> Edit</a>'; }
				if(actividad.tipoid == 2){ 
					editar = '<a onclick="editact('+actividad.id+');" class="text-muted margin-right-10"><span class="fa fa-pencil"></span> Edit</a>'; 
					vencimiento = '<span class="text-danger"> Due Date: <b>'+actividad.fechaInicio+' '+actividad.horaInicio+'</b></span>'; 
				}
				if(actividad.tipoid == 3){
					tipo = '<i class="fa fa fa-paperclip"></i>';
					documentlink = '<a href="'+pathapi+''+actividad.archivo[0].nombreFisico+'" class="text-muted margin-right-10" download><span class="fa fa-cloud-download"></span> download file</a>'; 
				}
				if(actividad.actividadTipo == 0){ tipo = '<i class="fa fa-sticky-note"></i>'; }
				if(actividad.actividadTipo == 1){ tipo = '<i class="fa fa-phone"></i>'; }
				if(actividad.actividadTipo == 2){ tipo = '<i class="fa fa-users"></i>'; }
				if(actividad.actividadTipo == 3){ tipo = '<i class="fa fa-clock-o"></i>'; }
				if(actividad.actividadTipo == 4){ tipo = '<i class="fa fa-flag"></i>'; }
				if(actividad.actividadTipo == 5){ tipo = '<i class="fa fa-envelope"></i>'; }
				if(actividad.actividadTipo == 6){ tipo = '<i class="fa fa-cutlery"></i>'; }
				
				$('#activitiestimeline').append('<div class="app-timeline-item" data-type="'+actividad.tipoid+'">'
					+'<div class="dot dot-'+actividad.EstadoColor+'"></div>'
					+'<div id="act'+actividad.id+'" class="content">'
						+'<div class="title">'+tipo+' <strong><a href="#">'+actividad.usuarioNombre+' '+actividad.usuarioApellido+'</a></strong>'+vencimiento+'</div>'
						+'<div class="scrollCustom" style="max-height: 240px;">'
						+actividad.nota
						+'</div>'
						+'<p>'
							+ '<a onclick="expandir('+actividad.id+');" class="text-muted margin-right-10"><span class="fa fa-expand"></span> Expand</a>'
							+ documentlink
							/*+'<a href="#" class="text-muted margin-right-10"><span class="fa fa-comment"></span> Comments</a>'*/
							/*+'<a href="#" class="text-muted"><span class="fa fa-bullhorn"></span> Report</a>'*/
							+'<span class="pull-right text-muted"> <i class="fa fa-calendar"></i> '+actividad.fecha+'</span>'
						+'</p>'
					+'</div>'
				+'</div>');
			});
			$('#activitiestimeline').append('<div class="app-timeline-more"><a href="#">...</a></div>');
		},
		complete : function(){
			$(".scrollCustom").mCustomScrollbar({axis:"y", autoHideScrollbar: true, scrollInertia: 200, advanced: {autoScrollOnFocus: false}});
		}
	});
}

function update_tblproc(){
	$.ajax({
		url: urlapi+"Api_account/account",
		Type:"GET",
		dataType:"json",
		data: dt_account_data,
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			total = 0;
			$("#dt_account tbody").empty();
			$.each(response.data,function(i,item){
				if(item.TypeItem == "proc"){
					total = total + parseFloat(item.ProcFee);
					$("#dt_account tbody").append('<tr>'
					+'<td class="padding-5">'+item.dateFormat+'</td>'
					+'<td class="padding-5">'+item.Doctor+'</td>'
					+'<td class="padding-5">'+item.Descript+'</td>'
					+'<td class="padding-5 text-center">'+item.ToothNum+'</td>'
					+'<td class="padding-5 text-right">'+item.ProcFeeFormat+'</td>'
					+'</tr>');
				}else if(item.TypeItem == "pay"){
					total = total - parseFloat(item.monto);
					$("#dt_account tbody").append('<tr data-id="'+item.id+'" class="editpay text-success">'
						+'<td class="padding-5">'+item.dateFormat+'</td>'
						+'<td class="padding-5">'+item.user+'</td>'
						+'<td class="padding-5">'								
							+(item.notas != "" ? '<i class="fa fa-info-circle popover-hover" aria-hidden="true" data-placement="top" data-container="body" data-trigger="click" data-content="'+item.notas+'"></i> ': "")
							+ item.TypeName
							+(item.archivo != "" ? ' <a data-file="'+item.archivo+'" class="showfile preview" data-preview-image="'+pathapi+'files/patients/'+patientid+'/'+item.archivo+'" data-preview-size="modal-lg">File</a>' : "")
						+'</td>'
						+'<td class="padding-5 text-center"></td>'
						+'<td class="padding-5 text-right">'+item.montoFormato+'</td>'
					+'</tr>');
				}else if(item.TypeItem == "adjust"){
					if(item.ItemValue == "+"){
						total = total + parseFloat(item.monto);
					}else{
						total = total - parseFloat(item.monto);
					}
					$("#dt_account tbody").append('<tr data-id="'+item.id+'" class="editaddj '+(item.ItemValue == "+" ? "text-info" : "text-danger")+'">'
						+'<td class="padding-5">'+item.dateFormat+'</td>'
						+'<td class="padding-5">'+item.user+'</td>'
						+'<td class="padding-5">'
							+(item.notas != "" ? '<i class="fa fa-info-circle popover-hover" aria-hidden="true" data-placement="top" data-container="body" data-trigger="click" data-content="'+item.notas+'"></i> ': "")
							+ item.ItemName
						+'</td>'
						+'<td class="padding-5 text-center"></td>'
						+'<td class="padding-5 text-right">'+item.ItemValue+''+item.montoFormato+'</td>'
					+'</tr>');
				} else if(item.TypeItem == "Comis"){
					comision = (item.comision * item.deuda)/100;
					total = total + parseFloat(comision);
					$("#dt_account tbody").append('<tr data-id="'+item.id+'" class="editaddj text-warning">'
						+'<td class="padding-5">'+item.dateFormat+'</td>'
						+'<td class="padding-5">'+item.user+'</td>'
						+'<td class="padding-5">Comision por plan de pagos: '+item.comision+'%</td>'
						+'<td class="padding-5 text-center"></td>'
						+'<td class="padding-5 text-right">'+comision+'</td>'
					+'</tr>');
				}
			});
		},
		complete: function(){
			app.block.loading.finish("#block-account");
			$('[data-toggle="popover"]').popover()
			
			$("#dt_account .popover-hover").on("mouseenter",function(){
				$(this).popover('show');
			}).on("mouseleave",function(){
				$(this).popover('hide');
			});
		}
	});
}