var dtproviders;
$(document).ready(function(){
	$.ajax({
		url: urlapi+"Api_data/archivostipo",
		type: "GET",
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$('#form_newfile [name="type"]').append('<option></option>');
			$.each(response.data,function(i,item){
				$('#form_newfile [name="type"]').append($('<option>',{text: item.nombre,value: item.id}));
				$('#form_editfile [name="type"]').append($('<option>',{text: item.nombre,value: item.id}));
			});
			$('.bs-select').selectpicker('refresh');
		}
	});
	
	dtproviders = $("#dtproviders").DataTable({
		dom: '<"col-md-6"B><"col-md-6"f>rt<"col-md-6 hidden-mobile"i><"col-md-6"p>',
		buttons: [
			{
				text: '<span class="fa fa-plus"></span> Add',
				className: 'btn btn-success btn-sm btn-icon-fixed',
				attr:{
					"data-backdrop": "static",
					"data-toggle": "modal", 
					"data-target": "#modal_newprovider"
					}
			},
			{
				text: '<span class="fa fa-pencil"></span> Edit',
				className: 'btn btn-warning btn-sm btn-icon-fixed',
				attr: { id: 'proviedit' }
				
			},
			{
				text: '<span class="fa fa-trash"></span> Delete',
				className: 'btn btn-danger btn-sm btn-icon-fixed',
				attr: { id: 'providel' }
			}
		],
		"ajax": {
			url: urlapi+"Api_data/providers",
			type: "get",
			data: {IsHidden:"none", specialty:"none"},
			beforeSend: function (xhr){
				xhr.setRequestHeader('Authorization', auth);
			},
		},
		columns: [
			{data: 'Abbr'},
			{data: 'LName'},
			{data: 'FName'},
			{data: 'horario'},
			{data: null },
		],
		"rowCallback": function( row, data ){
			if(data.IsHidden == 0){ active = '<span class="label label-success">Active</span>'}
			else{ active = '<span class="label label-danger">Inactive</span>';}
			$('td:eq(4)',row).html(active);
		}
	});
	
	$('#dtproviders tbody').on( 'click', 'tr', function () {
        if($(this).hasClass('selection')){
            $(this).removeClass('selection');
			$('#providel').prop('disabled',true);
			$('#proviedit').prop('disabled',true);
        }else{
			$('#providel').prop('disabled',false);
			$('#proviedit').prop('disabled',false);
            dtproviders.$('tr.selection').removeClass('selection');
            $(this).addClass('selection');
        }
    });
	
	$('#dtproviders tbody').on('dblclick','tr',function(e){
		dtproviders.$('tr.selection').removeClass('selection');
		$('#providel').prop('disabled',false);
		$('#proviedit').prop('disabled',false);
        $(this).addClass('selection');
		$('#proviedit').trigger('click');
	});
	
	$.validate({
		form : '#form_newfile',
		onError : function($form) {
			noty({
				layout: 'top',
				theme: 'defaultTheme',
				timeout: 2000,
				type: 'error', // success, error, warning, information, notification
				text: '<strong>ERROR</strong>Complete All inputs',
			});
		},
		onSuccess : function($form){
			var data = new FormData($form[0]);
			data.append("userid",apptools.userlogin);
			
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				enctype: 'multipart/form-data',
				processData: false,
				contentType: false,
				cache: false,
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.code == 0){
						apptools.sweetNoty('success','Completed','Data save on the system.');
						$('#modal_newfile').modal('hide');
						dtfiles.ajax.reload();
					}else{
						noty({
							layout: 'top',
							theme: 'defaultTheme',
							type: 'error',
							text: '<strong>Error</strong>'+response.message,
						});
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	$.validate({
		form : '#form_editprov',
		onError : function($form) {
			noty({
				layout: 'top',
				theme: 'defaultTheme',
				timeout: 2000,
				type: 'error', // success, error, warning, information, notification
				text: '<strong>ERROR</strong>Complete All inputs',
			});
		},
		onSuccess : function($form){
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.data.code == 0){
						apptools.sweetNoty('success','Completed','Data save on the system.');
						$('#modal_newfile').modal('hide');
						dtproviders.ajax.reload();
					}else{
						noty({
							layout: 'top',
							theme: 'defaultTheme',
							type: 'error',
							text: '<strong>Error</strong>'+response.message,
						});
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	$('#form_editprov [name="color"]').on('changeColor', function(event){
		$('#squarecolor').css('color', event.color.toString());
      });
	  
	$('#proviedit').on('click',function(e,d){
		var data = dtproviders.row($('.selection')).data();
		horario = data.horario.split("-");
		$('#form_editprov [name="id"]').val(data.id);
		$('#form_editprov [name="color"]').colorpicker('setValue',data.Color);
		$('#form_editprov [name="color"]').colorpicker('update');
		$('#form_editprov [name="Abbr"]').val(data.Abbr);
		$('#form_editprov [name="fname"]').val(data.FName);
		$('#form_editprov [name="lname"]').val(data.LName);
		$('#form_editprov [name="hourstart"]').val(horario[0]);
		$('#form_editprov [name="hourend"]').val(horario[1]);
		//$('#form_editprov [name="status"][value="'+data.IsHidden+'"]').prop('checked',true);
		if(data.IsHidden == 0){ $('#form_editprov [name="status"][value="0"]').prop('checked',true);}
		else{  $('#form_editprov [name="status"][value="0"]').prop('checked',false); }
		$('#modal_editprovider').modal();
	});
	
});