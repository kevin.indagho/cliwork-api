var dtfiles;
$(document).ready(function(){
	$.ajax({
		url: urlapi+"Api_data/archivostipo",
		type: "GET",
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$('#form_newfile [name="type"]').append('<option></option>');
			$.each(response.data,function(i,item){
				$('#form_newfile [name="type"]').append($('<option>',{text: item.nombre,value: item.id}));
				$('#form_editfile [name="type"]').append($('<option>',{text: item.nombre,value: item.id}));
			});
			$('.bs-select').selectpicker('refresh');
		}
	});
	
	dtfiles = $("#dtfiles").DataTable({
		dom: '<"col-md-6"B><"col-md-6"f>rt<"col-md-6 hidden-mobile"i><"col-md-6"p>',
		buttons: [
			{
				text: '<span class="fa fa-plus"></span> Add',
				className: 'btn btn-success btn-sm btn-icon-fixed',
				attr:{
					"data-backdrop": "static",
					"data-toggle": "modal", 
					"data-target": "#modal_newfile"
					}
			},
			{
				text: '<span class="fa fa-pencil"></span> Edit',
				className: 'btn btn-warning btn-sm btn-icon-fixed',
				attr: { id: 'filedit' }
				
			},
			{
				text: '<span class="fa fa-trash"></span> Delete',
				className: 'btn btn-danger btn-sm btn-icon-fixed',
				attr: { id: 'failedel' }
			}
		],
		"ajax": {
			url: urlapi+"Api_data/archivos",
			type: "get",
			data: { activo:"none"},
			beforeSend: function (xhr){
				xhr.setRequestHeader('Authorization', auth);
			},
		},
		columns: [
			{data: 'tipoNombre',class:"hidden-mobile"},
			{data: 'nombre'},
			{data: 'descripcion', class:"hidden-mobile"},
			{data: null, class:"hidden-mobile"},
			{defaultContent: '<button type="button" class="file edit btn btn-info btn-glow btn-xs"><i class="fa fa-cloud-download"></i></button>',width:"15px"},
		],
		"rowCallback": function( row, data ){
			if(data.activo == 1){ active = '<span class="label label-success">Active</span>'}
			else{ active = '<span class="label label-danger">Inactive</span>';}
			$('td:eq(3)',row).html(active);
		}
	});
	
	$('#dtfiles tbody').on( 'click', 'tr', function () {
        if($(this).hasClass('selection')){
            $(this).removeClass('selection');
        }else{
            dtfiles.$('tr.selection').removeClass('selection');
            $(this).addClass('selection');
        }
    });
	$('#dtfiles tbody').on('dblclick','tr',function(e){
		dtfiles.$('tr.selection').removeClass('selection');
        $(this).addClass('selection');
		$('#filedit').trigger('click');
	});
	
	$.validate({
		form : '#form_newfile',
		onError : function($form) {
			noty({
				layout: 'top',
				theme: 'defaultTheme',
				timeout: 2000,
				type: 'error', // success, error, warning, information, notification
				text: '<strong>ERROR</strong>Complete All inputs',
			});
		},
		onSuccess : function($form){
			var data = new FormData($form[0]);
			data.append("userid",apptools.userlogin);
			
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				enctype: 'multipart/form-data',
				processData: false,
				contentType: false,
				cache: false,
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.code == 0){
						apptools.sweetNoty('success','Completed','Data save on the system.');
						$('#modal_newfile').modal('hide');
						dtfiles.ajax.reload();
					}else{
						noty({
							layout: 'top',
							theme: 'defaultTheme',
							type: 'error',
							text: '<strong>Error</strong>'+response.message,
						});
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	$.validate({
		form : '#form_editfile',
		onError : function($form) {
			noty({
				layout: 'top',
				theme: 'defaultTheme',
				timeout: 2000,
				type: 'error', // success, error, warning, information, notification
				text: '<strong>ERROR</strong>Complete All inputs',
			});
		},
		onSuccess : function($form){
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.code == 0){
						apptools.sweetNoty('success','Completed','Data save on the system.');
						$('#modal_newfile').modal('hide');
						dtfiles.ajax.reload();
					}else{
						noty({
							layout: 'top',
							theme: 'defaultTheme',
							type: 'error',
							text: '<strong>Error</strong>'+response.message,
						});
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	$('#dtfiles tbody').on('click','.file',function(e,d){
		var data = dtfiles.row($(this).parents('tr')).data();
		window.open(pathapi+'files/template/'+data.fisico,'_blank');
		console.log(data);
	});
	
	$('#filedit').on('click',function(e,d){
		var data = dtfiles.row($('.selection')).data();
		
		$('#form_editfile [name="id"]').val(data.id);
		$('#form_editfile [name="type"]').val(data.tipo);
		$('#form_editfile [name="description"]').val(data.descripcion);
		$('#form_editfile [name="description"]').val(data.descripcion);
		$('#form_editfile [name="status"]').prop('checked',parseInt(data.activo));
		$('#modal_editfile').modal();
	});
	
});