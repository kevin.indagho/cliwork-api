var dtprocedures;
$(document).ready(function(){
	dtprocedures = $("#dtprocedures").DataTable({
		dom: '<"col-md-6"B><"col-md-6"f>rt<"col-md-6 hidden-mobile"i><"col-md-6"p>',
		buttons: [
			{
				text: '<span class="fa fa-plus"></span> Add',
				className: 'btn btn-success btn-sm btn-icon-fixed',
				attr:{
					"data-backdrop": "static",
					"data-toggle": "modal", 
					"data-target": "#modal_addproce"
					}
			},
			{
				text: '<span class="fa fa-pencil"></span> Edit',
				className: 'btn btn-warning btn-sm btn-icon-fixed',
				attr: { "disabled": "",id: 'procedit' }
				
			},
			{
				text: '<span class="fa fa-trash"></span> Delete',
				className: 'btn btn-danger btn-sm btn-icon-fixed',
				attr: { "disabled": "",id: 'procdel' }
			}
		],
		"ajax": {
			url: urlapi+"Api_data/procedures",
			type: "get",
			beforeSend: function (xhr){
				//xhr.setRequestHeader('Authorization', auth);
				console.log(apptools.headersajax());
				xhr.setRequestHeader('headers', apptools.headersajax());
			},
		},
		columns: [
			{data: 'ProcCode'},
			{data: 'AbbrDesc'},
			{data: 'Duration'},
			{data: 'Descript'},
			{data: null},
			{data: null},
			{data: null},
			{data: null},
		],
		"rowCallback": function( row, data ){
			if(data.isSurf == 1){ $('td:eq(4)',row).html('<span class="label label-success label-bordered label-ghost">Yes</span>'); }
			else{ $('td:eq(4)',row).html('<span class="label label-danger label-bordered label-ghost">No</span>'); }
			if(data.isCrown == 1){ $('td:eq(5)',row).html('<span class="label label-success label-bordered label-ghost">Yes</span>'); }
			else{ $('td:eq(5)',row).html('<span class="label label-danger label-bordered label-ghost">No</span>'); }
			if(data.isImplant == 1){ $('td:eq(6)',row).html('<span class="label label-success label-bordered label-ghost">Yes</span>'); }
			else{ $('td:eq(6)',row).html('<span class="label label-danger label-bordered label-ghost">No</span>'); }
			if(data.Active == 1){ $('td:eq(7)',row).html('<span class="label label-success label-bordered label-ghost">Active</span>'); }
			else if(data.Active == 0){ $('td:eq(7)',row).html('<span class="label label-danger label-bordered label-ghost">Inactive</span>'); }
		}
	});
	
	$('#dtprocedures tbody').on( 'click', 'tr', function () {
        if($(this).hasClass('selection')){
            $(this).removeClass('selection');
			$('#procdel').prop('disabled',true);
			$('#procedit').prop('disabled',true);
        }else{
			$('#procdel').prop('disabled',false);
			$('#procedit').prop('disabled',false);
            dtprocedures.$('tr.selection').removeClass('selection');
            $(this).addClass('selection');
        }
    });
	
	$('#dtprocedures tbody').on('dblclick','tr',function(e){
		dtprocedures.$('tr.selection').removeClass('selection');
        $(this).addClass('selection');
		$('#procedit').trigger('click');
	});
	
	$.validate({
		form : '#form_addproc',
		onError : function($form) {
			noty({
				layout: 'top',
				theme: 'defaultTheme',
				timeout: 2000,
				type: 'error', // success, error, warning, information, notification
				text: '<strong>ERROR</strong>Complete All inputs',
			});
		},
		onSuccess : function($form) {
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.code == 0){
						apptools.sweetNoty('success','Complete','Add procedure');
						$('#modal_addproce').modal('hide');
						dtprocedures.ajax.reload();
					}else{
						noty({
							layout: 'top',
							theme: 'defaultTheme',
							type: 'error',
							text: '<strong>Error</strong>'+response.message,
						});
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});

	$.validate({
		form : '#form_editproc',
		onError : function($form) {
			noty({
				layout: 'top',
				theme: 'defaultTheme',
				timeout: 2000,
				type: 'error', // success, error, warning, information, notification
				text: '<strong>ERROR</strong>Complete All inputs',
			});
		},
		onSuccess : function($form) {
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.code == 0){
						apptools.sweetNoty('success','Complete','Update procedure information');
						$('#modal_editproce').modal('hide');
						dtprocedures.ajax.reload();
					}else{
						noty({
							layout: 'top',
							theme: 'defaultTheme',
							type: 'error',
							text: '<strong>Error</strong>'+response.message,
						});
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	$('#procdel').on('click',function(){
		console.log($('.selection'));
		if($('.selection').length == 0){ apptools.notyCustom('information','<i class="fa fa-info"></i> Not Patient','Selected a patient from the list'); }
		else{
			var data = dtpatient.row($('.selection')).data();
			swal({
				title: "Are you sure delete this patient ?",
				text: data.FName +" "+data.LName ,
				type: "warning",
				showConfirmButton: 1,
				showCancelButton: 1,
				html:true,
				dangerMode: true,},
				function(isConfirm){
					if(isConfirm){
						
					}else{
					}
			});
		}
	});
	
	$('#procedit').on('click',function(){
		if($('.selection').length == 0){ apptools.notyCustom('information','<i class="fa fa-info"></i> Not Patient','Selected a patient from the list'); }
		else{
			var data = dtprocedures.row($('.selection')).data();
			$('#form_editproc [name="id"]').val(data.id);
			$('#form_editproc [name="code"]').val(data.ProcCode);
			$('#form_editproc [name="abbr"]').val(data.AbbrDesc);
			$('#form_editproc [name="descript"]').val(data.Descript);
			$('#form_editproc [name="duration"]').val(data.Duration);
			$('#form_editproc [name="status"][value="1"]').prop('checked',parseInt(data.Active));
			$('#form_editproc [name="issurf"][value="1"]').prop('checked',parseInt(data.isSurf));
			$('#form_editproc [name="iscrown"][value="1"]').prop('checked',parseInt(data.isCrown));
			$('#form_editproc [name="isimplant"][value="1"]').prop('checked',parseInt(data.isImplant));
			$('#modal_editproce').modal('show');
		}
	});
	
});