var dtusers;
$(document).ready(function(){
	
	$.ajax({
		url: urlapi+"Api_usuariotipo/tipos",
		type: "GET",
		data: {activo: 1},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$('#form_newuser [name="tipo"]').append('<option></option>');
			$.each(response.data,function(i,tipo){
				$('#form_newuser [name="tipo"]').append($('<option>',{
					text: tipo.name,
					value: tipo.id
				}));
				$('#form_edituser [name="tipo"]').append($('<option>',{
					text: tipo.name,
					value: tipo.id
				}));
			});
			$('.bs-select').selectpicker('refresh');
		}
	});
	
	$.validate({
		form : '#form_newuser',
		onError : function($form) {
			noty({
				layout: 'top',
				theme: 'defaultTheme',
				timeout: 2000,
				type: 'error', // success, error, warning, information, notification
				text: '<strong>ERROR</strong>Complete All inputs',
			});
		},
		onSuccess : function($form) {
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.code == 0){
						noty({
							layout: 'top',
							theme: 'defaultTheme',
							timeout: 2000,
							type: 'success',
							text: '<strong>Complete</strong> new user added',
						});
						$('#form_newuser').get(0).reset();
						$('#modal_newuser').modal('hide');
						dtusers.ajax.reload();
					}else{
						noty({
							layout: 'top',
							theme: 'defaultTheme',
							type: 'error',
							text: '<strong>Error</strong>'+response.message,
						});
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	$.validate({
		form : '#form_edituser',
		onError : function($form) {
			noty({
				layout: 'top',
				theme: 'defaultTheme',
				timeout: 2000,
				type: 'error', // success, error, warning, information, notification
				text: '<strong>ERROR</strong>Complete All inputs',
			});
		},
		onSuccess : function($form) {
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.code == 0){
						noty({
							layout: 'top',
							theme: 'defaultTheme',
							timeout: 2000,
							type: 'success',
							text: '<strong>Complete</strong> user edit',
						});
						$('#modal_newuser').modal('hide');
						dtusers.ajax.reload();
					}else{
						noty({
							layout: 'top',
							theme: 'defaultTheme',
							type: 'error',
							text: '<strong>Error</strong>'+response.message,
						});
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	dtusers = $("#dtusers").DataTable({
		dom: '<"col-md-6"B><"col-md-6"f>rt<"col-md-6 hidden-mobile"i><"col-md-6"p>',
		buttons: [
			{
				text: '<span class="fa fa-plus"></span> Add',
				className: 'btn btn-success btn-sm btn-icon-fixed',
				attr:{
					"data-backdrop": "static",
					"data-toggle": "modal", 
					"data-target": "#modal_newuser"
					}
			},
			{
				text: '<span class="fa fa-pencil"></span> Edit',
				className: 'btn btn-warning btn-sm btn-icon-fixed',
				attr: { id: 'useedit' }
				
			},
			{
				text: '<span class="fa fa-trash"></span> Delete',
				className: 'btn btn-danger btn-sm btn-icon-fixed',
				attr: { id: 'usedel' }
			}
		],
		"ajax": {
			url: urlapi+"Api_usuario/lista",
			type: "get",
			beforeSend: function (xhr){
				xhr.setRequestHeader('Authorization', auth);
			},
			data: {activo: "none",tipo:"none"},
		},
		columns: [
			{data: 'nombre'},
			{data: 'correo', class:"hidden-mobile"},
			{data: null, class:"hidden-mobile"},
			{defaultContent: '<button type="button" class="editar btn btn-warning btn-xs"><i class="fa fa-pencil"></i></button>'},
		],
		"rowCallback": function( row, data ){
			if(data.activo == 1){ active = '<span class="label label-success">Active</span>'}
			else{ active = '<span class="label label-danger">Inactive</span>';}
			$('td:eq(2)',row).html(active);
		}
	});
	
	$('#dtusers tbody').on( 'click', 'tr', function () {
        if($(this).hasClass('selection')){
            $(this).removeClass('selection');
        }else{
            dtusers.$('tr.selection').removeClass('selection');
            $(this).addClass('selection');
        }
    });
	
	$('#dtusers tbody').on('dblclick','tr',function(e){
		dtusers.$('tr.selection').removeClass('selection');
        $(this).addClass('selection');
		$('#useedit').trigger('click');
	});
	
	$('#useedit').on('click',function(){
		var data = dtusers.row($('.selection')).data();
		$('#form_edituser [name="id"]').val(data.id);
		$('#form_edituser [name="nombre"]').val(data.nombre);
		$('#form_edituser [name="apellidos"]').val(data.apellidos);
		$('#form_edituser [name="correo"]').val(data.correo);
		$('#form_edituser [name="tipo"]').val(data.tipoid);
		$('#form_edituser [name="activo"]').prop('checked',parseInt(data.activo));
		$('#modal_edituser').modal('show');
	});
	
});
